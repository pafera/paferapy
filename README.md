# NOTICE

As a result of recent world events, after twenty years of helping small businesses out in rural China, I'm now looking to move my family elsewhere. If you live in a country that's not likely to get involved in nuclear war, has universal healthcare and good schools, and needs someone who speaks English and Mandarin (and a little bit of Spanish!) with twenty years of programming and ten years of teaching experience, please send me a job offer!

# INTRODUCTION

The philosophy behind the Pafera framework is simplicity, efficiency, and responsiveness. Unlike some other frameworks where you basically have to learn a whole new design language, Pafera uses what you already know, but automates the boring parts to let you concentrate on spending your time in actual processing.

To use this framework properly, you should already know the basics of HTML, CSS, Python, JavaScript, and SQL. You are free to use the framework provided functions, or to roll your own if you're a veteran programmer. Creating pages can be as simple as typing Markdown into a textarea, or involving complex SQL queries and linking C libraries from Python in order to accomplish what you need. After twenty years of programming, I find that flexibility and choice are important aspects of having code that is useful to everyone. Efficient programming is the art of compromising between time, space, and responsiveness, and these differ so much from environment to environment that being able to quick switch methods and algorithms is the only way to ensure a good experience for all involved. If you have seen how many small businesses run off of a steaming pile of randomly recorded data with an Excel chart duct taped to the front, then you know what I'm talking about.

# INSTALL

If you're running Arch Linux, Ubuntu, or Termux on Android, open up a terminal to the directory that you want to install the framework in, then type

```
wget pafera.com/install.sh
chmod +x install.sh
./install.sh
```

As always, please look inside anything that you download from the internet to make sure that some devious hacker hasn't put something like `rm -rf ~/* && echo "[Nelson] Ha! Ha!"` in the script.

If you're installing on termux, you'll get a popup asking you for permission to use the microphone for voice recognition. Make sure to tap "Allow", or your voice recognition won't work.

At the end of the install, you'll generate OpenSSL keys. Just type in a four character or above password twice, then accept all of the defaults to enable https for your installation.

If you want to use speech to text, you'll need to download a model for Kaldi and place it at libs/kaldi/model. Android Termux doesn't have a compatible version of Vosk yet, so we switch to using termux-speech-to-text from the Termux: API APK instead. You'll have to hold the phone next to the person speaking, but holding a phone is a little tiny bit easier than holding a laptop or desktop.

If you want to use text to speech, you'll need to download festival and your preferred voice pack.

If you're not running one of the systems listed above, then you'll have to edit the install script to your own system, which shouldn't be too hard since most Linux distribution use very similar names for packages nowadays. If you're running Windows, then download the Windows Subsystem for Linux and install Ubuntu. If you're running a Mac, then download HomeBrew. 

If you're looking for a GUI install, then this framework *might* not be the best option for you. Hey, what's this Microsoft Frontpage thingie? Oooo... downloading now...

# DESIGN

The basic design of Pafera is the same as ages past: models, views, and controllers. The main difference between the modern era and previous eras is that nowdays, your models live on your database server, your controllers live on your web server, and your views live inside your browser. 

## Models

We start by defining models of your data and deriving these classes from apps.system.ModelBase. For example, if we wanted to make an application called... lolcats... Actually, that's already taken, so let's do mpmcats instead (That's totally *not* a trademark violation!), we could make the following:

```
# File apps/mpmcats/cat.py

from apps.system import *

class mpmcats_cat(ModelBase):
  _dbfields  = {
    'rid':          ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'image':        ('IMAGEFILE',           'NOT NULL', ),
    'name':         ('TRANSLATION',         'NOT NULL', BlankValidator()),
    'description':  ('TRANSLATION',         'NOT NULL', ),
    'modified':     ('DATETIME',            'NOT NULL', ),
    'likes':        ('INT',                 'NOT NULL DEFAULT 0', ),
    'dislikes':     ('INT',                 'NOT NULL DEFAULT 0', ),
    'flags':        ('INT',                 'NOT NULL DEFAULT 0', ),
  }
  _dbindexes  = ()
  _dblinks    = ()
  _dbdisplay  = ('image', 'name', 'description',)
  _dbflags    = 0
```

Asute readers will notice that this looks a lot like a SQL CREATE TABLE statement, and that's because it is. You are free to put any SQL that you want in the field definition, and it will be passed along to the underlying database. Remember, Pafera is based upon *enhancement*, not *replacement*. We do the bare minimum needed to clarify what we want, and let the framework take care of the rest.

By convention, we name models with the application that they're from followed by an underscore and then the model name itself, so our application is "mpmcats," and "cat" is the actual name of the model. This saves a lot of confusion if your buddy down the hall has also created an application which uses cats and you need to distinguish between the two models. It also ties in to the automatic import and export system that you'll see later.

Some intelligent readers might guess that `rid` stands for row ID, and althought that is a rather good guess, it is also wrong. Pafera enhances the normal SQL experience with special types, and `rid` is actually a random 32-bit ID with `rbid` being the 64-bit version.. Auto increment IDs are good for some things, but in a web environment where you don't want some acne-filled script kiddie living in his parents' basement going around downloading all of your sequentially numbered files, it's good to have the system give you a random ID to make it harder to guess what is accessible.

`IMAGEFILE` is also not a standard SQL type. This actually signals to the framework that this field contains the ID of a system_file object of the type image.

`name` sounds like a perfectly valid field name, but you might be wondering what `TRANSLATION` is. Well, Pafera was designed from the start to be multilingual, so if your cat is called "Kitty" in English but "猫咪" in Chinese, then this allows you to store *both* names for the cat inside of the same object, and later on find it with a 

```SELECT * FROM mpmcats_cat WHERE name LIKE '%Kitty%'
```

BlankValidator() is exactly what it sounds like. If you try to save a cat without a name, then this will throw an exception to tell you to please name the poor kitty first. In practice, you should probably validate data yourself before it gets to the database layer so that you can have more human readable messages for Claire, the 65 year old lady from accounting who needs reading glasses to turn on her computer.

We'll ignore the bottom variables for now, but you can probably guess what they do. I prefer descriptive programming where you can figure things out at a glance... which is probably why I never really got into Perl all that much.

## Controllers and APIs

Okay, so now that we have a cat model, let's define an API to do something with the cats.

```
# File apps/mpmcats/__init__.py

import time

from apps.system import *
from apps.mpmcats.cat import *

def catapi(g):
  results = {}
  
  modeltouse    = mpmcats_cat
  editorgroup   = 'cateditors'
  searchfields  = '*'
  
  try:
    d = json.loads(g.request.get_data())
    
    command = StrV(d, 'command')
    
    if command == 'search':
      keyword        = StrV(d, 'keyword')
      start          = IntV(d, 'start')
      limit          = IntV(d, 'limit', 1, 1000, 20)
      
      conds   = []
      params  = []
      
      if keyword:
        conds.append('name LIKE ?')
        params.append('%' + keyword + '%')
        
        conds.append('description LIKE ?')
        params.append('%' + keyword + '%')
      
      conds = "WHERE " + (" OR ").join(conds) if conds else ''
      
      objs = g.db.Find(
          modeltouse, 
          conds, 
          params, 
          start   = start, 
          limit   = limit,
          fields  = searchfields,
        )
        
      results['data']   = [x.ToJSON(searchfields) for x in objs]
      results['count']  = len(objs)
    elif command == 'load':
      idcode  = StrV(d, 'idcode')
      
      if not idcode:
        raise Exception(g.T.missingparams)
      
      results['data']  = g.db.Load(modeltouse, FromShortCode(idcode)).ToJSON()
    elif command == 'save':
      if editorgroup:
        g.db.RequireGroup(editorgroup, g.T.cantchange)
      
      newinfo  = DictV(d, 'data')
      
      idcode   = StrV(newinfo, 'idcode')
      
      newinfo['modified'] = time.time()
      
      if idcode:
        newobj  = g.db.Load(modeltouse, FromShortCode(idcode))
      else:
        newobj  = modeltouse()
      
      newobj.Set(**newinfo)
      
      g.db.Save(newobj)
      g.db.Commit()
      
      results['data']   = newobj.ToJSON()
    elif command == 'delete':
      if editorgroup:
        g.db.RequireGroup(editorgroup, g.T.cantdelete)
      
      idcodes  = ArrayV(d, 'idcodes')
      
      if not ids:
        raise Exception(g.T.missingparams)
      
      for obj in g.db.LoadMany(modeltouse, [FromShortCode(x) for x in idcodes]):
        g.db.Delete(obj)
        
      g.db.Commit()
    else:
      results['error']  = f'{g.T.cantunderstand} "{command}"'
  except Exception as e:
    results['error']  = str(e)
  
  return results
  
ROUTES  = {
  'catapi':          catapi,
}

```

Okay, that's a lot of code there, but it's actually pretty much our standard boilerplate JSON API that you can copy and paste as much as you want. We can now search for billions of cats, load a particular cat's info, add or edit existing cats, and delete previously added cats. In practice, you'll want a *lot* more error handling and custom logic, but this is good enough to satisfy a basic lolcats... er... mpmcats-style website.

```
from apps.system import *
```

imports all of our useful system functions.

```
from apps.mpmcats.cat import *
```

imports the model that we just created.

`catapi` is self-explanatory, but the following lines may not be.

```
results = {}
```

For the JSON API, we always return a dict of results to the caller.

```
modeltouse    = mpmcats_cat
editorgroup   = 'cateditors'
searchfields  = '*'
```

Setup for the boilerplate code. `modeltouse` is the model that we're working with in this API. `editorgroup` is the group which a user must belong to in order to add, edit, and delete this model. `searchfields` is which fields to return from a search. In this simple case, we just use the asterisk `*` to return all fields.

```
d = json.loads(g.request.get_data())

command = StrV(d, 'command')
```

These lines get the command and JSON data from the request. Note that everything is wrapped in an exception handler to avoid HTTP 500 server errors.

```
keyword        = StrV(d, 'keyword')
start          = IntV(d, 'start')
limit          = IntV(d, 'limit', 1, 1000, 20)
```

Pafera searches support the `keyword` parameter, which is shorthand for "give me everything that has this text in it." 

```
conds   = []
params  = []

if keyword:
  conds.append('name LIKE ?')
  params.append('%' + keyword + '%')
  
  conds.append('description LIKE ?')
  params.append('%' + keyword + '%')

conds = "WHERE " + (" OR ").join(conds) if conds else ''
```

These lines setup the SQL query.

```
objs = g.db.Find(
    modeltouse, 
    conds, 
    params, 
    start   = start, 
    limit   = limit,
    fields  = searchfields,
  )
```

The actual search happens here.

```
results['data']   = [x.ToJSON(searchfields) for x in objs]
results['count']  = len(objs)
```

And these two lines return the data as dicts, which will then be turned into JSON objects to send to the client. 

The rest of the code is fairly plain, but there is one line which might catch your eye:

```
idcode  = StrV(d, 'idcode')
```

If you remember the definition of `rid` in our cat model as `INTEGER PRIMARY KEY`, you might be a bit confused as to why our ID has suddenly become a string. Well, a 32-bit integer is plus or minus about two billion, and most humans don't tend to do well with `-2147174278` as an ID. However, if we had a way of converting a numerical ID to a shorter string of alphanumeric characters, then `-2147174278` will become `001bxW`, which is much easier to both read and recognize. You'll notice this for YouTube URLs and many other places, and although it can be a bit of a hassle converting back and forth between numerical IDs and short codes, it really makes a difference when you need to mail a paycheck to Milton in the basement and you don't quite remember his two billion something ID.

```
if editorgroup:
  g.db.RequireGroup(editorgroup)
```

These two lines are only present for add, edit, and delete operations, and as you might suspect, they limit such functionality only to those users who are in the group `editorgroup`, which we defined as `cateditors` above. If the current user is not in this group and tries to do one of these operations, then this check throws an exception and nothing happens.

```
ROUTES  = {
  'catapi':          catapi,
}
```

Lastly, we let the framework know that the URL catapi should be handled by the catapi handler. This sounds rather redundant, but explicitly making your functions web accessible is one of the cornerstones of modern website security, and we don't want to be on the NSA's blacklist when our website suddenly starts sprouting Russian or Chinese propaganda.

## Views, or "How I Learned to Stop Worrying and Love My Browser"

### Creating Our Homepage

Now that we have a model and an API, it's time to edit our homepage so that we can load funny cat pictures. Pafera handles URLs through custom routes as you saw in the controller, but it also handles URLs simply by clicking on the Database Manager and creating a system_page object with `path` set to the URL that you want. As much as you yourself enjoy programming, Mark the designer might not be accustomed to indenting Python code or matching curly braces with C code, and thus he might do better with a point and click interface that he can type Markdown into.

Assuming that you have the framework already installed, go to the right directory and type

```
scripts/runstandaloneserver.sh 8888
```

and then type

```
localhost:8888
```

into your browser.  If you see the blue Pafera welcome page with the green login button in the middle, then you're good to go. Follow the instructions and change your admin password, then click on the `Database Management` button at the bottom.

The database management page is essentially PhpMyAdmin/Adminer for Pafera, meaning that you can see and change all of your database tables and rows here. For now, we'll focus on the task at hand.  Click on the model select, choose "system_page" from the list, then type "system/index" into the search field and wait for the page card to appear. Click on the card, then we'll make some changes to the front page of your site.

First of all, delete all of the previous `content` and type the following so that no one will sue us.

```
<h1 class="Center">MPM Cats</h1>
<h3 class="Center">(Any relationship to Lolcats is purely coincidental)</h1>
<div class="CatsList"></div>
```

Then change `title` to 

```
MPM Cats - Funny Cat Memes!
```

And add the following to jsfiles

```
/system/paferalist.js
/system/paferachooser.js
/system/paferafilechooser.js
```

Afterwards, go to your favorite editor, open up `public/system/index.js`, and paste the following in:

```
"use strict";

// File public/system/index.js

// ********************************************************************
P.AddHandler(
  'pageload', 
  function()
  {
    let iscateditor  = P.HasGroup('cateditors');
    
    let fields  = [
      ['image',       'imagefile',    0, 'Image'],
      ['name',        'translation',  0, 'Name'],
      ['description', 'translation',  0, 'Description'],
      ['likes',       'int',          0, 'Likes'],
      ['dislikes',    'int',          0, 'Dislikes']
    ];
  
    G.catslist = new PaferaList({
      div:            '.CatsList',
      apiurl:          '/mpmcats/catapi',
      displayfields:  fields,
      reference:      'G.catslist',
      enablesearch:   1,
      enableadd:      iscateditor,
      enableedit:     iscateditor,
      enabledlete:    iscateditor,
      extraactions:   [
        [`👍 ${r.likes}`,     'Like',     3],
        [`👎 ${r.dislikes}`,  'Dislike',  1]
      ]
    });
    
    G.catslist.Display();
    G.catslist.List();
  }
);
```

If you go to the homepage of your site, you should notice that it has now changed into the following.

Now, things look a little empty here since we haven't added any cat pictures yet, but that's easy to solve. We'll just add a couple of pictures from my cat meme collection. Click on the add button, and you'll see the following.

First, click on the upload button, choose a file, and wait for the upload to finish. Afterwards, type in a name and description, then click "Finished."

You now have your first MPMCats post, and if you noticed, we didn't touch a single line of python code. Once we have defined the API, the rendering and interface is entirely the work of the JavaScript layer. Perhaps one day, python will be standard in every browser, but for now, JavaScript is still the way to go. 

### Explaining the Code

Most pages in Pafera exist as database entries, so you can essentially edit your website straight from your browser. What we put in the content is basic HTML code, but you'll notice that this line

```
<div class="CatsList"></div>
```

provides our list control with the location where it renders itself.

```
G.catslist = new PaferaList({
  div:            '.CatsList',
```

jsfiles, as you can probably guess, is a list of JavaScript files which the system will automatically load upon rendering the page. The line 

```
/system/paferalist.js
/system/paferachooser.js
/system/paferafilechooser.js
```

loads the system list control which handles retriving, editing, and displaying database content. The next two lines load the file chooser so that we can search already uploaded files.

You'll notice that we didn't have to add `/system/index.js` to the list. If the system finds a JavaScript file with the same base URL as the page, then it's loaded by default.

The lines

```
P.AddHandler(
  'pageload', 
```

obviously runs the following function when all page assets have finished downloading.

```
let iscateditor  = P.HasGroup('cateditors');
```

is a boolean variable used to check if the current user is in the `cateditors` group and enables changes. Otherwise, a normal user can only view, but not add, edit, or delete our funny cat pictures. 

Oh, but I'm not in the `cateditors` group and I can still add pictures, you say? Well, if you're logged in as an administrator, then you automatically skip all security checks. Nifty!

```
let fields  = [
  ['image',       'imagefile',    0, 'Image'],
  ['name',        'translation',  0, 'Name'],
  ['description', 'translation',  0, 'Description'],
  ['likes',       'int',          0, 'Likes'],
  ['dislikes',    'int',          0, 'Dislikes']
];
```

`fields`, as the name indicates, is a list of database fields that we want the list control to retrieve and render. `displayfields` and `editfields` obviously serve different purposes, but we'll use the same settings for both as of now.

```
G.catslist = new PaferaList({
  div:            '.CatsList',
  apiurl:         '/mpmcats/catapi',
  editfields:     fields,
  reference:      'G.catslist',
  enablesearch:   1,
  enableadd:      iscateditor,
  enableedit:     iscateditor,
  enabledlete:    iscateditor
});
```

These lines create the list control, points it to the API that we created, and enables changes if the current user is in the `cateditors` group. As long as your API follows the normal Pafera conventions of accepting JSON data with the `search`, `load`, `save`, and `delete` commands, then everything else is automatic.

Notice that the control keeps a reference of itself to be accessible from the global scope. This is for rapid prototyping where you don't want to go through the hassle of creating a new event handler for every little change.

Our final line

```
G.catslist.Display();
```

simply tells the list control to display itself inside the `.CatList` div that we placed inside the page, and as the legendary Porky Pig said, "That's all, folks!"

### Making Things Look Nice

Now, as you've probably noticed, the default rendering for the list control is not very... pleasing to the eye. That's because PaferaList 1.0™ is not psychic yet (we're working on telepathic abilities for version 3.11 for workgroups), and doesn't quite know what you want it to look like, so its job is simply to throw up the data onto the screen and let you see that it's working. 

So how do we edit the control to make our cards look nice? Well, first let's start by deriving a class from PaferaList.

```
// File public/system/index.js

class CatList extends PaferaList
{
  constructor(config)
  {
    super(config);
  }
}
```

Afterwards, we'll override the `OnRenderItem()` function to do our own rendering. The base class will render the toolbar and the card's outside, while we'll make the inside of the card look nicer.

```
// Inside class CatList

OnRenderItem(r)
{
  return `
      <div class="FlexAlignSpacer"></div>
      <div class="Center Pad50">
        <div>${P.ImgFile(r.image, 'Width1600')}</div>
        <div class="Size150 blue Pad25">${P.BestTranslation(r.name)}</div>
        <div class="purple Pad25">${P.BestTranslation(r.description)}</div>
        <div class="ButtonBar">
          <div class="Color3 Pad50 Raised Rounded" data-action="Like">👍 ${r.likes}</div>
          <div class="Width600"></div>
          <div class="Color1 Pad50 Raised Rounded" data-action="Dislike">👎 ${r.dislikes}</div>
        </div>
      </div>
      <div class="FlexAlignSpacer"></div>
  `;
}
```

If you reload, you'll now see that our cat pictures look *exactly* the same as before. Why didn't anything change?

Well, we have *defined* a new class, but we haven't started *using* the new class yet, so our page is still displaying the plain old PaferaList class. We need to change the line 

```
G.catslist = new PaferaList({
```

to

```
G.catslist = new CatList({
```

in order for our derived class to take effect, so let's go ahead and do that.

If you reload one more time, then you should see that our cat pictures look much nicer than before. Still not quite professional quality, but it's probably good enough for a cheap $5 WordPress plugin that some Fortune 500 company will buy in a heartbeat.

# Further Reading

By now, you should have a pretty good idea of what the Pafera Framework is and how to use it. For more detailed documentation, run scripts/generatedocs.sh and look in the public/docs folder, or just dig through the source code and see how things are done. As with all programming, getting your own hands dirty with a personal project is the best way to learn. 

For fun, you can try to implement the Like and Dislike buttons, which will require you to add handlers to the API in mpmcats/__init__.py and to the CatList JavaScript class. As a hint, you may want to look at the extraactions parameter in PaferaList.

# Guidelines and Tips

Keep your code readable, meaning other than small things like loop variables and such, give your variables and functions descriptive names. I should be able to tell what your code does at a glance, and if something looks strange, then put a comment beside it explaining why this logic was needed. This helps both others who need to work on your code at a later date, and you yourself when you come back in three years to an old client and wonder "Why did I do this here?"

Portability means that your code will be useful on different systems, meaning that you must limit yourself to the *lowest possible denominator* for those systems. As much fun as all of those cool PostgreSQL functions are to use, if you're in a situation where only SQLite is available, then you'll have to redo all of your SQL logic in another layer. I tend to use only the simplest database functions and implement all other logic in Python, since Python is available for pretty much every system more capable than embedded systems.

Most users in the current era, especially those living in rural areas or those less technically inclined, spend more time on their phones than they do their computers. Be aware to keep your interface designs simple and distinctive so that those users can figure out what to do at a glance on smaller screens. Pay particular attention to use of colors, spacing, and images. I really wish that I could slap whoever started using monochrome icons with no visual indicators that this is a button, put them in front of a four year old picking up a phone for the first time, and make them watch how long it takes for the child to figure out how to use their application.

You will *always* run into unexpected problems as a programmer, so make sure that your program is fault tolerant, checks all input, and has multiple layers of security. Someone intelligent enough can always find a way to create a bug given enough time, but it's the willfully ignorant that I find the most troublesome. The best way to test if your application will break is to give it to an elementary school child and see what they do with it. If they can mess your application up with no hopes of recovery, then you should probably change your approach. 8-)

Many programmers nowadays like to go to Stack Overflow, PasteBin, or other such places to find answers to their questions. This in itself is not a bad thing, but what *is* bad is blindly copying someone else's code without understanding what it does or the advantages and disadvantages of their approach. We rely on frameworks to do a lot of the boilerplate for us nowadays, which has dramatically improved rapid prototyping, but this does not take away the need to understand what you are doing. Even back during the old MFC/ATL days, sometimes it was necessary to override the framework or completely replace it in order to get the functionality that you needed. Balance development time with security, readability, and execution time. Adding a 300KiB library to your project for a string function is almost always a bad idea.

# Contact

If you're looking for commerical support billed at $40 per hour, please add me on Skype as pafera at protonmail, as that is one of the few foreign avenues of communication not blocked by the Great Firewall of China unlike Discord and Zoom. Thanks for reading, and good luck with all of your projects!
