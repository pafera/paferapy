#!/usr/bin/env python3

from vosk import Model, KaldiRecognizer, SetLogLevel
import sys
import os
import subprocess
import json

SetLogLevel(-1)

sample_rate=16000
model = Model("/srv/flasksite/libs/kaldi/model")
rec = KaldiRecognizer(model, sample_rate)

process = subprocess.Popen(['ffmpeg', 
        '-loglevel', 'quiet', 
        '-i', sys.argv[1],
        '-ar', str(sample_rate), 
        '-ac', '1', 
        '-f', 's16le', 
        '-af', 'loudnorm',
        '-'
    ],
    stdout = subprocess.PIPE
)

while True:
    data = process.stdout.read(4000)
    
    if len(data) == 0:
        break
        
    rec.AcceptWaveform(data)
    #if rec.AcceptWaveform(data):
        #print(rec.Result())
    #else:
        #print(rec.PartialResult())

print(json.loads(rec.FinalResult())['text'])
