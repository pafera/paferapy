#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# Standard Pafera app functions called when the app is added or 
# removed to the system.

from apps.system import *

# *********************************************************************
def OnAppInstall(g):
  db  = g.db
  
  needcommit  = 0
  
  adminuser = g.db.Find(
    system_user, 
    'WHERE phonenumber = ? AND place = ?',
    [
      'admin',
      'pafera'
    ]
  )[0]
  
  for r in [
      ['chaibao.customer',  'Chaibao Customer'],
      ['chaibao.store',     'Chaibao Store'],
    ]:
    
    group   = db.Find(system_group, 'WHERE groupname = ?', r[0])
    
    if not group:
      newgroup  = system_group()
      newgroup.Set(
        displayname = {'en': r[1]},
        groupname   = r[0],
      )
      db.Save(newgroup)
      db.Link(adminuser, newgroup)
      needcommit  = 1    
  
  for pagefragment in [
{
  'path':   'Chaibao Header',
  'content':  {
    'zh': f"""    
<div class="SiteHeader Flex FlexCenter FlexWrap white">
  <a href="/">
    <img src="/chaibao/logo.webp" style="height: 2em; float: left;">
    <span style="display: block; padding: 0.3em; float: left;">柴宝</span>
  </a>
  <a class="" href="/chaibao/search.html">🔎 找商品</a>
  <a class="" href="/chaibao/cart.html">🛒 购物车</a>
  <a class="" href="/chaibao/mystore.html">🏪 我的商店</a>
  <a class="NoFlex Width400 Right ChooseLanguageIcon" onclick="P.ChooseLanguagePopup(event)"></a>
  <a class="NoFlex Width400 Right SystemUserIcon" onclick="P.UserPopup(event)"></a>
</div>
""",
  },
  "translations": [
  ],
},
{
  'path':   'Chaibao Header',
  'content':  {
    'zh': f"""    
<div class="SiteHeader Flex FlexCenter FlexWrap white">
  <a href="/">
    <img src="/chaibao/logo.webp" style="height: 2em; float: left;">
    <span style="display: block; padding: 0.3em; float: left;">柴宝</span>
  </a>
  <a class="" href="/chaibao/search.html">🔎 找商品</a>
  <a class="" href="/chaibao/cart.html">🛒 购物车</a>
  <a class="" href="/chaibao/mystore.html">🏪 我的商店</a>
  <a class="NoFlex Width400 Right ChooseLanguageIcon" onclick="P.ChooseLanguagePopup(event)"></a>
  <a class="NoFlex Width400 Right SystemUserIcon" onclick="P.UserPopup(event)"></a>
</div>
""",
  },
}
  "translations": [
  ],
},  
    ]:
    if not db.FindOne(system_pagefragment, 'WHERE path = ?', pagefragment['path']):
      newpagefragment  = system_pagefragment()
      
      newpagefragment.Set(**pagefragment)
      db.Save(newpagefragment)
      needcommit  = 1
  
  headerid  = g.db.FindOne(
    system_pagefragment, 
    'WHERE path = ?', 
    'Chaibao Header',
    fields = 'rid',
  ).rid
  
  footerid  = headerid

  for page in [
{
  "content": {
    "en": "<div class=\"PageLoadContent\"></div>"
  },
  "jsfiles": [
    "/system/paferalist.js",
  ],
  "path": "/chaibao/index.html",
  "pyfile": "",
  "requiredgroups": [
  ],
  "title": {
    "en": "🐶 Chaibao",
    "zh": "🐶 柴宝",
  },
  "translations": [
  ],
},
{
  "content": {
    "en": "<div class=\"PageLoadContent\"></div>"
  },
  "jsfiles": [
    "/system/paferalist.js",
  ],
  "path": "/chaibao/search.html",
  "pyfile": "",
  "requiredgroups": [
  ],
  "title": {
    "en": "Chaibao Search",
    "zh": "🔎 找商品",
  },
  "translations": [
  ],
},
{
  "content": {
    "en": "<div class=\"PageLoadContent\"></div>"
  },
  "jsfiles": [
    "/system/paferalist.js",
  ],
  "path": "/chaibao/cart.html",
  "pyfile": "",
  "requiredgroups": [
  ],
  "title": {
    "en": "🛒 Shopping Cart",
    "zh": "🛒 购物车",
  },
  "translations": [
  ],
},
{
  "content": {
    "en": "<div class=\"PageLoadContent\"></div>"
  },
  "jsfiles": [
    "/system/paferalist.js",
  ],
  "path": "/chaibao/mystore.html",
  "pyfile": "",
  "requiredgroups": [
  ],
  "title": {
    "en": "🏪 My Store",
    "zh": "🏪 我的商店",
  },
  "translations": [
  ],
},
    ]:
    if not db.FindOne(system_page, 'WHERE path = ?', page['path']):
      newpage  = system_page()
      
      newpage.Set(
        headerid  = headerid,
        footerid  = footerid,
      )
        
      newpage.Set(**page)
      db.Save(newpage)
      needcommit  = 1
    
  if needcommit:
    db.Commit()
  
# *********************************************************************
def OnAppDelete(g):
  db  = g.db
  
  needcommit  = 0
  
  for r in [
      'mpmcateditors',
    ]:
    
    group   = db.Find(system_group, 'WHERE groupname = ?', r)
    
    if group:
      db.Delete(group[0])
      needcommit  = 1
  
  for r in db.Find(mpmcats_cat):
    g.db.Delete(r)
    needcommit  = 1
  
  if needcommit:
    db.Commit()
 
