#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system import *

# *********************************************************************
class chaibao_product(ModelBase):
  _dbfields  = {
    'rid':          ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'image':        ('IMAGEFILE',           'NOT NULL', ),
    'name':         ('TRANSLATION',         'NOT NULL', BlankValidator()),
    'description':  ('TRANSLATION',         'NOT NULL', ),
    'modified':     ('TIMESTAMP',           'NOT NULL', ),
    'likes':        ('INT',                 'NOT NULL DEFAULT 0', ),
    'dislikes':     ('INT',                 'NOT NULL DEFAULT 0', ),
    'flags':        ('INT',                 'NOT NULL DEFAULT 0', ),
  }
  _dbindexes  = ()
  _dblinks    = ()
  _dbdisplay  = ('image', 'name', 'description',)
  _dbflags    = 0
