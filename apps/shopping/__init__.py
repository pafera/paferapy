import time
import traceback

from pprint import pprint

from apps.system import *
from apps.mpmcats.cat import *

# =====================================================================
def catapi(g):
  results = {}
  
  modeltouse    = mpmcats_cat
  editorgroup   = 'mpmcateditors'
  searchfields  = '*'
  
  try:
    d = json.loads(g.request.get_data())
    
    command = StrV(d, 'command')
    
    if command == 'search':
      keyword       = StrV(d, 'keyword')
      name          = StrV(d, 'name', keyword)
      description   = StrV(d, 'description', keyword)
      fromtime      = IntV(d, 'fromtime')
      totime        = IntV(d, 'totime')
      start         = IntV(d, 'start')
      limit         = IntV(d, 'limit', 1, 1000, 20)
      orderby       = StrV(d, 'orderby', 'modified DESC')
      
      conds   = []
      params  = []
      
      if name:
        conds.append('name LIKE ?')
        params.append('%' + keyword + '%')
        
      if description:
        conds.append('description LIKE ?')
        params.append('%' + keyword + '%')
        
      if fromtime:
        conds.append('modified > ?')
        params.append(fromtime)
      
      if totime:
        conds.append('modified < ?')
        params.append(totime)
        
      conds = "WHERE " + (" OR ").join(conds) if conds else ''
      
      objs = g.db.Find(
          modeltouse, 
          conds, 
          params, 
          start   = start, 
          limit   = limit,
          fields  = searchfields,
          orderby = orderby,
        )
        
      results['data']   = [x.ToJSON(searchfields) for x in objs]
      results['count']  = len(objs)
    elif command == 'load':
      idcode  = StrV(d, 'idcode')
      
      if not idcode:
        raise Exception(g.T.missingparams)
      
      results['data']  = g.db.Load(modeltouse, FromCode(idcode)).ToJSON()
    elif command == 'save':
      if editorgroup:
        g.db.RequireGroup(editorgroup, g.T.cantchange)
      
      newinfo  = DictV(d, 'data')
      
      idcode   = StrV(newinfo, 'idcode')
      
      newinfo['modified'] = time.time()
      
      if idcode:
        newobj  = g.db.Load(modeltouse, FromCode(idcode))
      else:
        newobj  = modeltouse()
      
      newobj.Set(**newinfo)
      
      g.db.Save(newobj)
      g.db.Commit()
      
      results['data']   = newobj.ToJSON()
    elif command == 'delete':
      if editorgroup:
        g.db.RequireGroup(editorgroup, g.T.cantdelete)
      
      idcodes  = ArrayV(d, 'idcodes')
      
      if not ids:
        raise Exception(g.T.missingparams)
      
      for obj in g.db.LoadMany(modeltouse, [FromCode(x) for x in idcodes]):
        g.db.Delete(obj)
        
      g.db.Commit()
    elif command == 'like':
      idcode  = StrV(d, 'idcode')
      like    = IntV(d, 'like')
      
      # We use the current user's session to score liked or disliked IDs.
      # Of course, this means that the user only has to clear his cookies to 
      # like a picture again, but it's a good enough method for your average
      # crazy cat lady down the street.
      likedids = g.session.get('mpmcats.likedids', [])
      
      if idcode in likedids:
        raise Exception('You have already liked or disliked this picture.')
      
      newobj  = g.db.Load(modeltouse, FromCode(idcode))
      
      if like > 0:
        newobj.Set(likes = newobj.likes + 1)
      elif like < 0:
        newobj.Set(dislikes = newobj.dislikes + 1)
        
      g.db.Save(newobj)
      g.db.Commit()
      
      likedids.append(idcode)
      
      g.session['mpmcats.likedids'] = likedids
    else:
      results['error']  = f'{g.T.cantunderstand} "{command}"'
  except Exception as e:
    traceback.print_exc()
    results['error']  = str(e)
  
  return results

# *********************************************************************  
ROUTES  = {
  'catapi':          catapi,
}

