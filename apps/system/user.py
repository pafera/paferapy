#!/usr/bin/python
# -*- coding: utf-8 -*- 

import os
import shutil

from apps.system.validators import *
from apps.system.db import *
from apps.system.file import *

# Flag constants for the user object
USER_MUST_CHANGE_PASSWORD = 0x01
USER_DISABLED             = 0x02
USER_NEED_APPROVAL        = 0x04
USER_REJECTED             = 0x08
USER_CAN_MANAGE_SELF      = 0x10
USER_IS_ADMIN             = 0x20
USER_CAN_UPLOAD           = 0x40
USER_CAN_UPLOAD_ORIGINALS = 0x80
USER_CAN_POST_MESSAGES    = 0x100

# *********************************************************************
class system_user(ModelBase):
  """User accounts for the system. Handles logins, uploads, storage 
  quotas, access tokens, and the such.
  
  Pafera adds a place field to the traditional username/password
  combination, both for added security and to allow different places
  to use the same phone number for their own accounts. It's the 
  unique combination of (phonenumber, place, password) that
  identifies a particular user.
  
  Note that displayname is a translation field, meaning that that 
  a user can have their own names in different languages. 
  
  expiredate is when the user account will expire. Set it far into
  the future for your own administrator account.
  
  canmanage and managedby are used for managed accounts such as 
  students being managed by a teacher. Managed accounts cannot
  change anything besides change their own password. 
  
  accesstokens are used by the system to allow APIs to assume the 
  identity of a user. The APIs used must be explicitly specified for 
  the token to work. The token format is a dict such as
  {
    "token":      tokenvalue,
    "expiredate": timestamp,
    "apilist":    [
                    '/system/fileapi:search',
                    '/system/fileapi:load',
                    '/system/fileapi:save'
                  ]
  }
  
  blacklist, acquaintances, friends, and favorites are lists of other
  users to which the current user can send messages through the
  system provided messageapi. 
  """
  
  _dbfields     = {
    'rid':            ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'phonenumber':    ('TEXT', 'NOT NULL', BlankValidator()),
    'place':          ('TEXT', 'NOT NULL', BlankValidator()),
    'password':       ('PASSWORD', 'NOT NULL', BlankValidator()),
    'email':          ('TEXT', "NOT NULL DEFAULT ''",),
    'displayname':    ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'settings':       ('DICT', "NOT NULL DEFAULT ''",),
    'expiredate':     ('TIMESTAMP', "NOT NULL DEFAULT 0",),
    'storageused':    ('INT64', "NOT NULL DEFAULT 0",),
    'storagequota':   ('INT64', "NOT NULL DEFAULT 134217728",),
    'numcanmanage':   ('INT', "NOT NULL DEFAULT 0",),
    'canmanage':      ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'managedby':      ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'blacklist':      ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'acquaintances':  ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'friends':        ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'favorites':      ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'managedby':      ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'accesstokens':   ('DICT', "NOT NULL DEFAULT ''",),
    'passkeys':       ('DICT', "NOT NULL DEFAULT ''",),
    'flags':          ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = (
    ('unique', ('phonenumber', 'place')),
  )
  _dblinks    = ['system_group', 'system_user']
  _dbdisplay  = ['place', 'displayname', 'phonenumber']
  _dbflags    = 0
  
  USERGROUPS  = [
    'blacklist',
    'acquaintances',
    'friends',
    'favorites',
  ]
  
  _managejs = """
G.displayfuncs['system_user']  = function(obj, dblist)
{
  let model   = 'system_user';  
  let fields  = [G.modelids[model]];
  
  fields      = fields.concat(P.ToggledButtons('.DBFields')['on']);
  
  let modelid     = G.modelids[model];
  let modelinfo   = G.modelfields[model];
  let idcode      = ToShortCode(obj['rid']);  
  let ls          = [];
  
  if (dblist.usetable)
  {    
    ls.push(`<th class="Center">
      ${P.HeadShotImg(idcode, 'Square600')}<br>
      ${obj['rid']} (${idcode})
    </th>`);
  } else
  {
    ls.push(`<div class="blueg Center">${obj['rid']} (${idcode})</div>
      <div class="Center">
        ${P.HeadShotImg(idcode, 'Square600')}<br>
      </div>`
    );
  }
    
  for (let j = 1, m = fields.length; j < m; j++)
  {
    let k         = fields[j];    
    let value     = obj[k];
    let valuetype = modelinfo[k][0];
    
    if (dblist.usetable)
    {    
      if (valuetype == 'JSON')
      {
        ls.push('<td>' + JSON.stringify(value, null, 2) + '</td>');
      } else if (valuetype.indexOf('TRANSLATION') != -1)
      {
        ls.push('<td>' + P.BestTranslation(value) + '</td>');
      } else
      {    
        ls.push('<td>' + value + '</td>');
      }
    } else
    {
      if (valuetype == 'JSON')
      {
        ls.push('<pre>' + JSON.stringify(value, null, 2) + '</pre>');
      } else if (valuetype.indexOf('TRANSLATION') != -1)
      {
        ls.push('<div>' + P.BestTranslation(value) + '</div>');
      } else
      {    
        ls.push('<div>' + value + '</div>');
      }
    }
  }
  
  return ls.join('\\n');
}
"""
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def HeadShotPath(self):
    '''Returns the filesystem path for the user's headshot, or a 
    blank string if the object has not been initialized.
    '''
    if not self.rid:
      return ''
    
    return 'public/system/headshots/' + RIDDir(self.rid, '.webp')
  
  # -------------------------------------------------------------------
  def HeadShotURL(self):
    '''Returns the server URL for the user's headshot.
    '''
    if not self.rid:
      return ''
    
    return self.HeadShotPath()[7:]
  
  # -------------------------------------------------------------------
  def OnDBCreate(self, db):
    '''Standard handler for initalizing a new object.
    '''
    if not self.rid:
      return
    
    headshotpath  = self.HeadShotPath()
    
    os.makedirs(os.path.dirname(headshotpath), exist_ok = 1)
    
    shutil.copy('public/system/icons/newuser.webp', headshotpath)
    
  # -------------------------------------------------------------------
  def OnDBDelete(self, db):
    '''Standard handler for deleting an existing object.
    '''
    if not self.rid:
      return
    
    try:
      os.remove(self.HeadShotPath())
    except Exception as e:
      pass

    for r in db.Find(system_file, 'WHERE dbowner = ?', self.rid):
      db.Delete(r)
    
    if self.canmanage or self.managedby:
      
      allids  = self.canmanage + self.managedby
      
      for r in db.Find(
          system_user,
          f'''WHERE rid IN ({', '.join(['?' for x in allids])})''',
          [FromCode(x) for x in allids],
          fields  = 'rid, canmanage, managedby'
        ):
        
        useridcode  = ToShortCode(r.rid)
        
        if useridcode in self.canmanage:
          try:
            r.managedby.remove(useridcode)
            r.UpdateFields('managedby')
          except Exception as e:
            pass
          
        if useridcode in self.managedby:
          try:
            r.canmanage.remove(useridcode)
            r.UpdateFields('canmanage')
          except Exception as e:
            pass
          
        db.Update(r)

