#!/usr/bin/python
# -*- coding: utf-8 -*- 

import os
import json
import uuid

from apps.system.db import DB

from apps.system.user import *
from apps.system.group import *
from apps.system.page import *
from apps.system.session import *

import apps.learn.initapp
import apps.mpmcats.initapp

# =====================================================================
def InitConfig(g):
  """Creates all default settings and saves them to the configuration file.
  """
  
  g.siteconfig  = {
    "addsitenametotitle": 0,
    'adminemail':         '',
    'adminpassword':      uuid.uuid4().hex[0:12],    
    "dbtype":             "sqlite",
    "dbhost":             "localhost",
    "dbname":             "private/system/default.db",
    "dbflags":            DB_SECURE | DB_TRACK_CHANGES | DB_TRACK_VALUES,
    "dbuser":             "",
    "dbpassword":         "",
    "cssfiles":           [
                            "/system/normalize.css",
                            "/system/colors.css",
                            "/system/common.css",
                            "/system/paferalib.css",
                            "/system/pafera.css",
                          ],
    "jsfiles":            [
                            "/libs/hammer/hammer.min.js",
                            "/libs/howler/howler.min.js",
                            "/system/loader.min.js",
                            "/system/paferalib.js",
                            "/system/paferapage.js",
                            "/system/pafera.js",
                          ],
    "languages":          [
                            'ar',
                            'bn',
                            'de',
                            'en',
                            'es',
                            'fr',
                            'hi',
                            'ja',
                            'pt',
                            'ru',
                            'zh'
                          ],
    'sessiontimeout':     604800,
    'siteurl':            '',
    'urltracking':        1,
    'production':         0,
    'htmlcachetime':      10,
    'installedapps':      [],
  }
    
  os.makedirs('private/system', exist_ok = True)
  
  with open('private/system/pafera.cfg', 'w') as f:
    f.write(json.dumps(g.siteconfig, sort_keys = True, indent = 2))
    
# =====================================================================
def InitDB(g):
  """Setups the initial database users, groups, and pages.
  """
  db  = g.db
  
  if db.HasTable('system_user'):
    return
  
  LoadTranslation(g, 'system', 'registration', 'en')
  
  db.RegisterModel(system_session)
  
  u = system_user()
  u.Set(
    phonenumber   = 'admin',
    place         = 'pafera',
    displayname   = {'en': 'admin'},
    settings      = {
      'homeurl':    '/system/controlpanel.html',
    },
    flags         = USER_IS_ADMIN,
  )
  u.SetPassword('password', g.siteconfig['adminpassword'])
  db.Save(u)
  
  # Special group for logged in users. Every user is automatically
  # added upon creation, allowing you to use g.RequireGroup('users)
  # to limit content to only logged in users.
  users = system_group()
  users.Set(
    groupname     = 'users',
    displayname   = {'en': 'Users'}
  )
  db.Save(users)
  
  db.Link(u, users)
  
  for k, v in {
      'businesses':             'Businesses',
    }.items():
    
    newgroup  = system_group()
    newgroup.Set(
      displayname = v,
      groupname   = k,
    )
    db.Save(newgroup)
  
  siteheader = system_pagefragment()
  siteheader.Set(
    path    = 'Site Header',
    size    = 'auto',
    content = {
      'en': f"""    
  <div class="SiteHeader Flex FlexCenter FlexWrap white">
    <a href="/">
      <img src="/favicon.ico" style="height: 2em; float: left;">
      <span style="display: block; padding: 0.3em; float: left;">{g.T.pafera}</span>
    </a>
    <a class="LoginLink" href="/system/login.html">{g.T.login}</a>
    <a class="" href="/learn/home.html">{g.T.learn}</a>
    <a class="NoFlex Width400 Right ChooseLanguageIcon" onclick="P.ChooseLanguagePopup(event)"></a>
    <a class="NoFlex Width400 Right SystemUserIcon" onclick="P.UserPopup(event)"></a>
    <a class="NoFlex Width400 Right MessageCountIcon" href="/system/messages.html"></a>
  </div>
""",
    },
    contentfunc = "",
  )
    
  db.Save(siteheader)
  
  sitefooter = system_pagefragment()
  sitefooter.Set(
    path    = 'Site Footer',
    size    = 'auto',
    content = {
      'en': """  
  <div class="SiteFooter Center white Pad50">
    This site is powered by the Pafera WSGI Framework available at <a href="https://pafera.com/programming.html">pafera.com/programming.html</a>.
  </div>
  
<!-- These brs are for mobile browsers which don't implement CSS grid correctly in order to see the bottom of the page. -->
<br>
<br>
<br>
<br>
<br>
<br>
""",
    },
    contentfunc = "",
  )
    
  db.Save(sitefooter)
  
  p = system_page()
  p.Set(
    path    = '/system/index.html',
    title   = {
      'en': 'Welcome to Pafera!',
    },
    content = {
      'en': f"""  
  <div class="Grid GridCenter MinHeight80P">
    <div class="Center Margin25">
      <h1 class="Center">Welcome to Pafera!</h1>
      
      <div class="whiteb Rounded Raised MaxWidth2400 Pad100 Left">
        <p>If you can see this page, then your install was successful!</p>
        <p>Please look in private/system/pafera.cfg for your auto-generated admin password. Use this password along with phone number "admin" and place "pafera" to login to the system, then head to the control panel to start configuring your site.
        </p>
        <div class="ButtonBar">
          <a class="Color3" href="/login.html">
            Login
          </a>
        </div>
      </div>
    </div>
  </div>
""",
    },
    contentfunc   = "",
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
  )
    
  db.Save(p)
  
  p = system_page()
  p.Set(
    path    = '/system/controlpanel.html',
    title   = {
      'en': 'Welcome to Your New Pafera Install',
    },
    content = {
      'en': """
<div class="Pad50 white">
  <h1>Welcome to Your New Pafera Install</h1>
  
  <p>
  If you are seeing this screen, then your installation of the Pafera Framework was successful. Let's configure your site and get you on your way.
  </p>
  
  <p>
  Remember to set a new administrator password before doing anything else. The last thing that you want is to have your website start leaking every username and password to the whole world and have a nasty pile of angry lawsuits sitting on your desk.
  </p>
</div>
  
<div class="ConfigTiles FlexGrid30 Gap50"></div>
""",
    },
    contentfunc = "",
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
    translations  = [
      'system/admin',
    ],
    requiredgroups = [
      'admins',
    ]
  )
    
  db.Save(p)
  
  p = system_page()
  p.Set(
    path    = '/system/db.html',
    title   = {
      'en': 'Database Management',
    },
    content = {
      'en': """
<div class="PageLoadContent"></div>
""",
    },
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
    contentfunc = "",
    jsfiles     = [
      '/system/paferalist.js',
      '/system/paferachooser.js',
      '/system/paferasecurity.js',
    ],
    translations  = [
      'system/admin',
    ],
    requiredgroups = [
      'admins',
    ]
  )
    
  db.Save(p)
  
  p = system_page()
  p.Set(
    path    = '/system/translations.html',
    title   =  {
      'en': 'Translations',
    },
    content = {
      'en': """
<h1>Translations</h1>

<div class="ButtonBar TranslationActions"></div>
  
<div class="Pad50 Translations"></div>
  
""",
    },
    contentfunc = "",
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
    requiregroups = [
      'admins',
      'translators',
    ],
  )
    
  db.Save(p)
  
  p = system_page()
  p.Set(
    path    = '/system/login.html',
    title   = {
      'en': g.T.login,
    },
    content = {
      'en': f"""  
  <div class="Grid GridCenter MinHeight80P">
    <div class="Center Margin25">
      <h1 class="Center">{g.T.login}</h1>
      
      <div class="LoginForm whiteb Rounded Raised MaxWidth2400 Left"></div>
    </div>
  </div>
""",
    },
    jsfiles       = [
      '/libs/simplewebauthn/simplewebauthn.js'
    ],
    contentfunc   = "",
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
  )
    
  db.Save(p)
  
  p = system_page()
  p.Set(
    path    = '/system/register.html',
    title   = {
      'en': g.T.register,
    },
    content = {
      'en': f"""  
  <div class="Grid GridCenter MinHeight80P">
    <div class="Center Margin25 MaxWidth2400">
      <h1 class="Center">{g.T.register}</h1>
      
      <div class="RegistrationStatus"></div>
      
      <div class="RegistrationForm whiteb Rounded Raised Left"></div>
    </div>
  </div>
""",
    },
    contentfunc   = "",
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
    translations  = [
      'system/registration',
    ],
  )
    
  db.Save(p)
  
  p = system_page()
  p.Set(
    path    = '/system/needapproval.html',
    title   = {
      'en': g.T.needapproval,
    },
    content = {
      'en': f"""  
  <div class="Grid GridCenter MinHeight80P">
    <div class="Center Margin25">
      <h1 class="Center">{g.T.needapproval}</h1>
      
      <div class="ApprovalList"></div>
    </div>
  </div>
""",
    },
    contentfunc   = "",
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
    jsfiles       = [
      '/system/paferalist.js',
    ],
    translations  = [
      'system/registration',
    ],
    requiredgroups = [
      'users',
    ]
  )
    
  db.Save(p)  
  p = system_page()
  p.Set(
    path    = '/system/logout.html',
    title   = {
      'en': g.T.logout,
    },
    content = {
      'en': f"""
  <div class="Grid GridCenter MinHeight80P">
    <div class="Center Margin25">
      <h1 class="Center">{g.T.logout}</h1>
      
      <div class="whiteb Rounded Raised Pad100 MaxWidth2400 Left">{g.T.loggedout}</div>
    </div>
  </div>    
""",
    },
    contentfunc   = "g.session.New(g)",
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
  )
    
  db.Save(p)
  
  p = system_page()
  p.Set(
    path    = '/404.html',
    title   = {
      'en': "404: Page Not Found",
    },
    content = {
      'en': f"""
<div class="Grid GridCenter MinHeight80P">
  <div class="white Margin25 Flex FlexCenter FlexWrap">
    <div class="Center">
      <img src="/system/404.webp" class="Width800">
    </div>    
    <div class="Pad50">
    <h1>We couldn't find your webpage!</h1>
    <p>Are you sure that your URL is correct?</p>
    <p>We know that you're probably a bit sad right now, but don't worry. There are plenty of fun things that you can see on our homepage. Just click the button below!</p>
    <div class="ButtonBar">
      <a class="Color3 Pad50" href="/">Go to Our Homepage</a>
    </div>
  </div>
</div>
""",
    },
    contentfunc   = "",
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
  )
    
  db.Save(p)
  
  p = system_page()
  p.Set(
    path    = '/system/usersettings.html',
    title   = {
      'en': "User Settings",
    },
    content = {
      'en': f"""
<div class="PageLoadContent"></div>
""",
    },
    contentfunc   = "",
    jsfiles       = [
      '/libs/simplewebauthn/simplewebauthn.js',
      '/system/paferalist.js',
      '/system/paferachooser.js',
    ],
    translations  = [
      'system/admin',
      'system/registration',
    ],
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
    requiredgroups  = [
      'users',
    ]
  )
    
  db.Save(p)
  
  p = system_page()
  p.Set(
    path    = '/system/manageusers.html',
    title   = {
      'en': "Manage Users",
    },
    content = {
      'en': f"""
<div class="PageLoadContent"></div>
""",
    },
    contentfunc   = "",
    jsfiles       = [
      '/system/paferalist.js',
      '/system/paferachooser.js',
      '/system/paferafilechooser.js',
    ],
    translations  = [
      'system/admin',
    ],
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
    requiredgroups  = [
      'users',
    ],
    flags         = PAGE_LOAD_HOOKS,
  )
    
  db.Save(p)
  
  p = system_page()
  p.Set(
    path    = '/system/messages.html',
    title   = {
      'en': "Messages",
    },
    content = {
      'en': f"""
<div class="PageLoadContent"></div>
""",
    },
    contentfunc   = "",
    jsfiles       = [
      '/system/paferalist.js',
      '/system/paferachooser.js',
      '/system/paferafilechooser.js',
      '/system/messagelist.js',
    ],
    translations  = [
    ],
    headerid      = siteheader.rid,
    footerid      = sitefooter.rid,
    requiredgroups  = [
      'users',
    ],
  )
    
  db.Save(p)
  
  apps.learn.initapp.OnAppInstall(g)
  apps.mpmcats.initapp.OnAppInstall(g)
  
  db.Commit()

