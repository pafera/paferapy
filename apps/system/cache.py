#!/usr/bin/python
# -*- coding: utf-8 -*- 

import os
import os.path
import time

# *********************************************************************
class Cache(object):
  """Simple filesystem cache implementation for storing binary objects.
  
  Simply call cache.Store() to save the object and cache.Load() to 
  get the object back. path identifies the object while timeout 
  determines how long the object will be saved before timeout.
  """
  
  # -------------------------------------------------------------------
  def __init__(self, 
      cachedir = 'cache', 
      timeout = 10, 
      keepcharacters  = ['_']):
    """Creates an instance of the cache. 
    
    keepcharacters is used to determine what non-alphanumeric characters
    are allowed in the cache filename.
    """
    
    self.cachedir       = cachedir
    self.timeout        = timeout
    self.keepcharacters = keepcharacters
    
    os.makedirs(self.cachedir, exist_ok = True)
    
  # -------------------------------------------------------------------
  def SanitizePath(self, path):
    """Turns path from the application identifier into a name suitable
    for the file system. It will accept all alphanumeric characters or
    any characters in keepcharacters.
    """
    
    path  = path.replace('/', '_')
    return "".join(c for c in path if c.isalnum() or c in self.keepcharacters).rstrip()
  
  # -------------------------------------------------------------------
  def Load(self, path):
    """Returns a binary object stored in the system, or an empty string
    if the object has expired.
    
    path is the identifier for the object wanted.
    """
    
    cachefile = os.path.join(self.cachedir, self.SanitizePath(path))
    
    try:
      stats = os.stat(cachefile)
      
      if stats.st_mtime + self.timeout > time.time():
        return open(cachefile, 'rb').read()
    except Exception as e:
      pass
    
    return ''
  
  # -------------------------------------------------------------------
  def Store(self, path, data):
    """Stores data as a binary object into the system.
    
    path is the identifier for the object to be later retrieved.
    """
    
    cachefile = os.path.join(self.cachedir, self.SanitizePath(path))
    
    with open(cachefile, 'wb') as f:
      f.write(data)
  
