#!/usr/bin/python
# -*- coding: utf-8 -*- 

import subprocess
import re
import os
import hashlib

from apps.system.utils import *
from apps.system.validators import *
from apps.system.db import *

# File types
FILE_DOCUMENT      =	1;
FILE_IMAGE         =	2;
FILE_AUDIO         =	3;
FILE_VIDEO         =	4;
FILE_ARCHIVE       =	5;
FILE_PROGRAM       =	6;
FILE_SPREADSHEET   =	7;
FILE_PRESENTATION  =	8;
FILE_BOOK          =	9;
FILE_TEXT          =	10;
FILE_DISK_IMAGE    =	11;
FILE_WEBPAGE       =	12;

FILE_DELETED       =	126;
FILE_OTHER         =	127;

# Flags
FILE_PUBLIC       = 0x01;
FILE_PROTECTED    = 0x02;
FILE_PRIVATE      = 0x04;

# *********************************************************************
class system_file(ModelBase):
  """Class for managing all uploaded files. 
  
  This is a system-wide repository for user uploaded files that allows
  easy search and discovery while retaining security and ease of
  management. Files can be made searchable, requires a direct link to 
  access, or locked down to the point where a timed access token is 
  needed to access the file. 
  
  For efficiency, all uploaded images are converted to the webp format,
  audio to aac, and video to h264 MP4s. If you don't want this behavior,
  modify the system/upload handler in system/__init__.py or add your
  users to the uploadoriginals group.
  
  While files are normally automatically imported into the system
  directories under public/system/files and private/system/files, it 
  is possible to use existing files without importing them by setting
  the filepath field before calling file.Process(). This allows you
  to create things like image galleries while keeping your existing
  folder structure.
  """
  
  _dbfields     = {
    'rid':          ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'filepath':     ('TEXT', "NOT NULL DEFAULT ''",),
    'filename':     ('TEXT', 'NOT NULL', BlankValidator()),
    'extension':    ('TEXT', 'NOT NULL', BlankValidator()),
    'descriptions': ('TRANSLATION', "NOT NULL DEFAULT ''",),
    'modified':     ('TIMESTAMP', 'NOT NULL',),
    'size':         ('INT64', 'NOT NULL',),
    'hash':         ('TEXT', 'NOT NULL',),
    'filetype':     ('INT16', 'NOT NULL',),
    'width':        ('INT16', 'NOT NULL DEFAULT 0',),
    'height':       ('INT16', 'NOT NULL DEFAULT 0',),
    'length':       ('INT16', 'NOT NULL DEFAULT 0',),
    'flags':        ('INT', 'NOT NULL DEFAULT 0',),
    'accesstokens': ('DICT', "NOT NULL DEFAULT ''",),
  }
  _dbindexes  = ()
  _dblinks    = []
  _dbdisplay  = ['filename', 'descriptions', 'modified', 'size']
  _dbflags    = DB_SECURE
  
  # Generic file types for faster searches
  TYPES = {
    'document':     1,
    'image':        2,
    'sound':        3,
    'video':        4,
    'archive':      5,
    'program':      6,
    'spreadsheet':  7,
    'presentation': 8,
    'book':         9,
    'text':         10,
    'disk image':   11,
    'webpage':      12,
    'source code':  13,
    'deleted':      126,
    'other':        127,
  }
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
    self.fullpath = ''

  # -------------------------------------------------------------------
  def PublicPath(self):
    return 'public/system/files/' + CodeDir(ToShortCode(self.rid)) + '.' + self.extension
    
  # -------------------------------------------------------------------
  def PrivatePath(self):
    return 'private/system/files/' + CodeDir(ToShortCode(self.rid)) + '.' + self.extension
    
  # -------------------------------------------------------------------
  def FullPath(self):
    """Returns the filesystem path for this file.
    """    
    if self.filepath:
      return self.filepath
    
    if self.flags & FILE_PRIVATE:
      return self.PrivatePath()
    
    return self.PublicPath()
  
  # -------------------------------------------------------------------
  def ThumbnailFile(self):
    """Returns the filesystem path for an appropiate thumbnail.
    """
    path  = self.MakeThumbnail()
    
    if path:
      return path
    
    icondir = 'public/system/icons/'
    
    if self.filetype == FILE_DOCUMENT:
      return icondir + 'document.webp'
    elif self.filetype == FILE_SPREADSHEET:
      return icondir + 'spreadsheet.webp'
    elif self.filetype == FILE_PRESENTATION:
      return icondir + 'presentation.webp'
    elif self.filetype == FILE_IMAGE:
      return icondir + 'image.webp'
    elif self.filetype == FILE_AUDIO:
      return icondir + 'audio.webp'
    elif self.filetype == FILE_VIDEO:
      return icondir + 'video.webp'
    elif self.filetype == FILE_ARCHIVE:
      return icondir + 'archive.webp'
    elif self.filetype == FILE_PROGRAM:
      return icondir + 'program.webp'
    elif self.filetype == FILE_BOOK:
      return icondir + 'book.webp'
    elif self.filetype == FILE_TEXT:
      return icondir + 'text.webp'
    elif self.filetype == FILE_DISK_IMAGE:
      return icondir + 'diskiamge.webp'
    elif self.filetype == FILE_WEBPAGE:
      return icondir + 'webpage.webp'
    
    return icondir + 'unknown.webp'
  
  # -------------------------------------------------------------------
  def ThumbnailURL(self):
    """Returns a URL to display the thumbnail image.
    """
    thumbfile = self.ThumbnailFile()
    
    if self.flags & FILE_PRIVATE or thumbfile[0] != 'p':
      return '/system/thumbnailer/' + ToShortCode(self.rid)
    
    return thumbfile[6:]
  
  # -------------------------------------------------------------------
  def URL(self):
    """Returns a URL to display the thumbnail image.
    """
    if self.flags & FILE_PRIVATE:
      return '/system/downloader/' + ToShortCode(self.rid)
    
    return self.PublicPath()[6:]
  
  # -------------------------------------------------------------------
  def Process(self, g, sourcefile = '', importfile = 0, deleteoriginal = 0):
    """Analyzes an existing file and inserts it into the system registry, or 
    update any changed files. 
    
    sourcefile:     The full path to the file to process
    importfile:     Set to true to copy the file into the system directories
    deleteoriginal: Set to true to delete the original file
    """
    if not sourcefile and not self.rid:
      raise Exception('Cannot process a blank file.')
    
    if sourcefile:
      fullpath  = sourcefile
    else:
      fullpath  = self.FullPath()
      
    stats     = os.stat(fullpath)
    
    size      = stats.st_size 
    modified  = stats.st_mtime;

    # Only process updated or different sized files
    if self.size == size and self.modified == modified:
      return;
    
    self.DetermineFileType()
    
    self.Set(
      size      = size,
      modified  = modified,
      hash      = self.GetHash(sourcefile),
    )
    
    if not importfile:
      self.Set(
        fullpath  = sourcefile
      )
    
    g.db.Save(self)
    
    if importfile:
      fullpath  = self.FullPath()
      
      os.makedirs(os.path.dirname(fullpath), exist_ok = 1)
      
      if deleteoriginal:
        os.rename(sourcefile, self.FullPath())
      else:
        shutil.copy(sourcefile, self.FullPath())    
    
  # -------------------------------------------------------------------
  def GetHash(self, sourcefile = ''):
    hasher  = hashlib.sha1()
    
    if not sourcefile:
      sourcefile  = self.FullPath()
    
    with open(sourcefile, 'rb') as f:
      chunk = 1
      
      while chunk:
        chunk = f.read(1024 * 4)
        
        if not chunk:
          break
          
        hasher.update(chunk)
        
    return hasher.hexdigest()    
  
  # -------------------------------------------------------------------
  def DetermineFileType(self):
    extension = os.path.splitext(self.filename)[1]
    
    # Remove leading period
    if extension:
      extension = extension[1:]
      
    self.Set(
      extension = extension
    )
    
    if extension in [
      'jpg',
      'jpeg',
      'gif',
      'png',
      'tif',
      'tiff',
      'svg',
      'psd',
      'bmp',
      'pcd',
      'pcx',
      'pct',
      'pgm',
      'ppm',
      'tga',
      'img',
      'raw',
      'webp',
      'wbmp',
      'eps',
      'cdr',
      'ai',
      'dwg',
      'indd',
      'dss',
      'fla',
      'ss',
    ]:
      filetype =FILE_IMAGE
    elif extension in [
      'doc',
      'docx',
      'odt',
      'rtf',
      'pub',
      'wpd',
      'qxd',
      'cdl',
      'eml',
    ]:
      filetype  = FILE_DOCUMENT
    elif extension in [
      'chm',
      'pdf',
      'epub',
      'mobi',
      'cbr',
      'cbz',
      'cb7',
      'cbt',
      'cba',
      'ibooks',
      'kf8',
      'pdg',
      'azw',
      'prc',
    ]:
      filetype = FILE_BOOK
    elif extension in [
      'mp3',
      'aac',
      'wma',
      'oga',
      'ogg',
      'm4a',
      'wav',
      'aif',
      'aiff',
      'dvf',
      'm4b',
      'm4p',
      'mid',
      'midi',
      'ram',
      'mp2',
    ]:
      filetype  = FILE_AUDIO
    elif extension in [
      'mp4',
      'mpg',
      'avi',
      'wmv',
      'rm',
      'rmvb',
      'ogv',
      'ogm',
      'm4v',
      'mov',
      'flv',
      'f4v',
      '3gp',
      '3gpp',
      'vob',
      'asf',
      'divx',
      'mswmm',
      'asx',
      'amr',
      'mkv',
      'vp8',
      'webm',
    ]:
      filetype  = FILE_VIDEO
    elif extension in [
      'zip',
      'gz',
      'bz2',
      'rar',
      'tar',
      'lz',
      'lzma',
      'lzo',
      'rz',
      'sfark',
      'xz',
      'z',
      '7z',
      's7z',
      'ace',
      'cab',
      'pak',
      'rk',
      'sfx',
      'sit',
      'sitx',
      'qbb',
      'jar',
      'pst',
      'hqx',
      'sea',
    ]:
      filetype  = FILE_ARCHIVE
    elif extension in [
      'exe',
      'msi',
      'bat',
      'cmd',
    ]:
      filetype  = FILE_PROGRAM
    elif extension in [
      'xls',
      'xlsx',
      'ods',
      'qbw',
      'wps',
    ]:
      filetype  = FILE_SPREADSHEET
    elif extension in [
      'ppt',
      'pptx',
      'odp',
    ]:
      filetype  = FILE_PRESENTATION
    elif extension in [
      'txt',
      'ini',
      'cfg',
      'log',
    ]:
      filetype  = FILE_TEXT
    elif extension in [
      'htm',
      'html',
      'mhtml',
    ]:
      filetype  = FILE_WEBPAGE
    elif extension in [
      'deleted',
    ]:
      filetype  = FILE_DELETED
    else:
      filetype  = FILE_OTHER
    
    self.Set(
      filetype    = filetype,
    );
      
  # -------------------------------------------------------------------
  def MakeThumbnail(self, thumbsize = '256'):
    """Creates or updates a thumbnail if needed. 
    
    The default thumbnail size is 256x256, but if you are working with 
    high resolution screens, feel free to use 512x512 or higher. 
    
    You'll need to install imagemagick, ffmpeg, and ffmpegthumbnailer
    to use this functionality.
    """
    if self.flags & FILE_PRIVATE:
      thumbfile = 'private/system/thumbs/' + CodeDir(ToShortCode(self.rid)) + '.webp'
    else:
      thumbfile = 'public/system/thumbs/' + CodeDir(ToShortCode(self.rid)) + '.webp'
      
    os.makedirs(os.path.dirname(thumbfile), exist_ok = True)
    
    if os.path.exists(thumbfile):
      stats = os.stat(thumbfile)
      
      if stats.st_mtime > self.modified:
        return thumbfile
    
    fullpath  = self.FullPath()
    
    if self.filetype == FILE_IMAGE:
      # No need to make thumbnails for small images
      if self.size < (20 * 1024):
        if self.flags & FILE_PRIVATE:
          return self.PrivatePath()
        
        return self.PublicPath()
      
      result = subprocess.run(['identify', fullpath], stdout=subprocess.PIPE).stdout.decode('utf-8')
      
      if not self.width:
        matches = re.findall('([0-9]+)x([0-9]+)', result, re.MULTILINE)
        
        self.Set(
          width   = matches[0][0],
          height  = matches[0][1],
        )
        
        subprocess.run([
          'convert',
          fullpath,
          '-thumbnail',
          thumbsize + 'x' + thumbsize + '^',
          '-gravity',
          'center',
          '-extent',
          thumbsize + 'x' + thumbsize,
          thumbfile
        ])
    elif self.filetype == FILE_VIDEO:
      if not self.width:
        result = subprocess.run(['ffmpeg', '-i', fullpath], stdout=subprocess.PIPE).stdout.decode('utf-8')
        
        matches = re.findall('Duration: ([0-9]+):([0-9]+):([0-9]+)', result, re.MULTILINE)
        
        self.Set(
          length  = (
            (int(matches[0][0]) * 3600)
            + (int(matches[0][1]) * 60)
            + (int(matches[0][2]))
          )
        )
        
        matches = re.findall('([0-9]+)x([0-9]+)', result, re.MULTILINE)
        
        self.Set(
          width   = matches[0][0],
          height  = matches[0][1],
        )
        
        subprocess.run(['ffmpegthumbnailer', '-i', 'fullpath', '-o', thumbfile, '-s', '512', '-a'])
    
    if os.path.exists(thumbfile):
      return thumbfile
    
    return ''
  
  # -------------------------------------------------------------------
  def OnDBDelete(self, db):
    if self.flags & FILE_PRIVATE:
      thumbfile = 'private/system/thumbs/' + CodeDir(ToShortCode(self.rid)) + '.webp'
    else:
      thumbfile = 'public/system/thumbs/' + CodeDir(ToShortCode(self.rid)) + '.webp'
      
    if os.path.exists(thumbfile):
      os.remove(thumbfile)
      
    fullpath  = self.FullPath()
    
    if os.path.exists(fullpath):
      os.remove(fullpath)
