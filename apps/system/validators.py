#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# This file contains validator objects designed to be called as 
# functions in order to validate values. They are useful for 
# simple and quick validation schemes where you don't need
# advanced handling.
#
# To use them, simply create an instance of a class and then call it 
# as in 
#
# validator	= RangeValidator(0, 100)
# validator(200)
#
# The validator will throw an exception if the value is not correct.

import dateutil.parser

# *********************************************************************
class BaseValidator(object):
	"""Base class that doesn't do anything yet, but is a convenient
	place to add any functionality that is common to all validators.
	"""
	pass

# *********************************************************************
class FakeValidator(BaseValidator):
	"""Automatically passes all values.
	"""
	
	def __call__(self, fieldname, value):	
		pass

# *********************************************************************
class NullValidator(BaseValidator):
	"""Throws an exception on NULL values.
	"""
	def __call__(self, fieldname, value):	
		if value == None:
			raise Exception(f'{fieldname} cannot be null!')
		
# *********************************************************************
class BlankValidator(BaseValidator):
	"""Throws an exception on blank values.
	"""
	def __call__(self, fieldname, value):	
		if not value:
			raise Exception(f'{fieldname} cannot be blank!')
		
# *********************************************************************
class EmailValidator(BaseValidator):
	"""Throws an exception on invalid email addresses.
	
	Note that this is a simple regex, so it won't validate more advanced
	email addresses.
	"""
	def __call__(self, fieldname, value):	
		if not re.match(r"/[a-z0-9!#%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9] (?:[a-z0-9-]*[a-z0-9])?/i", value):
			raise Exception(f'{value} is not a valid email address!')

# *********************************************************************
class DateValidator(BaseValidator):
	"""Throws an exception on invalid date strings.
	
	Note that this simpy uses dateutil's parse(), so it won't 
	validate more advanced dates.
	"""
	def __call__(self, fieldname, value):	
		try:
			dateutil.parser.parse(value)
		except:
			raise Exception(f'{value} is not a valid date!')
			
# *********************************************************************
class TimeValidator(BaseValidator):
	"""Throws an exception on invalid time strings.
	
	Note that this simpy uses dateutil's parse(), so it won't 
	validate more advanced dates.
	"""
	def __call__(self, fieldname, value):	
		try:
			dateutil.parser.parse(value)
		except:
			raise Exception(f'{value} is not a valid time!')
			
# *********************************************************************
class DateTimeValidator(BaseValidator):
	"""Throws an exception on invalid datetime strings.
	
	Note that this simpy uses dateutil's parse(), so it won't 
	validate more advanced dates.
	"""
	def __call__(self, fieldname, value):	
		try:
			dateutil.parser.parse(value)
		except:
			raise Exception(f'{value} is not a valid date!')

# *********************************************************************
class RangeValidator(BaseValidator):
	"""Throws an exception if the value is too high or too low.
	"""
	
	# -------------------------------------------------------------------
	def __init__(self, min, max):
		self.min	=	min
		self.max	=	max
		
	# -------------------------------------------------------------------
	def __call__(self, fieldname, value):	
		if value < self.min:
			raise Exception(f'{value} is too low for {fieldname}!')
			
		if value > self.max:
			raise Exception(f'{value} is too high for {fieldname}!')
