#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system.db import *
from apps.system.validators import *

# *********************************************************************
class system_group(ModelBase):
  """Represents different groups for users to belong to. Testing for
  membership is its only purpose.
  """
  
  _dbfields     = {
    'rid':          ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'groupname':    ('TEXT', 'NOT NULL', BlankValidator()),
    'displayname':  ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'flags':        ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = (
    ('unique', ('groupname',)),
  )
  _dblinks    = ['system_user']
  _dbdisplay  = ['groupname', 'displayname', 'flags']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()

