#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system.validators import *
from apps.system.db import *

# *********************************************************************
class system_loginattempt(ModelBase):
  """A simple class to keep track of login attempts into the system to
  counter repeated attempts to login to the system. I'm quite aware
  that IP addresses do not translate to single users, but unless you're
  running a *really* popular site, you shouldn't have any problems
  limiting login attempts by IP.
  """
  
  _dbfields     = {
    'id':           ('INTAUTOINCREMENT', 'NOT NULL',),
    'ip':           ('TEXT', 'NOT NULL', BlankValidator()),
    'eventtime':    ('TIMESTAMP', 'NOT NULL',),
  }
  _dbindexes  = ()
  _dblinks    = []
  _dbdisplay  = ['eventtime', 'ip']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()

