#!/usr/bin/python
# -*- coding: utf-8 -*- 

import time

from apps.system.validators import *
from apps.system.db import *
from apps.system.session import *
from apps.system.user import *

MESSAGE_SYSTEM        = 0x1
MESSAGE_UNREAD        = 0x4
MESSAGE_READ          = 0x8
MESSAGE_REPLIED       = 0x20
MESSAGE_DELETED       = 0x40
MESSAGE_MAIL          = 0x80
MESSAGE_FORUM         = 0x100
MESSAGE_SEND_AS_SMS   = 0x200
MESSAGE_SEND_AS_EMAIL = 0x400

MESSAGE_LINK_INBOX_UNREAD     = 0
MESSAGE_LINK_INBOX_READ       = 1
MESSAGE_LINK_ARCHIVED         = 2
MESSAGE_LINK_DRAFTS           = 3
MESSAGE_LINK_SENT             = 4
MESSAGE_LINK_DELETED          = 5
MESSAGE_LINK_URGENT           = 6
MESSAGE_LINK_FAVORITES        = 7
MESSAGE_LINK_LIKE             = 8
MESSAGE_LINK_DISLIKE          = 9
MESSAGE_LINK_ALERT_ON_UPDATE  = 10
MESSAGE_LINK_ALERT_ON_REPLY   = 11

# *********************************************************************
class system_message(ModelBase):
  """Permits users to send messages to each other, or for the system to 
  send messages to an user. It's also the basis for forums as well.
  
  In order to save space, messages are stored only once on the system. 
  The actual sending and receiving process is done through links to 
  users or other objects. Messages are automatically deleted when the 
  last link is severed, or can be explicitly deleted by an administrator.
  
  When messages are used in forum mode, it's normally attached to an
  object. If you want to use security for your forum so that it's not 
  public to everyone, then your object must include three NEWLINELIST
  fields consisting of forumadmins, forumposters, and forumviewers that
  contain the allowed user IDs in short code format. If these are blank,
  then permissions will not be enforced and everything will be public.
  """
  
  _dbfields     = {
    'rbid':         ('INT64 PRIMARY KEY', 'NOT NULL',),
    'objid':        ('INT64', 'NOT NULL DEFAULT 0',),
    'objtype':      ('INT', 'NOT NULL DEFAULT 0',),
    'fromid':       ('INT', 'NOT NULL DEFAULT 0',),
    'toids':        ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'threadid':     ('INT64', 'NOT NULL DEFAULT 0',),
    'replyid':      ('INT64', 'NOT NULL DEFAULT 0',),
    'size':         ('INT64', 'NOT NULL DEFAULT 0',),
    'created':      ('TIMESTAMP', 'NOT NULL',),
    'modified':     ('TIMESTAMP', 'NOT NULL',),
    'title':        ('TRANSLATION', "NOT NULL DEFAULT ''"),
    'content':      ('TRANSLATION', 'NOT NULL'),
    'files':        ('NEWLINELIST', "NOT NULL DEFAULT ''"),
    'numviews':     ('INT', 'NOT NULL DEFAULT 0',),
    'numreplies':   ('INT', 'NOT NULL DEFAULT 0',),
    'likes':        ('INT', 'NOT NULL DEFAULT 0',),
    'dislikes':     ('INT', 'NOT NULL DEFAULT 0',),
    'flags':        ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = (
    ('', 'objid'),
    ('', 'fromid'),
    ('', 'threadid'),
    ('', 'replyid'),
    ('', 'eventtime'),
    ('', 'flags'),
  )
  _dblinks    = []
  _dbdisplay  = ['title', 'message', 'eventtime']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def OnDBDelete(self, db):
    '''Standard handler for deleting an existing object.
    '''
    if not self.rbid:
      return
    
    db.Execute(f'''DELETE FROM system_message__system_user
      WHERE linkaid = ?''',
      self.rbid,
    )
    
    # Delete all replies when a post is deleted
    if self.numreplies:
      for r in db.Find(
          system_message,
          'WHERE replyid = ?',
          self.rbid,
        ):
        db.Delete(r)

# =====================================================================
def SendSystemMessage(g, **kwargs):
  """Convenience function for sending messages. All arguments are 
  passed to the new message object with eventtime automatically set
  to the current time. The session of the user to whom the message 
  is sent is updated to show a new message.
  """
  if not CanPostMessages():
    raise Exception(g.T.cantcreate)
  
  kwargs['created']   = time.time()
  kwargs['modified']  = kwargs['created']
  
  g.db.RegisterModel(system_message)
  
  kwargs['rbid']     = g.db.FindValidRandomID(system_message, 'rbid')
  kwargs['threadid'] = kwargs['rbid']
  
  if 'flags' not in kwargs:
    kwargs['flags'] = MESSAGE_UNREAD | MESSAGE_MAIL
  
  msg = system_message()
  
  msg.Set(**kwargs)
  
  g.db.Insert(msg)
  
  if 'toids' in kwargs:
    toids = kwargs['toids']
    
    if not isinstance(toids, list):
      toids = [toids]
    
    questionmarks = ', '.join(['?' for x in toids])
    params        = [FromCode(x) for x in toids]
    
    users = g.db.Find(
      system_user,
      'WHERE rid IN (' + questionmarks + ')',
      params
    )
    
    g.db.LinkArray(msg, users, MESSAGE_LINK_INBOX_UNREAD, eraseprevious  = 0)
    
    # Find and update message counts for all users that this
    # message was sent to
    for session in g.db.Find(
        system_session,
        'WHERE userid IN (' + questionmarks + ') AND NOT CAST(flags & ? AS BOOLEAN)',
        params + [SESSION_EXPIRED]
      ):
      
      newmessagecount  = session.get('message.unread.count', 0)
      
      session['message.unread.count'] = newmessagecount  + 1
      
      g.db.Update(session)
      
  g.db.Commit()
      
# =====================================================================
def GetSystemMessagePermissions(g, objtype, objid):
  '''Returns forum permissions for the current user to see if they 
  can administrate, post, or view forums belonging to a particular
  object.
  '''
  
  # objtype can be the string of the model name or the ID of the 
  # model, so we handle both cases
  if isinstance(objtype, str):
    model   = GetModel(objtype)
  else:
    for k, v in g.db.objtypes.items():
      if v[3] == objtype:
        model   = GetModel(k)
    
  if 'forumadmins' in model._dbfields:
    g.db.RegisterModel(model)
    
    obj     = g.db.Load(
      model, 
      objid, 
      fields = model._dbid + ', forumadmins, forumposters, forumviewers'
    )    
  else:
    obj = 0
  
  permissions = {
    'admin':        0,
    'post':         1,
    'view':         1,    
  }
  
  if 'forumadmins' in model._dbfields:
    permissions['admin']  = 1 if g.session.usercode in obj.forumadmins or IsAdmin() else 0
  
  if 'forumposters' in model._dbfields:
    permissions['post']   = 1 if permissions['admin'] or g.session.usercode in obj.forumposters else 0
    
  if 'forumviewers' in model._dbfields:
    permissions['view']   = 1 if permissions['admin'] or g.session.usercode in obj.forumviewers else 0
  
  return [model, obj, permissions]

# =====================================================================
def LinkSystemMessage(g, msg, userids, folderid):
  '''Links mail messages to userids so that it'll appear within folderid, 
  erasing any previous links to other folders.
  '''
  if not isinstance(userids, list):
    userids = [userids]
  
  g.db.Execute(f'''DELETE FROM system_message__system_user
    WHERE linkaid = ? 
      AND linkbid IN ({', '.join(['?' for x in userids])})
      AND linktype IN (?, ?, ?, ?, ?, ?)''',
    [msg.rbid] 
      + userids
      + [MESSAGE_LINK_INBOX_UNREAD,
        MESSAGE_LINK_INBOX_READ,
        MESSAGE_LINK_ARCHIVED,
        MESSAGE_LINK_DRAFTS,
        MESSAGE_LINK_SENT,
        MESSAGE_LINK_DELETED
      ]
  )
  
  users = g.db.LoadMany(system_user, userids, 'rid')
  
  g.db.LinkArray(msg, users, folderid, eraseprevious = 0)
