#!/usr/bin/python
# -*- coding: utf-8 -*- 

import os
import json
import traceback
import importlib
import sys

from pprint import pprint

from apps.system.db import *

from apps.system.utils import *
from apps.system.validators import *

# Page flag constants
PAGE_DONT_CACHE   = 0x01
PAGE_LOAD_HOOKS   = 0x02

# *********************************************************************
class system_pagefragment(ModelBase):
  """Fragments are parts of a page which can be reused across different
  pages. Think headers and footers, navigation bars, widgets, and such.
  """
  
  _dbfields     = {
    'rid':          ('INT', 'NOT NULL PRIMARY KEY',),
    'path':         ('TEXT', 'NOT NULL', BlankValidator()),
    'content':      ('TRANSLATION', 'NOT NULL'),
    'markdown':     ('TRANSLATION', "NOT NULL DEFAULT ''"),
    'contentfunc':  ('MULTITEXT', "NOT NULL DEFAULT ''",),
    'translations': ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'flags':        ('INT', 'NOT NULL DEFAULT 0',),    
  }
  _dbindexes  = (
    ('unique', ('path',)),
  )
  _dbdisplay  = ['path', 'content']
  _dblinks    = []
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def Render(self, g):
    """Returns the text representation of this fragment in the current
    language.
    """
    if self.translations:
      for t in self.translations:
        try:
          appname, translationname  = t.split('/')
          langloaded  = LoadTranslation(g, appname, translationname)
          g.page.jsfiles.append(f'/{appname}/translations/{langloaded}/{translationname}.js')
        except Exception as e:
          print(f'!!!\tProblem loading translation for {self.path}', t, e)
    
    if not self.flags & PAGE_DONT_CACHE:
      result  = g.cache.Load('pagefragment_' + self.path + '_' + g.session.lang)
      
      if result:
        return result.decode('utf-8')    
    
    content = []
    
    content.append(BestTranslation(self.content, g.session.lang))
    
    if self.contentfunc:
      try:
        exec(self.contentfunc)
      except Exception as e:
        content.append('<pre>' + traceback.format_exc() + '</pre>')
        
    result  = '\n'.join(content).encode('utf-16', 'surrogatepass').decode('utf-16')
    
    if not self.flags & PAGE_DONT_CACHE:
      g.cache.Store('pagefragment_' + self.path + '_' + g.session.lang, result.encode('utf-8'))
      
    return result

# *********************************************************************
class system_page(ModelBase):
  """Represents a webpage in the system. Note that database pages can
  contain a variety of page fragments in addition to different languages,
  so they are quite versatile in what they can do.
  
  requiredgroups are a list of group names that are needed to view 
  the page.
  
  headerid, contentid, and the rest are used to load page fragments
  into various sections.
  
  path is the URL for the page such as /index.html
  
  pyfile is the module and name of a python function which will be 
  called with the global object g and add content to the page 
  by returning a string.
  
  template is the template file used to create the page. It's
  unused as of now since we only use one standard template.
  
  jsfiles is a list of URLs to load JavaScript files. They can be 
  local or remote. Note that the system will automatically load a
  JavaScript file of the same path and name. Thus, if you create 
  a page at /newapp/testing.html, the system will try to find and load
  /newapp/testing.js without you having to do anything.
  
  cssfiles is the same but for CSS files.
  
  jscode is inline JavaScript code to include in the <head> tag
  which will be executed after the script tags.
  
  csscode is inline CSS stylings for this particular page to be 
  included in the <head> tag.
  
  translations are system translation files to automatically load
  for use in the page. They will also be available to any JavaScript
  files included in the system. 
  
  title, content, and markdown are all translation fields, meaning 
  that you can have completely separate content for each language
  at the same URL. markdown text will be automatically converted
  by the Python markdown library and stored in content.
  
  contentfunc is pure python code run in the context of the 
  page.Render() function and passed to the exec() function. Yes,
  I'm quite aware of the dangers of dynamic code evaluation, but
  honestly, if you have someone running around randomly inserting
  code into your database, you already have much bigger problems
  than a page function. Look at the entry for /logout.html to see 
  a simple and convenient use of this field. 
  
  wallpaper is the URL to a file which will automatically fill 
  the background of the page. It's a quick way to give different
  pages distinctive feels.
  
  flags can contain PAGE_DONT_CACHE, which is self-explanatory, and 
  PAGE_LOAD_HOOKS. Hooks are JavaScript files with the same name
  as this page in other apps which can add functionality to this
  app's pages. See /system/manageusers.html for an example of this.
  """
  
  _dbfields     = {
    'rid':            ('INT', 'NOT NULL PRIMARY KEY',),
    'flags':          ('INT', 'NOT NULL DEFAULT 0',),
    'requiredgroups': ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    
    'headerid':     ('INT', 'NOT NULL DEFAULT 0',),
    'contentid':    ('INT', 'NOT NULL DEFAULT 0',),
    'footerid':     ('INT', 'NOT NULL DEFAULT 0',),
    'leftbarid':    ('INT', 'NOT NULL DEFAULT 0',),
    'topbarid':     ('INT', 'NOT NULL DEFAULT 0',),
    'rightbarid':   ('INT', 'NOT NULL DEFAULT 0',),
    'bottombarid':  ('INT', 'NOT NULL DEFAULT 0',),
    
    'path':         ('TEXT', 'NOT NULL', BlankValidator()),
    'pyfile':       ('TEXT', "NOT NULL DEFAULT ''",),
    'template':     ('TEXT', "NOT NULL DEFAULT ''",),
    'jsfiles':      ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'cssfiles':     ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'jscode':       ('MULTITEXT', "NOT NULL DEFAULT ''",),
    'csscode':      ('MULTITEXT', "NOT NULL DEFAULT ''",),
    'translations': ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    
    'title':        ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'content':      ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'contentfunc':  ('MULTITEXT', "NOT NULL DEFAULT ''",),
    'markdown':     ('TRANSLATION', "NOT NULL DEFAULT ''"),
    'wallpaper':    ('TEXT', "NOT NULL DEFAULT ''",),
  }
  _dbindexes  = (
    ('unique', ('path',)),
  )
  _dbdisplay  = ['path', 'title', 'content', 'cssfiles', 'jsfiles', 'translations', 'headerid', 'footerid']
  _dblinks    = []
  _dbflags    = 0
  
  _managejs = """
G.fragmentstart = 0;  

G.EditPageFragment  = function(fragmentid)
{
  if (fragmentid && fragmentid != '0')
  {
    G.fragmentid    = fragmentid;
    
    P.DialogAPI(
      '/system/dbapi',
      {
        command:  'load',
        model:    'system_pagefragment',
        modelid:  fragmentid
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        G.EditPageFragmentForm(d.data);
      }
    );
  } else
  {
    G.EditPageFragmentForm({});
  }
}

G.EditPageFragmentForm  = function(fragmentobj)
{
  let flags = fragmentobj.flags ? fragmentobj.flags : 0;
  
  P.EditPopup(
    [
      ['rid', 'hidden', '', '', '', ''],
      ['path', 'text', '', 'Path', '', 'required'],
      ['content', 'translation', '', 'Content', '', ''],
      ['contentfunc', 'multitext', '', 'Content Function', '', ''],
      ['flags', 'bitflags', flags, 'Flags', 
        [
          ["Don't Cache", 'dontcache', 0x01, flags & 0x01 ? 'on' : 'unset']
        ]
      ]
    ],
    function(formdiv, data, resultsdiv, e)
    {
      P.DialogAPI(
        '/system/dbapi',
        {
          command:  'save',
          model:    'system_pagefragment',
          data:     data
        },
        function(d, resultsdiv, popupclass)
        {
          P.ClosePopup(popupclass);
          
          P.HTML(resultsdiv, '<div class="Pad50 greeng">' + T.allgood + '</div>');
          
          G.fragmentinput.value = d.data.rid;
          
          if (e.target.classList.contains('SaveAndContinueButton'))
          {
            P.HTML('.EditFragmentAreaResults', '<div class="Center Pad25 greenb">' + T.allgood + '</div>');
            return;
          }
          
          setTimeout(
            function()
            {
              P.RemoveFullScreen();
            },
            500
          );
        }
      );
    },
    {
      cancelfunc:         function() { P.RemoveFullScreen(); },
      enabletranslation:  1,
      formdiv:            '.EditFragmentArea',
      extrabuttons:       `
        <a class="Color5" onclick="G.FragmentNew()">New</a>
        <a class="Color4" onclick="G.FragmentNone()">None</a>`,
      obj:                fragmentobj,
      saveandcontinue:    1
    }
  );
}

G.FragmentNew = function(start)
{
  G.EditPageFragmentForm({});
}

G.FragmentNone = function(start)
{
  G.fragmentinput.value = '';
  P.RemoveFullScreen();
}

G.ListFragments = function(start)
{
  if (start == undefined)
  {
    start = G.fragmentstart;
  }
  
  G.fragmentstart  = start;
  
  let filter  = E('.FragmentFilter').value;
  
  P.LoadingAPI(
    '.EditFragmentTiles',
    '/system/dbapi',
    {
      command:    'search',
      model:      'system_pagefragment',
      condition:  filter ? `WHERE path LIKE '%${EscapeSQL(filter)}%'` : '',
      start:      start,
      limit:      20,
      orderby:    'path'
    },
    function(d, resultsdiv)
    {
      let data        = d.data;
      
      G.fragmentobjs  = data;
      G.fragmentcount = d.count;
      
      let ls  = [];
      
      for (let i = 0, l = data.length; i < l; i++)
      {
        ls.push(`
          <div class="Color3 Pad50 Raised FragmentTile" data-objid="${data[i].rid}">${data[i].path}</div>
        `);
      }
      
      P.HTML(resultsdiv, ls);
      
      P.HTML('.EditFragmentTilesPageBar', P.PageBar(G.fragmentcount, G.fragmentstart, 20, G.fragmentobjs.length, 'G.ListFragments'));
    }
  );  
}

G.displayfuncs['system_page']  = function(obj, dblist)
{
  let model       = 'system_page';
  let fields      = [G.modelids[model]];
  fields      = fields.concat(P.ToggledButtons('.DBFields')['on']);
  
  let modelid     = G.modelids[model];
  let modelinfo   = G.modelfields[model];  
  
  let ls  = [];
  
  if (dblist.usetable)
  {    
    ls.push(`<th">${obj['rid']}</th>`);
  } else
  {
    ls.push(`<div class="blueg Center">${obj['rid']}</div>`);
  }
    
  for (let j = 1, m = fields.length; j < m; j++)
  {
    let k         = fields[j];    
    let value     = obj[k];
    let valuetype = modelinfo[k][0];
    
    if (dblist.usetable)
    {    
      if (k == 'content' || k == 'title')
      {      
        ls.push('<td>' + StripTags(P.BestTranslation(value)).toString().trim().substr(0, 32) + '</td>');
      } else if (valuetype == 'JSON')
      {
        ls.push('<td><pre>' + JSON.stringify(value, null, 2).toString().trim().substr(0, 32) + '</pre></td>');
      } else
      {    
        ls.push('<td>' + value.toString().trim().substr(0, 32) + '</td>');
      }
    } else
    {
      if (k == 'content' || k == 'title')
      {      
        ls.push('<div>' + StripTags(P.BestTranslation(value)).toString().trim().substr(0, 32) + '</div>');
      } else if (valuetype == 'JSON')
      {
        ls.push('<pre>' + JSON.stringify(value, null, 2).toString().trim().substr(0, 32) + '</pre>');
      } else
      {    
        ls.push('<div>' + value.toString().trim().substr(0, 32) + '</div>');
      }
    }
  }
  
  return ls.join('\\n');
}

G.customeditfuncs['system_page']  = function(obj)
{
  let flagsrow  = E('.DBListEdit .flagsRow');
  
  if (flagsrow)
  {
    let value   = obj.flags ? obj.flags : 0;
    
    P.HTML(
      '.DBListEdit .flagsRow', 
      `<div class="BitFlags flagsFlags" 
          data-name="flags" 
          data-value="${value}">
        </div>`
    );
    
    P.MakeToggleButtons(
      '.DBListEdit .flagsFlags', 
      [
        ["Don't Cache", 'dontcache', 0x01, value & 0x01 ? 'on' : 'unset'],
        ["Load Hooks", 'loadhooks', 0x02, value & 0x02 ? 'on' : 'unset']
      ],
      {
        flags: 	  value,
        ontoggle: function(el, flagname, newstate)
          {
            let flagsel = E('.DBListEdit .flagsFlags');
            
            flagsel.dataset.value = (newstate == 'on')
              ? parseInt(flagsel.dataset.value) | parseInt(el.dataset.value)
              : parseInt(flagsel.dataset.value) & (~parseInt(el.dataset.value));
          }
      }
    );    
  }
  
  P.OnClick(
    '.DBListEdit input[type=number]',
    P.LBUTTON,
    function(e)
    {
      let targetname  = e.target.getAttribute('name');
      
      if (targetname.endsWith('id') && targetname != 'rid')
      {
        P.AddFullScreen(
          'dpurpleg',
          '',
          `<h2 class="Center">Edit Fragment</h2>
          <div class="ButtonBar EditFragmentBar">
            <input type="text" class="FragmentFilter">
            <a class="Color1" onclick="G.ListFragments()">Search</a>
          </div>
          <div class="FlexGrid20 Gap50 EditFragmentTiles"></div>
          <div class="EditFragmentTilesPageBar"></div>
          <div class="EditFragmentArea"></div>`
        );
        
        P.OnClick(
          '.EditFragmentTiles',
          P.LBUTTON,
          function(e)
          {
            let tile  = P.TargetClass(e, '.FragmentTile');
            
            if (!tile)
              return;
            
            G.EditPageFragment(tile.dataset.objid);
          }
        )
        
        G.fragmentinput = e.target;
        
        G.EditPageFragment(e.target.value);
      }
    }
  );
}
    
"""
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def RenderCSS(self, url):
    """Converts a stylesheet URL into the full link tag.
    """
    
    if not url:
      return ''
    
    return f'<link rel="stylesheet" type="text/css" href="{url}" />'
    
  # -------------------------------------------------------------------
  def RenderJS(self, url):
    """Converts a stylesheet URL into the full script tag.
    """
    
    return f'<script src="{url}"></script>'
    
  # -------------------------------------------------------------------
  def Render(self, g):
    """Returns a string containing the full HTML of the page. 
    """
    g.page  = self
    
    if not self.jsfiles:
      self.jsfiles  = []
      
    if not self.cssfiles:
      self.cssfiles = []
      
    if self.translations:
      for t in self.translations:
        try:
          appname, translationname  = t.split('/')
          
          langloaded  = LoadTranslation(g, appname, translationname)
          
          self.jsfiles.insert(0, f'/{appname}/translations/{langloaded}/{translationname}.js')
        except Exception as e:
          print(f'!!!\tProblem loading translation for {self.path}', t, e)
    
    lang  = g.session.lang
    
    topbar    = g.db.Load(system_pagefragment, self.topbarid) if self.topbarid else ''
    bottombar = g.db.Load(system_pagefragment, self.bottombarid) if self.bottombarid else ''
    leftbar   = g.db.Load(system_pagefragment, self.leftbarid) if self.leftbarid else ''
    rightbar  = g.db.Load(system_pagefragment, self.rightbarid) if self.rightbarid else ''
    
    content  = []
    
    if self.headerid:
      content.append(g.db.Load(system_pagefragment, self.headerid).Render(g))
    
    if self.content:
      content.append(BestTranslation(self.content, g.session.langcodes))
    
    if self.contentid:
      content.append(g.db.Load(system_pagefragment, self.contentid).Render(g))
    
    if self.contentfunc:
      try:
        exec(self.contentfunc, globals(), locals())
      except Exception as e:
        content.append(f'<div class="Error">{e}</div>')
        
        if not g.db.flags & DB_PRODUCTION:
          content.append('<pre>' + traceback.format_exc() + '</pre>')
    
    # Add content from the python function if specified
    if self.pyfile:
      try:
        parts = self.pyfile.split('.')
        
        modfile     = '.'.join(parts[0:-1])
        renderfunc  = parts[-1]
        
        mod = importlib.import_module(modfile)
            
        content.append(mod.ROUTES[renderfunc](g))
      except Exception as e:
        content.append(f'<div class="Error">{e}</div>')
        
        if not g.db.flags & DB_PRODUCTION:
          content.append('<pre>' + traceback.format_exc() + '</pre>')
      
    if self.footerid:
      content.append(g.db.Load(system_pagefragment, self.footerid).Render(g))
      
    content = '\n'.join(content)
    
    title     = BestTranslation(self.title, g.session.langcodes)
    
    # Automatically load the corresponding JavaScript file if found
    pagebasename  = g.pagepath.split('.html')[0]
    
    # Strip the language code if found
    try:
      pagebasename  = pagebasename[0:pagebasename.rindex('.')]
    except Exception as e:
      pass
    
    pagejsfile  = 'public/' + pagebasename + '.js'
    pagejs      = ''
    
    if os.path.exists(pagejsfile):
      pagejs  = self.RenderJS(f'''/{pagebasename}.js''')
        
    if g.siteconfig['production']:
      cssfiles  = ['/system/all.min.css'] + self.cssfiles    
    else:
      cssfiles  = g.siteconfig['cssfiles'] + self.cssfiles    
      
    cssfiles  = '\n'.join([self.RenderCSS(x) for x in cssfiles])
    
    if g.siteconfig['production']:
      jsfiles   = ['/system/all.min.js'] + self.jsfiles    
    else:
      jsfiles   = g.siteconfig['jsfiles'] + self.jsfiles
    
    hookjsfiles = []
    
    if self.flags & PAGE_LOAD_HOOKS:
      parts = os.path.split(self.path)
      
      if len(parts) == 2:
        appname, pagename = parts
        
        # Remove leading slash
        appname = appname[1:]
        
        hookfile  = os.path.splitext(pagename)[0] + '.js'
        
        for dirname in os.listdir('public'):
          if dirname == appname:
            continue
          
          if os.path.isdir(os.path.join('public', dirname)):
            hookjs  = os.path.join('public', dirname, hookfile)
            
            if os.path.exists(hookjs):
              hookjsfiles.append('/' + dirname + '/' + hookfile)
              
    hookjsfiles = '\n'.join([self.RenderJS(x) for x in hookjsfiles])
      
    jsfiles   = '\n'.join([self.RenderJS(x) for x in jsfiles])
    
    wallpaper = ''
    
    if self.wallpaper:
      wallpaper  = f"""class="BackgroundCover" style="background-image: url('/system/wallpapers/{self.wallpaper}.webp')" """      
    
    rendered  = f"""<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8">	
  <meta name="apple-mobile-web-app-capable" content="yes" /> 
  <meta name="mobile-web-app-capable" content="yes" /> 
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <link rel="manifest" href="/manifest.json">
	
  <title>{title}</title>
  
  <script>
  // Preload translations
  window.T  = {{}}
  </script>
  
  {self.RenderJS('/system/translations/en/system.js')}

  {cssfiles}
  {jsfiles}
  
  <link rel="stylesheet" type="text/css" href="/system/singlepageapp.css" />
  <link id="ScreenSizeStylesheet" rel="stylesheet" type="text/css" href="/system/blank.css" />
  
  <styles>
  {self.csscode}
  </styles>
  
  <script>
  {self.jscode}
  </script>
</head>
<body {wallpaper}>

<script>

if (!document.addEventListener)
{{
  window.location	=	'/outdated.{lang}.html';
}}

</script>

<noscript>
  <div class="Error FullScreen">This site uses advanced technologies in order to provide a faster and more efficient experience for our visitors. Please enable JavaScript to use this site properly.</div>
</noscript>

<div class="ViewportGrid">
  <div class="TopBarGrid">{topbar.Render(g) if topbar else ''}</div>
  <div class="LeftBarGrid">{leftbar.Render(g) if leftbar else ''}</div>
  <div class="PageBodyGrid">
    {content}	
    <pre class="DebugMessages white"></pre>
  </div>
  <div class="RightBarGrid">{rightbar.Render(g) if rightbar else ''}</div>
  <div class="BottomBarGrid">{bottombar.Render(g) if bottombar else ''}</div>
</div>

{self.RenderJS('/system/translations/' + g.session.lang + '/system.js')}
{self.RenderJS('/system/sessionvars?pageurl=' + EncodeURL(g.pagepath))}
{pagejs}
{hookjsfiles}

</body>
</html>"""
    
    # Return proper encoding in utf-16
    return rendered.encode('utf-16', 'surrogatepass').decode('utf-16')
