#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# This file contains conversion functions that allow the same code to be 
# used in both python 2 and python 3 with minimum modifications. 

import sys
import subprocess
import inspect
from types import SimpleNamespace

PY2 = sys.version_info[0] == 2
PY3 = sys.version_info[0] == 3

# =====================================================================
if PY2:
	def S(v):
		"""Converts the value into a byte string.
		"""
		if not v:
			return str('')
		
		if isinstance(v, unicode):
			return v.encode(sys.getfilesystemencoding())
			
		return str(v)
		
	def U(v):
		"""Converts the value into a unicode string.
		"""
		if not v:
			return unicode('')
		
		if isinstance(v, str):
			return v.decode(sys.getfilesystemencoding())
			
		return unicode(v)
		
	def Print(v, *args):
		"""Print a unicode string with arguments.
		"""
		print(Printf(v, *args))
		
	def PrintB(v, *args):
		"""Print a byte string with arguments.
		"""
		print(S(Printf(v, *args)))
		
	def Printf(v, *args):
		"""Returns a formatted unicode string.
		"""
		if args:
			if isinstance(args, tuple):
				args	=	[U(a) for a in args]
				return U(v).format(*args)
			else:
				return U(v).format(U(args))
				
		return v
		
	def PrintL(v, localdict = None):
		"""Print a string formatted with local variables.
		"""
		
		if not localdict:
			localdict	=	inspect.currentframe().f_back.f_locals
		
		print(PrintLf(v, localdict))
		
	def PrintBL(v, localdict = None):
		"""Print a byte string formatted with local variables.
		"""
		
		if not localdict:
			localdict	=	inspect.currentframe().f_back.f_locals
		
		print(S(PrintLf(v, localdict)))
		
	def PrintLf(s, localdict	=	None):
		"""Returns a string formatted with local variables.
		"""
		if not localdict:
			localdict	=	inspect.currentframe().f_back.f_locals
			
		udict	=	{}
		
		for k, v in localdict.iteritems():
			if isinstance(v, str):
				udict[k]	=	v.decode(sys.getfilesystemencoding())
			else:
				udict[k]	=	v
			
		return U(s).format(**udict)
		
# =====================================================================
elif PY3:
	def S(v):
		"""Converts the value into a byte string.
		"""
		if not v:
			return bytes('', sys.getfilesystemencoding())
		
		if not isinstance(v, bytes):
			return bytes(v, sys.getfilesystemencoding())
			
		return v
		
	def U(v):
		"""Converts the value into a unicode string.
		"""
		if not v:
			return ''
		
		if not isinstance(v, str):
			return str(v, sys.getfilesystemencoding())
		
		return v
		
	def Print(v, *args):
		"""Prints the string with arguments.
		"""
		print(Printf(v, *args))
		
	def Printf(v, *args):
		"""Returns a string with converted arguments.
		"""
		if args:
			if isinstance(args, tuple):
				return v.format(*args)
			else:
				return v.format(args)
				
		return v
		
	def PrintL(v, localdict = None):
		"""Prints a string formatted with local variables.
		"""
		if not localdict:
			localdict	=	inspect.currentframe().f_back.f_locals
			
		print(PrintLf(v, localdict))
		
	def PrintLf(v, localdict = None):
		"""Returns a string formatted with local variables.
		"""
		if not localdict:
			localdict	=	inspect.currentframe().f_back.f_locals
		
		return v.format(**localdict)
		
	def RunCommand(cmdlist):
		"""Runs a command and returns unicode output
		"""
		cmdlist	=	[U(t) for t in cmdlist]
		return U(subprocess.check_output(cmdlist))
		
	def RunDaemon(cmdlist):
		"""Runs a command in the background
		"""
		cmdlist	=	[U(t) for t in cmdlist]
		subprocess.Popen(cmdlist)

# =====================================================================
def V(d, k, default = None):
	"""Utility function to get a value from a dict or object given its 
	name. Will return default if it can't be found.

	The V functions are convenience functions designed to ensure that 
	a value is the correct type before being used for any work. 
	"""
	try:
		return d[k]
	except:
		try:
			return getattr(d, k)
		except:
			pass

	return default

# =====================================================================
def IntV(d, k, min = None, max = None, default = 0):
	"""Utility function to get an int from a dict or object given its 
	name. Will return default if it can't be found.

	The V functions are convenience functions designed to ensure that 
	a value is the correct type before being used for any work. 
	"""
	v	= default

	try:
		v	= int(d[k])
	except:
		try:
			v	= int(getattr(d, k))
		except:
			return default

	if min is not None and v < min:
		v = min

	if max is not None and v > max:
		v = max
		
	return v

# =====================================================================
def FloatV(d, k, min = None, max = None, default = 0.0):
	"""Utility function to get a float from a dict or object given its 
	name. Will return default if it can't be found.

	The V functions are convenience functions designed to ensure that 
	a value is the correct type before being used for any work. 
	"""
	v	= default

	try:
		v	= float(d[k])
	except:
		try:
			v	= float(getattr(d, k))
		except:
			return default

	if min is not None and v < min:
		v = min

	if max is not None and v > max:
		v = max
		
	return v

# =====================================================================
def StrV(d, k, default = ''):
	"""Utility function to get a string from a dict or object given its 
	name. Will return default if it can't be found.

	The V functions are convenience functions designed to ensure that 
	a value is the correct type before being used for any work. 
	"""
	v	= default

	try:
		v	= str(d[k])
	except:
		try:
			v	= str(getattr(d, k))
		except:
			return default

	return v

# =====================================================================
def ArrayV(d, k, default = []):
	"""Utility function to get an array from a dict or object given its 
	name. Will return default if it can't be found.

	The V functions are convenience functions designed to ensure that 
	a value is the correct type before being used for any work. 
	"""
	v	= default

	try:
		v	= d[k]
	except:
		try:
			v	= getattr(d, k)
		except:
			return default

	if not isinstance(v, list) and not isinstance(v, tuple):
		v = []
		
	return v

# =====================================================================
def DictV(d, k, default = {}):
	"""Utility function to get a dict from a dict or object given its 
	name. Will return default if it can't be found.

	The V functions are convenience functions designed to ensure that 
	a value is the correct type before being used for any work. 
	"""
	v	= default

	try:
		v	= d[k]
	except:
		try:
			v	= getattr(d, k)
		except:
			return default

	if not isinstance(v, dict):
		v = {}
		
	return v
