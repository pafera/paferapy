#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system.db import *

from apps.system.validators import *

NEWUSER_WAITING         = 0x01
NEWUSER_DENIED          = 0x02
NEWUSER_VALIDATED_EMAIL = 0x04

# *********************************************************************
class system_newuser(ModelBase):
  """Newly registered users are created here until an administrator
  confirms their information, upon which they are transferred to 
  system_user.
  """
  
  _dbfields     = {
    'rid':            ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'sessionid':      ('INT64', 'NOT NULL',),
    'phonenumber':    ('TEXT', 'NOT NULL', BlankValidator()),
    'place':          ('TEXT', 'NOT NULL', BlankValidator()),
    'password':       ('PASSWORD', 'NOT NULL', BlankValidator()),
    'email':          ('TEXT', "NOT NULL DEFAULT ''",),
    'displayname':    ('TEXT', 'NOT NULL', BlankValidator()),
    'invitecode':     ('TEXT', "NOT NULL DEFAULT ''",),
    'emailcode':      ('TEXT', "NOT NULL DEFAULT ''",),
    'denyreason':     ('TEXT', "NOT NULL DEFAULT ''",),
    'expiredate':     ('TIMESTAMP', "NOT NULL",),
    'extrainfo':      ('DICT', "NOT NULL DEFAULT ''",),
    'flags':          ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = []
  _dbdisplay  = ['place', 'displayname', 'phonenumber']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def HeadShotPath(self):
    '''Returns the filesystem path for the user's headshot, or a 
    blank string if the object has not been initialized.
    '''
    if not self.rid:
      return ''
    
    return 'public/system/newheadshots/' + RIDDir(self.rid, '.webp')
  
  # -------------------------------------------------------------------
  def OnDBDelete(self, db):
    '''Standard handler for deleting an existing object.
    '''
    if not self.rid:
      return
    
    try:
      os.remove(self.HeadShotPath())
    except Exception as e:
      pass
