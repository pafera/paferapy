#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# Main system APIs for querying and controlling the site.
#
# Be aware that you'll need to be an administrator to use most 
# of the APIs.

import json
import os
import traceback
import types
import importlib
import shutil
import base64
import random
import uuid
import hashlib
import time
import csv
import shutil

from base64 import b64encode, b64decode
from pprint import pprint, pformat

from flask import g, Response, send_from_directory

import webauthn

from apps.system.types import *
from apps.system.db import *
from apps.system.newuser import *
from apps.system.user import *
from apps.system.group import *
from apps.system.page import *
from apps.system.session import *
from apps.system.file import *
from apps.system.loginattempt import *
from apps.system.loginlink import *
from apps.system.message import *

# *********************************************************************
# Instead of annoying unreadable curvy letters or barely distinguishable 
# blurry images, we simply run prompts through the JavaScript eval()
# function to check if the environment contains a proper browser.
#
# It's certainly not perfect, but it does give the hackers one 
# additional barrier to get through.
CAPTCHAS  = {
  'activeElement':    {
    'prompt':     'document.activeElement.toString()',
    'validator':  lambda x: 'Element' in x,
  },
  'charset':    {
    'prompt':     'document.charset.toString()',
    'validator':  lambda x: x,
  },
  'outerHeight':    {
    'prompt':     'window.outerHeight.toString()',
    'validator':  lambda x: int(x) > 0,
  },
  'innerWidth':    {
    'prompt':     'window.innerWidth.toString()',
    'validator':  lambda x: int(x) > 0,
  },
  'back':    {
    'prompt':     'window.T.back.toString()',
    'validator':  lambda x: x == g.T.back,
  },
  'location':    {
    'prompt':     'window.location.toString()',
    'validator':  lambda x: 'http' in x,
  },
  'userAgent':    {
    'prompt':     'navigator.userAgent.toString()',
    'validator':  lambda x: 'Mozilla' in x,
  },
}

# =====================================================================
def captchaapi(g):
  """Sets security for database objects. Note that the DB_SECURE flag
  must be set on both the database and the model in order to apply.
  """
  results = {}
  
  try:
    d = json.loads(g.request.get_data())
    
    command = StrV(d, 'command')
    
    if command == 'get':
      captcha = random.choice(list(CAPTCHAS.keys()))
        
      results['data'] = CAPTCHAS[captcha]['prompt']
      
      g.session['captcha']  = captcha
    else:
      results['error']  = f'{g.T.cantunderstand} "{command}"'
  except Exception as e:
    traceback.print_exc()
    results['error']  = str(e)
  
  return results

# =====================================================================
def CheckCaptcha(g, value):
  captcha = g.session.get('captcha')
  
  try:
    if (captcha 
        and captcha in CAPTCHAS
        and CAPTCHAS[captcha]['validator'](value)
      ):
      return
  except Exception as e:
    print(e)
  
  raise Exception(g.T.incorrectcaptcha)

# =====================================================================
def OnLoginSuccessful(g, results, userobj, session):
  """Setups the session upon a successful login.
  """
  session.Set(
    userid    = userobj.rid,
    userflags = userobj.flags,
  )
  session.data.update(userobj.settings)
  
  groups    = []
  groupids  = []
  
  for group in g.db.Linked(userobj, system_group):
    groups.append(group.groupname)
    groupids.append(group.rid)
    
  if userobj.flags & USER_IS_ADMIN:
    groups.append('admins')
    groupids.append(0)
    
  session['groups']   = groups
  session['groupids'] = groupids    
  
  session['message.unread.count'] = len(
    g.db.Linked(
      userobj,
      system_message,
      cond    = 'CAST(flags & ? AS BOOLEAN) AND CAST(flags & ? AS BOOLEAN)',
      params  = [MESSAGE_MAIL, MESSAGE_UNREAD],
      fields  = 'flags'
    )
  )
  
  session.needupdate  = 1
  
  results['homeurl']  = userobj.settings.get('homeurl', '');

# =====================================================================
def sessionvars(g):
  """In order to enable caching for static pages, we place all session
  and user specific information in this special handler. Using this
  technique, we can use nginx to rapidly serve static files while this
  function only has minimal work to do.
  """
  platform  = 'linux'
  
  if 'ANDROID_DATA' in os.environ:
    platform  = 'termux'
    
  r = Response(f"""
P.wallpaper   = '{g.session['wallpaper']}';
P.texttheme   = '{g.session['texttheme']}';
P.userid      = '{g.session.usercode}';
P.userflags   = '{g.session.userflags}';
P.groups      = {g.session['groups'] if g.session.userid else []};
P.lang        = '{g.session.lang}';
P.langcodes   = {g.session.langcodes};
P.languages   = {g.siteconfig['languages']};
P.platform    = '{platform}';

P.unreadmessagecount  = {g.session.get('message.unread.count', 0)};

let chooselanguageicon  = E('.ChooseLanguageIcon');

if (chooselanguageicon)
{{
  chooselanguageicon.innerHTML  = `<img class="Square200" src="/flags/${{P.lang}}.webp">`
}}

let systemusericon  = E('.SystemUserIcon');

if (systemusericon && P.userid)
{{
  systemusericon.innerHTML  = `<img class="Square200" src="/system/headshots/${{P.CodeDir(P.userid)}}.webp">`
}}

let messagecounticon  = E('.MessageCountIcon');

if (messagecounticon && P.userid)
{{
  if (P.unreadmessagecount)
  {{
    messagecounticon.innerHTML  = `<div class="redb Rounded Pad50Horizontal">${{P.unreadmessagecount}}</div>`;
  }} else
  {{
    messagecounticon.innerHTML  = '';
  }}
}}

""")

  r.headers["Cache-Control"]  = "no-cache, no-store, must-revalidate"
  r.headers["Pragma"]         = "no-cache"
  r.headers["Expires"]        = "0"
  r.headers['Cache-Control']  = 'public, max-age=0'  
  r.headers['Content-Type']   = 'text/javascript'

  if g.siteconfig['urltracking']:
    pageurl = g.request.args.get('pageurl')
    
    if pageurl:
      if not isinstance(g.session.urls, list):
        g.session.urls  = []
        
      g.session.urls.append([pageurl, time.time()])
      g.db.Update(g.session)
      g.db.Commit()
    
  return r
  
# =====================================================================
def fileapi(g):
  """Standard API to search through the files on the system and change 
  names or descriptions. 
  """
  results = {}
  
  modeltouse    = system_file
  editorgroup   = ''
  searchfields  = 'rid, filename, extension, descriptions, modified, size, filetype, flags'

  try:
    d = json.loads(g.request.get_data())
    
    command = StrV(d, 'command')
    
    if command == 'search':
      keyword       = StrV(d, 'keyword')
      filename      = StrV(d, 'filename', keyword)
      filetype      = StrV(d, 'filetype')
      modified      = IntV(d, 'modified')
      size          = IntV(d, 'size')
      start         = IntV(d, 'start')
      limit         = IntV(d, 'limit', 1, 1000, 100)
      orderby       = StrV(d, 'orderby', 'filename')
      filefilter    = StrV(d, 'filter')
      fileids       = ArrayV(d, 'fileids')
      showallfiles  = IntV(d, 'showallfiles')
      
      conds   = []
      params  = []
      
      if not showallfiles:
        conds.append('dbowner = ?')
        params.append(g.session.userid)
      
      if fileids:
        conds.append('rid IN (' + ', '.join(['?' for x in fileids]) + ')')
        params.extend([FromCode(x) for x in fileids])
      else:      
        if filename:
          conds.append("(filename LIKE ? OR LOWER(descriptions) LIKE ?)")
          params.append(filename)
          params.append(filename)
        
        if filetype:
          if filetype in system_file.TYPES.keys():
            filetype  = system_file.TYPES[filetype]
            conds.append("filetype = ?")
            params.append(filetype)
          
        if modified:
          conds.append("modified = ?")
          params.append(modified)
          orderby = 'modified'
        
        if size:
          conds.append("size > ?")
          params.append(size)
          orderby = 'size'
      
      conds = "WHERE " + (" AND ").join(conds)
      
      files = g.db.Find(
        modeltouse, 
        conds, 
        params, 
        start   = start, 
        limit   = limit,
        orderby = orderby,
        fields  = searchfields,
      )
        
      ls  = []
      
      for r in files:
        o = r.ToJSON(searchfields)
        o['description']  = BestTranslation(r.descriptions, g.session.langcodes)
        o['idcode']       = ToShortCode(r.rid)
        o['thumbnail']    = r.ThumbnailURL()
        o['url']          = r.URL()
        
        r.DetermineFileType()
        
        ls.append(o)
        
      results['data']   = ls
      results['count']  = len(files)
    elif command == 'load':
      idcode  = StrV(d, 'idcode')
      
      if not idcode:
        raise Exception(g.T.missingparams)
      
      r = g.db.Load(modeltouse, FromCode(idcode)).ToJSON()
      
      o = r.ToJSON('filename, modified, size, flags')
      
      o['modified']     = o['modified'][:19]
      o['description']  = BestTranslation(r.descriptions, g.session.langcodes)
      o['idcode']       = ToShortCode(r.rid)
      o['thumbnail']    = r.ThumbnailURL()
      
      results['data'] = o
    elif command == 'save':
      """The save API is only able to change filename and descriptions. The 
      rest of the statistics are automatically handled on upload.
      """
      if editorgroup:
        g.db.RequireGroup(editorgroup)
        
      newinfo  = DictV(d, 'data')
      
      if not V(newinfo, 'idcode'):
        raise Exception(g.T.cantcreate)
      
      s = g.db.Load(modeltouse, FromCode(newinfo['idcode']))
      
      filename      = StrV(newinfo, 'filename')
      descriptions  = DictV(newinfo, 'descriptions')
      flags         = IntV(newinfo, 'flags')
      
      if filename or descriptions or flags:
        if filename:
          s.Set(filename = filename)
          
        if descriptions:
          s.Set(descriptions = descriptions)
        
        if flags:
          # Changing security between private and others require changing the 
          # filesystem location
          makepublic  = s.flags & FILE_PRIVATE and not flags & FILE_PRIVATE
          makeprivate = not s.flags & FILE_PRIVATE and flags & FILE_PRIVATE
          
          if makepublic or makeprivate:
            publicpath  = s.PublicPath()
            privatepath = s.PrivatePath()
            
            if makepublic:
              os.makedirs(os.path.dirname(publicpath), exist_ok = 1)
              os.rename(privatepath, publicpath)
            else:
              os.makedirs(os.path.dirname(privatepath), exist_ok = 1)
              os.rename(publicpath, privatepath)
        
        g.db.Save(s)
        
        g.db.Commit()
        
      o = s.ToJSON('filename, extension, modified, size, flags')
      
      o['modified']     = o['modified'][:19]
      o['description']  = BestTranslation(s.descriptions, g.session.langcodes)
      o['idcode']       = ToShortCode(s.rid)
      o['thumbnail']    = s.ThumbnailURL()
      
      results['data']   = o
    elif command == 'delete':
      if editorgroup:
        g.db.RequireGroup(editorgroup)
      
      idcodes  = ArrayV(d, 'idcodes')
      
      if not idcodes:
        raise Exception(g.T.missingparams)
      
      userstosave = {}
      
      for r in g.db.LoadMany(modeltouse, [FromCode(x) for x in idcodes]):
        
        if r.dbowner not in userstosave:
          userstosave[r.dbowner]  = g.db.Load(system_user, r.dbowner, fields = 'rid, storageused')
          
        owner = userstosave[r.dbowner]
        
        owner.Set(
          storageused = owner.storageused - r.size
        )
        
        g.db.Delete(r)
        
      g.db.Commit()
    else:
      results['error']  = f'{g.T.cantunderstand} "{command}"'
  except Exception as e:
    traceback.print_exc()
    results['error']  = str(e)
  
  return results

# =====================================================================
def upload(g):
  """Handles uploads for the whole system. Files in Pafera are stored 
  under their shortcodes, and can be private or public. Note that you'll
  need ImageMagick and ffmpegthumbnailer installed on your system for
  automatic thumbnail generation.
  """
  
  results = {}
  
  try:
    LoadTranslation(g, 'system', 'admin')
    
    if len(g.urlargs) < 3:
      raise Exception(g.T.missingparams)
    
    fileid, filename, title = g.urlargs[:3]
    
    security  = 'public'
    
    if len(g.urlargs) > 3:
      security    = g.urlargs[3]
      
    isadmin = IsAdmin()
    
    if len(g.urlargs) > 4:
      dontconvert = int(g.urlargs[4] if g.urlargs[4] != '_' else 0) and CanUploadOriginals()
    else:
      dontconvert = 0
    
    if fileid == '_':
      fileid  = ''
    
    if not CanUpload():
      raise Exception(g.T.cantchange)
        
    bytesleft   = int(g.request.headers.get('content-length'))
    filesize    = bytesleft
    
    # The special headshot fileid is used to set user images
    if fileid == 'headshot':
      userid  = FromCode(filename)
      
      if userid != g.session.userid:
        u = g.db.Load(system_user, userid, fields = 'rid, managedby')
        
        if g.session.usercode not in u.managedby:
          raise Exception(g.T.cantchange)
      
      basename  = 'public/system/headshots/' + CodeDir(filename)
      jpgfile   = basename + '.jpg'
      webpfile  = basename + '.webp'
      
      with open(jpgfile, 'wb') as f:
        while bytesleft > 0:
          chunk = g.request.stream.read(65536)
          f.write(chunk)
          bytesleft -= len(chunk)
          
      subprocess.call(['convert', jpgfile, '-quality', '70', '-resize', '512x512>', webpfile])
                      
      os.remove(jpgfile)
      
      return results
    
    currentuser = g.db.Load(system_user, g.session.userid, fields = 'rid, settings, storageused, storagequota')
    
    if (not isadmin 
        and currentuser.storageused + bytesleft > currentuser.storagequota
      ):
      raise Exception(g.T.storagequotaexceeded)
    
    cachedir      = 'private/system/uploads'
    
    RemoveOlderFiles(cachedir, 86400, 1)
        
    uploadedfile  = cachedir + '/' + uuid.uuid4().hex
    
    hasher  = hashlib.sha1()
    
    with open(uploadedfile, 'wb') as f:
      while bytesleft > 0:
        chunk = g.request.stream.read(65536)
        f.write(chunk)
        hasher.update(chunk)
        bytesleft -= len(chunk)
        
    finalhash = hasher.hexdigest()
    
    # See if the file already exists on the system and is not private.
    # This way, we don't have 100 copies of the same file taking up 
    # space on the filesystem.
    f = g.db.FindOne(
      system_file, 
      'WHERE hash = ? AND size = ? AND NOT CAST(flags & ? AS BOOLEAN)', 
      [finalhash, filesize, FILE_PRIVATE]
    )
    
    if f:
      os.remove(uploadedfile)
    else:
      # If it's a new file, then let the file class process statistics.
      basename, extension = os.path.splitext(filename)
      
      descriptions  = {}
      
      if title != '_':
        descriptions[g.session.lang]  = title
      
      f = system_file()
      
      f.Set(
        filename      = filename,
        descriptions  = descriptions,
      )
      
      if security == 'protected':
        f.Set(flags = FILE_PROTECTED)
      elif security == 'private':
        f.Set(flags = FILE_PRIVATE)
      else:
        f.Set(flags = FILE_PUBLIC)
      
      f.Process(g, uploadedfile, importfile = 1, deleteoriginal = 1)
      
      # Since many users like to take five megapixel photos with bad cameras, we 
      # automatically convert uploaded images, audio files, and videos to webp,
      # aac, and h264 to both save space and bandwidth. If you wish to disable
      # this behavior, please add your users to the uploadoriginals group.
      #
      # Note that converting videos is quite time consuming, so make sure that you
      # have a fast server or set low resolution on your user settings.
      if f.filetype == FILE_IMAGE and f.extension != 'webp':
        origpath      = f.FullPath()
        convertedpath = os.path.splitext(origpath)[0] + '.webp'
        
        if not dontconvert:          
          maxsize       = currentuser.settings.get('maximagesize', '1280x1280')
          
          subprocess.run(['convert', origpath, '-resize', maxsize + '>', convertedpath])
          
          if os.path.exists(convertedpath):
            f.Set(
              size      = os.stat(convertedpath).st_size,
              filename  = os.path.splitext(f.filename)[0] + '.webp',
            )
            
            f.Set(
              hash  = f.GetHash()
            )
            
            os.remove(origpath)
            
        if os.path.exists(origpath):
          os.rename(origpath, convertedpath)
        
        # We use a false extension so that we don't need to access the database
        # in order to find out the real extension of a file
        f.Set(
          extension = 'webp',
        )
        g.db.Save(f)
      elif f.filetype == FILE_AUDIO and f.extension != 'mp3':
        origpath      = f.FullPath()
        convertedpath = os.path.splitext(origpath)[0] + '.mp3'
        
        if not dontconvert:
          maxquality  = currentuser.settings.get('maxaudioquality', '128k')
          
          subprocess.run(['ffmpeg', '-i', origpath, '-ab', maxquality, convertedpath])
          
          if os.path.exists(convertedpath):
            f.Set(
              filename  = os.path.splitext(f.filename)[0] + '.mp3',
              size      = os.stat(convertedpath).st_size,
            )
            
            f.Set(
              hash  = f.GetHash()
            )
            
            os.remove(origpath)
            
        if os.path.exists(origpath):
          os.rename(origpath, convertedpath)
          
        f.Set(
          extension = 'mp3',
        )
        
        g.db.Save(f)          
      elif f.filetype == FILE_VIDEO and f.extension != 'mp4':
        origpath      = f.FullPath()
        convertedpath = os.path.splitext(origpath)[0] + '.mp4'
        
        if not dontconvert:
          videoquality    = currentuser.settings.get('videoquality', '25')
          
          videowidth, videoheight = currentuser.settings.get('videosize', '800x800').split('x')
          
          audioquality    = currentuser.settings.get('audioquality', '128k')
          
          result = subprocess.run(['ffmpeg', '-i', origpath], stdout=subprocess.PIPE).stdout.decode('utf-8')
          
          matches = re.findall('([0-9]+)x([0-9]+)', result, re.MULTILINE)
          
          width   = matches[0][0]
          height  = matches[0][1]
          
          commandline = [
            'ffmpeg', 
            '-i', 
            origpath, 
            '-c:v', 
            'libx264', 
            '-crf', 
            videoquality, 
            '-c:a', 
            'aac', 
            '-strict',
            'experimental',
            '-b:a',
            audioquality,
          ]
          
          if width > videowidth:
            commandline.extend(['-vf', 'scale=' + videowidth + ':-1'])
          elif height > videoheight:
            commandline.extend(['-vf', 'scale=-1:' + videoheight])
            
          commandline.append(convertedpath)
            
          subprocess.run(commandline)
          
          if os.path.exists(convertedpath):
            f.Set(
              filename  = os.path.splitext(f.filename)[0] + '.mp4',
              size      = os.stat(convertedpath).st_size,
            )
            
            os.remove(origpath)
        
        if os.path.exists(origpath):
          os.rename(origpath, convertedpath)
          
        currentuser.Set(
          storageused = currentuser.storageused + filesize
        )
          
        f.Set(
          extension = 'mp4',
        )
        
        g.db.Save(f)
        
      g.db.Commit()
    
    results['id']         = ToShortCode(f.rid)
    results['thumbnail']  = f.ThumbnailURL()
  except Exception as e:
    results['error']  = str(e)
    traceback.print_exc()
  
  return json.dumps(results)

# =====================================================================
def dbimport(g):
  """Permits an administrator to upload database tables as JSON files
  inside of a 7zip archive. p7zip must be installed.
    
  This is handy because Sqlite, MySQL, and PostgreSQL all have slight
  differences in syntax, but good old fashioned JSON is mostly 
  consistent across all versions and platforms, so it's still the best 
  choice for moving data around.
  
  Why not CSV? Pafera uses advanced functionality by including lists,
  dicts, and text with newlines as database fields. CSV does not play 
  well with these, and thus JSON is actually easier to use despite
  the extra space that it takes.
  """  
  results = {}
  
  try:
    g.db.RequireGroup('admins', g.T.cantchange)
    
    baseexportdir = os.path.join('private', 'system', 'dbexport')
    exportdir     = baseexportdir + '/' + uuid.uuid4().hex
    
    os.makedirs(exportdir, exist_ok = 1)
    
    # Clean previous exports older than an hour
    currenttime = time.time()
    
    for r in os.listdir(baseexportdir):
      olddirname  = os.path.join(baseexportdir, r)
      
      if os.path.isdir(olddirname) and os.stat(olddirname).st_mtime + 3600 < currenttime:
        shutil.rmtree(olddirname)
        
    importfile  = os.path.join(exportdir, 'dbimport.7z')
    
    bytesleft = int(g.request.headers.get('content-length'))
    
    with open(importfile, 'wb') as f:
      while bytesleft > 0:
        chunk = g.request.stream.read(65536)
        f.write(chunk)
        bytesleft -= len(chunk)
          
    currentdir  = os.getcwd()
    
    os.chdir(exportdir)
    
    subprocess.run(['7z', 'e', 'dbimport.7z'])
    
    truncatetable  = 'truncatetable' in g.urlargs
    recreatetable  = 'recreatetable' in g.urlargs
    
    importlog = []
    
    numimported = 0
    numerrors   = 0
    
    for filename in os.listdir('.'):
      if filename.endswith('.json'):
        tablename = filename[:-5]
        
        with open(filename, 'r', encoding='utf-8') as inputfile:
          # Only link tables have double underscores
          if '__' in tablename:
            importlog.append(f'''Found link table {tablename}''')
            
            if truncatetable:
              try:
                if g.db.dbtype == 'sqlite':
                  g.db.Execute(f'''DELETE FROM {tablename}''')
                else:
                  g.db.Execute(f'''TRUNCATE {tablename}''')
              except Exception as e:
                importlog.append(f'''Problem truncating link table {tablename}''')
                  
              importlog.append(f'''Truncated link table {tablename}''')
            elif recreatetable:
              try:
                g.db.Execute(f'''DROP TABLE {tablename}''')
              except Exception as e:
                importlog.append(f'''Problem dropping link table {tablename}''')
              
              model1, model2  = tablename.split('__')
              
              model1  = GetModel(model1)
              model2  = GetModel(model2)
              
              g.db.CreateLinkTable(model1, model2)
              importlog.append(f'''Recreated link table {tablename}''')
            
            rowsimported = 0
            
            row = {}
            
            for line in inputfile:
              try:
                row = json.loads(line)
                
                g.db.Execute(
                  f'''INSERT INTO {tablename}(linkaid, linkbid, linktype, linknum, linkcomment, linkflags)
                  VALUES(?, ?, ?, ?, ?, ?)
                  ''',
                  [
                    row['linkaid'],
                    row['linkbid'],
                    row['linktype'],
                    row['linknum'],
                    row['linkcomment'],
                    row['linkflags'],
                  ]
                )
                  
                rowsimported  += 1
                numimported   += 1
              except Exception as e:
                importlog.append(f'''Failed to import row {row}: {e}''')
                numerrors += 1
              
            importlog.append(f'''Imported {rowsimported} links''')
          else:
            importlog.append(f'''Found model {tablename}''')
            
            model = GetModel(tablename)
            
            g.db.RegisterModel(model)
            
            if truncatetable:
              importlog.append(f'''Truncating model {tablename}''')
              
              g.db.Truncate(model)
              
              importlog.append(f'''Truncated table {tablename}''')
            elif recreatetable:
              try:
                g.db.Execute(f'''DROP TABLE {tablename}''')
              except Exception as e:
                importlog.append(f'''Problem dropping table {tablename}: {e}''')
              
              try:
                g.db.Execute('DELETE FROM system_objtype WHERE typename = ?', tablename)
              except Exception as e:
                importlog.append(f'''Problem erasing objtype {tablename}: {e}''')
                
              del g.db.objtypes[tablename]
              
              model._dbinit = 0
              
              g.db.RegisterModel(model)
              
              importlog.append(f'''Recreated model {tablename}''')
            
            rowsimported  = 0
            row           = {}
            
            for line in inputfile:
              try:
                obj = model()
                
                row = json.loads(line)
                
                obj.Set(**row)
                
                try:
                  g.db.Insert(obj)
                except Exception as e:
                  importlog.append(f'''Could not insert row {row}: {e}\nTrying to update''')
                  g.db.Update(obj)
                
                numimported   += 1            
                rowsimported  += 1
              except Exception as e:
                importlog.append(f'''Failed to import row {row}: {e}''')
                numerrors += 1
            
            importlog.append(f'''Imported {rowsimported} rows''')
        
    g.db.Commit()
    
    importlog.append(f'Finished importing {numimported} rows with {numerrors} errors')
        
    results['log']  = '<br>'.join(importlog)
    
    os.chdir(currentdir)
  except Exception as e:
    results['error']  = str(e)
       
  return json.dumps(results)

# =====================================================================
def dbexport(g):
  """Permits an administrator to download database tables as JSON files
  inside of a 7zip archive. p7zip must be installed.
  
  This is handy because Sqlite, MySQL, and PostgreSQL all have slight
  differences in syntax, but good old fashioned JSON is mostly 
  consistent across all versions and platforms, so it's still the best 
  choice for moving data around.
  
  Why not CSV? Pafera uses advanced functionality by including lists,
  dicts, and text with newlines as database fields. CSV does not play 
  well with these, and thus JSON is actually easier to use despite
  the extra space that it takes.
  """  
  results = {}
  
  try:
    g.db.RequireGroup('admins', g.T.cantchange)
    
    models     = g.request.args.get('models')
    starttime  = g.request.args.get('starttime')
    endtime    = g.request.args.get('endtime')
    ids        = g.request.args.get('ids')
    
    if starttime:
      starttime = int(starttime)
      
    if endtime:
      endtime   = int(endtime)
      
    if ids:
      ids = ids.split(',')
      
    if not models or ((starttime and not endtime) or (endtime and not starttime)):
      raise Exception(g.T.missingparams)    
    
    models  = models.split(',')
    
    modelobjs = {x: GetModel(x) for x in models}
    
    baseexportdir = 'private/system/dbexport/'
    exportdir     = baseexportdir + uuid.uuid4().hex
    
    os.makedirs(exportdir, exist_ok = 1)
    
    # Clean previous exports older than an hour
    currenttime = time.time()
    
    for r in os.listdir(baseexportdir):
      olddirname  = os.path.join(baseexportdir, r)
      
      if os.path.isdir(olddirname) and os.stat(olddirname).st_mtime + 3600 < currenttime:
        shutil.rmtree(olddirname)
        
    for modelname, model in modelobjs.items():
      g.db.RegisterModel(model)
      
      ls  = []
      
      conds   = []
      params  = []
      
      if 'lastmodified' in model._dbfields and starttime and endtime:
        conds.append('lastmodified >= ?')
        params.append(starttime)
        
        conds.append('lastmodified <= ?')
        params.append(endtime)
        
      if ids:
        conds.append(f'''{model._dbid} IN ({', '.join(['?' for x in ids])})''')
        params.extend(FromCode(x) for x in ids)
        
      if conds:
        conds = 'WHERE ' + ' AND '.join(conds)
      else:
        conds = ''
        
      objs  = g.db.Find(
        model,
        cond    = conds,
        params  = params,
        limit   = 99999999,
      )
      
      with open(
          exportdir + '/' + modelname + '.json', 
          'w', 
          encoding  = 'utf-8',
        ) as outputfile:
        
        for r in objs:
          outputfile.write(json.dumps(r.ToJSON(exportpassword = 1, autoidcode = 0)))
          outputfile.write("\n")
          
      for linkedmodelname in model._dblinks:
        linkedmodel = GetModel(linkedmodelname)
        
        linktable   = g.db.GetLinkTable(model, linkedmodel)
        
        if g.db.HasTable(linktable):
          with open(
              exportdir + '/' + linktable + '.json', 
              'w',
              encoding  = 'utf-8',
            ) as outputfile:
            for r in g.db.Execute(
                f'SELECT * FROM {linktable}',
                returndict  = 1
              ):
              outputfile.write(json.dumps(r))
              outputfile.write("\n")
      
    currentdir  = os.getcwd()
    
    os.chdir(exportdir)
    
    if len(models) > 3:
      exportfile  = ','.join(models[0:2] + ['andmore']) + '.7z'
    else:
      exportfile  = ','.join(models) + '.7z'
      
    subprocess.run(['7z', 'a', exportfile, '*json'])
    
    os.chdir(currentdir)
    
    return send_from_directory(exportdir, exportfile)    
  except Exception as e:
    return 'Error processing:' + str(e)
       
  return 'Should not get here'

# =====================================================================
def userapi(g):
  """Handles logins, registrations, approvals, and user information.
  Also handles adding friends, following other users, and so forth.
  
  Note that normal users cannot search for other users. You must be 
  an administrator or have managed users assigned to you in order to 
  see and change other users.
  """
  results = {}
  
  modeltouse    = system_user
  editorgroup   = ''
  searchfields  = '*'

  try:
    LoadTranslation(g, 'system', 'admin')
    LoadTranslation(g, 'system', 'registration')
    
    d = json.loads(g.request.get_data())
    
    command = StrV(d, 'command')
    
    if command == 'generateloginlink':     
      g.db.RegisterModel(system_loginlink)
      
      currenttime = time.time()
      
      # Delete all login links older than five minutes
      g.db.Execute(
        '''DELETE FROM system_loginlink
        WHERE modified < ?''',
        currenttime - (5 * 60)
      )
      
      loginlink = system_loginlink()
      
      loginlink.Set(
        sessionid = g.session.rbid,
        modified  = currenttime,
      )
      
      g.db.Insert(loginlink)
      g.db.Commit()
      
      results['linkid'] = ToLongCode(loginlink.rbid)
    elif command == 'checkloginlinkstatus':
      linkid    = StrV(d, 'linkid')
      
      try:
        loginlink = g.db.Load(system_loginlink, FromCode(linkid))
        
        # Login links expire after five minutes
        if loginlink.modified + (5 * 60) < time.time():
          raise Exception('expired')
        
        if loginlink.flags & LOGINLINK_VERIFIED:
          results['status']   = 'verified'
          results['homeurl']  = loginlink.homeurl
        else:
          results['status'] = 'waiting'
      except Exception as e:
        results['status'] = str(e)
    elif command == 'searchpasskeys':
      g.db.RequireGroup('users', g.T.cantview)
      
      user = g.db.Load(
        system_user, 
        g.session.userid,
        fields  = 'rid, passkeys'
      )
      
      results['data'] = [
        {
          'idcode':   k,
          'modified': v['modified'],
          'ip':       v['ip'],
        }
        for k, v in user.passkeys.items()
      ]
    elif command == 'getpasskeyauthentication':
      phonenumber   = StrV(d, 'phonenumber')
      place         = StrV(d, 'place')
      
      if not phonenumber or not place:
        raise Exception(g.T.missingparams)
      
      user = g.db.FindOne(
        system_user, 
        'WHERE phonenumber = ? AND place = ?',
        [phonenumber, place],
        fields  = 'rid, passkeys'
      )
      
      if not user or not user.passkeys:
        raise Exception(g.T.nothingfound)
      
      options = webauthn.generate_authentication_options(
        rp_id             = g.siteconfig['siteurl'],
        allow_credentials = [
          {
            "type":       "public-key", 
            "id":         ToBinary(cred['id']),
            "transports": cred['transports']
          }
          for cred in user.passkeys.values()
        ],
        user_verification = webauthn.helpers.structs.UserVerificationRequirement.REQUIRED,
      )

      g.session['passkey.challenge'] = FromBinary(options.challenge)
      
      results['data'] = webauthn.options_to_json(options)
    elif command == 'verifypasskey':
      phonenumber   = StrV(d, 'phonenumber')
      place         = StrV(d, 'place')
      
      if not phonenumber or not place:
        raise Exception(g.T.missingparams)
      
      user = g.db.FindOne(
        system_user, 
        'WHERE phonenumber = ? AND place = ?',
        [phonenumber, place],
        fields  = 'rid, passkeys, settings, flags',
      )
      
      if not user or not user.passkeys:
        raise Exception(g.T.nothingfound)
      
      info  = DictV(d, 'data')
      
      credential = webauthn.helpers.structs.AuthenticationCredential.parse_raw(json.dumps(info))
      
      currentkeyname  = ''
      publickey       = ''
      signcount       = 0
      
      for k, v in user.passkeys.items():
        credid  = ToBinary(v['id'])
        
        if credid == credential.raw_id:
          currentkeyname  = k
          publickey       = ToBinary(v['publickey'])
          signcount       = v['signcount']
          break
        
      if not publickey:
        raise Exception(g.T.nothingfound)
      
      verification = webauthn.verify_authentication_response(
        credential                    = credential,
        expected_challenge            = ToBinary(g.session['passkey.challenge']),
        expected_rp_id                = g.siteconfig['siteurl'],
        expected_origin               = 'https://' + g.siteconfig['siteurl'],
        credential_public_key         = publickey,
        credential_current_sign_count = signcount,
        require_user_verification     = True,
      )
      
      OnLoginSuccessful(g, results, user, g.session)
      
      # Update credential with new sign count
      user.passkeys[currentkeyname]['signcount']  = verification.new_sign_count
      
      user.Set(passkeys = user.passkeys)
      g.db.Update(user)
      
      g.db.Commit()
    elif command == 'registerpasskey':
      g.db.RequireGroup('users', g.T.cantchange)
      
      u = g.db.Load(system_user, g.session.userid, fields = 'phonenumber, place')
      
      options = webauthn.generate_registration_options(
        rp_name     = g.siteconfig['siteurl'],
        rp_id       = g.siteconfig['siteurl'],
        user_id     = g.session.usercode,
        user_name   = u.phonenumber + '|' + u.place,
        authenticator_selection = webauthn.helpers.structs.AuthenticatorSelectionCriteria(
          user_verification = webauthn.helpers.structs.UserVerificationRequirement.REQUIRED,
        ),
      )
      
      g.session['passkey.challenge'] = FromBinary(options.challenge)
      
      results['data'] = webauthn.options_to_json(options)
      
    elif command == 'verifypasskeyregistration':
      info    = DictV(d, 'data')
      keyname = StrV(d, 'keyname')
      
      if not keyname:
        raise Exception(g.T.missingparams)
      
      credential = webauthn.helpers.structs.RegistrationCredential.parse_raw(json.dumps(info))
      
      verification = webauthn.verify_registration_response(
        credential          = credential,
        expected_challenge  = ToBinary(g.session['passkey.challenge']),
        expected_rp_id      = g.siteconfig['siteurl'],
        expected_origin     = 'https://' + g.siteconfig['siteurl'],
        
        require_user_verification = True,
      )
      
      u = g.db.Load(system_user, g.session.userid, fields = 'rid')
      
      req = g.request
      
      u.passkeys[keyname] = {
        'id':         FromBinary(verification.credential_id),
        'publickey':  FromBinary(verification.credential_public_key),
        'signcount':  verification.sign_count,
        'transports': info.get('transports', []),
        'modified':   int(time.time()),
        'ip':         ClientIP(g),
      }
        
      u.Set(passkeys = u.passkeys)
        
      g.db.Update(u)
      g.db.Commit()
    elif command == 'deletepasskeys':
      g.db.RequireGroup('users', g.T.cantview)
      
      ids = ArrayV(d, 'ids')
      
      user = g.db.Load(
        system_user, 
        g.session.userid,
        fields  = 'rid, passkeys'
      )
      
      needupdate  = 1
      
      for keyname in ids:
        if keyname in user.passkeys:
          del user.passkeys[keyname]
          needupdate  = 1
          
      if needupdate:
        user.Set(passkeys = user.passkeys)
        
        g.db.Update(user)
        g.db.Commit()
    elif command == 'login':
      phonenumber = StrV(d, 'phonenumber').strip()
      place       = StrV(d, 'place').strip()
      password    = StrV(d, 'password').strip()
      
      if not phonenumber or not place or not password:
        raise Exception(g.T.missingparams)
      
      # Limit login attempts to once every two seconds per IP. It's a
      # crude but effective way to counter login spamming.
      previousattempt = g.db.Find(
        system_loginattempt,
        'WHERE ip = ?',
        [g.session.ip],
        orderby =  'eventtime DESC',
      )
      
      currenttime = time.time()
      
      if previousattempt and previousattempt[0].eventtime > currenttime - 2:
        time.sleep(2)
        raise Exception(g.T.nothingfound)
      
      loginattempt = system_loginattempt()
      loginattempt.Set(
        ip          = g.session.ip,
        eventtime   = currenttime,
      )
      g.db.Insert(loginattempt)
      g.db.Commit()
      
      userobj = g.db.FindOne(
        modeltouse, 
        'WHERE phonenumber = ? AND place = ?',
        [phonenumber, place],
        fields  = 'rid, flags, password, settings'
      )
      
      if userobj:
        if userobj.CheckPassword('password', password):
          OnLoginSuccessful(g, results, userobj, g.session)
        else:
          time.sleep(2)
          raise Exception(g.T.nothingfound)      
      else:
        time.sleep(2)
        raise Exception(g.T.nothingfound)      
    elif command == 'register':
      phonenumber   = StrV(d, 'phonenumber').strip()
      place         = StrV(d, 'place').strip()
      password      = StrV(d, 'password').strip()
      password2     = StrV(d, 'password2').strip()
      displayname   = StrV(d, 'displayname').strip()
      invitecode    = StrV(d, 'invitecode').strip()
      email         = StrV(d, 'email').strip()
      role          = IntV(d, 'role')
      headshot      = StrV(d, 'headshot').strip()
      comments      = StrV(d, 'comments').strip()
      captchavalue  = StrV(d, 'captchavalue').strip()
      
      previousregistration  = g.session.get('login.registration')
      
      if previousregistration:
        previoususer  = g.db.Load(system_newuser, FromCode(previousregistration['id']))
      else:
        previoususer  = 0
        
        if (not phonenumber 
            or not place 
            or not password 
            or not password2 
            or not headshot 
            or len(headshot) < 60 
            or not displayname 
            or not email
          ):
          raise Exception(g.T.missingparams)
      
      CheckCaptcha(g, captchavalue)
      
      currenttime = time.time()
      
      # New users have one week to be approved, else they will be deleted. 
      needcommit  = 0
      
      for r in g.db.Find(system_newuser, 'WHERE expiredate < ?', currenttime):
        g.db.Delete(r)
        needcommit  = 1
        
      if needcommit:
        g.db.Commit()
      
      if not previoususer:
        previoususer = g.db.FindOne(
          system_newuser, 
          'WHERE phonenumber = ? AND place = ?',
          [phonenumber, place]
        )
      
      # Attempting to register the same phone number with a different password
      # will be denied
      if previoususer and password and not previoususer.CheckPassword('password', password):
        raise Exception(g.T.phonenumberalreadytaken)
      
      imgdata = b64decodepad(headshot[23:])
      
      if not imgdata:
        raise Exception(g.T.wrongfileformat)
      
      if not previoususer:
        previoususer = system_newuser()
      
      roles = {}
      
      roles[1]  = g.T.student
      roles[2]  = g.T.teacher
      roles[3]  = g.T.school
      roles[4]  = g.T.business
      
      if not previousregistration:
        previoususer.Set(
          sessionid     = g.session.rbid,
          phonenumber   = phonenumber,
          place         = place
        )
        previoususer.SetPassword('password', password)
        
      previoususer.Set(
        email         = email,
        emailcode     = uuid.uuid4().hex,
        displayname   = displayname,
        invitecode    = invitecode,
        expiredate    = currenttime + (86400 * 7),
        extrainfo     = {
          'role':       roles[role] if role else '',
          'rolenum':    role,
          'comments':   comments,
        },
        flags         = NEWUSER_WAITING,
      )      
      
      g.db.Save(previoususer)
      
      idcode  = ToShortCode(previoususer.rid)
      
      headshotdir = 'public/system/newheadshots/' + idcode[:3]
      
      os.makedirs(headshotdir, exist_ok = True)
      
      basefile  = headshotdir + '/' + idcode[3:]
      jpgfile   = basefile + '.jpg'
      webpfile  = basefile + '.webp'
      
      with open(jpgfile, 'wb') as f:
        f.write(imgdata)          
        
      subprocess.call(['convert', jpgfile, '-quality', '70', '-resize', '512x512>', webpfile])
                      
      os.remove(jpgfile)
      
      results['registrationid'] = idcode
      
      g.session['login.registration'] = {
        'id':         results['registrationid'],
        'approved':   'waiting',
      }
      
      g.db.Commit()
      
      if (g.siteconfig.get('adminemail', 0) 
          and g.siteconfig.get('siteurl', 0)
        ):
        subprocess.run([
            'mail', 
            '-r', g.siteconfig['adminemail'], 
            '-s', g.T.verifyemail, 
            '<<<', g.T.validateemailmessage.format(
              siteurl   = g.siteconfig['siteurl'],
              emailcode = previoususer.emailcode,
            )
          ],
          shell = True,
        )
    elif command == 'deleteregistration':
      idcodes    = ArrayV(d, 'idcodes')
      
      if not idcodes:
        raise Exception(g.T.missingparams)
      
      isadmin = IsAdmin()
      
      for r in idcodes:
        u = g.db.Load(system_newuser, FromCode(r), 'rid, sessionid, invitecode')
        
        if not isadmin and g.session.usercode != u.invitecode:
          raise Exception(g.T.cantdelete)
        
        usersession = g.db.Load(system_session, u.sessionid, 'rid, data')
        
        del usersession['login.registration']
        
        g.db.Update(usersession)
        
        g.db.Delete(u) 
      
      g.db.Commit()
    elif command == 'listneedapprovals':
      isadmin = g.db.HasGroup('admins')
      
      start     = IntV(d, 'start', 0, 99999999, 0)
      limit     = IntV(d, 'limit', 10, 1000, 100)
      keyword   = StrV(d, 'keyword')
      
      conds   = []
      params  = []
      
      if not isadmin:
        conds.append('invitecode = ?')
        params.append(g.session.usercode)
        
      if keyword:
        conds.append('(phonenumber LIKE ? OR place LIKE ? OR email LIKE ? OR role LIKE ? OR comments LIKE ?)')
        
        for r in range(0, 5):
          params.append(keyword)
      
      conds = 'WHERE ' + ' AND '.join(conds) if conds else ''
      
      users = g.db.Find(
        system_newuser,
        conds     = conds,
        params    = params,
        start     = start,
        limit     = limit,
        orderby   = 'place, phonenumber'
      )
      
      results['data']   = [x.ToJSON() for x in users]
      results['count']  = len(users)
    elif command == 'approve':
      newusercode = StrV(d, 'userid')
      approve     = IntV(d, 'approve')
      reason      = StrV(d, 'reason')
      
      userid    = FromCode(newusercode)
      
      newuser = g.db.Load(system_newuser, userid)
      
      isadmin         = g.db.HasGroup('admins')
      
      if not isadmin and newuser.invitecode != g.session.usercode:
        raise Exception(g.T.cantchange)            
      
      usersession = g.db.Load(system_session, newuser.sessionid, 'rbid, data')
      
      if approve:
        user  = system_user()
        user.Set(**newuser.ToJSON(exportpassword = 1))
        user.Set(rid = 0)
        
        user.Set(
          managedby = [u.invitecode] if newuser.invitecode else g.session.usercode
        )
          
        g.db.Insert(user)
        
        usercode = ToShortCode(user.rid)
        
        newuserimage  = 'public/system/newheadshots/' + CodeDir(newusercode) + '.webp'
        userimage     = 'public/system/headshots/' + CodeDir(usercode) + '.webp'
        
        os.makedirs(os.path.split(userimage)[0], exist_ok = True)
        
        try:
          os.rename(newuserimage, userimage)
        except Exception as e:
          print(e)
        
        usersession['login.registration']['approved'] = 'approved'
        
        # Fix for using an admin account on the same session ID as a 
        # user being added, resulting in the admin user being replaced
        # with the added user
        if usersession.rbid != g.session.rbid:
          usersession.Set(
            userid  = user.rid,
          )
        
        usergroup = g.db.FindOne(system_group, 'WHERE groupname = ?', 'users')
        
        g.db.Link(user, usergroup)
        
        newgroups   = ['users']
        newgroupids = [usergroup.rid]
        
        rolenum  = newuser.extrainfo['rolenum']
        
        extragroup  = ''
        
        if rolenum == 1:
          extragroup  = 'students'
        elif rolenum == 2:
          extragroup  = 'teachers'
        elif rolenum == 3:
          extragroup  = 'school.administrators'
        elif rolenum == 4:
          extragroup  = 'businesses'
        
        if extragroup:
          usergroup = g.db.FindOne(system_group, 'WHERE groupname = ?', extragroup)
          
          g.db.Link(user, usergroup)
          
          newgroups.append(extragroup)
          newgroupids.append(usergroup.rid)
        
        # Fix for using an admin account on the same session ID as a 
        # user being added, resulting in the admin user being replaced
        # with the added user
        if usersession.rbid != g.session.rbid:
          usersession['groups']   = newgroups
          usersession['groupids'] = newgroupids
        
        if newuser.invitecode:
          manageuser  = g.db.Load(system_user, FromCode(u.invitecode), 'rid, canmanage')
          
          admins  = g.db.FindOne(system_group, 'WHERE groupname = ?', 'admins', fields = 'rid')
          
          # A user can manage another user only if it's an administrator or numcanmanage is available
          if (manageuser.numcanmanage < len(manageuser.canmanage) 
              or g.db.HasLink(system_user, manageuser.rid, system_group, admins.rid)
            ):
            manageuser.canmanage.append(newuser.invitecode)
            manageuser.UpdateFields('canmanage')
            g.db.Update(manageuser)
        else:
          manageuser  = g.db.Load(system_user, g.session.userid, 'rid, canmanage')
          manageuser.canmanage.append(usercode)
          manageuser.UpdateFields('canmanage')
          g.db.Update(manageuser)
          
        g.db.Delete(newuser)
      else:
        usersession['login.registration']['approved'] = 'denied'
        usersession['login.registration']['reason']   = reason
        
        newuser.Set(
          flags       = NEWUSER_DENIED,
          denyreason  = reason,
        )
        
        g.db.Update(newuser)
      
      usersession.UpdateFields('data')
      g.db.Update(usersession)
      
      g.db.Commit()
    elif command == 'checkapproval':
      results['data'] = g.session.get('login.registration', {})
    elif command == 'search':
      keyword   = StrV(d, 'keyword')
      start     = IntV(d, 'start', 0, 2147483647, 0)
      limit     = IntV(d, 'limit', 1, 2147483647, 100)
      
      conds   = []
      params  = []
      
      if keyword:
        conds.append('phonenumber LIKE ?')
        params.append(keyword)
        
        conds.append('displayname LIKE ?')
        params.append(keyword)
      
      if conds:
        conds = ' OR '.join(conds)
      else:
        conds = ''
        
      currentuser = g.db.Load(modeltouse, g.session.userid, 'rid, canmanage, managedby')
        
      userids   = ArrayV(d, 'userids')
      
      if userids:
        validuserids  = []
        
        for r in userids:
          if r in currentuser.canmanage or r in currentuser.managedby:
            validuserids.append(r)
            
        if not validuserids:
          return results
        
        cond    = 'WHERE rid IN (' + (', '.join(['?' for x in validuserids])) + ') '
        
        if conds:
          cond  += ' AND (' + conds + ')'
        
        params  = [FromCode(x) for x in validuserids] + params
        
        validusers  = g.db.Find(
          system_user,
          cond      = cond,
          params    = params,
          start     = start,
          limit     = limit,
          orderby   = 'phonenumber',
          fields    = searchfields
        )
        
        results['data'] = [
          x.ToJSON(searchfields)
          for x in validusers
        ]
        results['count']  = len(validusers)
        return results        
      
      allusers  = []
        
      if IsAdmin():
        allusers  = g.db.Find(
          system_user,
          cond      = 'WHERE ' + conds if conds else '',
          params    = params,
          start     = start,
          limit     = limit,
          orderby   = 'phonenumber',
        )
        
      results['allusers'] = [
        x.ToJSON('rid, phonenumber, place, displayname')
        for x in allusers
      ]
      results['alluserscount']  = len(allusers)
        
      if currentuser.canmanage:
        cond    = 'WHERE rid IN (' + (', '.join(['?' for x in currentuser.canmanage])) + ') '
        
        if conds:
          cond  += ' AND (' + conds + ')'
        
        params  = [FromCode(x) for x in currentuser.canmanage] + params
      
        managedusers  = g.db.Find(
          system_user,
          cond    = cond,
          params  = params,
          start   = start,
          limit   = limit,
          orderby = 'phonenumber',
          fields  = 'rid, phonenumber, place, displayname, expiredate, storagequota, numcanmanage, canmanage, managedby, flags',
        )
      else:
        managedusers  = []
          
      results['data'] = [
        x.ToJSON('rid, phonenumber, place, displayname, expiredate, storagequota, numcanmanage, canmanage, managedby, flags')
        for x in managedusers
      ]
      results['count']  = len(managedusers)
    elif command == 'save':
      data        = DictV(d, 'data')
      useridcode  = StrV(data, 'idcode')
      
      isnewuser   = not useridcode
      
      userid      = FromCode(useridcode) if useridcode else 0
      
      isadmin   = IsAdmin()
      
      if not isadmin and not userid:
        raise Exception(g.T.cantcreate)
      
      if userid:
        u = g.db.Load(system_user, userid, 'rid, numcanmanage, canmanage, managedby, flags')
      else:
        u = system_user()
      
      canmanage = isadmin or g.session.usercode in u.managedby
      
      if userid != g.session.userid and not canmanage:
        raise Exception(g.T.cantchange)
      
      newvalues = {}
      
      # If the user is not allowed to manage itself, then only the password
      # can be changed
      if not isadmin and userid == g.session.userid and not u.flags & USER_CAN_MANAGE_SELF:
        newpassword = StrV(data, 'password')
        
        if not newpassword:
          return results
        
        newvalues = {
          'password': newpassword
        }
      else:
        for r in [
            'phonenumber',
            'place',
            'email',
            'password',            
          ]:
          
          value = StrV(data, r)
          
          if value:
            newvalues[r]  = value
      
      if isadmin:
        displayname = DictV(data, 'displayname')
        
        if displayname:
          newvalues['displayname'] = displayname
          
        expiredate  = StrV(data, 'expiredate')
        
        if expiredate:
          newvalues['expiredate'] = expiredate
          
        homeurl     = StrV(data, 'homeurl')
        
        if homeurl:
          newvalues['homeurl'] = homeurl
          
        storagequota  = IntV(data, 'storagequota')
          
        if storagequota:
          newvalues['storagequota'] = storagequota
        
        numcanmanage  = IntV(data, 'numcanmanage')
        
        if numcanmanage:
          newvalues['numcanmanage'] = numcanmanage
          
        if 'flags' in data:
          newvalues['flags'] = IntV(data, 'flags')
          
        canmanagetoadd    = []
        canmanagetodelete = []
        
        managedbytoadd    = []
        managedbytodelete = []
        
        canmanage  = ArrayV(data, 'canmanage')
        
        if 'canmanage' in data:
          oldcanmanage  = set(u.canmanage)
          newcanmanage  = set(canmanage)
          
          newvalues['canmanage'] = list(newcanmanage)
          
          canmanagetoadd      = list(newcanmanage - oldcanmanage)
          canmanagetodelete   = list(oldcanmanage - newcanmanage)
          
          if not isadmin and len(canmanage) > u.numcanmanage:
            raise Exception(g.T.reachedmanagelimit)
          
        managedby  = ArrayV(data, 'managedby')
        
        # A user is automatically managed by the administrator that added it
        if not userid:
          managedby.append(g.session.usercode)
        
        if 'managedby' in data or managedby:
          newvalues['managedby'] = managedby
          
          oldmanagedby  = set(u.managedby)
          newmanagedby  = set(managedby)
        
          managedbytoadd      = list(newmanagedby - oldmanagedby)
          managedbytodelete   = list(oldmanagedby - newmanagedby)
          
        userstoedit = canmanagetoadd + canmanagetodelete + managedbytoadd + managedbytodelete
        
        if userstoedit:
          for r in g.db.Find(
              system_user,
              'WHERE rid IN (' + ', '.join(['?' for x in userstoedit]) + ')',
              [FromCode(x) for x in userstoedit],
              fields  = 'rid, canmanage, managedby'
            ):
            
            thisuserid  = ToShortCode(r.rid)
            
            if thisuserid in canmanagetoadd:
              r.managedby.append(useridcode)
              r.UpdateFields('managedby')
              
            if thisuserid in canmanagetodelete and useridcode in r.managedby:
              r.managedby.remove(useridcode)
              r.UpdateFields('managedby')
            
            if thisuserid in managedbytoadd:
              r.canmanage.append(useridcode)
              r.UpdateFields('canmanage')
              
            if thisuserid in managedbytodelete and useridcode in r.canmanage:
              r.canmanage.remove(useridcode)
              r.UpdateFields('canmanage')
              
            g.db.Update(r)
            
      u.Set(**newvalues)
            
      if 'password' in data:
        u.SetPassword('password', data['password'])
      
      g.db.Save(u) 
      
      newuseridcode = ToShortCode(u.rid)
      
      if isnewuser:
        adminuser = g.db.Load(system_user, g.session.userid, fields = 'rid, numcanmanage, canmanage')
        
        if not isadmin and adminuser.numcanmanage + 1 > len(adminuser.canmanage):
          g.db.Rollback()
          raise Exception(g.T.reachedmanagelimit)
        
        adminuser.canmanage.append(newuseridcode)
        adminuser.UpdateFields('canmanage')
        g.db.Update(adminuser)
      
      g.db.Commit()
      
      results['idcode'] = newuseridcode
    elif command == 'delete':
      idcodes    = ArrayV(d, 'idcodes')
      
      if not idcodes:
        raise Exception(g.T.missingparams)
      
      isadmin = IsAdmin()
      
      currentuser       = g.db.Load(system_user, g.session.userid, 'rid, canmanage')
      
      for r in idcodes:
        userid  = FromCode(r)
        
        u = g.db.Load(system_user, userid, 'rid, canmanage, managedby')
        
        if not isadmin and g.session.usercode not in u.managedby:
          raise Exception(g.T.cantdelete)
        
        currentuser.canmanage.remove(r)
        
        g.db.Delete(u) 
      
      currentuser.UpdateFields('canmanage')
      
      g.db.Update(currentuser)
      
      g.db.Commit()
    elif command == 'load':
      userid    = StrV(d, 'userid')
      
      if userid:
        userid  = FromCode(userid)
      else:
        userid  = g.session.userid
      
      u = g.db.Load(system_user, g.session.userid, 'rid, phonenumber, place, displayname, storageused, storagequota, numcanmanage, canmanage, managedby, accesstokens, flags')
      
      canmanage = IsAdmin() or g.session.usercode in u.managedby
      
      if userid != g.session.userid and not canmanage:
        raise Exception(g.T.cantchange)
      
      results['data'] = u.ToJSON('rid, phonenumber, place, displayname, storageused, storagequota, numcanmanage, canmanage, managedby, accesstokens, flags')
    elif command == 'setlanguage':
      newlang = StrV(d, 'data')
      
      if newlang not in g.siteconfig['languages']:
        raise Exception(newlang + ' is not supported.')
      
      newlangcodes  = [newlang]
      
      [newlangcodes.append(x) for x in g.session.langcodes if x not in newlangcodes]
      
      g.session.Set(
        lang        = newlang,
        langcodes   = newlangcodes,
      )
      
      g.db.Update(g.session)
      g.db.Commit()
    elif command == 'searchusergroups':
      """usergroups is a system-provided method of messaging other users. Every user
      in the system has four lists containing links to other users. This also provides
      a really basic method of access control to a particular user's posts.
      
      acquaintances:  everyone that you've ever messaged
      friends:        users which have accepted your friend requests
      favorites:      users which can bypass normal security checks to see every post
      blacklist:      users which can never message this user or see any posts
      """
      usertofind  = StrV(d, 'usertofind')
      keyword     = StrV(d, 'keyword')
      grouptype   = StrV(d, 'grouptype', 'friends')
      start       = IntV(d, 'start')
      limit       = IntV(d, 'limit', 10, 99999999, 100)
      
      cond    = []
      params  = []
      
      if grouptype not in system_user.USERGROUPS:
        raise Exception(g.T.missingparams)
      
      currentuser = g.db.Load(system_user, g.session.userid, fields = 'rid, ' + grouptype)
      
      userids = getattr(currentuser, grouptype, [])
      
      if not userids:
        results['data']   = []
        results['count']  = 0
      else:   
        params  = [FromCode(x) for x in userids]
        
        if keyword:
          conds.append('displayname LIKE ?')
          params.append('%' + keyword + '%')
          
        cond  = 'WHERE rid IN (' + ', '.join(['?' for x in userids]) + ') ' + ' AND '.join(cond)
        
        users = g.db.Find(
          system_user, 
          cond, 
          params, 
          fields = 'rid, displayname, place'
        )
        
        objs  = []
        
        for r in users:
          objs.append({
            'idcode':       ToShortCode(r.rid),
            'displayname':  BestTranslation(r.displayname, g.session.lang),
          })
          
        results['data']   = objs      
        results['count']  = len(users)
    elif command == 'changeusergroups':
      group       = StrV(d, 'group', 'friends')
      action      = StrV(d, 'action', 'add')
      userids     = ArrayV(d, 'userids')
      
      if not userids:
        raise Exception(g.T.missingparams)
      
      usergroups  = system_user.USERGROUPS
      
      if (group not in usergroups
          or action not in [
            'add',
            'delete',
          ]
        ):
        raise Exception(g.T.missingparams)
      
      currentuser     = g.db.Load(
        system_user, 
        g.session.userid, 
        fields = 'rid, ' + ', '.join(usergroups),
      )
      
      for groupname in usergroups:
        existinguserids = getattr(currentuser, groupname, [])
        
        needupdate  = 0
        
        if action == 'add':
          if groupname == group:
            existinguserids.extend(userids)
            needupdate  = 1
          else:
            for userid in userids:
              if userid in existinguserids:
                existinguserids.remove(userid)
                needupdate  = 1
        else:
          for userid in userids:
            if userid in existinguserids:
              existinguserids.remove(userid)
              needupdate  = 1
        
        if needupdate:
          kwargs  = {}
          kwargs[group] = list(set(existinguserids))
          
          currentuser.Set(**kwargs)
      
      g.db.Update(currentuser)
      g.db.Commit()        
    elif command == 'choosegroups':
      keyword     = StrV(d, 'keyword')
      userid      = StrV(d, 'userid')
      start       = IntV(d, 'start', 0, 99999999, 0)
      limit       = IntV(d, 'limit', 10, 99999999, 100)
      
      if not userid:
        raise Exception(g.T.missingparams)
      
      g.db.RequireGroup('admins', g.T.cantchange)
      
      user  = g.db.Load(system_user, FromCode(userid))
      
      cond    = []
      params  = []
      
      if keyword:
        conds.append('(groupname LIKE ? OR displayname LIKE ?)')
        params.append(keyword)
        params.append(keyword)
        
      if cond:
        cond  = 'WHERE ' + ' AND '.join(conds)
      else:
        cond  = ''
      
      existinggroups  = [x.ToJSON('rid, groupname, displayname') for x in g.db.Linked(user, system_group)]
      existingroupids = [x.idcode for x in existinggroups]
      
      allgroups = g.db.Find(
        system_group,
        cond    = cond,
        params  = params,
        start   = start,
        limit   = limit,
      )
       
      newgroups = []
      
      for r in allgroups:
        idcode  = ToShortCode(r.rid)
        
        if idcode not in existingroupids:
          newgroups.append(r.ToJSON('rid, groupname, displayname'))
          
      results['data']   = newgroups
      results['count']  = len(allgroups)  
      
      results['chosen']       = existinggroups
      results['chosencount']  = len(existinggroups)
    elif command == 'setgroups':
      userid    = StrV(d, 'userid')
      groupids  = ArrayV(d, 'groupids')
      
      g.db.RequireGroup('admins', g.T.cantchange)
      
      if not userid:
        raise Exception(g.T.missingparams)
      
      user  = g.db.Load(system_user, FromCode(userid), fields = 'rid')
      
      if groupids:
        groups  = g.db.LoadMany(system_group, [FromCode(x) for x in groupids], fields = 'rid')
        
        g.db.LinkArray(user, groups)
      else:
        g.db.Unlink(user, system_group)
      
      g.db.Commit()
    elif command == 'sendemail' or command == 'sendsms':
      email         = StrV(d, 'email')
      phonenumber   = StrV(d, 'phonenumber')
      captchavalue  = StrV(d, 'captcha')
      
      CheckCaptcha(g, captchavalue)
    else:
      results['error']  = f'{g.T.cantunderstand} "{command}"'
  except Exception as e:
    traceback.print_exc()
    results['error']  = str(e)
  
  return results
            
# =====================================================================
def controlpanelapi(g):
  """Configures settings for the whole site. You must be an 
  administrator in order to see and change settings. 
  """
  results = {}
  
  try:
    if 'admins' not in g.session['groups']:
      raise Exception(g.T.cantchange)
      
    d = json.loads(g.request.get_data())
  
    command = StrV(d, 'command')
    
    if command == 'getinfo':
      with open('private/system/pafera.cfg', 'r') as f:
        results['data'] = json.load(f)
    elif command == 'setinfo':
      oldinfo = {}
      newinfo = DictV(d, 'data')
      
      if not V(newinfo, 'dbflags'):
        newinfo['dbflags']  = 0
      
      with open('private/system/pafera.cfg', 'r') as f:
        oldinfo = json.load(f)
        
      try:
        newdb  = DB(
          connectionname  = 'newdb',
          dbhost          = newinfo['dbhost'],
          dbname          = newinfo['dbname'], 
          dbtype          = newinfo['dbtype'],
          dbuser          = newinfo['dbuser'],
          dbpassword      = newinfo['dbpassword'],
          dbflags         = newinfo['dbflags'],
        )
        
        import apps.system.initsite
        
        newg  = types.SimpleNamespace()
        newg.db = newdb
        newg.T  = g.T
        apps.system.initsite.InitDB(newg)
      except Exception as e:
        traceback.print_exc()
        raise Exception(g.T.cantconnect + newinfo['dbname'])
      
      admin = g.db.FindOne(
        system_user, 
        'WHERE phonenumber = ? AND place = ?',
        ['admin', 'pafera']
      )
      
      sitepassword  = StrV(newinfo, 'sitepassword')
      
      if not sitepassword and admin.CheckPassword('password', g.siteconfig['adminpassword']):
        print('password', g.siteconfig['adminpassword'])
        raise Exception(g.T.pleasesetnewpassword)
      else:
        admin.SetPassword('password', sitepassword)
        
        g.db.Update(admin)
        g.db.Commit()
        
        newinfo.pop('sitepassword')
        oldinfo['adminpassword']  = ''
        
      oldinfo.update(newinfo)
      
      newinfo['sessiontimeout']  = IntV(newinfo, 'sessiontimeout', 600, 2678400, 604800)
        
      with open('private/system/pafera.cfg', 'w') as f:
        f.write(json.dumps(oldinfo, sort_keys = True, indent = 2))
        
      if newinfo['production']:
        subprocess.run(['scripts/minifyall'])
    else:
      results['error']  = f'{g.T.cantunderstand} "{command}"'
  except Exception as e:
    results['error']  = str(e)
  
  return results
            
# =====================================================================
def dbapi(g):
  """Used by the web database management interface to handle objects
  in the system. You must be an administrator to use this API.
  """
  results = {}
  
  try:
    g.db.RequireGroup('admins', g.T.cantview)
    
    d = json.loads(g.request.get_data())
  
    command = StrV(d, 'command')
    
    if command == 'getmodels':
      results['data'] = sorted(list(g.db.objtypes.keys()))
    elif command == 'getmodelinfo':
      model = GetModel(StrV(d, 'model'))
      
      if not model:
        raise Exception(g.T.missingparams)
      
      g.db.RegisterModel(model)
      
      data  = {}
      
      # No need to specify validators
      for k, v in model._dbfields.items():
        data[k] = v[:2]
      
      results['data']           = data
      results['modelid']        = model._dbid
      results['managejs']       = getattr(model, '_managejs', '')
      results['links']          = getattr(model, '_dblinks', [])
      results['displayfields']  = getattr(model, '_dbdisplay', model._dbid)
    elif command == 'search':
      model   = GetModel(StrV(d, 'model'))
      fields  = ArrayV(d, 'fields')
      cond    = StrV(d, 'condition')
      params  = ArrayV(d, 'params')
      start   = IntV(d, 'start', 0, 99999999, 0)
      limit   = IntV(d, 'limit', 10, 99999999, 100)
      orderby = StrV(d, 'orderby')
      
      if not model:
        raise Exception(g.T.missingparams)
      
      if not fields:
        fields  = []
        
      g.db.RegisterModel(model)
      
      # Keyword search
      if fields and cond and ' ' not in cond:
        conds   = []
        params  = []
        
        condisnum = cond.isnumeric()
        
        if condisnum:
          condnum  = float(cond) if '.' in cond else int(cond)
        
        for fieldname in fields:
          fieldtype = model._dbfields[fieldname][0]
          
          if ('INT' in fieldtype or 'FLOAT' in fieldtype):
            if condisnum:
              conds.append(f'{fieldname} = ?')
              params.append(condnum)              
            else:
              continue
          else:
            conds.append(f'{fieldname} LIKE ?')
            params.append('%' + cond + '%')  
        
        if conds:
          cond  = 'WHERE ' + ' OR '.join(conds)
        else:
          cond  = ''
      
      objs  = g.db.Find(
        model,
        cond,
        params,
        start     = start,
        limit     = limit,
        fields    = ', '.join(fields) if fields else '*',
        orderby   = orderby,
      )
      
      results['count']  = len(objs)
      
      fieldnames  = ', '.join(fields)
      
      results['data']   = [x.ToJSON(fieldnames) for x in objs]
    elif command == 'save':
      model = GetModel(StrV(d, 'model'))
      data  = DictV(d, 'data')
      
      if not model or not data:
        raise Exception(g.T.missingparams)
      
      g.db.RegisterModel(model)
      
      dbid  = V(data, model._dbid)
      
      if dbid == '0':
        raise Exception(g.T.missingparams)
      
      if dbid:
        # Try to load the object if it already exists
        if 'INT' in model._dbfields[model._dbid][0]:
          dbid  = int(dbid)
        
        try:
          obj = g.db.Load(model, dbid)
        except Exception as e:
          print('dbapi.save():\tCould not load model ', model, dbid)
          obj = model()
      else:
        obj = model()
        
      for k, v in model._dbfields.items():
        if v[0] == 'PASSWORD':
          if V(data, k):
            obj.SetPassword(k, data[k])
            
          data.pop(k, None)
        elif v[0] == 'LIST':
          if not V(data, k):
            data[k] = []
        elif v[0] == 'JSON' or v[0] == 'DICT':
          if not V(data, k):
            data[k] = {}
            
      obj.Set(**data)
      
      g.db.Save(obj)
        
      g.db.Commit()
      
      results['data']   = obj.ToJSON()
    elif command == 'load':
      model   = GetModel(StrV(d, 'model'))
      modelid = V(d, 'modelid')
      
      if not model or not modelid:
        raise Exception(g.T.missingparams)
      
      g.db.RegisterModel(model)
      
      obj  = g.db.Load(model, modelid)
      
      results['data']   = obj.ToJSON()
    elif command == 'delete':
      model     = GetModel(StrV(d, 'model'))
      idcodes   = ArrayV(d, 'ids')
      
      if not model:
        raise Exception(g.T.missingparams)
      
      g.db.RegisterModel(model)
      
      if not idcodes:
        results['error']  = 'No IDs provided to delete.'
      else:
        for objid in idcodes:
          if objid == '0':
            raise Exception(g.T.missingparams)
          
          if not objid:
            continue
          
          try:
            obj = g.db.Load(model, FromCode(objid))
          except Exception as e:
            obj = g.db.Load(model, int(objid))
          
          g.db.Delete(obj)
          
        g.db.Commit()
    elif command == 'linked':
      model1    = GetModel(StrV(d, 'model1'))
      model2    = GetModel(StrV(d, 'model2'))
      id1       = V(d, 'id1')
      linktype  = IntV(d, 'type')
      
      if not model1 or not model2 or not id1:
        raise Exception(g.T.missingparams)
      
      g.db.RegisterModel(model1)
      g.db.RegisterModel(model2)
      
      try:
        obj1  = g.db.Load(model1, id1)
      except Exception as e:
        obj1  = g.db.Load(model1, FromCode(id1))
      
      displayfields = getattr(model2, '_dbdisplay', [])
      
      if displayfields:
        displayfields = displayfields + [model2._dbid]
      
      results['data'] = [x.ToJSON(', '.join(displayfields)) for x in g.db.Linked(obj1, model2, linktype)]
    elif command == 'link' or command == 'linkarray':
      model1    = GetModel(StrV(d, 'model1'))
      model2    = GetModel(StrV(d, 'model2'))
      id1       = V(d, 'id1')
      id2s      = ArrayV(d, 'id2s')
      linktype  = IntV(d, 'type')
      
      if not model1 or not model2 or not id1:
        raise Exception(g.T.missingparams)
      
      g.db.RegisterModel(model1)
      g.db.RegisterModel(model2)
      
      try:
        obj1  = g.db.Load(model1, id1)
      except Exception as e:
        obj1  = g.db.Load(model1, FromCode(id1))
      
      ls  = []
      
      for id in id2s:
        try:
          ls.append(g.db.Load(model2, id))
        except Exception as e:
          ls.append(g.db.Load(model2, FromCode(id)))
        
      if len(ls) == 1 and command == 'link':
        g.db.Link(obj1, ls[0], linktype)
      else:
        if ls:
          g.db.LinkArray(obj1, ls, linktype)
        else:
          g.db.Unlink(obj1, model2, linktype)
        
      g.db.Commit()
    elif command == 'cleanlinks':
      """Look for broken links where the original objects don't exist
      anymore and clean them from the system. 
      """
      report  = []
      
      nummodels       = 0
      numlinkscleaned = 0
      
      for r in list(g.db.objtypes.keys()):
        try:
          m1 = GetModel(r)
          
          nummodels += 1
          
          report.append(f'Found model {r}')
          
          for l in m1._dblinks:
            m2  = GetModel(l)
            
            linktable = g.db.GetLinkTable(m1, m2)
            
            linkobj1, linkobj2  = linktable.split('__')
            
            if r == linkobj1:
              linkobj1id  = m1._dbid
              linkobj2id  = m2._dbid
            else:
              linkobj1id  = m2._dbid
              linkobj2id  = m1._dbid
            
            report.append(f'\tFound link {linktable}')
            
            if g.db.HasTable(linktable):
              brokenlinks = []
              
              for a in g.db.Execute(f'''
                  SELECT lt.linkaid, lt.linkbid
                  FROM {linktable} AS lt
                  LEFT JOIN {linkobj1} AS l1 ON lt.linkaid = l1.{linkobj1id}
                  WHERE l1.{linkobj1id} IS NULL
                '''):
                report.append(f'''\t\tFound broken link {a[0]} -> {a[1]}''')
                brokenlinks.append((a[0], a[1]))              
                numlinkscleaned += 1
                
              for a in g.db.Execute(f'''
                  SELECT lt.linkaid, lt.linkbid
                  FROM {linktable} AS lt
                  LEFT JOIN {linkobj2} AS l2 ON lt.linkbid = l2.{linkobj2id}
                  WHERE l2.{linkobj2id} IS NULL
                '''):
                report.append(f'''\t\tFound broken link {a[0]} -> {a[1]}''')
                brokenlinks.append((a[0], a[1]))
                numlinkscleaned += 1
                
              if brokenlinks:
                g.db.ExecuteMany(f'''
                  DELETE FROM {linktable}
                  WHERE linkaid = ? AND linkbid = ?
                  ''',
                  brokenlinks
                )
        except Exception as e:
          report.append('Problem cleaning model:' + str(e))
      
      g.db.Commit()
      
      report.append(f'''Cleaned {numlinkscleaned} broken links from {nummodels} models''')
      
      results['report'] = '\n'.join(report)
    elif command == 'query':
      """Runs a direct database query. Seems rather dangerous, but if someone
      can access this API, then they already have an administrator account,
      so it really doesn't change anything.
      """
      query   = StrV(d, 'query')
      params  = ArrayV(d, 'params')
      
      if not query:
        raise Exception(g.T.missingparams)
      
      results['data'] = g.db.Execute(query, params, returndict = 1)
    elif command == 'exec':
      """Run arbitary python code. Seems rather dangerous, but if someone
      can access this API, then they already have an administrator account,
      so it really doesn't change anything.
      """
      code  = StrV(d, 'code')
      
      if not code:
        raise Exception(g.T.missingparams)
      
      exec(code)
    else:
      results['error']  = f'{g.T.cantunderstand} "{command}"'          
  except Exception as e:
    traceback.print_exc()
    results['error']  = str(e)
  
  return results

# =====================================================================
def translationsapi(g):
  """Handles frequently used translations for the system. You must be 
  an administrator or be part of the translators group in order to use
  this API.
  """
  results = {}
  
  try:
    if ('admins' not in g.session['groups'] 
        and 'translators' not in g.session['groups']
      ):
      raise Exception(g.T.cantchange)
    
    d = json.loads(g.request.get_data())
  
    command = StrV(d, 'command')
    
    if command == 'list':
      apps = {}
      
      for f in os.listdir('public'):
        apppath = os.path.join('public', f)
        
        if os.path.isdir(apppath):
          translations = {}
          
          for l in g.siteconfig['languages']:
            translationspath  = os.path.join(apppath, 'translations', l)
            
            if os.path.exists(translationspath):
              for t in os.listdir(translationspath):
                translations[os.path.splitext(t)[0]]  = 1
            
          apps[f] = list(translations.keys())
      
      results['data'] = apps
    elif command == 'load':
      app         = StrV(d, 'app')
      translation = StrV(d, 'translation')
      languages   = ArrayV(d, 'languages')
      
      if not app or not translation or not languages:
        raise Exception(g.T.missingparams)
      
      translations  = {}
      
      for lang in languages:
        try:
          T = types.SimpleNamespace()
          
          with open('public/' + app + '/translations/' + lang + '/' + translation + '.js', 'r') as f:
            exec(f.read())
          
          translations[lang]  = T.__dict__
        except Exception as e:
          pass
        
      results['data'] = translations
    elif command == 'save':
      app         = StrV(d, 'app')
      translation = StrV(d, 'translation')
      data        = DictV(d, 'data')
      
      if not app or not translation:
        raise Exception(g.T.missingparams)
      
      # Create blank files for new translations
      if not data:
        for l in g.siteconfig['languages']:
          data[l] = {}
      
      results['error']  = '';
      
      for lang, translations in data.items():
        try:
          dirname = 'public/' + app + '/translations/' + lang
          
          os.makedirs(dirname, 0o777, True)
          
          with open(dirname + '/' + translation + '.js', 'w') as f:
            for k, v in data[lang].items():
              f.write(f'T.{k}={json.dumps(v)}\n')
              
            f.write('\n')
        except Exception as e:
          traceback.print_exc()
          results['error']  += str(e) + '\n'
    elif command == 'rename' or command == 'copy':
      app         = StrV(d, 'app')
      newname     = StrV(d, 'newname')
      oldname     = StrV(d, 'oldname')
      
      if not app or not newname or not oldname:
        raise Exception(g.T.missingparams)
      
      apppath = os.path.join('public', app)
      
      for l in g.siteconfig['languages']:
        translationsdir  = os.path.join(apppath, 'translations', l)
        
        translationsfile  = os.path.join(translationsdir, oldname + '.js')
        
        if os.path.exists(translationsfile):
          if command == 'rename':
            os.replace(translationsfile, os.path.join(translationsdir, newname + '.js'))
          else:
            shutil.copy(translationsfile, os.path.join(translationsdir, newname + '.js'))
    elif command == 'delete':
      app         = StrV(d, 'app')
      translation = StrV(d, 'translation')
      
      for l in g.siteconfig['languages']:
        translationsfile  = os.path.join('public', app, 'translations', l, translation + '.js')
        
        if os.path.exists(translationsfile):
          os.remove(translationsfile)
    else:
      results['error']  = f'{g.T.cantunderstand} "{command}"'
  except Exception as e:
    traceback.print_exc()
    results['error']  = str(e)
  
  return results

# =====================================================================
def securityapi(g):
  """Sets security for database objects. Note that the DB_SECURE flag
  must be set on both the database and the model in order to apply.
  """
  results = {}
  
  try:
    d = json.loads(g.request.get_data())
    
    command = StrV(d, 'command')
    
    if command == 'load':
      modelname = StrV(d, 'model')
      model     = GetModel(modelname)
      objid     = StrV(d, 'objid')
      
      if not model or not objid:
        raise Exception(g.T.missingparams)
      
      g.db.RegisterModel(model)
      
      if not model._dbflags & DB_SECURE:
        raise Exception(g.T.cantchange + modelname)
      
      obj = g.db.Load(model, FromCode(objid), model._dbid)
      
      results['dbowner']        = ToShortCode(obj.dbowner)
      results['dbaccess']       = obj.dbaccess
      results['dbgroup']        = ToShortCode(obj.dbgroup) if obj.dbgroup else ''
      results['dbgroupaccess']  = obj.dbgroupaccess
      
      acl = obj.GetACL(g.db)
      
      acljson = {
        'users':  {},
        'groups': {}
      }
      
      if not acl:
        results['dbacl']  = acljson
      else:
        for k, v in acl['users'].items():
          userobj = {
            'displayname':  '',
            'access':       v
          }
          
          if IsAdmin():
            try:
              userobj['displayname']  = BestTranslation(
                g.db.Load(system_user, k, 'displayname').displayname, 
                g.session.langcodes
              )
            except Exception as e:
              userobj['displayname']  = f'Error: {e}'
            
          acljson['users'][k] = userobj
          
        for k, v in acl['groups'].items():
          groupobj = {
            'displayname':  '',
            'access':       v
          }
          
          if IsAdmin():
            try:
              groupobj['displayname']  = BestTranslation(
                g.db.Load(system_group, k, 'displayname').displayname, 
                g.session.langcodes
              )
            except Exception as e:
              groupobj['displayname']  = f'Error: {e}'
            
          acljson['groups'][k] = groupobj
          
        results['dbacl']  = acljson            
      
      if IsAdmin():
        try:
          results['ownername']  = BestTranslation(
            g.db.Load(system_user, obj.dbowner, 'displayname').displayname, 
            g.session.langcodes
          )
        except Exception as e:
          results['ownername']  = f'Error: {e}'
      else:
        results['ownername']  = results['dbowner']
      
      if obj.dbgroup:
        try:
          group = g.db.Load(system_group, obj.dbgroup, 'displayname')
          
          results['groupname']         = group.groupname
          results['groupdisplayname']  = BestTranslation(
            group.displayname, 
            g.session.langcodes
          )
        except Exception as e:
          results['groupname']          = ''
          results['groupdisplayname']   = f'Error: {e}'
      else:
        results['groupname']        = ''
        results['groupdisplayname'] = '[None]'
    elif command == 'searchgroups':
      keyword = StrV(d, 'keyword')
      start   = IntV(d, 'start', 0, 99999999, 0)
      limit   = IntV(d, 'limit', 10, 99999999, 100)
      
      cond    = []
      params  = []
      
      if keyword:
        conds.append('(groupname LIKE ? OR displayname LIKE ?)')
        params.append(keyword)
        params.append(keyword)
        
      if cond:
        cond  = 'WHERE ' + ' AND '.join(conds)
      
      objs    = []
      groups  = []
        
      if IsAdmin():
        groups  = g.db.Find(
          system_group,
          cond,
          params,
          start   = start,
          limit   = limit,
          fields  = 'rid, groupname, displayname'
        )
      else:
        groups  = g.db.Linked(
          g.db.Load(system_user, g.session.userid, 'rid'),
          cond    = cond,
          params  = params,
          start   = start,
          limit   = limit,
          fields  = 'rid, groupname, displayname'
        )
      
      for r in groups:
        objs.append({
          'idcode':       ToShortCode(r.rid),
          'displayname':  BestTranslation(r.displayname, g.session.langcodes),
          'groupname':    r.groupname
        })
          
      results['count']  = len(groups)
      results['data']   = objs      
    elif command == 'save':
      modelname       = StrV(d, 'model')
      model           = GetModel(modelname)
      objid           = StrV(d, 'objid')
      dbowner         = StrV(d, 'dbowner')
      dbaccess        = IntV(d, 'dbaccess')
      dbgroup         = StrV(d, 'dbgroup')
      dbgroupaccess   = StrV(d, 'dbgroupaccess')
      dbacl           = DictV(d, 'dbacl')
      
      if not model or not objid or not dbowner:
        raise Exception(g.T.missingparams)
      
      g.db.RegisterModel(model)
      
      if not model._dbflags & DB_SECURE:
        raise Exception(g.T.cantchange + modelname)
      
      obj = g.db.Load(model, FromCode(objid), model._dbid)
      
      acl = obj.GetACL(g.db)
      
      acljson = {
        'users':  {},
        'groups': {}
      }
      
      if not acl:
        results['dbacl']  = acljson
      else:
        for k, v in acl['users'].items():
          userobj = {
            'displayname':  '',
            'access':       v
          }
          
          if IsAdmin():
            try:
              userobj['displayname']  = BestTranslation(
                g.db.Load(system_user, k, 'displayname').displayname, 
                g.session.langcodes
              )
            except Exception as e:
              userobj['displayname']  = f'Error: {e}'
            
          acljson['users'][k] = userobj
          
        for k, v in acl['groups'].items():
          groupobj = {
            'displayname':  '',
            'access':       v
          }
          
          if IsAdmin():
            try:
              groupobj['displayname']  = BestTranslation(
                g.db.Load(system_group, k, 'displayname').displayname, 
                g.session.langcodes
              )
            except Exception as e:
              groupobj['displayname']  = f'Error: {e}'
            
          acljson['groups'][k] = groupobj
          
        results['dbacl']  = acljson            
      
      if IsAdmin():
        try:
          results['ownername']  = BestTranslation(
            g.db.Load(system_user, obj.dbowner, 'displayname').displayname, 
            g.session.langcodes
          )
        except Exception as e:
          results['ownername']  = f'Error: {e}'
      else:
        results['ownername']  = results['dbowner']
      
      if obj.dbgroup:
        try:
          group = g.db.Load(system_group, obj.dbgroup, 'displayname')
          
          results['groupname']         = group.groupname
          results['groupdisplayname']  = BestTranslation(
            group.displayname, 
            g.session.langcodes
          )
        except Exception as e:
          results['groupname']          = ''
          results['groupdisplayname']   = f'Error: {e}'
      else:
        results['groupname']        = ''
        results['groupdisplayname'] = '[None]'      
    else:
      results['error']  = f'{g.T.cantunderstand} "{command}"'
  except Exception as e:
    traceback.print_exc()
    results['error']  = str(e)
  
  return results

# =====================================================================
def verifyemail(g):
  if not g.urlargs:
    return g.T.missingparams
  
  user  = g.db.FindOne(
    system_newuser,
    'WHERE emailcode = ?',
    g.urlargs[0],
  )
  
  if user:
    user.Set(flags = user.flags | NEWUSER_VALIDATED_EMAIL)
    g.db.Update(user)
    g.db.Commit()
    return g.T.allgood
  
  return g.T.nothingfound

# =====================================================================
def messageapi(g):
  """Standard API to search through the files on the system and change 
  names or descriptions. 
  """
  results = {}
  
  modeltouse    = system_message
  editorgroup   = ''
  searchfields  = '*'
  
  try:
    d = json.loads(g.request.get_data())
    
    command = StrV(d, 'command')
    
    if command == 'addview':
      '''Forum posts keep track of the number of views to see which 
      threads are the most popular.
      
      Mail posts keep track of whether they have been read or not.
      '''
      idcode    = StrV(d, 'idcode')
      
      if not idcode:
        raise Exception(g.T.missingparams)
      
      msg = g.db.Load(system_message, FromCode(idcode), 'rbid, numviews, flags')
      
      if msg.flags & MESSAGE_MAIL:
        currentuser = system_user()
        currentuser.Set(userid = g.session.userid)
        
        if g.db.HasLink(currentuser, currentuser.rid, msg, msg.rbid, MESSAGE_LINK_INBOX_UNREAD):
          g.db.Unlink(currentuser, msg, MESSAGE_LINK_INBOX_UNREAD)
          g.db.Link(currentuser, msg, MESSAGE_LINK_INBOX_READ)
          
          unreadcount = g.session.get('message.unread.count', 0)
          
          if unreadcount:
            g.session['message.unread.count'] = unreadcount - 1
      else:
        msg.Set(
          numviews  = msg.numviews + 1
        )
        g.db.Update(msg)
        
      g.db.Commit()      
    elif command == 'addlike':
      '''Forum posts also enable likes and dislikes to see which 
      posts are most popular within a thread.
      '''
      idcode    = StrV(d, 'idcode')
      like      = IntV(d, 'like')
      
      if not idcode:
        raise Exception(g.T.missingparams)
      
      message     = g.db.Load(system_message, FromCode(idcode), 'rbid, likes, dislikes')
      message.Set(rbid = FromCode(idcode))
      
      currentuser = system_user()
      currentuser.Set(rid = g.session.userid)
      
      if (g.db.HasLink(message, message.rbid, currentuser, currentuser.rid, MESSAGE_LINK_LIKE)
          or g.db.HasLink(message, message.rbid, currentuser, currentuser.rid, MESSAGE_LINK_DISLIKE)
        ):
        raise Exception(g.T.alreadydone)
      
      if like:
        message.Set(likes = message.likes + 1)
      else:
        message.Set(dislikes = message.dislikes + 1)
        
      g.db.Update(message)
        
      g.db.Link(message, currentuser, MESSAGE_LINK_LIKE if like else MESSAGE_LINK_DISLIKE)
      
      g.db.Commit()
      
    elif command == 'checkpermissions':
      objtype       = StrV(d, 'objtype')
      objid         = StrV(d, 'objid')
      
      if not objid or not objtype:
        raise Exception(g.T.missingparams)
      
      model, obj, permissions = GetSystemMessagePermissions(g, objtype, FromCode(objid))
      
      results['data'] = permissions
    elif command == 'archive':
      idcodes       = ArrayV(d, 'idcodes')
      
      needupdate  = 0
      
      for message in g.db.LoadMany(
          system_message, 
          [FromCode(x) for x in idcodes],
          fields  = 'rbid, threadid, flags'
        ):
        
        # When a conversation is archived, the entire thread is archived
        for threadmessage in g.db.Find(
            system_message,
            'WHERE threadid = ?',
            message.threadid
          ):
          # Archive only works when the message is already in the user's inbox
          if not g.db.Execute('''SELECT linkaid
              FROM system_message__system_user
              WHERE linkaid = ? AND linkbid = ? AND linktype IN (?, ?)
              ''',
              [threadmessage.rbid, g.session.userid, MESSAGE_LINK_INBOX_UNREAD, MESSAGE_LINK_INBOX_READ]
            ):
            continue
          
          LinkSystemMessage(g, threadmessage, g.session.userid, MESSAGE_LINK_ARCHIVED)
          needupdate  = 1
        
      if needupdate:
        g.db.Commit()      
    elif command == 'getmembers':
      objtype       = StrV(d, 'objtype')
      objid         = StrV(d, 'objid')
      
      if not objid or not objtype:
        raise Exception(g.T.missingparams)
      
      model, obj, permissions = GetSystemMessagePermissions(g, objtype, FromCode(objid))
      
      usernames = {}
      
      if obj:
        userids = list(set(obj.forumadmins + obj.forumposters + obj.forumviewers))
        
        if userids:
          for r in g.db.Find(
              system_user,
              'WHERE rid IN (' + ', '.join(['?' for x in userids]) + ')',
              [FromCode(x) for x in userids],
              fields  = 'rid, displayname',
            ):
            usernames[ToShortCode(r.rid)] = BestTranslation(r.displayname, g.session.langcodes)
      
      results['data'] = usernames
    elif command == 'search':
      g.db.RequireGroup('users', g.T.cantview)
      
      keyword       = StrV(d, 'keyword')
      title         = StrV(d, 'title')
      content       = StrV(d, 'content')
      objid         = StrV(d, 'objid')
      objtype       = StrV(d, 'objtype')
      fromid        = StrV(d, 'fromjid')
      tojid         = StrV(d, 'toid')
      replyid       = StrV(d, 'replyid')
      threadid      = StrV(d, 'threadid')
      fromtime      = StrV(d, 'fromtime')
      totime        = StrV(d, 'totime')
      flags         = IntV(d, 'flags')
      minsize       = IntV(d, 'minsize')
      maxsize       = IntV(d, 'maxsize')
      start         = IntV(d, 'start')
      limit         = IntV(d, 'limit', 1, 1000, 20)
      orderby       = StrV(d, 'orderby', 'modified')
      folder        = StrV(d, 'folder')
      
      if objtype:
        objtype = g.db.TypeToID(objtype)
      
      if folder:
        if folder == 'inbox':
          folder  = [MESSAGE_LINK_INBOX_UNREAD, MESSAGE_LINK_INBOX_READ]
        elif folder == 'drafts':
          folder  = MESSAGE_LINK_DRAFTS
        elif folder == 'archives':
          folder  = MESSAGE_LINK_ARCHIVED
        elif folder == 'sent':
          folder  = MESSAGE_LINK_SENT
        elif folder == 'deleted':
          folder  = MESSAGE_LINK_DELETED        
      else:
        folder  = [MESSAGE_LINK_INBOX_UNREAD, MESSAGE_LINK_INBOX_READ]
      
      conds   = []
      params  = []
      
      if keyword:
        conds.append("(title LIKE ? OR content LIKE ?)")
        params.append(keyword)
        params.append(keyword)
      else:
        if title:
          conds.append("title LIKE ?")
          params.append(title)
        
        if content:
          conds.append("content LIKE ?")
          params.append(content)
        
      if objid:
        conds.append("objid = ?")
        
        objid = FromCode(objid)
        
        params.append(objid)
        
      if objtype:
        conds.append("objtype = ?")
        params.append(objtype)
        
      if fromid:
        conds.append("fromid = ?")
        params.append(fromid)
        
      if replyid:
        conds.append("replyid = ?")
        
        replyid = FromCode(replyid)
        
        params.append(replyid)
        
      if threadid:
        conds.append("threadid = ?")
        
        threadid  = FromCode(threadid)
        
        params.append(threadid)
        
      if fromtime:
        conds.append("modified > ?")
        params.append(fromtime)
      
      if totime:
        conds.append("modified < ?")
        params.append(totime)
        
      if minsize:
        conds.append("size > ?")
        params.append(minsize)
      
      if maxsize:
        conds.append("size < ?")
        params.append(maxsize)
        
      if flags:
        conds.append("CAST(flags & ? AS BOOLEAN)")
        params.append(flags)
        
      if threadid:
        # Threads are returned in order of creation
        orderby = 'created'
        
      conds = " AND ".join(conds) if conds else ''
      
      forumfields   = 'rbid, title, content, fromid, numreplies, files, numviews, likes, dislikes, threadid, replyid, created, modified, flags'
      messagefields = 'rbid, title, content, fromid, numreplies, files, toids, threadid, replyid, created, modified, flags'
            
      # Forum posts
      if objtype and objid:
        if not title and not content and not threadid:
          conds += ' AND replyid = 0'
          orderby = 'modified DESC'
              
        objs = g.db.Find(
          system_message,
          cond      = 'WHERE ' + conds, 
          params    = params, 
          start     = start, 
          limit     = limit,
          orderby   = orderby,
          fields    = forumfields,
        )
        
        ls  = []
        
        for r in objs:
          o = r.ToJSON(forumfields)
          
          del o['rbid']
          
          o['threadid'] = ToLongCode(o['threadid']) if o['threadid'] else ''
          o['replyid']  = ToLongCode(o['replyid']) if o['replyid'] else ''
          
          ls.append(o)
        
        results['data']   = ls
      # Mail messages
      else:      
        currentuser = system_user()
        currentuser.Set(rid = g.session.userid)
        
        if threadid:
          objs = g.db.Find(
            system_message,
            cond      = 'WHERE ' + conds, 
            params    = params, 
            start     = start, 
            limit     = limit,
            orderby   = 'created',
            fields    = messagefields,
          )
          
          # Mark all thread messages as read if it's expanded in the 
          # user's inbox
          if isinstance(folder, list) and MESSAGE_LINK_INBOX_UNREAD in folder:
            
            messageidstomark  = g.db.Execute(f'''SELECT linkaid
                FROM system_message__system_user
                WHERE linktype = ?
                  AND linkbid = ?
                  AND linkaid IN ({', '.join(['?' for x in objs])}) 
              ''',
              [MESSAGE_LINK_INBOX_UNREAD, g.session.userid] + [x.rbid for x in objs]
            )
            
            if messageidstomark:
              
              g.db.Execute(f'''UPDATE system_message__system_user
                  SET linktype = ?
                  WHERE linkaid IN ({', '.join(['?' for x in messageidstomark])}) 
                    AND linkbid = ?
                ''',
                [MESSAGE_LINK_INBOX_READ] + list(messageidstomark) + [g.session.userid]
              )
              
              unreadcount = g.session.get('message.unread.count', 0)
              
              if unreadcount:
                unreadcount -= len(messageidstomark)
                
                if unreadcount < 0:
                  unreadcount = 0
                  
                g.session['message.unread.count'] = unreadcount
                  
        else:
          objs = g.db.Linked(
            currentuser,
            system_message,
            linktypes = folder,
            cond      = conds, 
            params    = params, 
            start     = start, 
            limit     = limit,
            orderby   = orderby,
            fields    = messagefields + ', linktype',
          )
          

        ls  = []
        
        # This keeps track of thread IDs to ensure that we don't show
        # messages from the same thread as individual posts within the 
        # same batch of messages
        usedthreadids = []
        
        for r in objs:
          if not threadid:
            if r.threadid in usedthreadids:
              continue
            
            usedthreadids.append(r.threadid)
          
          o = r.ToJSON(forumfields)
          
          del o['rbid']
          
          o['threadid'] = ToLongCode(o['threadid']) if o['threadid'] else ''
          o['replyid']  = ToLongCode(o['replyid']) if o['replyid'] else ''
          
          if not threadid:
            o['linktype'] = r.linktype
          
          ls.append(o)
        
        results['data']   = ls
      
      fromids = {x['fromid']: {} for x in results['data']}
      
      if fromids:
        for r in g.db.Find(system_user,
            'WHERE rid IN (' + ', '.join(['?' for x in fromids]) + ')',
            list(fromids.keys()),
            fields  = 'rid, displayname'
          ):
          
          for s in results['data']:
            if s['fromid'] == r.rid:
              s['username'] = r.displayname
              
        for s in results['data']:
          s['fromid'] = ToShortCode(s['fromid']) if s['fromid'] else ''
      
      results['count']  = len(objs)
    elif command == 'load':
      g.db.RequireGroup('users', g.T.cantview)
      
      idcode  = StrV(d, 'idcode')
      
      if not idcode:
        raise Exception(g.T.missingparams)
      
      results['data']  = g.db.Load(modeltouse, FromCode(idcode)).ToJSON()
    elif command == 'save':
      g.db.RequireGroup('users', g.T.cantview)
      
      if not CanPostMessages():
        raise Exception(g.T.cantcreate)
      
      newinfo  = DictV(d, 'data')
      
      title     = StrV(newinfo, 'title')
      toids     = ArrayV(newinfo, 'toids')
      objtype   = StrV(newinfo, 'objtype')
      objid     = StrV(newinfo, 'objid')
      replyid   = StrV(newinfo, 'replyid')
      threadid  = StrV(newinfo, 'threadid')
      flags     = IntV(newinfo, 'flags')
      
      if not title or (objid and not objid) or (not objid and not toids and not replyid):
        raise Exception(g.T.missingparams)
      
      idcode   = StrV(newinfo, 'idcode')
      
      newinfo['modified'] = time.time()
      
      newinfo['objid']    = FromCode(objid) if objid else 0
      newinfo['replyid']  = FromCode(replyid) if replyid else 0
      newinfo['threadid'] = FromCode(threadid) if threadid else 0
      newinfo['files']    = ArrayV(newinfo, 'filelist')
        
      needinsert  = 0
        
      if idcode:
        newobj  = g.db.Load(system_message, FromCode(idcode))
        
        newinfo['fromid'] = newobj.fromid
        
        if newobj.fromid != g.session.userid:
          raise Exception(g.T.cantchange)
      else:
        g.db.RegisterModel(system_message)
        
        newobj  = system_message()
        
        newinfo['fromid']   = g.session.userid
        newinfo['created']  = newinfo['modified']
        
        if not newinfo['threadid']:
          newinfo['rbid']     = g.db.FindValidRandomID(system_message, 'rbid')
          newinfo['threadid'] = newinfo['rbid']
        
        needinsert  = 1
        
      if objtype:
        if not objid:
          raise Exception(g.T.missingparams)
        
        model, obj, permissions = GetSystemMessagePermissions(g, newinfo['objtype'], newinfo['objid'])
        
        newinfo['objtype']  = g.db.TypeToID(newinfo['objtype'])
        
        if not permissions['post']:
          raise Exception(g.T.cantcreate)
        
        # Special handling for sending a mail message to all forum members
        if len(toids) == 1 and toids[0] == 'all':
          toids = []
          
          for r in obj.forumadmins + obj.forumposters + obj.forumviewers:
            if r != g.session.usercode:
              toids.append(r)
          
          flags &= ~(MESSAGE_FORUM)
          flags |= MESSAGE_MAIL
          
          if needinsert:
            flags |= MESSAGE_UNREAD
        else:
          flags &= ~(MESSAGE_MAIL)
          flags |= MESSAGE_FORUM
      else:
        flags &= ~(MESSAGE_FORUM)
        flags |= MESSAGE_MAIL
        
        if needinsert:
          flags |= MESSAGE_UNREAD
        
      # Remove any duplicate toids
      toids = list(set(toids))
      
      newinfo['flags']  = flags
        
      # If the message is a reply, then update replies for parent posts
      if replyid:
        repliedtomessage  = g.db.Load(system_message, newinfo['replyid'], 'rbid, fromid, numreplies, replyid, threadid, toids')
        
        newinfo['threadid'] = repliedtomessage.threadid
        
        while 1:
          repliedtomessage.Set(
            numreplies  = repliedtomessage.numreplies + 1,
          )
          
          # Update the top post of a thread's modified time if someone posts a 
          # reply to the thread
          if not repliedtomessage.replyid:
            repliedtomessage.Set(
              modified    = newinfo['modified'],
            )
          
          g.db.Update(repliedtomessage)        
          
          if not repliedtomessage.replyid:
            break
          
          repliedtomessage  = g.db.Load(system_message, repliedtomessage.replyid, 'rbid, fromid, numreplies, replyid, threadid, toids')
      
      newobj.Set(**newinfo)
      
      if needinsert:
        g.db.Insert(newobj)
        
        if newobj.flags & MESSAGE_MAIL:
          sessionstoupdate  = []
          
          # The first mail message in a thread will have toids
          if toids:
            sessionstoupdate  = [FromShortCode(x) for x in toids]
            
            LinkSystemMessage(g, newobj, newobj.fromid, MESSAGE_LINK_SENT)
          # While reply messages will use the first messsage's toids plus the original sender
          else:
            sessionstoupdate  = [FromShortCode(x) for x in repliedtomessage.toids] + [repliedtomessage.fromid]
            
          LinkSystemMessage(g, newobj, sessionstoupdate, MESSAGE_LINK_INBOX_UNREAD)
          
          # Find and update message counts for all users that this
          # message was sent to
          for session in g.db.Find(
              system_session,
              'WHERE userid IN (' + ', '.join(['?' for x in sessionstoupdate]) + ') AND NOT CAST(flags & ? AS BOOLEAN)',
              sessionstoupdate + [SESSION_EXPIRED]
            ):
            
            newmessagecount  = session.get('message.unread.count', 0)
            
            session['message.unread.count'] = newmessagecount  + 1
            
            g.db.Update(session)          
      else:
        g.db.Update(newobj)
        
      g.db.Commit()
      
      o = newobj.ToJSON()
      
      del o['rbid']
      
      o['threadid'] = ToLongCode(o['threadid']) if o['threadid'] else ''
      o['replyid']  = ToLongCode(o['replyid']) if o['replyid'] else ''
      o['fromid']   = ToShortCode(o['fromid'])
          
      results['data'] = o
    elif command == 'delete':
      g.db.RequireGroup('users', g.T.cantview)
      
      isadmin = IsAdmin()
      
      idcodes  = ArrayV(d, 'idcodes')
      
      if not idcodes:
        raise Exception(g.T.missingparams)
      
      permissioncache = {}
      
      for r in g.db.LoadMany(modeltouse, [FromCode(x) for x in idcodes]):
        if r.flags & MESSAGE_FORUM:
          permissionkey = str(r.objtype) + '.' + str(r.objid)
          
          candelete = 0
          
          if permissionkey not in permissioncache:
            if isadmin:
              permissioncache[permissionkey]  = 1
            else:
              model, obj, permissions = GetSystemMessagePermissions(g, r.objtype, r.objid)
              
              permissioncache[permissionkey]  = permissions['admin']
            
          candelete = permissioncache[permissionkey]
          
          # Forum messages can only be deleted by administrators and if they have no replies. 
          # Otherwise, the post will simply be displayed with "[Deleted]" as the content.
          #
          # To really delete a thread, it must be deleted while the MESSAGE_DELETED flags
          # is already in effect.
          if (isadmin or candelete) and (not r.numreplies or r.flags & MESSAGE_DELETED):
            g.db.Delete(r)
            
            results['reallydeleted']  = 1
          else:
            if (not (isadmin or candelete)) and r.fromid != g.session.usercode:
              raise Exception(g.T.cantchange)
            
            r.Set(flags = MESSAGE_DELETED)
            
            g.db.Update(r)
        else:
          # Mail messages are deleted by moving to the deleted folder.
          # Only by deleting from the deleted folder is a message truly
          # deleted from a user, and only the user who sent it can deleted
          # the database entry.
          currentuser = system_user()
          currentuser.Set(rid = g.session.userid)
          
          if g.db.HasLink(system_user, g.session.userid, r, r.rbid, MESSAGE_LINK_DELETED):
            g.db.Unlink(currentuser, r)
            
            if r.fromid == currentuser.rid:
              g.db.Delete(r)
              
              results['reallydeleted']  = 1
          else:
            g.db.Execute('''DELETE FROM system_message__system_user 
              WHERE linkaid = ? AND linkbid = ?''',
              [r.rbid, currentuser.rid]
            )
            g.db.Link(currentuser, r, MESSAGE_LINK_DELETED)
        
      g.db.Commit()
    elif command == 'postadminmessage':
      name          = StrV(d, 'name')
      email         = StrV(d, 'email')
      phonenumber   = StrV(d, 'phonenumber')
      title         = StrV(d, 'title')
      message       = StrV(d, 'message')
      captchavalue  = StrV(d, 'captchavalue').strip()
      
      if (not name 
          or not email
          or not message
        ):
        raise Exception(g.T.missingparams)
      
      CheckCaptcha(g, captchavalue)
      
      adminuser = g.db.FindOne(
        system_user,
        'WHERE phonenumber = ? AND place = ?',
        ['admin', 'pafera'],
        fields  = 'rid'
      )
      
      SendSystemMessage(
        g,
        objid     = g.db.TypeToID('system_user'),
        toids     = ToShortCode(adminuser.rid),
        title     = 'From Website Contact Form: ' + title,
        content   = '\n'.join([name, email, phonenumber, '', message])
      )
      
      g.db.Commit()
    else:
      results['error']  = f'{g.T.cantunderstand} "{command}"'
  except Exception as e:
    traceback.print_exc()
    results['error']  = str(e)
  
  return results

# =====================================================================
def thumbnailer(g):
  if not g.urlargs:
    return g.T.missingparams
  
  f = g.db.Load(system_file, FromCode(g.urlargs[0]))
  
  dirname, filename = os.path.split(os.path.abspath(f.ThumbnailFile()))
  
  return send_from_directory(dirname, filename)

# =====================================================================
def downloader(g):
  if not g.urlargs:
    return g.T.missingparams
  
  f = g.db.Load(system_file, FromCode(g.urlargs[0]))
  
  dirname, filename = os.path.split(os.path.abspath(f.FullPath()))
  
  return send_from_directory(dirname, filename, as_attachment = 1)

# =====================================================================
def qrlogin(g):
  if not g.urlargs:
    return g.T.missingparams
  
  #try:
  currenttime = time.time()
  
  # Login links expire after five minutes
  g.db.Delete(
    system_loginlink,
    'WHERE modified < ?',
    currenttime - (5 * 60)
  )
  
  loginlink = g.db.Load(system_loginlink, FromCode(g.urlargs[0]))
  
  waitingsession  = g.db.Load(system_session, loginlink.sessionid)
  
  currentuser     = g.db.Load(
    system_user, 
    g.session.userid, 
    fields  = 'rid, passkeys, settings, flags',
  )
  
  OnLoginSuccessful(g, {}, currentuser, waitingsession)
  
  loginlink.Set(
    flags = LOGINLINK_VERIFIED,
    homeurl = currentuser.settings.get('homeurl', '')
  )
  
  g.db.Update(loginlink)
  g.db.Update(waitingsession)
  g.db.Commit()    
  return g.T.verified
  #except Exception as e:
  #  print(e)
  #  return g.T.expired
    
  return g.T.nothingfound

# *********************************************************************
ROUTES  = {
  'captchaapi':       captchaapi,
  'controlpanelapi':  controlpanelapi,
  'dbapi':            dbapi,
  'dbexport':         dbexport,
  'dbimport':         dbimport,
  'downloader':       downloader,
  'fileapi':          fileapi,
  'messageapi':       messageapi,
  'securityapi':      securityapi,
  'sessionvars':      sessionvars,
  'thumbnailer':      thumbnailer,
  'translationsapi':  translationsapi,
  'upload':           upload,
  'userapi':          userapi,
  'verifyemail':      verifyemail,
  'qr':               qrlogin,
}
