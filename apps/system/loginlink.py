#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system.validators import *
from apps.system.db import *

LOGINLINK_VERIFIED  = 0x01

# *********************************************************************
class system_loginlink(ModelBase):
  """A simple class to enable QR or link logins where the user already
  has their account open on a phone, and want to simultaneously use 
  another device.
  """
  
  _dbfields     = {
    'rbid':         ('INT64 PRIMARY KEY', 'NOT NULL',),
    'sessionid':    ('INT64', 'NOT NULL', BlankValidator()),
    'modified':     ('TIMESTAMP', 'NOT NULL',),
    'homeurl':      ('TEXT', "NOT NULL DEFAULT ''",),
    'flags':        ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = []
  _dbdisplay  = ['sessionid', 'modified', 'flags']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()

