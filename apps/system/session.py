#!/usr/bin/python
# -*- coding: utf-8 -*- 

import time
import datetime
import traceback

import user_agent_parser

from apps.system.types import *
from apps.system.validators import *
from apps.system.db import *

# Flags for the session object
SESSION_EXPIRED = 0x01

# *********************************************************************
# Bootstrap class for the database.
class system_useragent(ModelBase):
  """Tracker for the various types of programs that visit the site.
  Note that this will not track every single visitor since sessions
  depend on JavaScript being enabled to load sessionvars.js, but can 
  be a good approximation of human visits.
  """
  
  _dbfields     = {
    'id':               ('INTAUTOINCREMENT', 'NOT NULL',),
    'header':           ('TEXT', 'NOT NULL', BlankValidator()),
    'os':               ('TEXT', "NOT NULL DEFAULT ''", ),
    'osversion':        ('TEXT', "NOT NULL DEFAULT ''", ),
    'browser':          ('TEXT', "NOT NULL DEFAULT ''", ),
    'browserversion':   ('TEXT', "NOT NULL DEFAULT ''", ),
    'devicetype':       ('TEXT', "NOT NULL DEFAULT ''", ),
    'devicename':       ('TEXT', "NOT NULL DEFAULT ''", ),
    'devicehost':       ('TEXT', "NOT NULL DEFAULT ''", ),
    'flags':            ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = (
  )
  _dblinks    = []
  _dbdisplay  = ['platform', 'browser', 'version']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()

# *********************************************************************
class system_session(ModelBase):
  """Keeps track of user sessions, language selection, and application
  data.
  
  This class also has the ability to record every URL that the user
  visited and how long they stayed on that page if 
  g.siteconfig['urltracking'] is enabled. It makes the system a bit 
  slower, but gives you much better analysis of what the user did and
  what they were interested in.
  
  The session data will be automatically saved at the end of a 
  request only if you have used the __setitem__ function. Otherwise,
  you'll need to manually set the needupdate variable to a true value.
  """
  
  _dbfields     = {
    'rbid':         ('INT64 PRIMARY KEY', 'NOT NULL',),
    'userid':       ('INT', 'NOT NULL DEFAULT 0',),
    'useragentid':  ('INT', 'NOT NULL',),
    'length':       ('INT', 'NOT NULL DEFAULT 0',),
    'flags':        ('INT', 'NOT NULL DEFAULT 0',),
    'userflags':    ('INT', 'NOT NULL DEFAULT 0',),
    'ip':           ('TEXT', 'NOT NULL', BlankValidator()),
    'lang':         ('TEXT', 'NOT NULL', BlankValidator()),
    'langcodes':    ('LIST', 'NOT NULL', BlankValidator()),
    'data':         ('DICT', "NOT NULL DEFAULT ''",),
    'starttime':    ('TIMESTAMP', 'NOT NULL',),
    'endtime':      ('TIMESTAMP', 'NOT NULL DEFAULT 0',),
    'urls':         ('LIST', "NOT NULL DEFAULT ''",),
  }
  _dbindexes  = (
  )
  _dblinks    = []
  _dbdisplay  = ['userid', 'length', 'lang', 'starttime']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
    self.data       = {}
    self.urls       = []
    self.needupdate = 0
  
  # -------------------------------------------------------------------
  def CheckExpired(self, g):
    """See if the current session has expired and creates a new one if
    necessary.
    """
    currenttime = time.time()
    
    if currenttime > self.endtime + IntV(g.siteconfig, 'sessiontimeout', 600, 2678400, 604800):
      self.New(g)
      '''with open('expired.log', 'a+') as f:
        f.write(f""">>> Session {self.rbid} expired:
  User ID:\t{g.session.userid}
  Current Time:\t{currenttime}
  End Time:\t{self.endtime}
  Length:\t{currenttime - self.starttime}
  Session Timeout:\t{IntV(g.siteconfig, 'sessiontimeout', 600, 2678400, 604800)}\n""")
      '''  
    
  # -------------------------------------------------------------------
  def New(self, g):
    """Creates a new session, closing the old session.
    
    This also setups the useragent, languages, and other default settings. 
    """
    currenttime = time.time()
    
    if self.rbid:
      self.Set(
        endtime   = currenttime,
        length    = currenttime - self.starttime,
        flags     = SESSION_EXPIRED,
      )
      g.db.Update(self)
      g.db.Commit()
      
      self.Set(
        rbid    = 0,
        userid  = 0,
      )
      
    req   = g.request 
    ua    = g.request.user_agent
    
    useragentstring = ua.string if ua.string else '[No header: Probably a hacking attempt]'
    
    useragent = g.db.Find(system_useragent, 'WHERE header = ?', useragentstring)
    
    # Some testing tools don't provide platform, browser, and such.
    # Therefore, we use blank values instead.
    if not useragent:
      useragent = system_useragent()
      
      agentvalues = user_agent_parser.Parser(useragentstring)
      
      useragent.Set(
        header          = useragentstring,
        browser         = U(agentvalues.browser),
        browserversion  = U(agentvalues.browser_version),
        os              = U(agentvalues.os),
        osversion       = U(agentvalues.os_version),
        devicetype      = U(agentvalues.device_type),
        devicename      = U(agentvalues.device_name),
        devicehost      = U(agentvalues.device_host),
        flags           = 0,
      )
      g.db.Insert(useragent)
      g.db.Commit()
    else:
      useragent = useragent[0]
      
    lang  = req.accept_languages.best_match(g.siteconfig['languages'])
    
    if not lang:
      lang  = 'en'
      
    langcodes = list(g.request.accept_languages.values())
    
    if not langcodes:
      langcodes = ['en']
      
    self.Set(
      userid        = 0,
      useragentid   = useragent.id,
      ip            = ClientIP(g),
      lang          = lang,
      langcodes     = langcodes,
      length        = 0,
      starttime     = currenttime,
      endtime       = currenttime,
      flags         = 0,
      urls          = [],
      data          = {
        'texttheme':    'default',
        'wallpaper':    'blue',
        'usericon':     '',
        'groups':       [],
      },
    )
    
    g.db.Insert(self)
    g.db.Commit()
    
  # -------------------------------------------------------------------
  def __getitem__(self, key):
    """Convenience functions to allow the session object to be used as 
    a dict that saves settings at the end of the request.
    """
    
    if key in self.data:
      return self.data[key]
    
    return None
  
  # -------------------------------------------------------------------
  def __setitem__(self, key, value):
    """Convenience functions to allow the session object to be used as 
    a dict that saves settings at the end of the request.
    """    
    self.data[key]  = value
    
    self.UpdateFields('data')
    
  # -------------------------------------------------------------------
  def __delitem__(self, key):
    """Convenience functions to allow the session object to be used as 
    a dict that saves settings at the end of the script.
    """
    if key in self.data:
      del self.data[key]
      
    self.UpdateFields('data')
      
  # -------------------------------------------------------------------
  def get(self, key, defaultvalue = None):
    """Convenience functions to allow the session object to be used as 
    a dict that saves settings at the end of the script.
    """
    return self.data.get(key, defaultvalue)
