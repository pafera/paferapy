#!/usr/bin/python
# -*- coding: utf-8 -*- 

import base64
import os
import datetime
import importlib
import random
import struct
import time

from traceback import print_exc
from urllib.parse import quote as EncodeURL

from flask import g

# =====================================================================
def ListToDict(*args):
  """Converts a list of arguments into a dict. Useful for the 
  translation API.
  """
  d = {}
  
  for i in range(0, len(args)):
    d[args[i]]  = d[args[i + 1]]
    
  return d

# =====================================================================
def MakeRID():
  """Returns a random 32-bit int ID.
  """
  return random.randint(-2147483648, 2147483647)

# =====================================================================
def MakeRBID():
  """Returns a random 64-bit int ID.
  """  
  return random.randint(-9223372036854775808, 9223372036854775807)

# =====================================================================
def Bound(val, min, max):
  """Returns a value that is no smaller than min or larger than max.
  """
  if val < min:
    val = min
    
  if val > max:
    val = max
    
  return val

# =====================================================================
def ToShortCode(
  val, 
  chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_'
):
  """Turns a 32-bit value into a six character alphanumeric code. Useful
  for shortening JSON data or use in URLs.
  
  Thanks to http://programanddesign.com/php/base62-encode/
  """
  val  = int(val) + 2147483648
  
  base = len(chars)
  code  = ''
  
  while True:
    mod = int(val % base)
    code = chars[mod] + code
    val = (val - mod) / base
    
    if val <= 0:
      break
    
  while len(code) < 6:
    code  = '0' + code
  
  return code

# =====================================================================
def FromShortCode(
  code, 
  chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_'
):
  """Turns a six character alphanumeric code into a 32-bit value. It's 
  the reverse of ToShortCode()
  """
  base  = len(chars)
  
  l     = len(code)
  val   = 0;

  chars = [c for c in chars]
  arr   = {}

  for i in range(0, base):
    arr[chars[i]] = i

  for i in range(0, l):
    val = val + (arr[code[i]] * (base ** (l - i - 1)))

  val  = val - 2147483648

  return val

# =====================================================================
def ToLongCode(
  val, 
  chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_'
):
  """The same as ToShortCode(), but for 64 bit integers instead of 32-bit
  integers.
  """
  packed      = struct.pack('q', val)
  val1, val2  = struct.unpack('ii', packed)
  
  return ToShortCode(val1) + ToShortCode(val2)

# =====================================================================
def FromLongCode(
  val, 
  chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_'
):
  """The same as FromShortCode(), but for 64 bit integers instead of 32-bit
  integers.
  """
  packed  = struct.pack('ii', FromShortCode(val[:6]), FromShortCode(val[6:]))
  val     = struct.unpack('q', packed)[0]
  
  return val

# =====================================================================
def FromCode(
  val, 
  chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_'
):
  """Calls the appropiate code to int function depending on the length of 
  val.
  """
  if len(val) == 6:
    return FromShortCode(val, chars)
  
  return FromLongCode(val, chars)

# =====================================================================
def CodeDir(code):
  """Separates a filename into three character segments with the 
  directory separator '/' in between. Useful for avoiding having
  millions of small files inside of the same directory, which is known
  to severely degrade performance on some filesystems.
  """
  ls	=	[]
  i   = 0
  l   = len(code)
  
  while i < l:
    ls.append(code[i:i + 3])
    i += 3

  return '/'.join(ls)

# =====================================================================
def RIDDir(rid, extension):
  """Equivalent to CodeDir(ToShortCode(rid)) + extension
  """
  return CodeDir(ToShortCode(rid)) + extension

# =====================================================================
def FromBinary(value):
  """Converts a binary string to JSON acceptable encoding.
  """
  return base64.b64encode(value).decode('utf-8')

# =====================================================================
def ToBinary(value):
  """Converts a converted binary string back to its binary form.
  """
  return base64.b64decode(value.encode('utf-8'))

# =====================================================================
def BestTranslation(translations, languages, defaultlang = 'en'):
  """Returns the best translation found in a dict of different
  translations. 
  
  languages should be a list of of ISO 639-1 language codes.
  
  defaultlang is set to 'en', so be sure to set it to your native
  language if used on other systems.
  """
  if not translations:
    return '--'
  
  if not isinstance(translations, dict):
    raise Exception('Translations must be a dict!')
  
  if not isinstance(languages, list):
    languages = [languages]
  
  for l in languages:
    if l in translations and translations[l]:
      return translations[l]
    
  if defaultlang in translations:
    return translations[defaultlang]
  
  return translations[list(translations.keys())[0]]

# =====================================================================
def LoadTranslation(g, app, translation, langcodes = 0):
  """Load the translation into the global language dict in g.T
  
  languages should be a list of of ISO 639-1 language codes.
  
  'en' will always be the last language code on the list.
  """
  T = g.T
  
  if langcodes:
    languages = langcodes
  else:
    languages = [g.session.lang] + g.session.langcodes
    
  if not isinstance(languages, list):
    languages = [languages]
  
  for lang in languages:
    try:
      with open(f'public/{app}/translations/{lang}/{translation}.js', 'r') as f:
        exec(f.read())        
        
      return lang
    except Exception as e:
      print('>>> Problem loading translation: ', e)
    
  raise Exception(f"Translation {app}/{translation} was not found for {languages}")

# =====================================================================
def GetModel(model):
  """Returns the model object from the correct module.
  """
  if not model:
    raise Exception('GetModel: Missing model name')
  
  parts = model.split('_')
  
  if len(parts) != 2:
    raise Exception(f"GetModel: {model} doesn't look like a valid model name.")
  
  module  = importlib.import_module('apps.' + parts[0])
  
  if model not in dir(module):
    raise Exception(f"GetModel: Model {model} not found in module apps.{parts[0]}")
  
  return getattr(module, model)

# =====================================================================
def b64decodepad(s):
  """Helper function to ensure that a b64 string is correctly padded to 
  four spaces.
  """
  
  #if not isinstance(s, bytes):
  #  s = s.encode('utf-8')
  
  missing_padding = len(s) % 4
  
  if missing_padding:
    s += '=' * (4 - missing_padding)
  
  return base64.b64decode(s)

# =====================================================================
def EscapeSQL(s):
  """Escapes special characters to be used in a SQL query string. 
  
  Taken from mysql-connector's version.
  """
  ls  = [];
  
  for c in s:
    if c == '\\n':      
      ls.append('\\\\n')
    elif c == '\\r':
      ls.append('\\\\r')
    elif c == '\\':
      ls.append('\\\\')
    elif c == "'":
      ls.append("\\'")
    elif c == '"': 
      ls.append('\\"')
    elif c == '\u00a5' or c == '\u20a9':
      #escape characters interpreted as backslash by mysql
      #fall through
      pass
    else:
      ls.append(c)
  
  return ''.join(ls)

# =====================================================================
def KeyFromValue(ls, value):
  """Does a reverse lookup to find the key of a value in a dict.
  """
  return ls.keys()[ls.values().index(value)]

# =====================================================================
def IsAdmin():
  """Determines if the current user is an administrator. The session
  must have already been initialized.
  """
  return g.session.userflags & 0x20

# =====================================================================
def CanUpload():
  """Determines if the current user can upload files. The session
  must have already been initialized.
  """
  return IsAdmin() or g.session.userflags & 0x40

# =====================================================================
def CanUploadOriginals():
  """Determines if the current user can upload files without conversion. 
  The session must have already been initialized.
  """
  return IsAdmin() or g.session.userflags & 0x80

# =====================================================================
def CanPostMessages():
  """Determines if the current user can post messages. 
  The session must have already been initialized.
  """
  return IsAdmin() or g.session.userflags & 0x100

# =====================================================================
def ClientIP(g):
  """Returns the IP address of the browser connecting to the server.
  """
  req = g.request
  
  return req.environ['HTTP_X_FORWARDED_FOR'] if req.environ.get('HTTP_X_FORWARDED_FOR') else req.environ['REMOTE_ADDR']

# =====================================================================
def RemoveOlderFiles(dirname, seconds, cleanprobability = 100):
  """Remove files older than seconds from the directory. Handy for
  cleaning up upload directories.
  
  This function will create dirname if it doesn't already exist.
  
  cleanprobability is a percentage for the chance of deletion whenever
  this function is called. The default of 100 means that this function
  will always delete files, while lesser values will increase the
  probability of this function doing nothing as to lesson system load.
  """
  os.makedirs(dirname, exist_ok = True)
    
  if random.randint(0, 10000) < (cleanprobability * 100):
    currenttime = time.time()
    
    for r in os.listdir(dirname):
      fullpath  = dirname + '/' + r
      
      if os.stat(fullpath).st_mtime + seconds < currenttime:
        os.remove(fullpath)
