#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# Standard Pafera app functions called when the app is added or 
# removed to the system.

from apps.system import *

# *********************************************************************
def OnAppInstall(g):
  db  = g.db
  
  needcommit  = 0
  
  adminuser = g.db.Find(
    system_user, 
    'WHERE phonenumber = ? AND place = ?',
    [
      'admin',
      'pafera'
    ]
  )[0]
  
  for r in [
      ['mpmcateditors', 'MPM Cat Editors'],
    ]:
    
    group   = db.Find(system_group, 'WHERE groupname = ?', r[0])
    
    if not group:
      newgroup  = system_group()
      newgroup.Set(
        displayname = {'en': r[1]},
        groupname   = r[0],
      )
      db.Save(newgroup)
      db.Link(adminuser, newgroup)
      needcommit  = 1    
  
  headerid  = g.db.FindOne(
    system_pagefragment, 
    'WHERE path = ?', 
    'Site Header',
    fields = 'rid',
  ).rid
  
  footerid  = g.db.FindOne(
    system_pagefragment, 
    'WHERE path = ?', 
    'Site Footer',
    fields = 'rid',
  ).rid
  
  for page in [
{
  "content": {
    "en": "<div class=\"PageLoadContent\"></div>"
  },
  "jsfiles": [
    "/system/paferalist.js",
  ],
  "path": "/mpmcats/index.html",
  "pyfile": "",
  "requiredgroups": [
  ],
  "title": {
    "en": "MPM Cats",
  },
  "translations": [
  ],
},
    ]:
    if not db.FindOne(system_page, 'WHERE path = ?', page['path']):
      newpage  = system_page()
      
      newpage.Set(
        headerid  = headerid,
        footerid  = footerid,
      )
        
      newpage.Set(**page)
      db.Save(newpage)
      needcommit  = 1
    
  if needcommit:
    db.Commit()
  
# *********************************************************************
def OnAppDelete(g):
  db  = g.db
  
  needcommit  = 0
  
  for r in [
      'mpmcateditors',
    ]:
    
    group   = db.Find(system_group, 'WHERE groupname = ?', r)
    
    if group:
      db.Delete(group[0])
      needcommit  = 1
  
  for r in db.Find(mpmcats_cat):
    g.db.Delete(r)
    needcommit  = 1
  
  if needcommit:
    db.Commit()
 
