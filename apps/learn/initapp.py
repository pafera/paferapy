#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# Standard Pafera app functions called when the app is added or 
# removed to the system.

from apps.system import *

# *********************************************************************
def OnAppInstall(g):
  db  = g.db
  
  needcommit  = 0
  
  adminuser = g.db.FindOne(
    system_user, 
    'WHERE phonenumber = ? AND place = ?',
    [
      'admin',
      'pafera'
    ]
  )
  
  for r in [
      ['students', 'Students'],
      ['teachers', 'Teachers'],
      ['school.administrators', 'School Administrators'],
    ]:
    
    group   = db.FindOne(system_group, 'WHERE groupname = ?', r[0])
    
    if not group:
      group  = system_group()
      group.Set(
        displayname = {'en': r[1]},
        groupname   = r[0],
      )
      db.Save(group)
      
    db.Link(adminuser, group)
    needcommit  = 1    
  
  headerid  = g.db.FindOne(
    system_pagefragment, 
    'WHERE path = ?', 
    'Site Header',
    fields = 'rid',
  ).rid
  
  footerid  = g.db.FindOne(
    system_pagefragment, 
    'WHERE path = ?', 
    'Site Footer',
    fields = 'rid',
  ).rid
  
  for page in [
{
  "content": {
    "en": "<div class=\"PageContent\"></div>"
  },
  "jsfiles": [
    "/system/paferalist.js",
    "/system/paferachooser.js",
    "/system/paferafilechooser.js",
    "/learn/renderers.js"
  ],
  "path": "/learn/manage.html",
  "pyfile": "",
  "requiredgroups": [
    'teachers',
    'school.administrators',
    'school.system.administrators'
  ],
  "title": {
    "en": "Manage Learning"
  },
  "translations": [
    'learn/default',
    'learn/challenges',
    'learn/admin',
  ],
},
{
  "content": {
    "en": ""
  },
  "jsfiles": [
    "/system/paferalist.js",
    "/system/paferachooser.js",
    "/system/paferafilechooser.js",
    "/learn/renderers.js"
  ],
  "path": "/learn/home.html",
  "pyfile": "apps.learn.homefunc",
  "requiredgroups": [
    "students",
    "teachers"
  ],
  "title": {
    "en": "Learning"
  },
  "translations": [
    'learn/default',
    'learn/admin',
    'learn/challenges',
  ],
},
{
  "content": {
    "en": "<div class=\"PageLoadContent\"></div>"
  },
  "jsfiles": [
    "/system/paferalist.js",
    "/system/paferachooser.js",
    "/system/paferafilechooser.js",
    "/libs/opusrecorder/recorder.min.js",
    "/learn/renderers.js",
    "/learn/voicerecognition.js"
  ],
  "path": "/learn/challenges.html",
  "pyfile": "apps.learn.challengesfunc",
  "requiredgroups": [
    "students",
    "teachers"
  ],
  "title": {
    "en": "Challenges"
  },
  "translations": [
    'learn/default',
    'learn/challenges'
  ],
},
{
  "content": {
    "en": "<div class=\"PageLoadContent\"></div>",
    "zh": ""
  },
  "jsfiles": [
    "/system/paferalist.js",
    "/system/paferachooser.js",
    "/learn/studentbar.js",
    "/learn/chooseproblems.js"
  ],
  "path": "/learn/classroom.html",
  "pyfile": "",
  "requiredgroups": [
    "students",
    "teachers"
  ],
  "title": {
    "en": "Classroom",
    "zh": ""
  },
  "translations": [
    'learn/default',
    'learn/challenges'
  ],
},
{
  "content": {
    "en": "<div class=\"PageContent\"></div>"
  },
  "jsfiles": [
    "/system/paferalist.js",
    "/system/paferachooser.js",
    "/libs/opusrecorder/recorder.min.js",
    "/learn/renderers.js",
    "/learn/voicerecognition.js",
    "/learn/chooseproblems.js",
    "/learn/studentbar.js",
    "/libs/phaser/phaser.min.js",
    "/phaser/phasergame.js"
  ],
  "path": "/learn/teacherclassroom.html",
  "pyfile": "",
  "requiredgroups": [
    "teachers"
  ],
  "title": {
    "en": "Teacher's Classroom Display",
    "zh": ""
  },
  "translations": [
    'learn/default',
    'learn/challenges',
  ],
},
{
  "content": {
    "en": ""
  },
  "jsfiles": [
    "/system/paferalist.js",
    "/system/paferachooser.js",
    "/libs/opusrecorder/recorder.min.js",
    "/learn/renderers.js",
    "/learn/voicerecognition.js"
  ],
  "path": "/learn/courses.html",
  "pyfile": "apps.learn.coursesfunc",
  "requiredgroups": [
    "students",
    "teachers"
  ],
  "title": {
    "en": "Courses"
  },
  "translations": [
    'learn/default',
    'learn/challenges',
    'learn/admin',
  ],
},
{
  "content": {
    "en": '''<h1>Pronounciation of Common English Words</h1>
      <div class="PageLoadContent"></div>'''
  },
  "jsfiles": [
    "/libs/opusrecorder/recorder.min.js",
    "/learn/voicerecognition.js"
  ],
  "path": "/learn/commonwords.html",
  "pyfile": "",
  "requiredgroups": [
    "students",
    "teachers"
  ],
  "title": {
    "en": "Pronounciation of Common English Words"
  },
  "translations": [
    'learn/default',
    'learn/challenges',
  ],
},
{
  "content": {
    "en": '''<div class="PageLoadContent"></div>'''
  },
  "jsfiles": [
    "/libs/opusrecorder/recorder.min.js",
    "/learn/voicerecognition.js"
  ],
  "path": "/learn/phonicsreview.html",
  "pyfile": "",
  "requiredgroups": [
    "students",
    "teachers"
  ],
  "title": {
    "en": "English Phonics"
  },
  "translations": [
    'learn/default',
    'learn/challenges',
  ],
},
{
  "content": {
    "en": '''<div class="PageLoadContent"></div>'''
  },
  "jsfiles": [
    "/libs/opusrecorder/recorder.min.js",
    "/learn/voicerecognition.js"
  ],
  "path": "/learn/phonicssyllables.html",
  "pyfile": "",
  "requiredgroups": [
    "students",
    "teachers"
  ],
  "title": {
    "en": "English Syllables"
  },
  "translations": [
    'learn/default',
    'learn/challenges',
  ],
},
    ]:
    if not db.Find(system_page, 'WHERE path = ?', page['path']):
      newpage  = system_page()
      
      if page['path'] != "/learn/teacherclassroom.html":
        newpage.Set(
          headerid  = headerid,
          footerid  = footerid,
        )
        
      newpage.Set(**page)
      db.Save(newpage)
      needcommit  = 1
    
  if needcommit:
    db.Commit()
  
# *********************************************************************
def OnAppDelete(g):
  db  = g.db
  
  needcommit  = 0
  
  groupstodelete  = [
    'students',
    'teachers',
    'school.administrators',
    'school.system.administrators',
  ]
    
  for r in db.Find(
      system_group, 
      'WHERE groupname IN (' + ', '.join(['?' for x in groupstodelete]) + ')', 
      groupstodelete
    ):
    db.Delete(group[0])
    needcommit  = 1
  
  for r in db.Find(system_page, 'WHERE path LIKE ?', '/learn/%'):
    g.db.Delete(r)
    needcommit  = 1
      
  if needcommit:
    db.Commit()
