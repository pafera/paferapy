#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system import *

from apps.learn.schoolclass import *

# Card flags
CARD_COURSE               = 0x1
CARD_UNIT                 = 0x2
CARD_LESSON               = 0x4
CARD_ASIDE                = 0x8
CARD_PROBLEM              = 0x10
CARD_ANSWER               = 0x20
CARD_PRIVATE              = 0x40
CARD_PROTECTED            = 0x80
CARD_PUBLIC               = 0x100
CARD_ALWAYS_SHOW_CONTENT  = 0x200

CARD_SECURITY           = CARD_PUBLIC | CARD_PROTECTED | CARD_PRIVATE

# Link types
CARD_CHILD              =  0x1

# Problem link types. Each type will be used only for their 
# respective tests.
CARD_LINK_PROBLEM       =  0x1
CARD_LINK_QUIZ_PROBLEM  =  0x2
CARD_LINK_TEST_PROBLEM  =  0x3
CARD_LINK_EXAM_PROBLEM  =  0x4

CARD_TYPES  = {
  1:  'English',
  2:  'Chinese',
  3:  'Russian',
  4:  'Math',
}

# *********************************************************************
class learn_card(ModelBase):
  """Represents a flashcard, which is the basic unit that the Pafera
  Learning System is based upon. 
  
  A card can contain text, images, sounds, videos, markdown, HTML, or 
  can serve as containers for other cards. Courses and lessons are 
  all individual cards themselves, whereas problems are simply 
  defining a relationship between various cards.
  
  Each card has one owner which can edit the contents of the card, but 
  you can assign multiple editors to help. In the case that someone 
  changes a card but is not an owner or editor, the system will use 
  a copy-on-change scheme to give them their own copy of the card  
  without changing the original card.
  
  Cards can have permissions like files. Public means that everyone
  can use the card. Protected means that only users in allowedusers
  can use the card, while private means that only the owner of the
  card can use it.
  """
  
  _dbfields     = {
    'rid':            ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'cardtype':       ('INT', 'NOT NULL',),
    'image':          ('IMAGEFILE', "NOT NULL DEFAULT ''",),
    'sound':          ('SOUNDFILE', "NOT NULL DEFAULT ''",),
    'video':          ('VIDEOFILE', "NOT NULL DEFAULT ''",),
    'title':          ('TEXT', "NOT NULL DEFAULT ''",),
    'description':    ('TEXT', "NOT NULL DEFAULT ''",),
    'content':        ('MULTITEXT', "NOT NULL DEFAULT ''",),
    'markdown':       ('MULTITEXT', "NOT NULL DEFAULT ''",),
    'html':           ('MULTITEXT', "NOT NULL DEFAULT ''",),
    'ownerid':        ('INT', 'NOT NULL',),
    'editors':        ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'allowedusers':   ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'allowedclasses': ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'flags':          ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = [
    'learn_card', 
    'learn_problem',
    'learn_school',
    'learn_schoolclass',
    'learn_student',
    'learn_teacher',
  ]
  _dbdisplay  = ['title', 'content']
  _dbflags    = DB_TRACK_CHANGES | DB_TRACK_VALUES
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
# =====================================================================
def UpdateAllowedCards(g, obj):
  """In order to facilitate teachers being able to create and sell 
  courses on our website, we check which cards a student is able to 
  see and deny access to anyone who has not joined the proper course.
  
  This function updates allowed cards for teachers and students.
  """
  cardids = []
  
  for r in g.db.Linked(obj, learn_schoolclass, fields = 'rid'):
    for course in g.db.Linked(r, learn_card, fields = 'rid'):
      cardids.append(ToShortCode(course.rid))
      
      cardids.extend(GetAllCards(g, course))
  
  # Take out repeated cards to save space
  cardids = sorted(set(cardids))
  
  obj.Set(allowedcards  = cardids)
  
  g.db.Update(obj)
  
# =====================================================================
def GetAllCards(g, card):
  """This function returns all child cards given a parent card.
  """
  cardids = []  
  
  for r in g.db.Linked(card, learn_card, CARD_CHILD, fields = 'rid, flags'):
    
    cardids.append(ToShortCode(r.rid))
    
    if not (r.flags & CARD_LESSON):
      cardids.extend(GetAllCards(g, r))
      
  return cardids

# =====================================================================
def GetCardTitles(g, lessonids):
  """This is a convenience function to get all titles for a given set 
  of cardids. Useful for selecting child cards.
  """
  lessontitles  = {}
  
  for r in g.db.Find(
      learn_card,
      'WHERE rid IN (' + ', '.join(['?' for x in lessonids]) + ')',
      lessonids,
      fields    = 'rid, title'
    ):
    lessontitles[ r.rid ]  = r.title
  
  return lessontitles
        
