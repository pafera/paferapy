#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system import *

from apps.learn.schoolclass import *

STUDENT_ACCEPTED  = 0x0
STUDENT_APPLIED   = 0x1

# *********************************************************************
class learn_student(ModelBase):
  """This class keeps track of student information, purchased items,
  and allowed cards.
  """
  
  _dbfields     = {
    'userid':             ('INTEGER', 'PRIMARY KEY NOT NULL',),
    'displayname':        ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'profile':            ('TRANSLATION', "NOT NULL DEFAULT ''",),
    'phonenumber':        ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'email':              ('TRANSLATION', "NOT NULL DEFAULT ''",),
    'grade':              ('TRANSLATION', "NOT NULL DEFAULT ''", ),
    'coins':              ('INT', 'NOT NULL DEFAULT 0',),
    'items':              ('DICT', "NOT NULL DEFAULT ''",),
    'allowedcards':       ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'flags':              ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = [
    'learn_card',
    'learn_schoolclass', 
    'learn_school', 
    'learn_teacher',
  ]
  _dbdisplay  = ['displayname', 'phonenumber', 'email']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()

  # -------------------------------------------------------------------
  def OnDBDelete(self, db):
    '''Standard handler for deleting an existing object.
    '''
    if not self.rid:
      return
    
    for r in db.Linked(self, learn_schoolclass):
      db.Unlink(self, r)
