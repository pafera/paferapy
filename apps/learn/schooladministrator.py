#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system import *

import apps.learn.school

# *********************************************************************
class learn_schooladministrator(ModelBase):
  
  _dbfields     = {
    'userid':             ('INTEGER', 'PRIMARY KEY NOT NULL',),
    'displayname':        ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'profile':            ('TRANSLATION', "NOT NULL DEFAULT ''",),
    'phonenumber':        ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'email':              ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'contactinfo':        ('TRANSLATION', "NOT NULL DEFAULT 0",),
    'flags':              ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = ['learn_school']
  _dbdisplay  = ['displayname', 'profile', 'phone', 'email']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def OnDBDelete(self, db):
    '''Standard handler for deleting an existing object.
    '''
    if not self.rid:
      return
    
    for r in db.Linked(self, apps.learn.school.learn_school):
      db.Unlink(self, r)
