#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system import *

from apps.learn.schoolclass import *

TEACHER_ACCEPTED  = 0x0
TEACHER_APPLIED   = 0x1

# *********************************************************************
class learn_teacher(ModelBase):
  """Contains teacher contact information and allowed cards.
  """
  
  _dbfields     = {
    'userid':             ('INTEGER', 'PRIMARY KEY NOT NULL',),
    'displayname':        ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'profile':            ('TRANSLATION', "NOT NULL DEFAULT ''", ),
    'phonenumber':        ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'email':              ('TRANSLATION', 'NOT NULL', ),
    'contactinfo':        ('TRANSLATION', "NOT NULL DEFAULT ''",),
    'allowedcards':       ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'numclasses':         ('INT', 'NOT NULL DEFAULT 0',),
    'numstudents':        ('INT', 'NOT NULL DEFAULT 0',),
    'flags':              ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = [
    'learn_schoolclass', 
    'learn_school', 
    'learn_student',
    'learn_card',
  ]
  _dbdisplay  = ['displayname', 'profile', 'phonenumber', 'email']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
  
  # -------------------------------------------------------------------
  def OnDBDelete(self, db):
    '''Standard handler for deleting an existing object.
    '''
    if not self.rid:
      return
    
    for r in db.Linked(self, learn_schoolclass):
      db.Unlink(self, r)
