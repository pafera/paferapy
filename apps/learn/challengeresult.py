#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system import *

# Result flags
CHALLENGERESULT_UNFINISHED  = 0x01
CHALLENGERESULT_FINISHED    = 0x02
CHALLENGERESULT_PASSED      = 0x04

# *********************************************************************
class learn_challengeresult(ModelBase):
  """This class represents an attempt by a student to finish a 
  challenge. It contains their scores, individual results for each 
  problem, and any answers or files submitted.
  """
  
  _dbfields     = {
    'rid':                ('INTEGER', 'PRIMARY KEY NOT NULL',),
    'userid':             ('INT', 'NOT NULL',),
    'classid':            ('INT', 'NOT NULL',),
    'challengetype':      ('INT16', 'NOT NULL',),
    'timeused':           ('INT16', 'NOT NULL'),
    'starttime':          ('TIMESTAMP', 'NOT NULL',),
    'endtime':            ('TIMESTAMP', 'NOT NULL',),
    'challenge':          ('TEXT', 'NOT NULL',),
    'challengeid':        ('INT', 'NOT NULL',),
    'problemresults':     ('LIST', "NOT NULL DEFAULT ''",),
    'answer':             ('TEXT', "NOT NULL DEFAULT ''",),
    'lessonids':          ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'problemids':         ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'files':              ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'score':              ('INT16', 'NOT NULL DEFAULT 0',),
    'maximumscore':       ('INT16', 'NOT NULL DEFAULT 0',),
    'percentright':       ('INT16', 'NOT NULL DEFAULT 0',),
    'scoreadded':         ('INT16', 'NOT NULL DEFAULT 0',),
    'flags':              ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = [
  ]
  _dbdisplay  = ['challenge', 'challengeid', 'lessonids', 'score', 'percentright']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()

