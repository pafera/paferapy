#!/usr/bin/python
# -*- coding: utf-8 -*- 

import time

from apps.system import *

import apps.learn.card
import apps.learn.problem

# *********************************************************************
class learn_studylist(ModelBase):
  """The studylist contains every problem that a student needs to know
  and keeps track of how fast the student answers them. Problems that 
  require more time or were answered incorrectly will be repeated more 
  often so that the student can get a better grasp on them.
  """
  
  _dbfields     = {
    'rid':          ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'classid':      ('INT', 'NOT NULL', BlankValidator()),
    'userid':       ('INT', 'NOT NULL', ),
    'problemid':    ('INT', 'NOT NULL', ),
    'score':        ('INT', 'NOT NULL DEFAULT 0',),
    'results':      ('DICT', "NOT NULL DEFAULT ''",),
    'flags':        ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = []
  _dbdisplay  = ['classid', 'userid', 'courseid', 'problemid', 'reviewscore']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def GetProblems(self, g, numproblems = 6, orderby = 'score'):
    problemids  = [
      x.problemid
      for x in g.db.Find(
        learn_studylist,
        'WHERE classid = ? AND userid = ?',
        [
          self.classid,
          self.userid,
        ],
        fields    = 'problemid',
        orderby   = orderby,
        limit     = numproblems,
      )
    ]
    
    return apps.learn.problem.GetAllProblems(g, [], apps.learn.card.CARD_LINK_PROBLEM, problemids)
    
