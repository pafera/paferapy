#!/usr/bin/python
# -*- coding: utf-8 -*- 

import time
import datetime
from pprint import pprint

from apps.system import *

import apps.learn.challenge
import apps.learn.schoolclass

# Flag constants
ANSWER_LOCKED             = 0x01
ANSWER_ANSWERED           = 0x02
ANSWER_ENABLE_COPY_PASTE  = 0x04
ANSWER_TEACHER            = 0x08

# *********************************************************************
class learn_answer(ModelBase):
  """This class stores submitted student answers to show in the 
  classroom and keeps track of their scores. It has facilities for
  showing different prompts, putting students into groups, and 
  purchasing helpful items.
  """
  
  _dbfields     = {
    'rid':          ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'classid':      ('INT', 'NOT NULL DEFAULT 0',),
    'userid':       ('INT', 'NOT NULL', BlankValidator()),
    'numright':     ('INT16', 'NOT NULL DEFAULT 0',),
    'numwrong':     ('INT16', 'NOT NULL DEFAULT 0',),
    'numstrikes':   ('INT16', 'NOT NULL DEFAULT 0',),
    'scoreadded':   ('INT16', 'NOT NULL DEFAULT 0',),
    'groupnum':     ('INT16', 'NOT NULL DEFAULT 0',),
    'bonusscore':   ('INT16', 'NOT NULL DEFAULT 0',),
    'score':        ('INT', 'NOT NULL DEFAULT 0',),
    'displayname':  ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'answer':       ('MULTITEXT', "NOT NULL DEFAULT ''",),
    'prompt':       ('TEXT', "NOT NULL DEFAULT ''",),
    'promptdata':   ('DICT', "NOT NULL DEFAULT ''",),
    'items':        ('DICT', "NOT NULL DEFAULT ''",),
    'coderesult':   ('DICT', "NOT NULL DEFAULT ''",),
    'choices':      ('DICT', "NOT NULL DEFAULT ''",),
    'flags':        ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = []
  _dbdisplay  = ['classid', 'displayname', 'userid', 'numright', 'numwrong', 'numstrikes', 'answer']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def UpdateScore(self):
    """Recalculates the current score. Be sure to call db.Commit()
    after you finish all calculations, or the updates won't be saved
    into the database.
    """
    newscore  = self.numright - self.numwrong + self.bonusscore
    
    if self.numstrikes:
      newscore  -= 2 ** self.numstrikes
    
    self.Set(
      scoreadded  = newscore - self.score,
      score       = newscore,
    )
    
  # -------------------------------------------------------------------
  def AddHomeworkScores(self, g):
    """Adds up all of the homework scores from the last time that 
    class was in session and applies them to the current answer.
    This is automatically called by /learn/answerapi when a new 
    answer is created.
    """
    cls = g.db.Load(apps.learn.schoolclass.learn_schoolclass, self.classid, fields = 'flags')
    
    if not cls.flags & apps.learn.schoolclass.CLASS_ADD_HOMEWORK_SCORES:
      return
    
    currenttime = time.time()
    
    useridcode    = ToShortCode(self.userid)
    homeworkscore = 0
    
    for r in g.db.Find(
      apps.learn.challenge.learn_challenge,
      'WHERE classid = ? AND endtime < ?',
      [
        self.classid,
        currenttime,
      ],
      fields    = 'endtime, challengetype, results',
      orderby   = 'endtime DESC',
    ):
      if (r.challengetype == apps.learn.challenge.CHALLENGE_CLASSPARTICIPATION
          and r.results
          and useridcode in r.results
        ):
        break
      
      if r.results and useridcode in r.results:
        homeworkscore += r.results[useridcode][4]
        
    self.Set(
      bonusscore  = self.bonusscore + homeworkscore,
    )
    
    self.UpdateScore()
    

