#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system import *

# *********************************************************************
class learn_classgrade(ModelBase):
  """The Pafera Learning System is designed to separate student 
  performance into class participation grades, review grades, and 
  persistence grades. This class tracks these grades for each student
  to be easily compared.
  """
  
  _dbfields     = {
    'rid':                ('INTEGER', 'PRIMARY KEY NOT NULL',),
    'userid':             ('INT', 'NOT NULL', BlankValidator()),
    'classid':            ('INT', 'NOT NULL', BlankValidator()),
    'classscore':         ('INT', 'NOT NULL DEFAULT 0',),
    'reviewscore':        ('INT', 'NOT NULL DEFAULT 0',),
    'persistencescore':   ('INT', 'NOT NULL DEFAULT 0',),
    'finalscore':         ('INT', 'NOT NULL DEFAULT 0',),
    'numhomework':        ('INT16', 'NOT NULL DEFAULT 0',),
    'homeworkcompleted':  ('INT16', 'NOT NULL DEFAULT 0',),
    'classpercent':       ('INT16', 'NOT NULL DEFAULT 0',),
    'reviewpercent':      ('INT16', 'NOT NULL DEFAULT 0',),
    'persistencepercent': ('INT16', 'NOT NULL DEFAULT 0',),
    'flags':              ('INT16', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = [
  ]
  _dbdisplay  = ['userid', 'classid', 'finalscore']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
