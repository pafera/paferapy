#!/usr/bin/python
# -*- coding: utf-8 -*- 

from pprint import pprint

from apps.system import *

from apps.learn.card import *

# Flag constants
PROBLEM_REQUEST_REVIEW  = 0x01

PROBLEM_CARD_FIELDS = [
  'problem',
  'answer',
  'explanation',
  'choice1',
  'choice2',
  'choice3',
  'choice4',
  'choice5',
  'choice6',
  'choice7',
]

# *********************************************************************
class learn_problem(ModelBase):
  """A problem in the Pafera Learning System is defined as a 
  collection of cards. The two required cards are a problem card and 
  an answer card, but cards can also be added to explain how to do the 
  problem or explicitly define choices for multiple choice challenges.
  
  Like cards, a problem also has owners and editors, and will be 
  copied if someone does not have permission to change the original card.
  """
  
  _dbfields     = {
    'rid':            ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'problemid':      ('INT', 'NOT NULL'),
    'answerid':       ('INT', 'NOT NULL'),
    'explanationid':  ('INT', 'NOT NULL DEFAULT 0'),
    'choice1id':      ('INT', 'NOT NULL DEFAULT 0'),
    'choice2id':      ('INT', 'NOT NULL DEFAULT 0'),
    'choice3id':      ('INT', 'NOT NULL DEFAULT 0'),
    'choice4id':      ('INT', 'NOT NULL DEFAULT 0'),
    'choice5id':      ('INT', 'NOT NULL DEFAULT 0'),
    'choice6id':      ('INT', 'NOT NULL DEFAULT 0'),
    'choice7id':      ('INT', 'NOT NULL DEFAULT 0'),
    'timelimit':      ('INT16', 'NOT NULL DEFAULT 0'),
    'points':         ('INT16', 'NOT NULL DEFAULT 0'),
    'ownerid':        ('INT', 'NOT NULL',),
    'editors':        ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'flags':          ('INT', 'NOT NULL DEFAULT 0'),
  }
  _dbindexes  = ()
  _dblinks    = ['learn_card']
  _dbdisplay  = ['problemid', 'answerid']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()

# =====================================================================
def GetAllProblems(
    g, 
    cardids, 
    problemtype = CARD_LINK_PROBLEM, 
    extraproblemids = []
  ):
  """This function returns a list of problems as dicts given lesson ids
  or explicit problem ids.
  """
  
  problems        = []
  problemcardids  = []
  
  if not isinstance(cardids, list):
    cardids = [cardids]
  
  cardids = list(set(cardids))
  
  for cardid in cardids:
    c  = g.db.Load(learn_card, cardid, 'rid')
    
    GetChildProblems(g, c, problems, problemcardids, problemtype)
  
  if extraproblemids:
    extraproblemids = list(set(extraproblemids))
    
    for r in g.db.LoadMany(
      learn_problem,
      [x for x in extraproblemids],
    ):
      o = {}
      
      o['idcode']         = ToShortCode(r.rid)
      o['timelimit']      = r.timelimit
      o['points']         = r.points
      o['flags']          = r.flags
      
      for cardfield in PROBLEM_CARD_FIELDS:
        attrname    = cardfield + 'id'
        idcodename  = attrname + 'code'
        
        attrvalue   = getattr(r, attrname, 0)
        
        if attrvalue:
          o[idcodename] = ToShortCode(attrvalue)
      
      problems.append(o)
      
      problemcardids.append(r.problemid)
      problemcardids.append(r.answerid)      
  
  cards = {}
  
  if len(problemcardids) == 0:
    return []
  
  problemcardids  = list(set(problemcardids))
  
  for r in g.db.LoadMany(
      learn_card, 
      problemcardids,
      'rid, cardtype, content, image, sound, video, flags',
    ):
    o = r.ToJSON('cardtype, content, image, sound, video, flags')
    
    o['idcode'] = ToShortCode(r.rid)
    
    cards[o['idcode']]  = o
  
  intactproblems  = []
  
  for p in problems:
    if p['problemidcode'] not in cards or p['answeridcode'] not in cards or not cards[p['problemidcode']] or not cards[p['answeridcode']]:
      print(f'''GetAllProblems():\tSkipping incomplete problem with ID {p['idcode']}''')
      continue
    
    for cardfield in PROBLEM_CARD_FIELDS:
      idcodename  = cardfield + 'idcode'
      
      if idcodename in p and p[idcodename] in cards:
        p[cardfield]  = cards[p[idcodename]]
    
    intactproblems.append(p)
    
  return intactproblems

# =====================================================================
def GetChildProblems(g, card, problems, problemcardids, problemtype):
  """A helper function for GetAllProblems() to iterate through 
  lesson trees.
  """
  for r in g.db.Linked(card, learn_card, CARD_CHILD, fields = 'rid'):
    GetChildProblems(g, r, problems, problemcardids)
  
  for r in g.db.Linked(card, learn_problem, problemtype, fields = 'rid, problemid, answerid'):
    o = {}
    
    o['idcode']         = ToShortCode(r.rid)
    o['timelimit']      = r.timelimit
    o['points']         = r.points
    o['flags']          = r.flags
    
    for cardfield in PROBLEM_CARD_FIELDS:
      attrname    = cardfield + 'id'
      idcodename  = attrname + 'code'
      
      attrvalue   = getattr(r, attrname, 0)
      
      if attrvalue:
        o[idcodename] = ToShortCode(attrvalue)
    
    problems.append(o)
    
    problemcardids.append(r.problemid)
    problemcardids.append(r.answerid)
