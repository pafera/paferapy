#!/usr/bin/python
# -*- coding: utf-8 -*- 

from apps.system import *

import apps.learn.schooladministrator

import apps.learn.schoolclass
from apps.learn.teacher import *
from apps.learn.student import *

# Flag constants
SCHOOL_AUTO_APPROVE   = 0x01

# *********************************************************************
class learn_school(ModelBase):
  """Represents a school, which organizes students, teachers, and classes.
  
  This class is pretty much just an easy way to search for contact 
  information. 
  """
  
  _dbfields     = {
    'rid':                ('INTEGER', 'PRIMARY KEY NOT NULL',),
    'icon':               ('IMAGEFILE', "NOT NULL DEFAULT ''",),
    'displayname':        ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'description':        ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'address':            ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'phonenumber':        ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'email':              ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'contactinfo':        ('TRANSLATION', "NOT NULL DEFAULT ''",),
    'courseids':          ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'numadministrators':  ('INT', 'NOT NULL DEFAULT 0',),
    'numteachers':        ('INT', 'NOT NULL DEFAULT 0',),
    'numclasses':         ('INT', 'NOT NULL DEFAULT 0',),
    'numstudents':        ('INT', 'NOT NULL DEFAULT 0',),
    'flags':              ('INT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = [
    'learn_card',
    'learn_schooladministrator',
    'learn_schoolclass', 
    'learn_student',
    'learn_teacher', 
    'system_user',
  ]
  _dbdisplay  = ['displayname', 'description', 'address', 'phonenumber', 'email']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def OnDBDelete(self, db):
    '''Standard handler for deleting an existing object.
    '''
    if not self.rid:
      return
    
    for r in db.Linked(self, apps.learn.schoolclass.learn_schoolclass):
      db.Delete(r)
      
    for r in db.Linked(self, apps.learn.schooladministrator.learn_schooladministrator):
      db.Unlink(self, r)
      
    for r in db.Linked(self, learn_teacher):
      db.Unlink(self, r)
      
    for r in db.Linked(self, learn_student):
      db.Unlink(self, r)
