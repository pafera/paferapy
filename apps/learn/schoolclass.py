#!/usr/bin/python
# -*- coding: utf-8 -*- 

import random
import time
import datetime

from statistics import mean

from apps.system import *

import apps.learn.card

from apps.learn.classgrade import *
from apps.learn.challenge import *
from apps.learn.school import *
from apps.learn.teacher import *
from apps.learn.student import *

# Flag constants
CLASS_AUTO_APPROVE        = 0x01
CLASS_AUTO_HOMEWORK       = 0x02
CLASS_REVIEW_STUDYLIST    = 0x04
CLASS_ADD_HOMEWORK_SCORES = 0x08
CLASS_ENABLE_PRACTICE     = 0x10
CLASS_PUBLIC              = 0x20
CLASS_ENABLE_FORUM        = 0x40

# *********************************************************************
class learn_schoolclass(ModelBase):
  """Container class for students, courses, and teachers. This 
  permits easy searching for students to apply and teachers to manage
  courses and students.
  """
  
  _dbfields     = {
    'rid':              ('INTEGER PRIMARY KEY', 'NOT NULL',),
    'schoolid':         ('INT', 'NOT NULL',),
    'schoolname':       ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'icon':             ('IMAGEFILE', "NOT NULL DEFAULT ''",),
    'coursenum':        ('TEXT', 'NOT NULL', BlankValidator()),
    'displayname':      ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'description':      ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'schedule':         ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'teachers':         ('TRANSLATION', "NOT NULL DEFAULT ''",),
    'rankings':         ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'courses':          ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'numstudents':      ('INT', 'NOT NULL DEFAULT 0',),
    'capacity':         ('INT', 'NOT NULL',),
    'settings':         ('DICT', "NOT NULL DEFAULT ''",),
    'resettimezone':    ('SMALLINT', 'NOT NULL DEFAULT 0',),
    'forumadmins':      ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'forumposters':     ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'forumviewers':     ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'flags':            ('SMALLINT', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = [
    'learn_school', 
    'learn_teacher', 
    'learn_student',
    'learn_card',
  ]
  _dbdisplay  = ['coursenum', 'displayname', 'description']
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def UpdateChallenges(self, g):
    """Look for any challenges that have passed endtime and calculate
    statistics for them.
    """
    
    needcommit  = 0
    
    for c in g.db.Find(learn_challenge,
        'WHERE classid = ? AND starttime < ? AND CAST(flags & ? AS BOOLEAN)',
        [
          self.rid,
          time.time(),
          CHALLENGE_NEEDS_ANALYSIS
        ]
      ):
      c.Analyze(g)
      needcommit  = 1
      
    if needcommit:
      g.db.Commit()
    
  # -------------------------------------------------------------------
  def UpdateGrades(self, g, studentid):
    """Recalculate grades for studentid and update class rankings.
    """
    
    grade = g.db.Find(
      learn_classgrade,
      'WHERE classid = ? AND userid = ?',
      [
        self.rid,
        studentid,
      ]
    )
      
    if not grade:
      grade = learn_classgrade()
      
      grade.Set(
        classid = self.rid,
        userid  = studentid,
      )
    else:
      grade = grade[0]
      
    classparticipations = []
    homeworks           = []
    
    studentcode       = ToShortCode(studentid)
    persistencebonus  = 0
    persistencestreak = 0
    numhomework       = 0
    homeworkcompleted = 0
    
    for r in g.db.Find(
        learn_challenge,
        'WHERE classid = ? AND endtime < ?',
        [
          self.rid,
          time.time(),
        ],
        fields  = 'challengetype, results',
      ):
      
      if studentcode not in r.results:
        persistencestreak  = 0
        continue
      
      result  = r.results[studentcode]
      
      if r.challengetype == CHALLENGE_CLASSPARTICIPATION:
        classparticipations.append((result['score'], result['percentage']))
      elif r.challengetype == CHALLENGE_HOMEWORK:
        numhomework += 1
        
        homeworks.append((result[2], result[3]))
          
        completed = result[1] == CHALLENGE_COMPLETED
        
        if completed:
          homeworkcompleted += 1
          persistencestreak += 1
          persistencebonus  += persistencestreak
          
    grade.Set(
      numhomework         = numhomework,
      homeworkcompleted   = homeworkcompleted,
      classscore          = sum([x[0] for x in classparticipations]) if classparticipations else 0,
      classpercent        = mean([x[1] for x in classparticipations]) if classparticipations else 0,
      reviewscore         = sum([x[0] for x in homeworks]) if homeworks else 0,
      reviewpercent       = mean([x[1] for x in homeworks]) if homeworks else 0,
      persistencescore    = persistencebonus,
      persistencepercent  = (homeworkcompleted / numhomework * 100) if numhomework else 0,
    )
    
    grade.Set(
      finalscore  = (
        (grade.classscore + grade.reviewscore + grade.persistencescore) 
          * (grade.classpercent / 100) 
          * (grade.reviewpercent / 100) 
          * (grade.persistencepercent / 100)
      ),
    )
    
    g.db.Save(grade)
    
    if not self.rankings:
      self.rankings = []
    
    newrankings = [
      ToShortCode(x.userid) 
      for x in g.db.Find(
        learn_classgrade,
        'WHERE classid = ?',
        self.rid,
        fields    = 'userid',
        orderby   = 'finalscore, classpercent, reviewpercent, persistencepercent',
      )
    ]
        
    if self.rankings != newrankings:
      self.Set(rankings = newrankings)
      
      g.db.Update(self)
    
    g.db.Commit()
    
  # -------------------------------------------------------------------
  def GenerateAutoHomework(self, g):
    """If autohomework is enabled, this function will generate a random
    challenge every day for your students to complete.
    
    If review studylist is enabled, this will generate a challenge 
    every day using each student's studylist.
    """
    
    if self.flags & CLASS_AUTO_HOMEWORK or self.flags & CLASS_REVIEW_STUDYLIST:
      currenttime = time.time()
      
      currenthomework = g.db.Find(
        learn_challenge,
        'WHERE classid = ? AND starttime < ? AND endtime > ? AND challengetype = ?',
        [
          self.rid,
          currenttime,
          currenttime,
          CHALLENGE_HOMEWORK,
        ]
      )
      
      needhomework  = 1 if self.flags & CLASS_AUTO_HOMEWORK else 0
      needstudylist = 1 if self.flags & CLASS_REVIEW_STUDYLIST else 0
      
      for s in currenthomework:
        if self.flags & CLASS_REVIEW_STUDYLIST and s.flags & CHALLENGE_USE_STUDYLIST:
          needstudylist = 0
        elif self.flags & CLASS_AUTO_HOMEWORK:
          needhomework  = 0
          
      if needhomework or needstudylist:
        today = datetime.datetime.now(
          datetime.timezone(
            datetime.timedelta(hours = self.resettimezone)
          )
        )
          
        today = today.replace(hour = 0, minute = 0, second = 0)
        
        try:
          tomorrow  = today.replace(day = today.day + 1)
        except Exception as e:
          tomorrow  = today.replace(month = today.month + 1, day = 1)
        
        if not DictV(self.settings, 'autohomework'):
          print('learn_schoolclass.GenerateAutoHomework: No autohomework settings found for ', self.rid)
          return;
        
        lessonidspool   = self.settings['autohomework']['lessonids']
        problemidspool  = self.settings['autohomework']['problemids']
        challengetypes  = self.settings['autohomework']['challengetypes']
          
        if needhomework and (lessonidspool or problemidspool) and challengetypes:
          newhomework = learn_challenge()
          newhomework.Set(
            classid         = self.rid,
            challengetype   = CHALLENGE_HOMEWORK,
            starttime       = today,
            endtime         = tomorrow,
            challenge       = random.choice(challengetypes),
            flags           = CHALLENGE_NEEDS_ANALYSIS,
          )
          
          newhomework.Set(**(self.settings['autohomework']))
          
          if self.settings['autohomework']['problemselect'] == 'randomly':
            newhomework.Set(
              lessonids   = [random.choice(lessonidspool)],
            )
          elif self.settings['autohomework']['problemselect'] == 'sequentially':
            pasthomework  = g.db.Find(
              learn_challenge,
              'WHERE classid = ? AND endtime < ? AND challengetype = ?',
              [
                self.rid,
                currenttime,
                CHALLENGE_HOMEWORK,
              ],
              orderby = 'endtime DESC',
            )
              
            for p in pasthomework:
              if p.lessonids:
                lastlessonid  = p.lessonids[0]
                
                try:
                  lastlessonpos = lessonidspool.index(lastlessonid)
                  
                  if lastlessonpos == len(lessonidspool) - 1:
                    newhomework.Set(lessonids = [lessonidspool[0]])
                  else:
                    newhomework.Set(lessonids = [lessonidspool[lastlessonpos + 1]])
                except Exception as e:
                  pass
                
          if newhomework.lessonids:
            cardtitles  = apps.learn.card.GetCardTitles(g, [FromCode(x) for x in newhomework.lessonids])
            
            newtitle  = {}
            newtitle[g.session.lang]  = ', '.join(cardtitles.values())
            
            newhomework.Set(title = newtitle)
          else:
            newtitle  = {}
            newtitle[g.session.lang]  = g.T.dailychallenge
            
            newhomework.Set(title = newtitle)
          
          g.db.Save(newhomework)
          
        if needstudylist and challengetypes:
          newhomework = learn_challenge()
          
          newtitle  = {}
          newtitle[g.session.lang]  = g.T.studylist
          
          newhomework.Set(**(self.settings['autohomework']))
          
          newhomework.Set(
            title           = newtitle,
            classid         = self.rid,
            lessonids       = [],
            problemids      = [],
            challengetype   = CHALLENGE_HOMEWORK,
            starttime       = today,
            endtime         = tomorrow,
            challenge       = random.choice(challengetypes),
            flags           = CHALLENGE_NEEDS_ANALYSIS | CHALLENGE_USE_STUDYLIST,
          )
          
          g.db.Save(newhomework)
          
        g.db.Commit()          
  
  # -------------------------------------------------------------------
  def OnDBDelete(self, db):
    '''Standard handler for deleting an existing object.
    '''
    if not self.rid:
      return
    
    for r in db.Linked(self, learn_school):
      db.Unlink(self, r)
      
    for r in db.Linked(self, learn_teacher):
      db.Unlink(self, r)
      
    for r in db.Linked(self, learn_student):
      db.Unlink(self, r)
