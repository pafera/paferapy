#!/usr/bin/python
# -*- coding: utf-8 -*- 

import json
import time

from statistics import mean, median

from apps.system import *

import apps.learn.challengeresult
import apps.learn.schoolclass
import apps.learn.student
import apps.learn.answer

# Challenge types
CHALLENGE_HOMEWORK            = 1
CHALLENGE_CLASSWORK           = 2
CHALLENGE_CLASSPARTICIPATION  = 3
CHALLENGE_QUIZ                = 4
CHALLENGE_TEST                = 5
CHALLENGE_EXAM                = 6

# Challenge flags
CHALLENGE_NEEDS_ANALYSIS    = 0x01
CHALLENGE_USE_STUDYLIST     = 0x02

# Challenge results
CHALLENGE_DIDNT_TRY         = 0
CHALLENGE_TRIED             = 1
CHALLENGE_COMPLETED         = 2
CHALLENGE_LAZY_TRIED        = 3
CHALLENGE_EXCELLENT         = 4

# *********************************************************************
class learn_challenge(ModelBase):
  """In the Pafera Learning System, we handle homework and classwork 
  by issuing challenges to students. Points are then calculated based
  upon how the students did, and grades are automatically assigned by 
  the system. 
  
  There are a variety of challenges available, so check challenges.js 
  to see what they are and test them to see what you can do with them. 
  
  Problems for challenges can be assigned by adding lessons, individual
  problems, or can be added straight from their study list. 
  
  Upon a challenge ending, the next time that you visit /learn/home.html,
  student scores will be automatically calculated and applied to their
  grades. You can then see such statistics as the average score, what
  percentage of students passed, which problems were often answered
  incorrectly, and such in the analysis section.
  """
  
  _dbfields     = {
    'rid':                ('INTEGER', 'PRIMARY KEY NOT NULL',),
    'classid':            ('INT', 'NOT NULL', BlankValidator()),
    'starttime':          ('TIMESTAMP', 'NOT NULL',),
    'endtime':            ('TIMESTAMP', 'NOT NULL',),
    'challenge':          ('TEXT', 'NOT NULL', BlankValidator()),
    'title':              ('TRANSLATION', 'NOT NULL', BlankValidator()),
    'instructions':       ('TRANSLATION', "NOT NULL DEFAULT ''",),
    'question':           ('TRANSLATION', "NOT NULL DEFAULT ''",),
    'image':              ('IMAGEFILE', "NOT NULL DEFAULT ''",),
    'sound':              ('SOUNDFILE', "NOT NULL DEFAULT ''",),
    'video':              ('VIDEOFILE', "NOT NULL DEFAULT ''",),
    'files':              ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'lessontitles':       ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'lessonids':          ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'problemids':         ('NEWLINELIST', "NOT NULL DEFAULT ''",),
    'results':            ('DICT', "NOT NULL DEFAULT ''",),
    'analysis':           ('DICT', "NOT NULL DEFAULT ''",),
    'averagescore':       ('INT16', 'NOT NULL DEFAULT 0',),
    'averageright':       ('INT16', 'NOT NULL DEFAULT 0',),
    'averagepercent':     ('INT16', 'NOT NULL DEFAULT 0',),
    'percenttried':       ('INT16', 'NOT NULL DEFAULT 0',),
    'percentcomplete':    ('INT16', 'NOT NULL DEFAULT 0',),
    'minscore':           ('INT16', 'NOT NULL DEFAULT 0',),
    'minpercentage':      ('INT16', 'NOT NULL DEFAULT 0',),
    'numpoints':          ('INT16', 'NOT NULL', BlankValidator()),
    'didnttrypenalty':    ('INT16', 'NOT NULL DEFAULT 0', ),
    'numtries':           ('INT16', 'NOT NULL', BlankValidator()),
    'numproblems':        ('INT16', 'NOT NULL DEFAULT 0', ),
    'timelimit':          ('INT16', 'NOT NULL DEFAULT 0',),
    'challengetype':      ('INT16', 'NOT NULL DEFAULT 0',),
    'flags':              ('INT16', 'NOT NULL DEFAULT 0',),
  }
  _dbindexes  = ()
  _dblinks    = [
  ]
  _dbdisplay  = [
    'classid', 
    'challenge', 
    'lessontitles', 
    'problemids', 
    'minscore', 
    'minpercentage',
    'numpoints'
  ]
  _dbflags    = 0
  
  # -------------------------------------------------------------------
  def __init__(self):
    super().__init__()
    
  # -------------------------------------------------------------------
  def GetNumRemainingAttempts(self, g):
    """Returns the number of remaining tries available to the current 
    user.
    
    As a side effect, all previous results will be available in 
    obj.previousresults as learn_challengeresult objects.
    """
    if not hasattr(self, 'previousresults'):
      self.previousresults = g.db.Find(
        apps.learn.challengeresult.learn_challengeresult,
        'WHERE challengeid = ? AND userid = ?',
        [
          self.rid, 
          g.session.userid
        ],
        fields    = 'rid, problemresults, score, percentright',
        orderby   = 'endtime',
      )
    
    return Bound(self.numtries - len(self.previousresults), 0, 9999)
  
  # -------------------------------------------------------------------
  def Analyze(self, g):
    """Calculates all scores and saves statistics for this challenge.
    
    Typically, this will be done automatically for you after endtime 
    has passed.
    """
    
    # Points for Write Answer challenges are manually assigned by the 
    # teacher, so no statistics or points need to be calculated.
    if self.challenge == 'Write Answer':
      return
    
    cls       = g.db.Load(apps.learn.schoolclass.learn_schoolclass, self.classid)    
    
    currenttime = time.time()
    
    self.results  = {}      
    studentnames  = {}
    allproblems   = {}
    rightproblems = {}
    wrongproblems = {}
    answers       = {}    
    
    # Classwork scores are automatically applied to the current answer object
    if (self.challengetype == CHALLENGE_CLASSWORK 
        or self.challengetype == CHALLENGE_CLASSPARTICIPATION
        or self.challenge == 'Typing'
      ):
      for r in g.db.Find(
          apps.learn.answer.learn_answer,
          'WHERE classid = ? AND NOT CAST(flags & ? AS BOOLEAN)',
          [
            self.classid,
            apps.learn.answer.ANSWER_TEACHER
          ],
          fields  = 'rid, userid, displayname, numright, numwrong, bonusscore, score'
        ):
        
        answers[ToShortCode(r.userid)] = r        
    
    # Class participation scores are calculated from the current answer
    # object
    if self.challengetype == CHALLENGE_CLASSPARTICIPATION:
      for studentid, answer in answers.items():
        o = answer.ToJSON('displayname, numright, numwrong, bonusscore, score')
        
        if answer.numright + answer.numwrong:
          o['percentage'] = int(answer.numright / (answer.numright + answer.numwrong) * 100)
        else:
          o['percentage'] = 0
        
        self.results[studentid]  = o
        
      self.Set(results = self.results)
      
      if time.time() > self.endtime:
        self.Set(flags = self.flags & (~CHALLENGE_NEEDS_ANALYSIS))
      
      g.db.Update(self)
      
      for studentid in answers.keys():
        cls.UpdateGrades(g, FromCode(studentid))
        
      g.db.Commit()
      return self      
    
    # Get all students for the class and see if any failed to even attempt the challenge
    for r in g.db.Linked(cls, apps.learn.student.learn_student, fields = 'userid, displayname'):
      studentid = ToShortCode(r.userid)
      
      studentnames[studentid]  = r.displayname
      
      if studentid not in self.results:
        self.results[studentid] = [r.displayname, CHALLENGE_DIDNT_TRY, 0, 0, -self.didnttrypenalty]
    
    studenttries    = {}
    studentscores   = {}
    studentpercents = {}
    
    # Iterate through all results and calculate statistics
    for r in g.db.Find(
        apps.learn.challengeresult.learn_challengeresult,
        'WHERE challengeid = ?',
        self.rid
      ):
      
      studentid = ToShortCode(r.userid)
      
      if studentid not in studenttries:
        studenttries[studentid]     = 1
        studentpercents[studentid]  = [r.percentright]
        studentscores[studentid]    = [r.score]
      else:
        studenttries[studentid]  += 1
        studentpercents[studentid].append(r.percentright)
        studentscores[studentid].append(r.score)
        
      if studentid not in studentnames:
        studentnames[studentid]  = '[Removed Student ' + studentid + ']'
      
      if r.problemresults:
        for p in r.problemresults:
          if 'id' in p:
            if p['id'] not in allproblems:
              problemtext = '[Text not found]'
              
              try:
                problemtext = g.db.Execute(f'''
                    SELECT content
                    FROM learn_problem
                    JOIN learn_card ON learn_problem.problemid = learn_card.rid
                    WHERE learn_problem.rid = ?
                  ''',
                  FromCode(p['id'])
                )[0][0]
              except Exception as e:
                print('>>> Problem getting problem text', e)
              
              allproblems[ p['id'] ]  = problemtext
                
            if p['status'] == 'wrong':
              if p['id'] not in wrongproblems:
                wrongproblems[ p['id'] ]  = {
                    'problemtext':  allproblems[p['id']],
                    'studentids':   [],
                  }
                
              wrongproblems[ p['id'] ]['studentids'].append(ToShortCode(r.userid))            
            elif p['status'] == 'right':
              if p['id'] not in rightproblems:
                rightproblems[ p['id'] ]  = {
                    'problemtext':  allproblems[p['id']],
                    'studentids':   [],
                  }
                
              rightproblems[ p['id'] ]['studentids'].append(ToShortCode(r.userid))            
    
    # In order to obtain the failed status, a student must have used up all of their tries 
    # and have gotten a median percent right of 20 percent or above.
    #
    # If not, then we assume that the student was lazy and didn't actually try to do 
    # do the assignment, resulting in the CHALLENGE_LAZY_TRIED flag.
    #
    # A percentage right of over 90 percent results in the CHALLENGE_EXCELLENT flag.
    for studentid in studenttries.keys():
      status      = CHALLENGE_LAZY_TRIED
      topscore    = max(studentscores[studentid])
      toppercent  = max(studentpercents[studentid])
      numpoints   = 0
      
      if toppercent >= 90:
        status    = CHALLENGE_EXCELLENT
        numpoints = self.numpoints * 2
      elif (
          (self.minscore and topscore >= self.minscore)
          or (self.minpercentage and toppercent >= self.minpercentage)
        ):
        status  = CHALLENGE_COMPLETED
        numpoints = self.numpoints
      elif (
          (studenttries[studentid] == self.numtries)
          and (median(studentpercents[studentid]) >= 20)
        ):
        status  = CHALLENGE_TRIED
      else:
        status  = CHALLENGE_LAZY_TRIED
        numpoints = -(self.didnttrypenalty / 2)      
      
      self.results[studentid] = [studentnames[studentid], status, topscore, toppercent, numpoints]
    
    # Apply penalties if someone didn't complete their classwork
    if self.challengetype == CHALLENGE_CLASSWORK and self.didnttrypenalty and self.endtime < currenttime:
      for studentid, results in self.results.items():
        if results[1] == CHALLENGE_DIDNT_TRY:
          if studentid not in answers:
            if studentid not in studentnames:
              studentnames[studentid]  = {
                'en': f'[Removed Student {k}]'
              }
            
            newanswer = apps.learn.answer.learn_answer()
            newanswer.Set(
              classid     = self.classid,
              userid      = userid,
              displayname = studentnames[userid],
            )
            
            newanswer.AddHomeworkScores(g)
            
            g.db.Insert(newanswer)
            g.db.Commit()
            
            answers[studentid] = newanswer
            
          answerobj = answers[studentid]
          
          answerobj.Set(
            bonusscore  = answerobj.bonusscore - self.didnttrypenalty,
          )
          
          answerobj.UpdateScore()
          
          g.db.Update(answerobj)
    
    self.Set(
      results   = self.results,
      analysis  = {
        'rightproblems':  rightproblems,
        'wrongproblems':  wrongproblems,
      }
    )
    
    # Calculate averages 
    numdidnttry     = 0
    numfailed       = 0
    numcompleted    = 0
    averagescore    = []
    averagepercent  = []
    
    for v in self.results.values():
      if v[1] == CHALLENGE_DIDNT_TRY:
        numdidnttry += 1
      else:
        numfailed   += 1
        
        averagescore.append(v[2])
        averagepercent.append(v[3])
        
        if v[1] == CHALLENGE_COMPLETED or v[1] == CHALLENGE_EXCELLENT:
          numcompleted  += 1
    
    totalids  = numdidnttry + numfailed + numcompleted
    
    if totalids:
      percenttried     = numfailed / totalids * 100
      percentcomplete  = numcompleted / totalids * 100
    else:
      percenttried     = 0
      percentcomplete  = 0
    
    self.Set(
      percenttried      = percenttried,
      percentcomplete   = percentcomplete,
      averagescore      = median(averagescore) if averagescore else 0,
      averagepercent    = median(averagepercent) if averagepercent else 0,
    ) 
    
    # If endtime has passed, then no further data will be added and we don't
    # need to do this again
    if currenttime > self.endtime:
      self.Set(flags  = self.flags & (~CHALLENGE_NEEDS_ANALYSIS))
      
    g.db.Update(self)

# =====================================================================
def GetCurrentChallenges(g, classid, challengetype = CHALLENGE_HOMEWORK):
  """Returns a list of all currently running challenges for the logged in user.
  
  Useful for seeing what this student has finished and what they still 
  need to do. 
  """
  
  challenges  = []
  currenttime = time.time()
  
  for r in g.db.Find(
      learn_challenge,
      'WHERE classid = ? AND starttime < ? AND endtime > ? AND challengetype = ?',
      [
        classid,
        currenttime,
        currenttime,
        challengetype,
      ],
      fields  = 'rid, challenge, title, endtime, numpoints, numtries, flags',
    ):
    
    completed = 0
    
    results = g.db.Find(
      apps.learn.learn_challengeresult,
      'WHERE challengeid = ? AND userid = ?',
      [r.rid, g.session.userid],
      fields  = 'rid, flags'
    )
      
    for s in results:
      if s.flags & apps.learn.challengeresult.CHALLENGERESULT_PASSED:
        completed = 1
        break
    
    c = r.ToJSON('rid, challenge, title, endtime, numpoints, numtries, flags')
    
    c['triesleft']      = Bound(r.numtries - len(results), 0, 9999)
    c['completed']      = completed
    c['timeremaining']  = int(r.endtime - currenttime)
    
    challenges.append(c)
    
  return challenges
