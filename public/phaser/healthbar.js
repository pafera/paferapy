class HealthBar {

  // ------------------------------------------------------------------
  constructor (scene, x, y)
  {
    this.bar = new Phaser.GameObjects.Graphics(scene);

    this.x = x;
    this.y = y;
    this.value = 100;
    this.p = 76 / 100;

    this.Draw();

    scene.add.existing(this.bar);
  }

  // ------------------------------------------------------------------
  Change(amount)
  {
    this.value += amount;

    if (this.value < 0)
    {
      this.value = 0;
    }

    this.Draw();
    
    return this;
  }

  // ------------------------------------------------------------------
  Draw ()
  {
    this.bar.clear();

    //  BG
    this.bar.fillStyle(0x000000);
    this.bar.fillRect(this.x, this.y, 80, 16);

    //  Health

    //this.bar.fillStyle(0xffffff);
    //this.bar.fillRect(this.x + 2, this.y + 2, 76, 12);

    if (this.value > 66)
    {
        this.bar.fillStyle(0x5555ff);
    } else if (this.value > 33)
    {
        this.bar.fillStyle(0x00ff00);
    } else
    {
        this.bar.fillStyle(0xff0000);
    }

    let d = Math.floor(this.p * this.value);

    this.bar.fillRect(this.x + 2, this.y + 2, d, 12);
    
    return this;
  } 
}
