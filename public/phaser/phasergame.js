"use strict";

var phasergame;
var shibaplatformscene;
var homescene;

var cursors;
var shibadirection    = 'left';
var fullscreenbutton  = 0;

var player;
var stars;
var bombs;
var platforms;
var gamescore = 0;
var gameOver = 0;
var scoreText;
var sounds  = {};

var currentmap    = '';
var visitedscenes = [];

var rshiba        = 0;
var bshiba        = 0;
var rshibahp      = 0;
var bshibahp      = 0;
var balls         = 0;
var clocks        = 0;
var players       = 0;
var foods         = 0;
var weapons       = 0;
var familymembers = 0;

var runningscenes   = [];
var currentscene    = 0;
var gameobjs        = {};
var nextgameobjid   = 0;
var gamestarted     = 0;

var WEAPONS = {
  chair:    {
    speed:   1
  },
  sofa:     {
    speed:   2
  },
  desk:     {
    speed:   3
  },
  TV:       {
    speed:   4
  },
  door:     {
    speed:   5
  },
  window:   {
    speed:   6
  },
  table:    {
    speed:   7
  },
  bed:      {
    speed:   8
  }
}

var FOODS = [
  'beef',
  'cake',
  'candy',
  'chicken',
  'fish',
  'fries',
  'ice cream',
  'noodles',
  'pork',
  'rice'
];

var FAMILY_MEMBERS  = [
  'dad',
  'mom',
  'brother',
  'sister',
  'grandpa',
  'grandma',
  'friend',
  'uncle',
  'aunt',
  'nephew',
  'niece',
  'cousin'
];

var ANIMALS = [
  'bird',
  'cat', 
  'chicken.pet',
  'dog', 
  'dragon', 
  'ferret', 
  'fish.pet', 
  'hamster', 
  'mouse', 
  'pig', 
  'rabbit', 
  'snake', 
  'turtle', 
];

var ANIMAL_SPEEDS = {
  bird:           12,
  cat:            8, 
  'chicken.pet':  6,
  dog:            10, 
  dragon:         14, 
  ferret:         8, 
  'fish.pet':     5, 
  hamster:        4, 
  mouse:          4, 
  pig:            4, 
  rabbit:         5, 
  snake:          3, 
  turtle:         1
};

var FAMILY_FOODS  = {};

for (let i = 0, l = FAMILY_MEMBERS.length; i < l; i++)
{
  let r = FAMILY_MEMBERS[i];
  
  FAMILY_FOODS[r] = {};
  
  for (let j = 0, m = FOODS.length; j < m; j++)
  {
    let s = FOODS[j];
    
    FAMILY_FOODS[r][s]  = RandInt(-10, 20);
  }
}

var FAMILY_ANIMALS  = {};

var familyshuffle = Clone(FAMILY_MEMBERS);

Shuffle(familyshuffle);

var animalsshuffle  = Clone(ANIMALS);

Shuffle(animalsshuffle);

for (let i = 0, l = FAMILY_MEMBERS.length; i < l; i++)
{
  let r = familyshuffle[i];
  
  FAMILY_ANIMALS[r] = animalsshuffle[i];
}

var defaultphysics  = {
  default: 'arcade',
  arcade: {
      gravity: { y: 300 },
      debug: false
  }
};

var zerogravityphysics  = {
  default: 'arcade',
  arcade: {
      gravity: { y: 0 },
      debug: false
  }
};

// ********************************************************************
class GameObject extends Phaser.GameObjects.Sprite {
  
  static nextgameobjid  = 0;
  
  static NextID()
  {
    GameObject.nextgameobjid++;
    return GameObject.nextgameobjid;
  }
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config.scene, config.x, config.y, config.file);
    
    this.gameobjid    = GameObject.NextID();
    this.gameobjname  = config.name ? config.name : this.gameobjid;
    this.gameobjtype  = config.type ? config.type : Object.getPrototypeOf(this).constructor.name;
    
    this.ignoreoverlapids = [];
    this.speed            = 0;
  }
  
  // ------------------------------------------------------------------
  Update()
  {
  }
}


// ********************************************************************
class BattlePlayer extends GameObject {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.hpbar    = new HealthBar(config.scene, config.x - 45, config.y - 55);
    config.scene.add.existing(this.hpbar);
    
    this.ballsleft  = 30;
    this.alive      = 1;
    
    this.ammoleftdisplay = config.scene.add.text(config.x - 20, config.y - 58, this.ballsleft, {fontSize: '20px'});
    
    this.setSize(48, 48);
  }
  
  // ------------------------------------------------------------------
  IHave(numballs)
  {
    if (this.x > 100)
    {
      return;
    }
    
    let i = ballamounts.indexOf(numballs);
    
    let newballs  = 0;
    
    if (!this.alive || i == -1 || (i + 1) > this.ballsleft)
    {
      return;
    }
    
    this.ballsleft  -= (i + 1);
    this.ammoleftdisplay.setText(this.ballsleft);
        
    newballs  = balls.create(this.x, this.y, 'ball');
    newballs.gameobjtype  = 'ball';
    newballs.gameobjid      = GameObject.NextID();    
    newballs.setVelocityX(60);
    newballs.anims.play(numballs)
    newballs.numballs = i + 1;
    newballs.ignoreoverlapids  = [this.gameobjid];
    newballs.playerfrom        = this;
    newballs.playercollided    = 0;
  }
  
  // ------------------------------------------------------------------
  Its(numoclock)
  {
    if (this.x < 700)
    {
      return;
    }
    
    let i = clockamounts.indexOf(numoclock);
    
    let newballs  = 0;
    
    if (!this.alive || i == -1 || (i + 1) > this.ballsleft)
    {
      return;
    }
    
    this.ballsleft  -= (i + 1);
    this.ammoleftdisplay.setText(this.ballsleft);
        
    newballs  = balls.create(this.x, this.y, 'clock');
    newballs.gameobjtype  = 'clock';
    newballs.gameobjid      = GameObject.NextID();    
    newballs.setVelocityX(-60);
    newballs.anims.play(clockamounts[i])
    newballs.numballs = i + 1;
    newballs.ignoreoverlapids  = [this.gameobjid];
    newballs.playerfrom        = this;
    newballs.playercollided    = 0;
  }
  
  // ------------------------------------------------------------------
  Hadouken(speed)
  {
    speed = speed || 200;
    
    if (this.gameobjname != 'jim' || runningscenes.indexOf('bombbattle') == -1)
    {
      return;
    }
    
    sounds['hadouken'].play();
    let newballs  = 0;
    
    newballs  = balls.create(this.x, this.y, 'hadouken');
    newballs.gameobjtype  = 'hadouken';
    newballs.setVelocityX(-speed);
    newballs.anims.play("hadouken")
    newballs.numballs = 600;
    newballs.gameobjid      = GameObject.NextID();    
    newballs.ignoreoverlapids  = [this.gameobjid];
    newballs.playerfrom        = this;
    newballs.playercollided = 0;
  }
  
  // ------------------------------------------------------------------
  OnHit()
  {
    this.hpbar.Change(-34);
  }
}

// ********************************************************************
class FoodPlayer extends GameObject {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.score        = 0;
    this.target       = 0;
    this.items        = [];
    this.codelines    = [];
    this.haserror     = 0;
    
    this.setSize(48, 60)
      .setDisplaySize(48, 60);
    
    this.scoredisplay = config.scene.add.text(
      config.x - 6, 
      config.y + 30, 
      this.score, 
      {
        color:      'yellow',
        fontSize:   '24px',
        fontStyle:  'bold'
      }
    );
    
    this.itemdisplay  = 0;
  }
  
  // ------------------------------------------------------------------
  DoYouLike(target)
  {
    this.target = target;
  }
  
  // ------------------------------------------------------------------
  DoYouHave(target)
  {
    switch (target)
    {
      case 'a chicken':
        target  = 'a chicken.pet';
        break;
      case 'a fish':
        target  = 'a fish.pet';
        break;
    }
    
    if (IsString(target) && target.length > 3)
    {
      this.target = target.substr(2);
    }
  }
  
  // ------------------------------------------------------------------
  ThisIs(weapon, target)
  {
    let self  = this;
    
    if (target)
    {
      switch (weapon)
      {
        case 'a chair':
        case 'a sofa':
        case 'a desk':
        case 'a TV':
          break;
        default:
          throw new Error('没有这个武器')
      };
      
      self.UseWeapon(weapon, target);
    } else
    {
      switch (weapon)
      {
        case 'my dad':
        case 'my mom':
        case 'my brother':
        case 'my sister':
          break;
        default:
          throw new Error('没有这个家人')
      };
      
      weapon  = weapon.substr(3);
      
      let allgameobjs = Keys(self.scene.gameobjs);
      
      if (allgameobjs.indexOf(weapon) > -1)
      {
        self.target = weapon;
      }
    }
  }
  
  // ------------------------------------------------------------------
  ThatIs(weapon, target)
  {
    let self  = this;
    
    if (target)
    {
      switch (weapon)
      {
        case 'a door':
        case 'a window':
        case 'a table':
        case 'a bed':
          break;
        default:
          throw new Error('没有这个武器')
      };
      
      self.UseWeapon(weapon, target);
    } else
    {
      switch (weapon)
      {
        case 'my grandpa':
        case 'my grandma':
        case 'my friend':
        case 'my uncle':
        case 'my aunt':
        case 'my nephew':
        case 'my niece':
        case 'my cousin':
          break;
        default:
          throw new Error('没有这个家人')
      };
      
      weapon  = weapon.substr(3);
      
      let allgameobjs = Keys(self.scene.gameobjs);
      
      if (allgameobjs.indexOf(weapon) > -1)
      {
        self.target = weapon;
      }
    }
  }
  
  // ------------------------------------------------------------------
  UseWeapon(weapon, target)
  {
    let self  = this;
    
    weapon  = weapon.substr(2)
    
    let targetplayer  = self.scene.gameobjs[target];
    
    if (!targetplayer)
    {
      throw new Error('找不到' + target)
    }
    
    let thisplayer  = this;
    
    P.API(
      '/learn/answersapi',
      {
        command:      'buyitem',
        data:         weapon,
        quantity:     -1,
        useridcode:   thisplayer.student.idcode
      },
      function(d)
      {
        if (d.error)
        {
          let resultsdiv  = E('.' + thisplayer.gameobjname + 'Results');
          P.HTML(
            resultsdiv, 
            `<div class="redb Pad50 Margin25">${d.error}</div>
          `);
          return;
        }
        
        let p   = new Furniture({
          scene:  self.scene, 
          file:   weapon,
          x:      thisplayer.x, 
          y:      thisplayer.y,
          target: targetplayer,
          speed:  WEAPONS[weapon].speed
        });
        
        p.playerfrom  = thisplayer.gameobjname;
        
        self.scene.gameobjs[p.gameobjid]   = p; 
        
        self.scene.weapons.add(p);
        
        thisplayer.RunNextCodeLine();
      }
    );
  }
  
  // ------------------------------------------------------------------
  Update()
  {
    let self  = this;
    
    if (self.target)
    {
      let t = self.scene.gameobjs[self.target];
      
      if (t)
      {
        if (t.x > self.x + 2)
        {
          self.x  += 0.5;
          self.UpdateChildren();
        } else if (t.x + 2 < self.x)
        {
          self.x  -= 0.5;
          self.UpdateChildren();
        } else if (t.y > self.y + 2)
        {
          self.y  += 0.5;
          self.UpdateChildren();
        } else if (t.y + 2 < self.y)
        {
          self.y  -= 0.5;
          self.UpdateChildren();
        }
      } else
      {
        if (!IsEmpty(self.codelines))
        {
          self.RunNextCodeLine();
        }
      }
    } 
  }

  // ------------------------------------------------------------------
  UpdateChildren()
  {
    let self  = this;
    
    self.scoredisplay.setPosition(this.x - 6, this.y + 30);
    
    if (self.itemdisplay)
    {
      self.itemdisplay.setPosition(this.x - 6, this.y + 10);
    }
  }
  
  // ------------------------------------------------------------------
  Destroy()
  {
    this.scoredisplay.destroy();
    
    if (this.itemdisplay)
    {
      this.itemdisplay.destroy();
    }
    
    this.destroy();
  }

  // ------------------------------------------------------------------
  RunNextCodeLine()
  {
    if (!this.haserror && !IsEmpty(this.codelines))
    {
      let nextline    = this.codelines.splice(0, 1)[0];
      let resultsdiv  = E('.' + this.gameobjname + 'Results');
      
      E('.' + this.gameobjname + 'Code').innerText  = nextline;
      
      try
      {
        let parts = nextline.split('.');
        
        if (parts && parts[0] != this.gameobjname)
        {
          throw new Error('学生名字错了！');
        }
        
        let results  = eval(nextline);
        
        P.HTML(
          resultsdiv, 
          `<div class="lgreenb Pad50 Margin25">${results}</div>
        `);
        
        if (this.target 
          && !HasKey(currentscene.gameobjs, this.target)
        )
        {
          this.RunNextCodeLine();
        } else
        {
          P.API(
            '/learn/answersapi',
            {
              command:        'setcoderesult',
              studentidcode:  this.student.idcode,
              coderesult: {
                line:   nextline,
                result: ''
              }
            },
            function(d)
            {
            }
          );
        }
      } catch (error)
      {
        this.haserror = 1;
        
        P.HTML(
          resultsdiv, 
          `<div class="redb Pad50 Margin25">${error}</div>
        `);
        
        P.API(
          '/learn/answersapi',
          {
            command:    'setcoderesult',
            studentidcode:  this.student.idcode,
            coderesult: {
              line:   nextline,
              result: `${error}`
            }
          },
          function(d)
          {
          }
        );
      }
    }
  }
}

// ********************************************************************
class FoodItem extends GameObject {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.gameobjname  = config.file;
    
    config.scene.add.existing(this);
    
    this.setSize(64, 64);
    this.setDisplaySize(64, 64);
    
    this.points       = RandInt(1, 10);
    this.gameobjtype  = 'fooditem';
    
    this.scoredisplay = config.scene.add.text(
      config.x - 6, 
      config.y + 20, 
      this.points, 
      {
        color:      'white',
        fontSize:   '24px',
        fontStyle:  'bold'
      }
    );
  }
  
  // ------------------------------------------------------------------
  UpdateChildren()
  {
    this.scoredisplay.setPosition(this.x - 6, this.y + 40);
  }
  
  // ------------------------------------------------------------------
  Destroy()
  {
    this.scoredisplay.destroy();
    this.destroy();
  }
}

// ********************************************************************
class Furniture extends GameObject {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.gameobjname  = config.file;
    this.speed        = config.speed;
    
    config.scene.add.existing(this);
    
    this.setSize(64, 64);
    this.setDisplaySize(64, 64);
    
    this.target       = config.target;
  }

  // ------------------------------------------------------------------
  Update()
  {
    let self  = this;
    
    let t = self.target;
    
    if (t)
    {
      if (t.x > self.x + 10)
      {
        self.x  += self.speed;
      } else if (t.x + 10 < self.x)
      {
        self.x  -= self.speed;
      } 
      
      if (t.y > self.y + 10)
      {
        self.y  += self.speed;
      } else if (t.y + 10 < self.y)
      {
        self.y  -= self.speed;
      }
    }
  }
  
  // ------------------------------------------------------------------
  Destroy()
  {
    this.destroy();
  }
}

// ********************************************************************
class FamilyMember extends GameObject {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.gameobjname  = config.file;
    
    config.scene.add.existing(this);
  }
  
  // ------------------------------------------------------------------
  UpdateChildren()
  {
  }
  
  // ------------------------------------------------------------------
  Destroy()
  {
    this.destroy();
  }
}


// ********************************************************************
class Animal extends GameObject {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.gameobjname  = config.file;
    this.speed        = ANIMAL_SPEEDS[config.file];
    
    config.scene.add.existing(this);
  }
  
  // ------------------------------------------------------------------
  UpdateChildren()
  {
  }
  
  // ------------------------------------------------------------------
  Destroy()
  {
    this.destroy();
  }
}

// ----------------------------------------------------------------
function PreloadResources(game, images, spritesheets, audios)
{
  images        = images        || [];
  spritesheets  = spritesheets  || [];
  audios        = audios        || [];
  
  for (let i = 0, l = images.length; i < l; i++)
  {
    let item  = images[i];
    
    game.load.image(item[0], item[1]);
  }
  
  for (let i = 0, l = spritesheets.length; i < l; i++)
  {
    let item  = spritesheets[i];
    
    game.load.spritesheet(item[0], item[1], item[2]);
  }
    
  for (let i = 0, l = audios.length; i < l; i++)
  {
    let item  = audios[i];
    
    game.load.audio(item[0], item[1]);
  }
}

// ----------------------------------------------------------------
function DefaultOnPreload()
{
  PreloadResources(
    this,
    [
      ['airport', '/phaser/images/airport.webp'],
      ['bank', '/phaser/images/bank.webp'],
      ['bathroom', '/phaser/images/bathroom.webp'],
      ['bone', '/phaser/images/tracy.webp'],
      ['bomb', '/phaser/images/bomb.webp'],
      ['bus.station', '/phaser/images/bus.station.webp'],
      ['dpad', '/phaser/images/dpad.webp'],
      ['fullscreen', '/phaser/images/fullscreen.webp'],
      ['gray.floor', '/phaser/images/gray.floor.webp'],
      ['ground', '/phaser/images/platform.webp'],
      ['gym', '/phaser/images/gym.webp'],
      ['hadouken', '/phaser/images/hadouken.webp'],
      ['home', '/phaser/images/home.webp'],
      ['hospital', '/phaser/images/hospital.webp'],
      ['library', '/phaser/images/library.webp'],
      ['movie.theater', '/phaser/images/movie.theater.webp'],
      ['park', '/phaser/images/park.webp'],
      ['restart', '/phaser/images/restart.webp'],
      ['school', '/phaser/images/school.webp'],
      ['sky', '/phaser/images/sky.webp'],
      ['store', '/phaser/images/store.webp'],
      ['supermarket', '/phaser/images/supermarket.webp'],
      ['swimming.pool', '/phaser/images/swimming.pool.webp'],
      ['toms.store', '/phaser/images/toms.store.webp'],
      ['train.station', '/phaser/images/train.station.webp'],
      ['zoo', '/phaser/images/zoo.webp']
    ],
    [
      ['explosion', '/phaser/sprites/explosion.webp', { frameWidth: 236, frameHeight: 331 }],
      ['shiba', '/phaser/sprites/shiba.webp', { frameWidth: 128, frameHeight: 128 }],
      ['balls', '/phaser/sprites/balls.webp', { frameWidth: 64, frameHeight: 64 }],
      ['clocks', '/phaser/sprites/clocks.webp', { frameWidth: 64, frameHeight: 64 }]
    ],
    [
      ['crunch', ['/phaser/sounds/crunch.mp3']],
      ['dog.yelp', ['/phaser/sounds/dog.yelp.mp3']],
      ['explosion', ['/phaser/sounds/explosion.mp3']]
    ]
  ); 
}

// ----------------------------------------------------------------
function PreloadStudents(game)
{
  PreloadResources(
    game,
    [
      ['ada', '/phaser/images/ada.webp'],
      ['aria', '/phaser/images/aria.webp'],
      ['ava', '/phaser/images/ava.webp'],
      ['bob', '/phaser/images/bob.webp'],
      ['cat', '/phaser/images/cat.webp'],
      ['eli', '/phaser/images/eli.webp'],
      ['ella', '/phaser/images/ella.webp'],
      ['kk', '/phaser/images/kk.webp'],
      ['ethan', '/phaser/images/ethan.webp'],
      ['finn', '/phaser/images/finn.webp'],
      ['jj', '/phaser/images/jj.webp'],
      ['haven', '/phaser/images/haven.webp'],
      ['iris', '/phaser/images/iris.webp'],
      ['jacob', '/phaser/images/jacob.webp'],
      ['jim', '/phaser/images/jim.webp'],
      ['john', '/phaser/images/john.webp'],
      ['karateshiba', '/phaser/images/karateshiba.webp'],
      ['leo', '/phaser/images/leo.webp'],
      ['liam', '/phaser/images/liam.webp'],
      ['luna', '/phaser/images/luna.webp'],
      ['michael', '/phaser/images/michael.webp'],
      ['milo', '/phaser/images/milo.webp'],
      ['sage', '/phaser/images/sage.webp'],
      ['tom', '/phaser/images/tom.webp'],
      ['tracy', '/phaser/images/tracy.webp']
    ]
  );    
}

// ----------------------------------------------------------------
function DefaultOnCreate(game, background)
{
  currentscene  = game;
  
  background  = background  || 'sky';
  
  currentmap  = background;
  
  cursors             = game.input.keyboard.createCursorKeys();
  
  shibadirection  = 'right';
  
  game.physics.resume();
  
  //  A simple background for our game
  game.add.image(400, 300, background);
  
  platforms = game.physics.add.staticGroup();

  if (currentmap == 'toms.store'
    || currentmap == 'bathroom'
    || currentmap == 'store'
    || currentmap == 'movie.theater'
    || currentmap == 'gym'
    || currentmap == 'library'
  ) 
  {
    platforms.create(400, 568, 'gray.floor').setScale(2).refreshBody();
  } else
  {
    platforms.create(400, 568, 'ground').setScale(2).refreshBody();
  }

  if (currentmap == 'sky')
  {
    platforms.create(0, 330, 'ground');
    platforms.create(800, 330, 'ground');
  }

  // The player and its settings
  player = game.physics.add.sprite(100, 450, 'shiba')
    .setSize(32, 32)
    .setOffset(32, 90);

  //  Player physics properties. Give the little guy a slight bounce.
  player.setCollideWorldBounds(true);

  //  Our player animations, turning, walking left and walking right.
  game.anims.create({
    key: 'left',
    frames: game.anims.generateFrameNumbers('shiba', { start: 2, end: 3 }),
    frameRate: 10,
    repeat: -1
  });

  game.anims.create({
    key: 'sitdownleft',
    frames: [ { key: 'shiba', frame: 0 } ],
    frameRate: 10
  });
  
  game.anims.create({
    key: 'sitdownright',
    frames: [ { key: 'shiba', frame: 7 } ],
    frameRate: 10
  });
  
  game.anims.create({
    key: 'standupleft',
    frames: [ { key: 'shiba', frame: 1 } ],
    frameRate: 10
  });
  
  game.anims.create({
    key: 'standupright',
    frames: [ { key: 'shiba', frame: 6 } ],
    frameRate: 10
  });
  
  game.anims.create({
    key: 'right',
    frames: game.anims.generateFrameNumbers('shiba', { start: 4, end: 5 }),
    frameRate: 10,
    repeat: -1
  });

  game.anims.create({
    key: 'shibaexplosion',
    frames: game.anims.generateFrameNumbers('explosion', { start: 0, end: 17 }),
    frameRate: 10,
    repeat: 0
  });
  
  //  Some stars to collect, 12 in total, evenly spaced 70 pixels apart along the x axis
  stars = game.physics.add.group({
    key: 'bone',
    repeat: 6,
    setXY: { x: 100, y: 0, stepX: 100 }
  });

  bombs = game.physics.add.group();

  //  The score
  scoreText = game.add.text(48, 0, gamescore.toString(), { fontSize: '32px', fill: '#000' });

  //  Collide the player and the stars with the platforms
  game.physics.add.collider(player, platforms);
  game.physics.add.collider(stars, platforms);

  //  Checks to see if the player overlaps with any of the stars, if he does call the collectStar function
  game.physics.add.overlap(player, stars, OnStarCollected, null, game);

  if (currentmap == 'sky')
  {
    game.physics.add.collider(bombs, platforms);
    game.physics.add.collider(player, bombs, OnBombCollide, null, game);
  }
  
  sounds['crunch']      = game.sound.add('crunch');
  sounds['explosion']   = game.sound.add('explosion');
  
  game.add.image(16, 16, 'bone')
    .setDisplaySize(32, 32);
    
  AddFullScreenControls(game);
  AddDPad(game);
}

// ====================================================================
function AddDPad(game)
{
  if (navigator.userAgent.toLowerCase().match (/mobile/i))
  {
    function OnDPadTap(pointer, x, y, e)
    {
      if (!pointer.isDown)
        return;
      
      if (x > 160)
      {
        cursors.right.isDown  = 1;
      } else 
      {
        cursors.right.isDown  = 0;
      }
      
      if (x < 120)
      {
        cursors.left.isDown  = 1;
      } else 
      {
        cursors.left.isDown  = 0;
      }
      
      if (y > 150)
      {
        cursors.down.isDown  = 1;
      } else 
      {
        cursors.down.isDown  = 0;
      }
      
      if (y < 110)
      {
        cursors.up.isDown  = 1;
      } else 
      {
        cursors.up.isDown  = 0;
      }
    }
    
    function OnDPadRelease(pointer, x, y, e)
    {
      cursors.left.isDown   = 0;
      cursors.up.isDown     = 0;
      cursors.right.isDown  = 0;
      cursors.down.isDown   = 0;
    }
    
    game.add.image(700, 500, 'dpad')
      .setSize(256, 256)
      .setDisplaySize(128, 128)
      .setOrigin(0.5)
      .setInteractive({ useHandCursor: true })
      .on('pointerdown', OnDPadTap)
      .on('pointermove', OnDPadTap)
      .on('pointerup', OnDPadRelease)
      .on('pointerout', OnDPadRelease);
  }
}

// ----------------------------------------------------------------
function DefaultOnUpdate()
{
  if (gameOver)
  {
    return;
  }

  if (cursors.left.isDown)
  {
    shibadirection  = 'left';
    player.setVelocityX(-320);

    player.anims.play('left', true);
  } else if (cursors.right.isDown)
  {
    shibadirection  = 'right';
    player.setVelocityX(320);

    player.anims.play('right', true);
  } else if (cursors.down.isDown)
  {
    player.setVelocityX(0);

    player.anims.play('sitdown' + shibadirection);
  } else
  {
    player.setVelocityX(0);

    player.anims.play('standup' + shibadirection);
  }

  if (cursors.up.isDown && player.body.touching.down)
  {
    player.setVelocityY(-380);
  }
}

// ----------------------------------------------------------------
function OnStarCollected(player, star)
{    
  sounds['crunch'].play();
  star.disableBody(true, true);

  //  Add and update the score
  gamescore++;
  scoreText.setText(gamescore);

  if (currentmap == 'sky'
    && stars.countActive(true) === 0
  )
  {
    //  A new batch of stars to collect
    stars.children.iterate(function (child) {

        child.enableBody(true, child.x, 0, true, true);

    });

    var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

    var bomb = bombs.create(x, 16, 'bomb');
    bomb.setBounce(1);
    bomb.setCollideWorldBounds(true);
    bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
    bomb.allowGravity = false;
  }
}

// ----------------------------------------------------------------
function OnBombCollide(player, bomb)
{
  sounds['explosion'].play();
  bomb.anims.play('shibaexplosion');
  player.destroy();
  
  this.physics.pause();
  gameOver = 1;
}

// ----------------------------------------------------------------
function CreateBalls(game)
{
  game.anims.create({
    key: 'one ball',
    frames: [ { key: 'balls', frame: 0 } ],
    frameRate: 10
  });

  game.anims.create({
    key: 'two balls',
    frames: [ { key: 'balls', frame: 1 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: 'three balls',
    frames: [ { key: 'balls', frame: 2 } ],
    frameRate: 10
  });

  game.anims.create({
    key: 'four balls',
    frames: [ { key: 'balls', frame: 3 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: 'five balls',
    frames: [ { key: 'balls', frame: 4 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: 'six balls',
    frames: [ { key: 'balls', frame: 5 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: 'seven balls',
    frames: [ { key: 'balls', frame: 6 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: 'eight balls',
    frames: [ { key: 'balls', frame: 7 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: 'nine balls',
    frames: [ { key: 'balls', frame: 8 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: 'ten balls',
    frames: [ { key: 'balls', frame: 9 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: 'eleven balls',
    frames: [ { key: 'balls', frame: 10 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: 'twelve balls',
    frames: [ { key: 'balls', frame: 11 } ],
    frameRate: 10
  });
  
  game.anims.create({
    key: "one o'clock",
    frames: [ { key: 'clocks', frame: 0 } ],
    frameRate: 10
  });

  game.anims.create({
    key: "two o'clock",
    frames: [ { key: 'clocks', frame: 1 } ],
    frameRate: 10
  });

  game.anims.create({
    key: "three o'clock",
    frames: [ { key: 'clocks', frame: 2 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: "four o'clock",
    frames: [ { key: 'clocks', frame: 3 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: "five o'clock",
    frames: [ { key: 'clocks', frame: 4 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: "six o'clock",
    frames: [ { key: 'clocks', frame: 5 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: "seven o'clock",
    frames: [ { key: 'clocks', frame: 6 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: "eight o'clock",
    frames: [ { key: 'clocks', frame: 7 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: "nine o'clock",
    frames: [ { key: 'clocks', frame: 8 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: "ten o'clock",
    frames: [ { key: 'clocks', frame: 9 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: "eleven o'clock",
    frames: [ { key: 'clocks', frame: 10 } ],
    frameRate: 10
  });    
  
  game.anims.create({
    key: "twelve o'clock",
    frames: [ { key: 'clocks', frame: 11 } ],
    frameRate: 10
  });
    
  game.anims.create({
    key: "hadouken",
    frames: [ { key: 'hadouken', frame: 0 } ],
    frameRate: 10
  });
}

// ----------------------------------------------------------------
function OnBallBallOverlap(ball1, ball2)
{
  if ((ball1.ignoreoverlapids.indexOf(ball2.gameobjid) != -1)
    || (ball2.ignoreoverlapids.indexOf(ball1.gameobjid) != -1)
  )
  {
    return;
  }
      
  sounds['explosion'].play();
  
  ball1.ignoreoverlapids.push(ball2.gameobjid)
  ball2.ignoreoverlapids.push(ball1.gameobjid)
  
  if (ball1.numballs > ball2.numballs)
  {
    ball1.numballs  -= ball2.numballs;
    
    if (ball1.gameobjtype == 'ball')
    {
      ball1.anims.play(ballamounts[ball1.numballs - 1])
    } else if (ball2.gameobjtype == 'clock')
    {
      ball1.anims.play(clockamounts[ball1.numballs - 1])
    }
    
    ball2.anims.play('shibaexplosion');
    
    ball2.on(
      'animationcomplete',
      function()
      {
        ball2.destroy();
      },
      ball2
    );
  } else if (ball1.numballs < ball2.numballs)
  {
    ball2.numballs  -= ball1.numballs;
    
    if (ball2.gameobjtype == 'ball')
    {
      ball2.anims.play(ballamounts[ball2.numballs - 1])
    } else if (ball2.gameobjtype == 'clock')
    {
      ball2.anims.play(clockamounts[ball2.numballs - 1])
    }
    
    ball1.anims.play('shibaexplosion');
    
    ball1.on(
      'animationcomplete',
      function()
      {
        ball1.destroy();
      },
      ball1
    );
  } else 
  {
    ball1.anims.play('shibaexplosion');
    ball2.anims.play('shibaexplosion');
    
    ball1.on(
      'animationcomplete',
      function()
      {
        ball1.destroy();
        ball2.destroy();
      },
      ball1
    );
  }
}

// ----------------------------------------------------------------
function BombBattleOnPreload()
{
  PreloadResources(
    this,
    [
      ['fullscreen', '/phaser/images/fullscreen.webp'],
      ['ground', '/phaser/images/platform.webp'],
      ['restart', '/phaser/images/restart.webp'],
      ['hadouken', '/phaser/images/hadouken.webp'],
      ['sky', '/phaser/images/sky.webp']
    ],
    [
      ['explosion', '/phaser/sprites/explosion.webp', { frameWidth: 236, frameHeight: 331 }],
      ['balls', '/phaser/sprites/balls.webp', { frameWidth: 64, frameHeight: 64 }],
      ['clocks', '/phaser/sprites/clocks.webp', { frameWidth: 64, frameHeight: 64 }]
    ],
    [
      ['hadouken', ['/phaser/sounds/hadouken.mp3']],
      ['dog.yelp', ['/phaser/sounds/dog.yelp.mp3']],
      ['explosion', ['/phaser/sounds/explosion.mp3']]
    ]
  );
  
  PreloadStudents(this);
}

// ----------------------------------------------------------------
function BombBattleOnCreate()
{
  let game  = this;
  currentscene  = this;
  
  game.physics.resume();
  
  //  A simple background for our game
  game.add.image(400, 300, 'sky');
  
  platforms = game.physics.add.staticGroup();

  platforms.create(200, 590, 'ground');
  platforms.create(600, 590, 'ground');

  platforms.create(200, 480, 'ground');
  platforms.create(600, 480, 'ground');
  
  platforms.create(200, 370, 'ground');
  platforms.create(600, 370, 'ground');
  
  platforms.create(200, 260, 'ground');
  platforms.create(600, 260, 'ground');
  
  platforms.create(200, 150, 'ground');
  platforms.create(600, 150, 'ground');
  
  CreateBalls(game);
  
  game.anims.create({
    key: 'shibaexplosion',
    frames: game.anims.generateFrameNumbers('explosion', { start: 0, end: 17 }),
    frameRate: 10,
    repeat: 0
  });
  
  players = game.physics.add.group();
  balls   = game.physics.add.group();

  game.physics.add.overlap(players, balls, OnPlayerBallOverlap, null, game);
  game.physics.add.overlap(balls, balls, OnBallBallOverlap, null, game);
  
  sounds['hadouken']    = game.sound.add('hadouken');
  sounds['explosion']   = game.sound.add('explosion');
  sounds['dog.yelp']    = game.sound.add('dog.yelp');
  
  AddFullScreenControls(game);
  
  setTimeout(
    function()
    {
      let x = 750,
        y = -20;
      
      G.studentbar.students  = Shuffle(G.studentbar.students);
      
      let studentjim  = 0;
      
      for (let i = 0, l = G.studentbar.students.length; i < l; i++)
      {
        let item        = G.studentbar.students[i];
        let gameobjname = item.studentname.toLowerCase();
        
        if (gameobjname == 'jim')
        {
          studentjim  = item;
          continue;
        }
        
        if (x > 50)
        {
          x = 50;
          y +=  112;
        } else
        {
          x = 750;
        }
        
        let p   = new BattlePlayer({
          scene:  currentscene, 
          file:   gameobjname, 
          x:      x, 
          y:      y
        });
        
        p.gameobjname = gameobjname;
        p.student     = item;
        
        currentscene.add.existing(p);
        players.add(p);
        gameobjs[gameobjname] = p;
        window[gameobjname]   = p;
      }
      
      if (x < 100)
      {
        x = 750;
        
        let gameobjname = 'jim';
        
        let p   = new BattlePlayer({
          scene:  currentscene, 
          file:   gameobjname, 
          x:      x, 
          y:      y
        });
        
        p.gameobjname = gameobjname;
        p.student     = studentjim;
        
        currentscene.add.existing(p);
        players.add(p);
        gameobjs[gameobjname] = p;
        window[gameobjname]   = p;
      }
    },
    3000
  )
}

// ----------------------------------------------------------------
function OnPlayerBallOverlap(player, ball)
{
  if (ball.playercollided || ball.ignoreoverlapids.indexOf(player.gameobjid) != -1)
  {
    // Ball is leaving the player's hands, so no collision occurs
    return;
  }
  
  sounds['explosion'].play();
  ball.anims.play('shibaexplosion');
  ball.playercollided = 1;
  
  ball.on(
    'animationcomplete',
    function()
    {
      ball.destroy();
    },
    ball
  );
  
  sounds['dog.yelp'].play();
  
  if (ball.gameobjtype == 'hadouken')
  {
    player.hpbar.Change(-100);
  } else
  {
    player.hpbar.Change(-34);
    
    L(ball)
    L(ball.playerfrom)
    
    G.studentbar.ChangeScores([
      {
        idcode: ball.playerfrom.student.idcode,
        bonus:  ball.numballs
      }
    ]);
  }  
  
  if (player.hpbar.value <= 0)
  {
    let objname = player.objname;
    player.alive  = 0;
    
    player.destroy();    
    
    G.studentbar.ChangeScores([
      {
        idcode: ball.playerfrom.student.idcode,
        bonus:  20
      }
    ]);
  }
}

// ====================================================================
function AddFullScreenControls(game)
{
  fullscreenbutton = game.add.image(772, 24, 'fullscreen')
    .setDisplaySize(32, 32)
    .setInteractive({ useHandCursor: true })
    .on('pointerover', function()
      {
        this.setTintFill(0xffff00);
      }
    )
    .on('pointerout', function()
      {
        this.clearTint();
      }        
    )
    .on('pointerdown', function() 
      {
        let canvas  = E('#GameCanvas canvas');
        
        if (phasergame.scale.isFullscreen) 
        {
          phasergame.scale.stopFullscreen();
          canvas.style.width  = '32em';
          canvas.style.height = '26em';
        } else 
        {
          phasergame.scale.startFullscreen();
          canvas.style.width  = '100vw';
          canvas.style.height = '100vh';
        }
      }
    );
    
  game.add.image(732, 24, 'restart')
    .setDisplaySize(32, 32)
    .setInteractive({ useHandCursor: true })
    .on('pointerover', function()
      {
        this.setTintFill(0xffff00);
      }        
    )
    .on('pointerout', function()
      {
        this.clearTint();
      }        
    )
    .on('pointerdown', function() 
      {
        G.RestartGame();
      }
    );
}

// ********************************************************************
class BaseScene extends Phaser.Scene
{
  // ----------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.gameobjs = {};
    this.sounds   = {};
    this.width    = 800;
    this.height   = 600;
    
    this.updatetypes  = [];
  }
  
  // ----------------------------------------------------------------
  PreloadFurniture()
  {
    PreloadResources(
      this,
      [
        ['chair', '/phaser/images/chair.webp'],
        ['sofa', '/phaser/images/sofa.webp'],
        ['desk', '/phaser/images/desk.webp'],
        ['TV', '/phaser/images/TV.webp'],
        ['door', '/phaser/images/door.webp'],
        ['window', '/phaser/images/window.webp'],
        ['table', '/phaser/images/table.webp'],
        ['bed', '/phaser/images/bed.webp']
      ]
    );
  }
    
  // ----------------------------------------------------------------
  PreloadStudents()
  {
    PreloadResources(
      this,
      [
        ['ada', '/phaser/images/ada.webp'],
        ['aria', '/phaser/images/aria.webp'],
        ['ava', '/phaser/images/ava.webp'],
        ['bob', '/phaser/images/bob.webp'],
        ['cat', '/phaser/images/cat.webp'],
        ['eli', '/phaser/images/eli.webp'],
        ['ella', '/phaser/images/ella.webp'],
        ['kk', '/phaser/images/kk.webp'],
        ['ethan', '/phaser/images/ethan.webp'],
        ['finn', '/phaser/images/finn.webp'],
        ['iris', '/phaser/images/iris.webp'],
        ['jacob', '/phaser/images/jacob.webp'],
        ['jj', '/phaser/images/jj.webp'],
        ['jim', '/phaser/images/jim.webp'],
        ['john', '/phaser/images/john.webp'],
        ['karateshiba', '/phaser/images/karateshiba.webp'],
        ['leo', '/phaser/images/leo.webp'],
        ['liam', '/phaser/images/liam.webp'],
        ['luna', '/phaser/images/luna.webp'],
        ['michael', '/phaser/images/michael.webp'],
        ['milo', '/phaser/images/milo.webp'],
        ['sage', '/phaser/images/sage.webp'],
        ['tom', '/phaser/images/tom.webp'],
        ['tracy', '/phaser/images/tracy.webp']
      ]
    );    
  }
  
  // ----------------------------------------------------------------
  RepositionOverlappingObjects(ignorelist)
  {
    ignorelist  = ignorelist  || [];
    
    let self  = this;
    
    // Reposition overlapping objects
    for (let k1 in self.gameobjs)
    {
      let overlapped  = 1;
      
      while (overlapped)
      {
        overlapped  = 0;
        
        for (let k2 in self.gameobjs)
        {
          if (k1 == k2)
          {
            continue;
          }
          
          let o1   = self.gameobjs[k1];
          let o2   = self.gameobjs[k2];
          
          if (
            ignorelist 
            && ignorelist.indexOf(o1.gameobjtype) != -1
          )
          {
            continue;
          }
          
          if (
            o1.x + o1.width + 32 > o2.x
            && o1.x < o2.x + o2.width + 32
            && o1.y + o1.height + 32 > o2.y
            && o1.y < o2.y + o2.height + 32
          )
          {
            overlapped  = 1;
            
            o1.setPosition(RandInt(50, self.width - 50), RandInt(50, self.height - 50));
            o1.UpdateChildren();
            
            break;
          }
        }          
      }
    }    
  }  

  // ----------------------------------------------------------------
  AddStudentAvatars()
  {
    let self  = this;
    
    let ls    = [];
    
    G.studentbar.students  = Shuffle(G.studentbar.students);
      
    for (let i = 0, l = G.studentbar.students.length; i < l; i++)
    {
      let item        = G.studentbar.students[i];
      let gameobjname = item.studentname.toLowerCase();
      
      ls.push(`
        <div class="StudentResults">
          <h3>${item.studentname}</h3>
          <pre class="Size150 Pad25 yellow ${gameobjname}Code"></pre>
          <div class="${gameobjname}Results"></div>
        </div>
      `);
      
      let p   = new self.playerclass({
        scene:  currentscene, 
        file:   gameobjname, 
        x:      RandInt(50, 750), 
        y:      RandInt(50, 550)
      });
      
      p.gameobjname = gameobjname;
      p.student     = item;
      
      self.add.existing(p);
      self.players.add(p);
      self.gameobjs[gameobjname] = p;
      window[gameobjname]   = p;
    }
    
    P.HTML('.CodeResults', ls);
  }    
  
  // ----------------------------------------------------------------
  update()
  {
    let self  = this;
    
    let objs  = Values(self.gameobjs);
    
    for (let i = 0, l = objs.length; i < l; i++)
    {
      let r  = objs[i];
      
      if (self.updatetypes.indexOf(r.gameobjtype) != -1)
      {
        r.Update();
      } 
    }    
  }  
}


// ********************************************************************
class FindFoodScene extends BaseScene
{
  // ----------------------------------------------------------------
  constructor()
  {
    super({ 
      key:      'FindFood',
      physics:  zerogravityphysics
    });
    
    this.updatetypes  = ['FoodPlayer', 'Furniture'];
    this.playerclass  = FoodPlayer;
  }
  
  // ----------------------------------------------------------------
  preload()
  {
    let self  = this;
    
    self.PreloadStudents();        
    self.PreloadFurniture();
    
    PreloadResources(
      this,
      [
        ['soccer field', '/phaser/images/soccer.field.webp'],
        
        ['beef', '/phaser/images/beef.webp'],
        ['cake', '/phaser/images/cake.webp'],
        ['candy', '/phaser/images/candy.webp'],
        ['chicken', '/phaser/images/chicken.webp'],
        ['fish', '/phaser/images/fish.webp'],
        ['fries', '/phaser/images/fries.webp'],
        ['fullscreen', '/phaser/images/fullscreen.webp'],
        ['ice cream', '/phaser/images/ice.cream.webp'],
        ['noodles', '/phaser/images/noodles.webp'],
        ['pork', '/phaser/images/pork.webp'],
        ['rice', '/phaser/images/rice.webp'],
        ['restart', '/phaser/images/restart.webp'],
      
        ['dad', '/phaser/images/dad.webp'],
        ['mom', '/phaser/images/mom.webp'],
        ['brother', '/phaser/images/brother.webp'],
        ['sister', '/phaser/images/sister.webp'],
        ['grandpa', '/phaser/images/grandpa.webp'],
        ['grandma', '/phaser/images/grandma.webp'],
        ['friend', '/phaser/images/friend.webp'],
        ['uncle', '/phaser/images/uncle.webp'],
        ['aunt', '/phaser/images/aunt.webp'],
        ['nephew', '/phaser/images/nephew.webp'],
        ['niece', '/phaser/images/niece.webp'],
        ['cousin', '/phaser/images/cousin.webp']        
      ],
      [
      ],
      [
        ['crunch',    ['/phaser/sounds/crunch.mp3']],
        ['smack',     ['/phaser/sounds/smack.mp3']],
        ['footstep',  ['/phaser/sounds/footstep.mp3']],
        ['yay',       ['/phaser/sounds/yay.mp3']],
        ['yum',       ['/phaser/sounds/yum.mp3']],
        ['disgust',   ['/phaser/sounds/disgust.mp3']],
        ['ding',      ['/phaser/sounds/ding.mp3']]
      ]
    );
  }  

  // ----------------------------------------------------------------
  create()
  {
    let self  = this;
      
    currentscene  = this;
    
    self.physics.resume();
    
    self.add.image(400, 300, 'soccer field');
    
    self.players       = self.physics.add.group();
    self.foods       = self.physics.add.group();
    self.weapons       = self.physics.add.group();
    self.familymembers = self.physics.add.group();
    
    self.physics.add.overlap(self.players, self.foods, self.OnPlayerFoodOverlap, null, self);
    self.physics.add.overlap(self.players, self.weapons, self.OnPlayerWeaponOverlap, null, self);
    self.physics.add.overlap(self.players, self.familymembers, self.OnPlayerFamilyOverlap, null, self);
    
    self.sounds['crunch']    = self.sound.add('crunch');
    self.sounds['smack']     = self.sound.add('smack');
    self.sounds['footstep']  = self.sound.add('footstep');
    self.sounds['ding']      = self.sound.add('ding');
    self.sounds['yay']       = self.sound.add('yay');
    self.sounds['yum']       = self.sound.add('yum');
    self.sounds['disgust']   = self.sound.add('disgust');
    
    self.physics.pause();
    
    setTimeout(
      function()
      {
        let x = 40,
          y   = 25;
          
        for (let i = 0, l = FAMILY_MEMBERS.length; i < l; i++)
        {
          let r = FAMILY_MEMBERS[i];
          
          let p   = new FamilyMember({
            scene:  currentscene, 
            file:   r,
            x:      x,
            y:      y
          });
          
          self.gameobjs[r]  = p; 
          
          self.familymembers.add(p);
          
          x +=  65;
        }
          
        for (let i = 0, l = FOODS.length; i < l; i++)
        {
          let k = FOODS[i];
          
          let p   = new FoodItem({
            scene:  currentscene, 
            file:   k,
            x:      RandInt(50, 750), 
            y:      RandInt(50, 550)
          });
          
          self.gameobjs[k]  = p; 
          
          self.foods.add(p);
        }

        self.AddStudentAvatars();
        self.RepositionOverlappingObjects(['FamilyMember']);
        
        self.physics.resume();
        
        setTimeout(
          function()
          {
            gamestarted = 1;
          },
          1000
        );
      },
      3000
    )    
  }  

  // ----------------------------------------------------------------
  OnPlayerFoodOverlap(player, food)
  {
    let self  = this;
    
    if (!gamestarted)
    {
      return;
    }
    
    let foodname  = food.gameobjname;
    
    player.food   = foodname;
    player.score  += food.points;
    player.scoredisplay.setText(player.score);
    
    G.studentbar.ChangeScores([
      {
        idcode: player.student.idcode,
        bonus:  food.points
      }
    ]);
    
    self.sounds['crunch'].play();
    
    delete gameobjs[foodname];
    food.Destroy();  
    
    if (player.itemdisplay)
    {
      player.itemdisplay.destroy();
    }
    
    player.itemdisplay = currentscene.add.image(
      player.x, 
      player.y + 10, 
      foodname
    ).setDisplaySize(32, 32);
    
    let allplayers  = self.players.getChildren();
    
    for (let i = 0, l = allplayers.length; i < l; i++)
    {
      let p = allplayers[i];
      
      if (p.target == foodname)
      {
        p.RunNextCodeLine();
      }
    }
  }  
    
  // ----------------------------------------------------------------
  OnPlayerWeaponOverlap(player, weapon)
  {
    let self  = this;
    
    if (!gamestarted)
    {
      return;
    }
    
    if (player.gameobjname != weapon.target.gameobjname)
    {
      return;
    }
    
    if (player.gameobjname == 'jim' && weapon.target.gameobjname == 'jim')
    {
      weapon.target     = window[weapon.playerfrom];
      weapon.playerfrom = 'jim';
      return;
    }  
    
    self.sounds['smack'].play();
    
    let weaponname  = weapon.gameobjname;
    
    if (player.x > weapon.x)
    {
      player.x  +=  weapon.speed * 50;
      player.UpdateChildren();
    } else if (player.x < weapon.x)
    {
      player.x  -=  weapon.speed * 50;
      player.UpdateChildren();
    }
    
    if (player.y > weapon.y)
    {
      player.y  +=  weapon.speed * 50;
      player.UpdateChildren();
    } else if (player.y < weapon.y)
    {
      player.y  -=  weapon.speed * 50;
      player.UpdateChildren();
    }
    
    delete self.gameobjs[weaponname];
    
    weapon.Destroy();
    
  }  
  
  // ----------------------------------------------------------------
  OnPlayerFamilyOverlap(player, family)
  {
    let self  = this;
    
    if (!player.food && player.target == family.gameobjname)
    {
      player.RunNextCodeLine();
      return;
    }
    
    let familyname  = family.gameobjname;
    let points      = FAMILY_FOODS[familyname][player.food];
      
    points  = parseInt(points);
    
    player.food = 0;
    
    if (player.fooddisplay)
    {
      player.fooddisplay.destroy();
      player.fooddisplay  = 0;  
    }
    
    if (!points)
    {
      return;
    }
    
    player.score  += points;
    player.scoredisplay.setText(player.score);
    
    G.studentbar.ChangeScores([
      {
        idcode: player.student.idcode,
        bonus:  points
      }
    ]);
    
    if (points > 0)
    {
      self.sounds['yum'].play();
    } else
    {
      self.sounds['disgust'].play();
    }
    
    player.RunNextCodeLine();
  }
}

// ********************************************************************
class AnimalChaserScene extends BaseScene
{
  // ----------------------------------------------------------------
  constructor()
  {
    super({ 
      key:      'AnimalChaser',
      physics:  zerogravityphysics
    });
    
    this.updatetypes  = ['FoodPlayer', 'Furniture', 'Animal'];
    this.playerclass  = FoodPlayer;
  }
  
  // ----------------------------------------------------------------
  preload()
  {
    let self  = this;
    
    self.PreloadStudents();        
    self.PreloadFurniture();
    
    PreloadResources(
      this,
      [
        ['park.overhead', '/phaser/images/park.overhead.webp'],
        
        ['beef', '/phaser/images/beef.webp'],
        ['cake', '/phaser/images/cake.webp'],
        ['candy', '/phaser/images/candy.webp'],
        ['chicken', '/phaser/images/chicken.webp'],
        ['fish', '/phaser/images/fish.webp'],
        ['fries', '/phaser/images/fries.webp'],
        ['fullscreen', '/phaser/images/fullscreen.webp'],
        ['ice cream', '/phaser/images/ice.cream.webp'],
        ['noodles', '/phaser/images/noodles.webp'],
        ['pork', '/phaser/images/pork.webp'],
        ['rice', '/phaser/images/rice.webp'],
        ['restart', '/phaser/images/restart.webp'],
      
        ['bird', '/phaser/images/bird.webp'],
        ['cat', '/phaser/images/cat.webp'],
        ['chicken.pet', '/phaser/images/chicken.pet.webp'],
        ['dog', '/phaser/images/dog.webp'],
        ['dragon', '/phaser/images/dragon.webp'],
        ['ferret', '/phaser/images/ferret.webp'],
        ['fish.pet', '/phaser/images/fish.pet.webp'],
        ['hamster', '/phaser/images/hamster.webp'],
        ['mouse', '/phaser/images/mouse.webp'],
        ['pig', '/phaser/images/pig.webp'],
        ['rabbit', '/phaser/images/rabbit.webp'],
        ['snake', '/phaser/images/snake.webp'],
        ['turtle', '/phaser/images/turtle.webp'],
        
        ['dad', '/phaser/images/dad.webp'],
        ['mom', '/phaser/images/mom.webp'],
        ['brother', '/phaser/images/brother.webp'],
        ['sister', '/phaser/images/sister.webp'],
        ['grandpa', '/phaser/images/grandpa.webp'],
        ['grandma', '/phaser/images/grandma.webp'],
        ['friend', '/phaser/images/friend.webp'],
        ['uncle', '/phaser/images/uncle.webp'],
        ['aunt', '/phaser/images/aunt.webp'],
        ['nephew', '/phaser/images/nephew.webp'],
        ['niece', '/phaser/images/niece.webp'],
        ['cousin', '/phaser/images/cousin.webp']        
      ],
      [
      ],
      [
        ['smack', ['/phaser/sounds/smack.mp3']],
        ['footstep', ['/phaser/sounds/footstep.mp3']],
        ['yay', ['/phaser/sounds/yay.mp3']],
        ['ding', ['/phaser/sounds/ding.mp3']]
      ]
    );
  }  

  // ----------------------------------------------------------------
  create()
  {
    let self  = this;
      
    currentscene  = this;
    
    self.physics.resume();
    
    self.add.image(400, 300, 'park.overhead');
    
    self.players       = self.physics.add.group();
    self.animals       = self.physics.add.group();
    self.weapons       = self.physics.add.group();
    self.familymembers = self.physics.add.group();
    
    self.physics.add.overlap(self.players, self.animals, self.OnPlayerAnimalOverlap, null, self);
    self.physics.add.overlap(self.players, self.weapons, self.OnPlayerWeaponOverlap, null, self);
    self.physics.add.overlap(self.players, self.familymembers, self.OnPlayerFamilyOverlap, null, self);
    
    self.sounds['smack']     = self.sound.add('smack');
    self.sounds['footstep']  = self.sound.add('footstep');
    self.sounds['ding']      = self.sound.add('ding');
    self.sounds['yay']       = self.sound.add('yay');
    
    self.physics.pause();
    
    setTimeout(
      function()
      {
        let x = 40,
          y   = 25;
          
        for (let i = 0, l = FAMILY_MEMBERS.length; i < l; i++)
        {
          let r = FAMILY_MEMBERS[i];
          
          let p   = new FamilyMember({
            scene:  currentscene, 
            file:   r,
            x:      x,
            y:      y
          });
          
          self.gameobjs[r]  = p; 
          
          self.familymembers.add(p);
          
          x +=  65;
        }
          
        for (let i = 0, l = ANIMALS.length; i < l; i++)
        {
          let k = ANIMALS[i];
          
          let p   = new Animal({
            scene:  currentscene, 
            file:   k,
            x:      RandInt(50, 750), 
            y:      RandInt(50, 550)
          });
          
          self.gameobjs[k]  = p; 
          
          self.animals.add(p);
        }

        self.AddStudentAvatars();
        self.RepositionOverlappingObjects(['FamilyMember']);
        
        self.physics.resume();
        
        setTimeout(
          function()
          {
            gamestarted = 1;
          },
          1000
        );
      },
      3000
    )    
  }  
  
  // ----------------------------------------------------------------
  OnPlayerAnimalOverlap(player, animal)
  {
    let self  = this;
    
    if (player.target == animal.gameobjname)
    {
      if (player.items.indexOf(animal.gameobjname) != -1)
      {
        let itemname  = animal.gameobjname;
        
        self.sounds['ding'].play();
        
        player.score  += animal.speed;
        player.scoredisplay.setText(player.score);
        
        G.studentbar.ChangeScores([
          {
            idcode: player.student.idcode,
            bonus:  animal.speed
          }
        ]);
        
        if (player.itemdisplay)
        {
          player.itemdisplay.destroy();
        }
        
        player.itemdisplay = self.add.image(
          player.x, 
          player.y + 10, 
          itemname
        ).setDisplaySize(32, 32);
        
        delete self.gameobjs[animal.gameobjname];
        animal.destroy();  
        
        let allplayers  = self.players.getChildren();
        
        for (let i = 0, l = allplayers.length; i < l; i++)
        {
          let p = allplayers[i];
          
          if (p.target == itemname)
          {
            p.RunNextCodeLine();
          }
        }        
      } else
      {
        self.sounds['footstep'].play();
        
        animal.x  -= (player.x - animal.x + RandInt(-10, 10)) * animal.speed;
        
        if (animal.x > 750)
        {
          animal.x  =  650 + RandInt(-50, 50);
        } else if (animal.x < 50)
        {
          animal.x  =  150 + RandInt(-50, 50);
        }
        
        animal.y  -= (player.y - animal.y + RandInt(-10, 10)) * animal.speed;
        
        if (animal.y > 550)
        {
          animal.y  =  450 + RandInt(-50, 50);
        } else if (animal.y < 50)
        {
          animal.y  =  150 + RandInt(-50, 50);
        }
      }
    }
  }  
  
  // ----------------------------------------------------------------
  OnPlayerWeaponOverlap(player, weapon)
  {
    let self  = this;
    
    if (!gamestarted)
    {
      return;
    }
    
    if (player.gameobjname != weapon.target.gameobjname)
    {
      return;
    }
    
    if (player.gameobjname == 'jim' && weapon.target.gameobjname == 'jim')
    {
      weapon.target     = window[weapon.playerfrom];
      weapon.playerfrom = 'jim';
      return;
    }  
    
    self.sounds['smack'].play();
    
    let weaponname  = weapon.gameobjname;
    
    if (player.x > weapon.x)
    {
      player.x  +=  weapon.speed * 50;
      player.UpdateChildren();
    } else if (player.x < weapon.x)
    {
      player.x  -=  weapon.speed * 50;
      player.UpdateChildren();
    }
    
    if (player.y > weapon.y)
    {
      player.y  +=  weapon.speed * 50;
      player.UpdateChildren();
    } else if (player.y < weapon.y)
    {
      player.y  -=  weapon.speed * 50;
      player.UpdateChildren();
    }
    
    delete self.gameobjs[weaponname];
    
    weapon.Destroy();
    
  }  
  
  // ----------------------------------------------------------------
  OnPlayerFamilyOverlap(player, family)
  {
    let self  = this;
    
    let familyitem  = FAMILY_ANIMALS[family.gameobjname];
    
    if (familyitem)
    {
      if (player.target == family.gameobjname)
      {
        if (player.items.indexOf(familyitem) == -1)
        {
          self.sounds['ding'].play();
          player.items.push(familyitem);
          
          if (player.itemdisplay)
          {
            player.itemdisplay.destroy();
          }
          
          player.itemdisplay = self.add.image(
            player.x, 
            player.y + 10, 
            'ice cream'
          ).setDisplaySize(32, 32);
          
          player.RunNextCodeLine();
        } else if (player.itemdisplay && player.itemdisplay.texture.key == familyitem)
        {
          let points  = ANIMAL_SPEEDS[familyitem];
          
          self.sounds['yay'].play();
          player.score  += points;
          
          player.scoredisplay.setText(player.score);
          
          G.studentbar.ChangeScores([
            {
              idcode: player.student.idcode,
              bonus:  points
            }
          ]);
          
          player.itemdisplay.destroy();
          player.itemdisplay  = 0;
          player.RunNextCodeLine();
        }
      }
    }
  }
}

// ********************************************************************
class ColorPlayer extends FoodPlayer {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.sequence = [];
  }
  
  // ------------------------------------------------------------------
  MyFavoriteColorIs(target)
  {
    this.target = target;
  }  
}

// ********************************************************************
class ColorCrystal extends GameObject {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.color        = config.color;          
    this.colornum     = config.colornum;
    
    this.gameobjname  = config.colorname;    
    
    config.scene.add.existing(this);
    
    this.setSize(24, 32);
    this.setTint(this.color);
  }
  
  // ------------------------------------------------------------------
  UpdateChildren()
  {
  }
  
  // ------------------------------------------------------------------
  Destroy()
  {
    this.destroy();
  }
  
  // ------------------------------------------------------------------
  Flash()
  {
    let self  = this;
    
    let originalcolor = self.color;
    
    self.setTintFill(0xffffff);
    self.scene.sounds['piano-' + (self.colornum + 1)].play();
    
    setTimeout(
      function()
      {
        self.clearTint();
        self.setTint(originalcolor);
      },
      1000
    )
  }
}

// ********************************************************************
class ColorSoundsScene extends BaseScene
{
  // ----------------------------------------------------------------
  constructor()
  {
    super({ 
      key:      'ColorSounds',
      physics:  zerogravityphysics
    });
    
    this.updatetypes  = ['ColorPlayer', 'Furniture'];
    this.playerclass  = ColorPlayer;
    
    this.COLORS  = [
      'black',
      'blue',
      'brown',
      'gray',
      'green',
      'orange',
      'pink',
      'purple',
      'red',
      'violet',
      'white',
      'yellow'
    ];
    
    this.COLORRGBS  = [
      0x222222,
      0x0000ff,
      0xb8860b,
      0x999999,
      0x00ff00,
      0xffa500,
      0xffc0cb,
      0x800080,
      0xff0000,
      0x8a2be2,
      0xffffff,
      0xffff00
    ];
    
    this.OnPlayerWeaponOverlap  = AnimalChaserScene.prototype.OnPlayerWeaponOverlap;
  }

  // ----------------------------------------------------------------
  preload()
  {
    let self  = this;
    
    self.PreloadFurniture();
    self.PreloadStudents();    
    
    PreloadResources(
      this,
      [
        ['cave', '/phaser/images/cave.webp'],        
        ['crystal', '/phaser/images/crystal.webp']
      ],
      [
      ],
      [
        ['piano-1', ['/phaser/sounds/piano-01.mp3']],
        ['piano-2', ['/phaser/sounds/piano-02.mp3']],
        ['piano-3', ['/phaser/sounds/piano-03.mp3']],
        ['piano-4', ['/phaser/sounds/piano-04.mp3']],
        ['piano-5', ['/phaser/sounds/piano-05.mp3']],
        ['piano-6', ['/phaser/sounds/piano-06.mp3']],
        ['piano-7', ['/phaser/sounds/piano-07.mp3']],
        ['piano-8', ['/phaser/sounds/piano-08.mp3']],
        ['piano-9', ['/phaser/sounds/piano-09.mp3']],
        ['piano-10', ['/phaser/sounds/piano-10.mp3']],
        ['piano-11', ['/phaser/sounds/piano-11.mp3']],
        ['piano-12', ['/phaser/sounds/piano-12.mp3']],
        ['smack', ['/phaser/sounds/smack.mp3']],
        ['yay', ['/phaser/sounds/yay.mp3']],
        ['disgust', ['/phaser/sounds/disgust.mp3']],
        ['ding', ['/phaser/sounds/ding.mp3']]
      ]
    );
    
  }  

  // ----------------------------------------------------------------
  CreateSequence()
  {
    let self  = this;
    
    let maxscore  = 0;
    
    for (let i = 0, l = G.studentbar.students.length; i < l; i++)
    {
      let r = G.studentbar.students[i];
      
      if (r.score > maxscore)
      {
        maxscore  = r.score;
      }
    }
    
    let sequencelength  = Math.round(maxscore / 10) + 2;
    
    if (sequencelength > 8)
    {
      sequencelength  = 8;
    }
    
    let sequence  = [];
    let numcolors = self.COLORS.length;
    
    for (let i = 0; i < sequencelength; i++)
    {
      let colorname = self.COLORS[RandInt(0, numcolors)];
      
      while (colorname == sequence[sequence.length - 1])
      {
        colorname = self.COLORS[RandInt(0, numcolors)];
      }
      
      sequence.push(colorname);
    }    
    
    self.sequence = sequence;
  }
  
  // ----------------------------------------------------------------
  PlaySequence()
  {
    let self  = this;
    
    for (let i = 0, l = self.sequence.length; i < l; i++)
    {
      setTimeout(
        function()
        {
          self.gameobjs[self.sequence[i]].Flash();
        },
        i * 1500
      );
    }
  }
  
  // ----------------------------------------------------------------
  create()
  {
    let self  = this;
      
    currentscene  = this;
    
    self.physics.resume();
    
    self.add.image(400, 300, 'cave');
    
    self.players       = self.physics.add.group();
    self.crystals      = self.physics.add.group();
    self.weapons       = self.physics.add.group();
    
    self.physics.add.overlap(self.players, self.crystals, self.OnPlayerCrystalOverlap, null, self);
    self.physics.add.overlap(self.players, self.weapons, self.OnPlayerWeaponOverlap, null, self);
    
    self.sounds['piano-1']   = self.sound.add('piano-1');
    self.sounds['piano-2']   = self.sound.add('piano-2');
    self.sounds['piano-3']   = self.sound.add('piano-3');
    self.sounds['piano-4']   = self.sound.add('piano-4');
    self.sounds['piano-5']   = self.sound.add('piano-5');
    self.sounds['piano-6']   = self.sound.add('piano-6');
    self.sounds['piano-7']   = self.sound.add('piano-7');
    self.sounds['piano-8']   = self.sound.add('piano-8');
    self.sounds['piano-9']   = self.sound.add('piano-9');
    self.sounds['piano-10']  = self.sound.add('piano-10');
    self.sounds['piano-11']  = self.sound.add('piano-11');
    self.sounds['piano-12']  = self.sound.add('piano-12');
    self.sounds['ding']      = self.sound.add('ding');
    self.sounds['disgust']   = self.sound.add('disgust');
    self.sounds['yay']       = self.sound.add('yay');
    self.sounds['smack']     = self.sound.add('smack');
    
    self.physics.pause();
    
    setTimeout(
      function()
      {
        let x = 40,
          y   = 25;
          
        for (let i = 0, l = self.COLORS.length; i < l; i++)
        {
          let r = self.COLORS[i];
          
          let p   = new ColorCrystal({
            scene:      currentscene, 
            file:       'crystal',
            x:          x,
            y:          y,
            colornum:   i,
            color:      self.COLORRGBS[i],
            colorname:  r
          });
          
          self.gameobjs[r]  = p; 
          
          self.crystals.add(p);
          
          x +=  65;
          y +=  48;
        }
          
        self.AddStudentAvatars();
        self.RepositionOverlappingObjects(['ColorCrystal']);
        
        self.physics.resume();
        
        setTimeout(
          function()
          {
            gamestarted = 1;
            
            self.CreateSequence();
            self.PlaySequence();
          },
          1000
        );
      },
      3000
    )    
  }  
  
  // ----------------------------------------------------------------
  OnPlayerCrystalOverlap(player, crystal)
  {
    let self  = this;
    
    if (player.target == crystal.gameobjname)
    {
      player.sequence.push(crystal.gameobjname);
      player.target = '';
      
      let sequencelength  = player.sequence.length;
      
      if (IsEqual(player.scene.sequence.slice(0, sequencelength), player.sequence))
      {
        let points  = sequencelength;
        
        self.sounds['ding'].play();
        
        crystal.Flash();
        
        player.score  += points;
        
        player.scoredisplay.setText(player.score);
        
        G.studentbar.ChangeScores([
          {
            idcode: player.student.idcode,
            bonus:  points
          }
        ]);
        
        player.RunNextCodeLine();
      } else
      {
        self.sounds['disgust'].play();
        player.setTint(0xff0000);
      }
    }
  }
}


// ********************************************************************
class BodyPartsPlayer extends FoodPlayer {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.scoredisplay.setColor('#009900');
    
    this.parts  = [];
  }
  
  // ------------------------------------------------------------------
  Touch(target)
  {
    if (target && IsString(target))
    {
      let parts = target.split(' ');
      
      if (parts.length == 2 && parts[0] == 'your')
      {
        this.target = parts[1];
      } else
      {
        throw new Error('没有这个身体部');
      }
    }
  }  
  
  // ------------------------------------------------------------------
  RunCodeLine(code)
  {
    let self  = this;
    
    code  = code  || '';
    
    code  = code.trim().replace(/\r/g, "").split(/\n/)[0];
    
    if (code)
    {
      let resultsdiv  = E('.' + self.gameobjname + 'Results');
      
      if (!resultsdiv)
      {
        return;
      }
      
      E('.' + self.gameobjname + 'Code').innerText  = code;
      
      try
      {
        let parts = code.split('.');
        
        if (parts && parts[0] != this.gameobjname)
        {
          throw new Error('学生名字错了！');
        }
        
        let results  = eval(code);
        
        P.HTML(
          resultsdiv, 
          `<div class="lgreenb Pad50 Margin25">${results}</div>
        `);
        
        P.API(
          '/learn/answersapi',
          {
            command:        'setcoderesult',
            studentidcode:  this.student.idcode,
            coderesult: {
              line:   code,
              result: ''
            }
          },
          function(d)
          {
          }
        );
      } catch (error)
      {
        this.haserror = 1;
        
        P.HTML(
          resultsdiv, 
          `<div class="redb Pad50 Margin25">${error}</div>
        `);
        
        P.API(
          '/learn/answersapi',
          {
            command:    'setcoderesult',
            studentidcode:  this.student.idcode,
            coderesult: {
              line:   code,
              result: `${error}`
            }
          },
          function(d)
          {
          }
        );
      }
    }
  }  
}

// ********************************************************************
class RedDot extends GameObject {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    config.file = 'reddot';
    
    super(config);
    
    config.scene.add.existing(this);
    
    this.setSize(8, 8);
    this.setDisplaySize(8, 8);
    
    this.gameobjtype  = 'reddot';
  }
  
  // ------------------------------------------------------------------
  UpdateChildren()
  {
  }
  
  // ------------------------------------------------------------------
  Destroy()
  {
    this.destroy();
  }
}


// ********************************************************************
class BodyPartsScene extends BaseScene
{
  // ----------------------------------------------------------------
  constructor()
  {
    super({ 
      key:      'BodyParts',
      physics:  zerogravityphysics
    });
    
    this.updatetypes  = ['BodyPartsPlayer', 'Furniture'];
    this.playerclass  = BodyPartsPlayer;
    
    this.PARTS  = {
      arm:  {
        x:        293,
        y:        274
      },
      body:  {
        x:        386,
        y:        313
      },
      ear:  {
        x:        434,
        y:        102
      },
      eye:  {
        x:        371,
        y:        81
      },
      face:  {
        x:        357,
        y:        122
      },
      finger:  {
        x:        291,
        y:        378
      },
      foot:  {
        x:        329,
        y:        569
      },
      hand:  {
        x:        305,
        y:        353
      },
      hair:  {
        x:        399,
        y:        42
      },
      head:  {
        x:        398,
        y:        70
      },
      knee:  {
        x:        353,
        y:        471
      },
      leg:  {
        x:        428,
        y:        434
      },
      mouth:  {
        x:        387,
        y:        128
      },
      nose:  {
        x:        397,
        y:        101
      },
      shoulder:  {
        x:        430,
        y:        182
      },
      toe:  {
        x:        489,
        y:        577
      }
    };
    
    this.partsleft    = Keys(this.PARTS);
    this.currentpart  = 0;
    
    this.timeleft     = 1;
    
    this.OnPlayerWeaponOverlap  = AnimalChaserScene.prototype.OnPlayerWeaponOverlap;
  }

  // ----------------------------------------------------------------
  preload()
  {
    let self  = this;
    
    self.PreloadFurniture();
    self.PreloadStudents();
    
    let sounds  = [
      ['smack', ['/phaser/sounds/smack.mp3']],
      ['yay', ['/phaser/sounds/yay.mp3']],
      ['disgust', ['/phaser/sounds/disgust.mp3']],
      ['ding', ['/phaser/sounds/ding.mp3']]
    ];
    
    for (let i = 0, l = self.partsleft.length; i < l; i++)
    {
      let r = self.partsleft[i];
      
      sounds.push([r, '/phaser/sounds/' + r + '.mp3']);
    }
    
    PreloadResources(
      this,
      [
        ['body', '/phaser/images/body.webp'],        
        ['reddot', '/phaser/images/shiny.dot.webp']
      ],
      [
      ],
      sounds
    );    
  }  

  // ----------------------------------------------------------------
  CreateSequence()
  {
    let self  = this;
    
    self.partsleft = Shuffle(self.partsleft);
    
    self.timerid  = setInterval(
      function()
      { 
        self.OnTimer();
      }, 
      1000
    );
  }
  
  // ----------------------------------------------------------------
  OnTimer()
  {
    let self  = this;
    
    self.timeleft--;
    
    self.timedisplay.setText(SecondsToTime(self.timeleft, 1));
    
    if (self.timeleft == 0)
    {
      self.PlayNextPart();
    }
    
    for (let i = 0, l = G.studentbar.students.length; i < l; i++)
    {
      let r = G.studentbar.students[i];
      
      let studentname = r.studentname.toLowerCase();
      
      let student = self.gameobjs[studentname];
      
      if (student)
      {
        student.RunCodeLine(r.answer);
      }
    }
  }
  
  // ----------------------------------------------------------------
  PlayNextPart()
  {
    let self  = this;
    
    if (self.partsleft.length)
    {
      self.currentpart = self.partsleft.pop();
      
      self.sounds[self.currentpart].play();      
      
      self.timeleft = 30;
    } else
    {
      clearInterval(self.timerid);
      self.timerid  = 0;
    }
  }
  
  // ----------------------------------------------------------------
  create()
  {
    let self  = this;
      
    currentscene  = this;
    
    self.physics.resume();
    
    self.add.image(400, 300, 'body');
    
    self.players       = self.physics.add.group();
    self.bodyparts     = self.physics.add.group();
    self.weapons       = self.physics.add.group();
    
    self.physics.add.overlap(self.players, self.bodyparts, self.OnPlayerBodyPartOverlap, null, self);
    self.physics.add.overlap(self.players, self.weapons, self.OnPlayerWeaponOverlap, null, self);
    
    for (let i = 0, l = self.partsleft.length; i < l; i++)
    {
      let r = self.partsleft[i];
      
      self.sounds[r]  = self.sound.add(r);
    }
    
    self.sounds['ding']      = self.sound.add('ding');
    self.sounds['disgust']   = self.sound.add('disgust');
    self.sounds['yay']       = self.sound.add('yay');
    self.sounds['smack']     = self.sound.add('smack');
    
    self.timedisplay  = self.add.text(
      10, 
      10, 
      '00:30',
      {
        color:      'blue',
        fontSize:   '24px',
        fontStyle:  'bold'
      }
    );
    
    self.physics.pause();
    
    setTimeout(
      function()
      {
        for (let i = 0, l = self.partsleft.length; i < l; i++)
        {
          let r     = self.partsleft[i];
          let part  = self.PARTS[r];
          
          let p   = new RedDot({
            scene:      currentscene, 
            x:          part.x,
            y:          part.y
          });
          
          p.gameobjname = r;
          p.points      = 8;
          
          self.gameobjs[r]  = p; 
          
          self.bodyparts.add(p);
        }
          
        self.AddStudentAvatars();
        self.RepositionOverlappingObjects(['reddot']);
        
        self.physics.resume();
        
        setTimeout(
          function()
          {
            gamestarted = 1;
            
            self.CreateSequence();
          },
          1000
        );
      },
      3000
    )    
  }  
  
  // ----------------------------------------------------------------
  OnPlayerBodyPartOverlap(player, bodypart)
  {
    let self  = this;
    
    let partname  = bodypart.gameobjname;
    
    if (player.target == partname
      && partname == self.currentpart
      && player.parts.indexOf(partname) == -1
    )
    {
      player.parts.push(partname);
      player.target = '';
      
      let points = bodypart.points > 1 
        ? Math.round(bodypart.points / 2)
        : 0;
      
      if (points)
      {
        bodypart.points -= points;
        
        self.sounds['ding'].play();
        
        player.score  += points;
        
        player.scoredisplay.setText(player.score);
        
        G.studentbar.ChangeScores([
          {
            idcode: player.student.idcode,
            bonus:  points
          }
        ]);
      }
    }
  }
}


// ********************************************************************
class FruitsPlayer extends BodyPartsPlayer {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.scoredisplay.setColor('#ffffff');
    
    this.thesefruits  = [];
    this.thosefruits  = [];
    
    this.targetmode   = '';
  }
  
  // ------------------------------------------------------------------
  TheseAre(target)
  {
    let self  = this;
    
    if (target && IsString(target) && self.scene.fruitsadded.indexOf(target) != -1)
    {
      this.targetmode = 'these';      
      this.target     = target;
    } else
    {
      throw new Error('没有这个水果');
    }
  }  
  
  // ------------------------------------------------------------------
  ThoseAre(target)
  {
    let self  = this;
    
    if (target 
      && IsString(target) 
      && self.scene.fruitsadded.indexOf(target) != -1
    )
    {
      self.targetmode = 'those';
      self.target     = '';
      
      if (
        target == self.scene.targetfruit
        && self.thosefruits.indexOf(target) == -1
        && self.scene.targetmode == 'those'
      )
      {
        self.thosefruits.push(target);
        
        let fruit = self.scene.gameobjs[target];
        
        let points = fruit.points > 1 
          ? Math.round(fruit.points / 2)
          : 0;
        
        if (points)
        {
          fruit.points -= points;
          
          self.scene.sounds['ding'].play();
          
          self.score  += points;
          
          self.scoredisplay.setText(self.score);
          
          G.studentbar.ChangeScores([
            {
              idcode: self.student.idcode,
              bonus:  points
            }
          ]);
        }
      }
    } else
    {
      throw new Error('没有这个水果');
    }
  }  
}

// ********************************************************************
class Fruit extends GameObject {
  
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    config.scene.add.existing(this);
  }
  
  // ------------------------------------------------------------------
  UpdateChildren()
  {
  }
  
  // ------------------------------------------------------------------
  Destroy()
  {
    this.destroy();
  }
}


// ********************************************************************
class FruitsScene extends BaseScene
{
  // ----------------------------------------------------------------
  constructor()
  {
    super({ 
      key:      'Fruits',
      physics:  zerogravityphysics
    });
    
    this.updatetypes  = ['FruitsPlayer', 'Furniture'];
    this.playerclass  = FruitsPlayer;
    
    this.phrases    = [
      'these.are.apples',
      'these.are.bananas',
      'these.are.cherries',
      'these.are.grapes',
      'these.are.grapefruits',
      'these.are.lemons',
      'these.are.oranges',
      'these.are.peaches',
      'these.are.pears',
      'these.are.pineapples',
      'these.are.strawberries',
      'these.are.watermelons',
      'those.are.apples',
      'those.are.bananas',
      'those.are.cherries',
      'those.are.grapes',
      'those.are.grapefruits',
      'those.are.lemons',
      'those.are.oranges',
      'those.are.peaches',
      'those.are.pears',
      'those.are.pineapples',
      'those.are.strawberries',
      'those.are.watermelons'
    ];
    
    this.timeleft     = 1;
    
    this.OnPlayerWeaponOverlap  = AnimalChaserScene.prototype.OnPlayerWeaponOverlap;
    
    this.fruitsadded  = [];
  }

  // ----------------------------------------------------------------
  preload()
  {
    let self  = this;
    
    L(this, currentscene)
    currentscene  = this;
    
    self.PreloadFurniture();
    self.PreloadStudents();
    
    let sounds  = [
      ['smack', ['/phaser/sounds/smack.mp3']],
      ['yay', ['/phaser/sounds/yay.mp3']],
      ['disgust', ['/phaser/sounds/disgust.mp3']],
      ['ding', ['/phaser/sounds/ding.mp3']]
    ];
    
    let images  = [
      ['jungle', '/phaser/images/jungle.webp']
    ];
    
    for (let i = 0, l = self.phrases.length; i < l; i++)
    {
      let r = self.phrases[i];
      
      let fruit = r.split('.')[2];
      
      sounds.push([r, '/phaser/sounds/' + r + '.mp3']);
      images.push([fruit, '/phaser/images/' + fruit + '.webp'])
    }
    
    PreloadResources(
      this,
      images,
      [
      ],
      sounds
    );    
  }  

  // ----------------------------------------------------------------
  CreateSequence()
  {
    let self  = this;
    
    self.phrases = Shuffle(self.phrases);
    
    self.timerid  = setInterval(
      function()
      { 
        self.OnTimer();
      }, 
      1000
    );
  }
  
  // ----------------------------------------------------------------
  OnTimer()
  {
    let self  = this;
    
    self.timeleft--;
    
    self.timedisplay.setText(SecondsToTime(self.timeleft, 1));
    
    if (self.timeleft == 0)
    {
      self.PlayNextPart();
    }
    
    for (let i = 0, l = G.studentbar.students.length; i < l; i++)
    {
      let r = G.studentbar.students[i];
      
      let studentname = r.studentname.toLowerCase();
      
      let student = self.gameobjs[studentname];
      
      if (student)
      {
        student.RunCodeLine(r.answer);
      }
    }
  }
  
  // ----------------------------------------------------------------
  PlayNextPart()
  {
    let self  = this;
    
    if (self.phrases.length)
    {
      self.currentphrase = self.phrases.pop();
      
      let parts = self.currentphrase.split('.');
      
      self.targetmode   = parts[0];
      self.targetfruit  = parts[2];
            
      self.sounds[self.currentphrase].play();     
      
      self.gameobjs[self.targetfruit].points  = 8;
      
      self.timeleft = 30;
    } else
    {
      clearInterval(self.timerid);
      self.timerid  = 0;
    }
  }
  
  // ----------------------------------------------------------------
  create()
  {
    let self  = this;
      
    currentscene  = this;
    
    self.physics.resume();
    
    self.add.image(400, 300, 'jungle');
    
    self.players       = self.physics.add.group();
    self.fruits        = self.physics.add.group();
    self.weapons       = self.physics.add.group();
    
    self.physics.add.overlap(self.players, self.fruits, self.OnPlayerBodyPartOverlap, null, self);
    self.physics.add.overlap(self.players, self.weapons, self.OnPlayerWeaponOverlap, null, self);
    
    for (let i = 0, l = self.phrases.length; i < l; i++)
    {
      let r = self.phrases[i];
      
      self.sounds[r]  = self.sound.add(r);
    }
    
    self.sounds['ding']      = self.sound.add('ding');
    self.sounds['disgust']   = self.sound.add('disgust');
    self.sounds['yay']       = self.sound.add('yay');
    self.sounds['smack']     = self.sound.add('smack');
    
    self.timedisplay  = self.add.text(
      10, 
      10, 
      '00:30',
      {
        color:      'white',
        fontSize:   '24px',
        fontStyle:  'bold'
      }
    );
    
    self.physics.pause();
    
    setTimeout(
      function()
      {
        for (let i = 0, l = self.phrases.length; i < l; i++)
        {
          let r     = self.phrases[i];
          let fruit = r.split('.')[2];
          
          if (self.fruitsadded.indexOf(fruit) != -1)
          {
            continue;
          }
          
          self.fruitsadded.push(fruit);
          
          let p   = new Fruit({
            scene:      currentscene, 
            file:       fruit
          });
          
          p.gameobjname = fruit;
          
          self.gameobjs[fruit]  = p; 
          
          self.fruits.add(p);
        }
          
        self.AddStudentAvatars();
        self.RepositionOverlappingObjects(['fruit']);
        
        self.physics.resume();
        
        setTimeout(
          function()
          {
            gamestarted = 1;
            
            self.CreateSequence();
          },
          1000
        );
      },
      3000
    )    
  }  
  
  // ----------------------------------------------------------------
  OnPlayerBodyPartOverlap(player, fruit)
  {
    let self  = this;
    
    let fruitname  = fruit.gameobjname;
    
    if (player.target == fruitname
      && fruitname == self.targetfruit
      && player.thesefruits.indexOf(fruitname) == -1
      && self.targetmode == 'these'
    )
    {
      player.thesefruits.push(fruitname);
      player.target = '';
      
      let points = fruit.points > 1 
        ? Math.round(fruit.points / 2)
        : 0;
      
      if (points)
      {
        fruit.points -= points;
        
        self.sounds['ding'].play();
        
        player.score  += points;
        
        player.scoredisplay.setText(player.score);
        
        G.studentbar.ChangeScores([
          {
            idcode: player.student.idcode,
            bonus:  points
          }
        ]);
      }
    }
  }
}


// ********************************************************************
var ballamounts = [
  'one ball',
  'two balls',
  'three balls',
  'four balls',
  'five balls',
  'six balls',
  'seven balls',
  'eight balls',
  'nine balls',
  'ten balls',
  'eleven balls',
  'twelve balls'
];

var clockamounts = [
  "one o'clock",
  "two o'clock",
  "three o'clock",
  "four o'clock",
  "five o'clock",
  "six o'clock",
  "seven o'clock",
  "eight o'clock",
  "nine o'clock",
  "ten o'clock",
  "eleven o'clock",
  "twelve o'clock",
];

// ********************************************************************
let PHASER_SCENES  = {
  'Bomb Shiba': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'sky');
              },
    update:   DefaultOnUpdate
  },
  'Home': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'home');
              },
    update:   DefaultOnUpdate
  },
  'School': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'school');
              },
    update:   DefaultOnUpdate
  },
  'Store': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'store');
              },
    update:   DefaultOnUpdate
  },
  'Bathroom': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'bathroom');
              },
    update:   DefaultOnUpdate
  },
  "Tom's Store": {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'toms.store');
              },
    update:   DefaultOnUpdate
  },
  'Supermarket': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'supermarket');
              },
    update:   DefaultOnUpdate
  },
  'Movie Theater': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'movie.theater');
              },
    update:   DefaultOnUpdate
  },
  'Hospital': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'hospital');
              },
    update:   DefaultOnUpdate
  },
  'Bus Station': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'bus.station');
              },
    update:   DefaultOnUpdate
  },
  'Train Station': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'train.station');
              },
    update:   DefaultOnUpdate
  },
  'Airport': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'airport');
              },
    update:   DefaultOnUpdate
  },
  'Gym': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'gym');
              },
    update:   DefaultOnUpdate
  },
  'Swimming Pool': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'swimming.pool');
              },
    update:   DefaultOnUpdate
  },
  'Park': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'park');
              },
    update:   DefaultOnUpdate
  },
  'Bank': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'bank');
              },
    update:   DefaultOnUpdate
  },
  'Library': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'library');
              },
    update:   DefaultOnUpdate
  },
  'Zoo': {
    physics:  defaultphysics,
    preload:  DefaultOnPreload,
    create:   function ()
              {
                DefaultOnCreate(this, 'zoo');
              },
    update:   DefaultOnUpdate
  },
  'Bomb Battle': {
    physics:  zerogravityphysics,
    preload:  BombBattleOnPreload,
    create:   BombBattleOnCreate,
    update:   function() {}
  },
  'Find Food':       FindFoodScene,
  'Animal Chaser':   AnimalChaserScene,
  'Color Sounds':    ColorSoundsScene,
  'Body Parts':      BodyPartsScene,
  'FruitsGrabber':   FruitsScene
}

// ====================================================================
G.LoadPhaserScene = function(scene)
{
  if (currentscene)
  {
    phasergame.scene.pause();
    currentscene.physics.pause(scene);
    runningscenes.remove(currentscene.scene.key);
  }
  
  phasergame.scene.run(scene);
  phasergame.scene.bringToTop(scene);
  runningscenes.push(scene);
  
  setTimeout(
    function()
    {
      currentscene = phasergame.scene.getScene(scene);
      currentscene.physics.resume();
    },
    200
  );
}

// ====================================================================
G.InitPhaser  = function(scenename)
{
  scenename   = scenename || 'shibabattle';  
  
  var gameconfig = {
    type:   Phaser.AUTO,
    width:  800,
    height: 600,
    parent: 'GameCanvas',
    scene:  [PHASER_SCENES[scenename]]
  };
  
  phasergame          = new Phaser.Game(gameconfig);  
  
  // Seems like a roundabout way to add scenes, but I've found that 
  // phaser works best when first initialized with a single scene
  for (let k in PHASER_SCENES)
  {
    if (k == scenename)
    {
      continue;
    }
    
    phasergame.scene.add(k, PHASER_SCENES[k], false);
  }
  
  /*G.LoadPhaserScene(scenename);*/
  
  let canvas  = E('#GameCanvas canvas');
  canvas.style.width  = '32em';
  canvas.style.height = '26em';  
}

// ====================================================================
G.RestartGame = function()
{
  gamescore = 0;
  G.LoadPhaserScene(scenename)
}

// ====================================================================
G.RunCode = function()
{
  G.RunCodeLines(E('.StudentCode').value);
}

// ====================================================================
G.RunBattleCode = function()
{
  let errors  = [];
  
  for (let i = 0, l = G.studentbar.students.length; i < l; i++)
  {
    let item  = G.studentbar.students[i];
    let codelines = item.answer.trim().replace(/\r/g, "").split(/\n/);
    
    errors.push(`
      <h3>${item.studentname}</h3>
      <pre class="lyellow Pad50 Size150">${codelines}</pre>
    `);
    
    try
    {
      if (codelines.length > 1 || codelines[0].indexOf(';') != -1)
      {
        throw new Error('Only one command can be sent in battle.');
      }
      
      let parts = codelines[0].split('.');
      
      if (parts && parts[0] != item.studentname.toLowerCase())
      {
        throw new Error('Student name was wrong.');
      }
      
      errors.push(`
        <div class="lgreenb Pad50 Margin25 Size150">${eval(codelines[0])}</div>
      `);
    } catch (error)
    {
      errors.push(
        '<div class="redb Pad50 Margin25 Size150">' + error + '</div>'
      );
    }
    
    errors.push('<br>');
  }
  
  P.HTML('.CodeResults', errors);
}

// ====================================================================
G.RunFoodCode = function()
{
  let errors  = [];
  
  for (let i = 0, l = G.studentbar.students.length; i < l; i++)
  {
    let item      = G.studentbar.students[i];
    let codelines = item.answer.trim().replace(/\r/g, "").split(/\n/);
    let gamename  = item.studentname.toLowerCase();
    
    let playerobj = currentscene.gameobjs[gamename];
      
    if (playerobj)
    {
      playerobj.codelines = codelines;
      playerobj.RunNextCodeLine();
    }
  }
}

// ====================================================================
G.RunCodeLines = async function(code)
{
  G.currentline = 0;
  G.codelines   = code.replace(/\r/g, "").split(/\n/);
  
  let currentline = '';
  
  try
  {
    for (let i = 0, l = G.codelines.length; i < l; i++)
    {
      currentline = G.codelines[i];
      
      P.HTML(
        ".CodeResults",
        '<div class="lgreenb Pad50 Margin25 Size150">' + eval(currentline) + '</div>'
      );
      await Sleep(1000);
    }
  } catch (error)
  {
    P.HTML(
      ".CodeResults",
      `<pre class="Pad50 Size150 lyellow">${currentline}</pre>
      <div class="redb Pad50 Margin25 Size150">${error}</div>
    `);
  }
}


// *****************************************************************((()))
window.shiba = {
  'Say': function(speech)
  {
    if (speech)
    {
      let url = '/learn/texttomp3/' + encodeURIComponent(speech);
      
      P.Play(
        url, 
        url,
        {
          autoplay: 1,
          format:   'mp3'
        }
      );
    }
  },
  'SitDown': function()
  {
    cursors.down.isDown = 1;
  },  
  'StandUp': function()
  {
    cursors.down.isDown = 0;
  },  
  'Jump': function()
  {
    cursors.up.isDown = 1;
    
    setTimeout(
      function()
      {
        cursors.up.isDown = 0;
      },
      1000
    )
  },
  'Turn': function(direction)
  {
    switch (direction)
    {
      case 'left':
        cursors.left.isDown = 1;
        
        setTimeout(
          function()
          {
            cursors.left.isDown = 0;
          },
          50
        );
        break;
      case 'right':
        cursors.right.isDown = 1;
        
        setTimeout(
          function()
          {
            cursors.right.isDown = 0;
          },
          50
        );        
        break;
      case 'around':
        if (shibadirection == 'right')
        {
          cursors.left.isDown   = 1;
          cursors.right.isDown  = 0;
        } else
        {
          cursors.left.isDown   = 0;
          cursors.right.isDown  = 1;
        }
        
        setTimeout(
          function()
          {
            cursors.left.isDown = 0;
            cursors.right.isDown = 0;
          },
          50
        );
        break;
      default:
        break;
    }
  },
  'Walk': function(direction)
  {
    switch (direction)
    {
      case 'forward':
        if (shibadirection == 'left')
        {
          cursors.left.isDown   = 1;
          cursors.right.isDown  = 0;
        } else
        {
          cursors.left.isDown   = 0;
          cursors.right.isDown  = 1;
        }
        
        setTimeout(
          function()
          {
            cursors.left.isDown = 0;
            cursors.right.isDown = 0;
          },
          250
        );        
        break;
      case 'backward':        
        if (shibadirection == 'right')
        {
          cursors.left.isDown   = 1;
          cursors.right.isDown  = 0;
        } else
        {
          cursors.left.isDown   = 0;
          cursors.right.isDown  = 1;
        }
        
        setTimeout(
          shiba.TurnAround,
          250
        ); 
        break;
    }
    
  },
  'Run': function()
  {
    if (shibadirection == 'left')
    {
      cursors.left.isDown   = 1;
      cursors.right.isDown  = 0;
    } else
    {
      cursors.left.isDown   = 0;
      cursors.right.isDown  = 1;
    }
  },
  'Stop': function()
  {
    cursors.left.isDown   = 0;
    cursors.right.isDown  = 0;
  },
  'Go': function(place)
  {
    place = place || '';
    
    switch (place.toLowerCase())
    {
      case 'shibaplatform':
        G.LoadPhaserScene('shibaplatform')
        break;
      case 'home':
        G.LoadPhaserScene('home')
        break;
      case 'to school':
        G.LoadPhaserScene('school')
        break;
      case 'to the store':
        G.LoadPhaserScene('store')
        break;
      case 'to the bathroom':
        G.LoadPhaserScene('bathroom')
        break;
      case "to tom's store":
        G.LoadPhaserScene('toms.store')
        break;
    };
  },
  'ILive': function(place)
  {
    place = place || '';
    
    switch (place.toLowerCase())
    {
      case 'beside the airport':
        G.LoadPhaserScene('airport')
        break;
      case 'beside the school':
        G.LoadPhaserScene('school')
        break;
      case 'beside the supermarket':
        G.LoadPhaserScene('supermarket')
        break;
      case 'beside the movie theater':
        G.LoadPhaserScene('movie.theater')
        break;
      case 'beside the hospital':
        G.LoadPhaserScene('hospital')
        break;
      case 'beside the bus station':
        G.LoadPhaserScene('bus.station')
        break;
      case 'beside the train station':
        G.LoadPhaserScene('train.station')
        break;
      case 'beside the swimming pool':
        G.LoadPhaserScene('swimming.pool')
        break;
      case 'beside the park':
        G.LoadPhaserScene('park')
        break;
      case 'beside the gym':
        G.LoadPhaserScene('gym')
        break;
      case 'beside the bank':
        G.LoadPhaserScene('bank')
        break;
      case 'beside the library':
        G.LoadPhaserScene('library')
        break;
      case 'beside the zoo':
        G.LoadPhaserScene('zoo')
        break;
    }
  }
}
