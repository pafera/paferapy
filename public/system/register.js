"use strict";

// ====================================================================
G.SubmitRegistration 	= function(el, fields)
{
  let missingfirstregistrationfields = 0;
  
  if (!G.registrationstatus)
  {
    if (
      !fields.phonenumber
      || !fields.place
      || !fields.password
      || !fields.password2
      || !fields.headshot
    )
    {
      missingfirstregistrationfields = 1;
    }
  }
  
  if (missingfirstregistrationfields || !fields.email || !fields.displayname || !fields.headshot)
  {
    P.ErrorPopup(T.missingfields);
    return;
  }
  
  if (!IsEmail(fields.email))
  {
    P.ErrorPopup(T.email + ' ' + T.invalid);
    return;
  }
  
  if (fields.password != fields.password2)
  {
    P.ErrorPopup(T.passwordmismatch);
    return;
  }
  
  fields.command  = 'register';
  
  P.ResizeImage(
    '.headshot', 
    512,
    function(dataurl)
    {
      fields.headshot = dataurl;
      
      P.LoadingAPI(
        '.RegistrationFormResults',
        '/system/userapi',
        fields,
        function(d, resultsdiv)
        {
          P.HTML('.RegistrationStatus', '');
          
          P.HTML(
            '.RegistrationForm',
            `<div class="Pad50">
              ${T.registrationsuccessful}
            </div>
            <div class="Size200 bold Center Pad50 green">
              ${d.registrationid}
            </div>`
          );
        }
      );
    }
  );
}

// ====================================================================
G.ShowRegistrationForm 	= function (role)
{
  if (G.registrationstatus == 'approved')
  {
    return;
  }
  
  let fields  = [];
  
  if (!G.registrationstatus)
  {
    fields.extend([
      ['phonenumber', 'text', '', T.phonenumber, '', 'required'],
      ['place', 'text', '', T.place, '', 'required'],
      ['password', 'password', '', T.password, '', 'required'],
      ['password2', 'password', '', T.passwordagain, '', 'required'],
    ]);
  }
  
  fields.extend([
    ['email', 'email', '', T.email, '', 'required'],
    ['displayname', 'text', '', T.displayname, '', 'required'],
    ['headshot', 'file', '', T.headshot, '', 'required'],
    ['invitecode', 'invitecode', '', T.invitecode],
    ['role', 'radioflags', '', '', 
      [
        [T.student,   1, 1, ''],
        [T.teacher,   2, 0, ''],
        [T.school,    3, 0, ''],
        [T.business,  4, 0, ''],
      ]
    ],
    ['comments', 'multitext', '', T.comments],
    ['captcha', 'captcha', '', T.confirmhuman]
  ]);
  
  P.EditPopup(
    fields,
    G.SubmitRegistration,
    {
      formdiv:					'.RegistrationForm',
      gobuttontext:			T.finished, 
      cancelbuttontext:	T.back,
      cancelfunc:				function()
        {
          window.history.back();
        }
    }
  );

  P.OnEnter(
    '.RegistrationForm input',
    function()
    {
      G.SubmitRegistration('.RegistrationForm', P.FormToArray('.RegistrationForm'));
    }
  );
}

// ====================================================================
G.OnPageLoad = function ()
{
  G.registrationstatus  = 0;
  
  P.DialogAPI(
    '/system/userapi',
    {
      command:  'checkapproval'
    },
    function(d, resultsdiv, popupclass)
    {
      P.ClosePopup(popupclass);
      
      let data  = d.data;
      
      if (!IsEmpty(data))
      {
        G.registrationstatus  = data.approved;
        
        // Pending approval
        switch (data.approved)
        {
          case 'waiting':
            P.HTML(
              '.RegistrationStatus',
              `<div class="white Pad50">
                <p>${T.alreadyregistered}</p>
                
                <p>${T.yourregistrationcodeis}</p>
                
                <p class="Size200 bold Center whiteb Pad50 green">${data.id}</p>
              </div>
              `
            );
            break;
          case 'denied':
            P.HTML(
              '.RegistrationStatus',
              `<div class="white Pad50">
                <div class="Error">
                  ${T.approvalrejected}
                
                  <div class="Pad50 dredb Bold">
                    ${data.reason}
                  </div>
                </div>
              </div>
              `
            );
            break;
          case 'approved':
            P.HTML(
              '.RegistrationStatus',
              `<div class="white Pad50">
                <p>${T.approvalaccepted}</p>
                
                <div class="ButtonBar">
                  <a class="Color3" href="/system/usersettings.html">${T.start}</a>
                </div>
              </div>
              `
            );
        }
      } 
      
      G.ShowRegistrationForm();
    }
  );
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
