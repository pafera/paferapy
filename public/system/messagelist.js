"use strict";

G.MESSAGE_SYSTEM    = 0x1;
G.MESSAGE_UNREAD    = 0x4;
G.MESSAGE_READ      = 0x8;
G.MESSAGE_REPLIED   = 0x20;
G.MESSAGE_DELETED   = 0x40;
G.MESSAGE_MAIL      = 0x80;
G.MESSAGE_FORUM     = 0x100;

G.MESSAGE_LINK_INBOX_UNREAD = 0;
G.MESSAGE_LINK_INBOX_READ   = 1;
G.MESSAGE_LINK_ARCHIVED     = 2;
G.MESSAGE_LINK_DRAFTS       = 3;
G.MESSAGE_LINK_SENT         = 4;
G.MESSAGE_LINK_DELETED      = 5;
G.MESSAGE_LINK_URGENT       = 6;
G.MESSAGE_LINK_FAVORITES    = 7;
G.MESSAGE_LINK_LIKE         = 8;
G.MESSAGE_LINK_DISLIKE      = 9;

G.MESSAGE_FOLDERS = {
  'inbox':      T.inbox,
  'archives':   T.archive,
  'drafts':     T.drafts,
  'sent':       T.sent,
  'deleted':    T.deleted
}

// ********************************************************************
class MessageList extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    config.apiurl = '/system/messageapi';
    
    config.displayfields  = [];
    
    // Disable default clicking behavior
    config.oncardclick  = function(cardelement, dbid)
    {
    }
    
    // Forum mode uses objtype, while mail mode doesn't
    if (config.objtype)
    {
      config.orderby  = config.orderby || {
        'title':      T.title,
        'modified':   T.time,
        'views':      T.views,
        'replies':    T.replies
      };
      
      config.extrabuttons = `
        ${config.showmembers
          ? `<a class="Color4" onclick="${config.reference}.ShowMembers()">Show Members</a>`
          : ''
        }
      `;
    } else
    {
      config.extrabuttons = `
        ${P.ObjectToSelect(G.MESSAGE_FOLDERS, 'Folders', 'Folders', 'inbox')}
      `;
      
      config.orderby  = config.orderby || {
        'title':     T.title,
        'modified':  T.time
      };
    }
    
    config.enablesearch = 1;
    
    config.liststyles   = 'Pad25';
    config.cardstyles   = 'Rounded Margin25 Pad25';
    
    super(config);
    
    this.folder       = config.folder       || 'inbox';
    this.objtype      = config.objtype      || '';
    this.objid        = config.objid        || '';
    this.threadid     = config.threadid     || '';
    
    this.extraparams  = {
      objtype:      this.objtype,
      objid:        this.objid,
      threadid:     this.threadid
    };
    
    this.extrasaveparams  = {
      objtype:      this.objtype,
      objid:        this.objid,
      threadid:     this.threadid
    }
    
    if (this.objtype)
    {
      this.extraactions = [];
      this.liststyles   = 'Width100P Pad25';
    }
    
    this.threaditems          = [];
    this.collapsedmessageids  = [];
  }
  
  // ------------------------------------------------------------------
  GetEditFields(obj = {})
  {
    let fields  = [];
    
    if (!obj.notitle)
    {
      fields.push(['title',     'translation',  obj.title ? obj.title : '', T.title, '', 'required']);
    }
    
    fields.push(['content',   'translation',  obj.content ? obj.content : '', T.message, '', 'required']);
    
    if (P.userflags & P.USER_CAN_UPLOAD)
    {
      fields.push(['filelist',  'filelist',   obj.filelist ? obj.filelist : [], T.files]);
    }
    
    return fields;
  }
  
  // ------------------------------------------------------------------
  CheckAndDisplay()
  {
    let self  = this;
    
    let parentdisplay = super.Display
    
    if (self.objtype)
    {
      let params  = Clone(self.extraparams)
      
      params.command  = 'checkpermissions';
      
      P.LoadingAPI(
        '.' + self.div,
        '/system/messageapi',
        params,
        function(d, resultsdiv)
        {
          let permissions = d.data;
          
          if (!permissions.view)
          {
            P.HTML(
              resultsdiv, 
              `<div class="Error">${T.cantview}</div>`
            );
            return;
          }
          
          self.enableadd    = permissions.post;
          self.enabledelete = permissions.admin;
                    
          self.Display();
          self.List();
        }
      );
      return;
    }
    
    self.Display();
    
    P.On(
      '.' + self.div + ' .Folders',
      'change',
      function(e)
      {
        switch (e.target.value)
        {
          case 'inbox':
            self.flags  = G.MESSAGE_UNREAD | G.MESSAGE_READ;
            break;
          case 'archive':
            self.flags  = G.MESSAGE_ARCHIVED;
            break;
          case 'drafts':
            self.flags  = G.MESSAGE_DRAFT;
            break;
          case 'sent':
            self.flags  = G.MESSAGE_SENT;
            break;
          case 'deleted':
            self.flags  = G.MESSAGE_DELETED;
            break;
        };
        
        self.List(0);
      }
    )
  }

  // ------------------------------------------------------------------
  Display()
  {
    let self  = this;
    
    super.Display();
    
    let folders = E('.Folders');
    
    if (folders)
    {
      P.On(
        folders,
        'change',
        function(e)
        {
          self.folder = folders.value;
          
          self.extraparams.folder = self.folder;
          
          self.List(0);
        }
      )
    }
  }
  
  // ------------------------------------------------------------------
  OnListParams(params)
  {
    let self  = this;
    
    params.flags  = self.flags;
  }

  // ------------------------------------------------------------------
  RenderItem(r, istoppost = 1)
  {
    let self  = this;
    
    let unread  = (r.flags & G.MESSAGE_MAIL
      ? r.linktype == G.MESSAGE_LINK_INBOX_UNREAD
      : r.flags & G.MESSAGE_READ
        ? 1
        : 0
    );
    
    P.AddDelayedEvent('rendermessagefiles', 300);
    
    return `
      <div class="Message ${unread ? 'lgrayb' : 'whiteb'} ${istoppost ? 'Rounded Raised TopPost MarginBottom' : ''} Pad25" 
          data-threadid="${r.threadid}"
          data-dbid="${r.idcode}"
        >
        <div class="MessageTitle HoverHighlight" 
            ${istoppost
              ? `onclick="${self.reference}.ExpandThread(this, '${r.idcode}', '${r.threadid}')" `
              : `onclick="${self.reference}.ToggleMessageBody(this, '${r.idcode}')"`
            }            
          >
          ${istoppost
            ? `<div class="TitleTextPad25 Rounded Bold ${unread ? 'blue' : 'blue'}">
                ${r.flags & G.MESSAGE_DELETED 
                  ? T.deleted
                  : P.BestTranslation(r.title)            
                }
              </div>`
            : ''
          }
          <div class="Grid GridCenter"
            style="grid-template-columns: 4em 1fr 12em;" 
            >
            <div>
              ${r.fromid 
                ? `<a class="Center Pad25 HoverHighlight" onclick="P.UserActionsPopup('${r.fromid}', this)">
                    ${P.HeadShotImg(r.fromid, 'Square300')}
                  </a>`
                : `<img src="/system/icons/email.webp" class="Square300">`
              }
            </div>
            <div>
              ${r.fromid 
                ? P.BestTranslation(r.username)
                : T.system
              }<br>
              ${UTCToLocal(new Date(r.created * 1000))}
            </div>
            <div class="Pad25 Right">
              ${istoppost 
                ? `${self.objtype ? `👁️ ${r.numviews}` : ''}`
                : ''
              }
              💬 ${r.numreplies}
              ${self.objtype
                ? `👍 <span class="NumLikes">${r.likes}</span> 👎 <span class="NumDislikes">${r.dislikes}</span>`
                : ''
              }
            </div>
          </div>
        </div>
        <div class="MessageContent ${istoppost || self.collapsedmessageids.indexOf(r.idcode) != -1 ? 'Hidden' : ''}">
          <div class="Pad50Vertical PreLine MessageText">
            ${r.flags & G.MESSAGE_DELETED 
              ? T.deleted
              : P.BestTranslation(r.content)              
            }
          </div>
          <div class="FlexGrid16 MessageFiles" data-fileids="${r.files.join(',')}" data-rendered=""></div>
          <div class="ButtonBar Pad25">
            ${self.objtype
              ? `<a class="Color3" onclick="${self.reference}.Like(this, '${r.idcode}')">👍</a>
                <a class="Color1" onclick="${self.reference}.Dislike(this, '${r.idcode}')">👎</a>`
              : ''
            }
            ${self.enableadd && r.fromid && !(r.flags & G.MESSAGE_DELETED)
              ? `<a class="Color4" onclick="${self.reference}.ReplyTo(this, '${r.idcode}')">${T.reply}</a>`
              : ''
            }
            ${r.fromid == P.userid
              ? `<a class="Color5" onclick="${self.reference}.EditMessage(this, '${r.idcode}')">${T.edit}</a>`
              : ``
            }
            ${self.enabledelete
              ? `<a class="Color1" onclick="${self.reference}.Delete(this, '${r.idcode}')">${T.delete}</a>`
              : ''
            }
            ${r.flags & G.MESSAGE_MAIL && E('.Folders').value == 'inbox'
              ? `<a class="Color2" onclick="${self.reference}.Archive(this, '${r.idcode}')">${T.archive}</a>`
              : ''
            }
            ${istoppost
              ? `<a class="Color6" 
                  onclick="${self.reference}.RenderThread(P.Parent(this, '.Message'))">
                  ${T.refresh}
                </a>`
              : ''
            }
          </div>
        </div>
        ${istoppost
          ? `<div class="MessageThread"></div>`
          : ''
        }
      </div>
    `;
  }  
  
  // ------------------------------------------------------------------
  FindMessageObj(dbid)
  {
    let self  = this;
    
    for (let r of self.items)
    {
      if (r.idcode == dbid)
      {
        return r;
      }
    }
    
    for (let r of self.threaditems)
    {
      if (r.idcode == dbid)
      {
        return r;
      }
    }
    
    P.ErrorPopup(T.nothingfound);
  }
  
  // ------------------------------------------------------------------
  EditMessage(button, dbid)
  {
    let self  = this;
    
    let obj = self.FindMessageObj(dbid);
    
    self.WriteMessage(
      obj,
      function(d, resultsdiv)
      {
        let message = E(`[data-dbid='${dbid}']`);
        
        message.outerHTML = self.RenderItem(d.data, d.data.replyid ? 0 : 1);
        
        P.CloseThisPopup(resultsdiv);
      }
    );
  }
  
  // ------------------------------------------------------------------
  WriteMessage(obj, onfinished)
  {
    let self  = this;
    
    P.EditPopup(
      self.GetEditFields(obj),
      function(formdiv, data, resultsdiv, e, saveandcontinue)
      {
        let params      = {
          command:  'save',
          data:     data
        };
        
        if (self.extraparams)
        {
          for (let k in self.extraparams)
          {
            params[k] = self.extraparams[k];
          }
        }          
        
        if (self.extrasaveparams)
        {
          for (let k in self.extrasaveparams)
          {
            params.data[k] = self.extrasaveparams[k];
          }
        }          
        
        P.LoadingAPI(
          resultsdiv,
          self.apiurl,
          params,
          onfinished
        );
      },
      {
        enabletranslation:  1,
        obj:                obj,
        title:              T.writemessage
      }
    );    
    
    setTimeout(
      function()
      {
        E('.titleTextArea').style.height = '1em';
      },
      300
    );
  }
  
  // ------------------------------------------------------------------
  Archive(button, dbid)
  {
    P.DialogAPI(
      '/system/messageapi',
      {
        command:    'archive',
        idcodes:    [dbid]
      },
      function(d, resultsdiv)
      {
        P.CloseThisPopup(resultsdiv);
        
        P.Parent(button, '.Message').remove();
      }
    );
  }
    
  // ------------------------------------------------------------------
  Delete(button, dbid)
  {
    let self  = this;
    
    let message = P.Parent(button, '.Message');
    
    let params  = {
      command:    'delete',
      idcodes:    [dbid]        
    };
    
    for (let k in self.extraparams)
    {
      params[k] = self.extraparams[k];
    }
    
    P.DialogAPI(
      '/system/messageapi',
      params,
      function(d, resultsdiv, popupclass)
      {
        if (d.reallydeleted || E('.Folders'))
        {
          message.remove();
        } else
        {
          message.querySelector('.MessageText').innerText = T.deleted;
        }
        
        P.CloseThisPopup(popupclass);
      }
    )
  }
  
  // ------------------------------------------------------------------
  ToggleMessageBody(messagetitle, dbid)
  {
    let self  = this;
    
    let messagecontent  = P.Parent(messagetitle, '.Message').querySelector('.MessageContent');
    
    if (P.IsVisible(messagecontent))
    {
      P.Hide(messagecontent);
      
      self.collapsedmessageids.push(dbid);
    } else
    {
      P.Show(messagecontent);
      
      self.collapsedmessageids.remove(dbid);
    } 
  }
  
  // ------------------------------------------------------------------
  ExpandThread(messagetitle, dbid, threadid)
  {
    let self  = this;
    
    let messagediv = P.Parent(messagetitle, '.Message');
    
    if (messagediv.expanded)
    {
      messagediv.classList.remove('whiteb');
      messagediv.classList.add('lgrayb');
      
      P.Hide(messagediv.querySelector('.MessageContent'));
      P.Hide(messagediv.querySelector('.MessageThread'));
      
      messagediv.expanded  = 0;
    } else
    {    
      messagediv.classList.remove('lgrayb');
      messagediv.classList.add('whiteb');
      
      P.Show(messagediv.querySelector('.MessageContent'));
      P.Show(messagediv.querySelector('.MessageThread'));
      
      messagediv.expanded  = 1;
      
      P.API(
        '/system/messageapi',
        {
          command:    'addview',
          idcode:     dbid
        }
      );
      
      if (threadid)
      {
        self.RenderThread(messagediv);
      }
    }
  }  
  
  // ------------------------------------------------------------------
  RenderPost(ls, postobj, level)
  {
    let self    = this;    
    let indent  = level;
    
    if (indent > 8)
    {
      indent  = 8;
    }
    
    ls.push(`
      <div style="margin-left: ${indent}em;">
        ${self.RenderItem(postobj, 0)}
      </div>
    `)
    
    if (postobj.replies)
    {
      for (let post of postobj.replies)
      {
        self.RenderPost(ls, post, level + 1);
      }
    }
  }
  
  // ------------------------------------------------------------------
  RenderThread(messagediv)
  {
    let self  = this;
    
    let toppostid = messagediv.dataset.dbid;
    let threadid  = messagediv.dataset.threadid;
    let threaddiv = messagediv.querySelector('.MessageThread');
    
    let searchparams  = Clone(self.extraparams);    
    
    searchparams.command  = 'search';
    searchparams.threadid = threadid;
    searchparams.limit    = 9999;
    
    P.LoadingAPI(
      threaddiv,
      '/system/messageapi',
      searchparams,
      function(d, resultsdiv)
      {
        self.threaditems  = d.data;
        
        if (!self.threaditems.length )
        {
          P.HTML(resultsdiv, '');
        } else
        {
          // Create a tree structure using replyid          
          let toppost      = self.threaditems[0];
          
          for (let i = 0, l = self.threaditems.length; i < l; i++)
          {
            let currentpost = self.threaditems[i];
            
            for (let j = 0, m = l; j < m; j++)
            {
              if (i == j)
              {
                continue;
              }
              
              let chosenpost  = self.threaditems[j]
              
              if (chosenpost.replyid == currentpost.idcode)
              {
                if (!currentpost.replies)
                {
                  currentpost.replies = [];
                }
                
                currentpost.replies.push(chosenpost);
              }
            }
          }
          
          let ls  = [];
          
          // Don't need to render the top post again if we're starting
          // from the top
          if (toppost.idcode == toppostid)
          {
            if (IsArray(toppost.replies))
            {
              for (let r of toppost.replies)
              {
                self.RenderPost(ls, r, 1);
              }
            }
          } else
          {
            // Render the whole thread below the specified message so that 
            // the user gets context for what they found
            self.RenderPost(ls, toppost, 1);
          }
          
          P.HTML(resultsdiv, ls);
        }
      }
    );
  }
    
  // ------------------------------------------------------------------
  ReplyTo(replybutton, dbid)
  {
    let self  = this;
    
    let repliedmessageobj = self.FindMessageObj(dbid);
    let title             = Clone(repliedmessageobj.title);
    
    for (let k in title)
    {
      let titletext = title[k];
      
      if (!titletext.startsWith('RE: '))
      {
        title[k]  = 'RE: ' + titletext;
      }
    }
    
    let editform  = P.EditPopup(
      self.GetEditFields({
        notitle:      1
      }),
      function(formdiv, data, resultsdiv, e, saveandcontinue)
      {
        for (let k in self.extrasaveparams)
        {
          data[k] = self.extrasaveparams[k];
        }
        
        data.title      = title;
        data.replyid    = dbid;
        
        P.LoadingAPI(
          resultsdiv,
          '/system/messageapi',
          {
            command:    'save',
            data:       data
          },
          function(d, resultsdiv)
          {
            P.CloseThisPopup(resultsdiv);
            
            self.RenderThread(
              P.Parent(replybutton, '.TopPost')
            );
          }
        );
      },
      {        
        title:              `${T.reply}: ${P.BestTranslation(repliedmessageobj.title)}`,
        enabletranslation:  1
      }
    );

    setTimeout(
      function()
      {
        E('.titleTextArea').style.height = '1em';
      },
      300
    );
  }
  
  // ------------------------------------------------------------------
  Like(button, dbid)
  {
    let self  = this;
    
    self.SendLike(button, dbid, 1);
  }
  
  // ------------------------------------------------------------------
  Dislike(button, dbid)
  {
    let self  = this;
    
    self.SendLike(button, dbid, 0);
  }
  
  // ------------------------------------------------------------------
  SendLike(button, dbid, like)
  {
    let self  = this;
    
    P.API(
      '/system/messageapi',
      {
        command:    'addlike',
        idcode:     dbid,
        like:       like
      },
      function(d)
      {
        if (d.error)
        {
          P.ErrorPopup(d.error);
        } else
        {
          let el  = 0;
          
          if (like)
          {
            el  = P.Parent(button, '.Message').querySelector('.NumLikes');
          } else
          {
            el  = P.Parent(button, '.Message').querySelector('.NumDislikes');
          }
          
          el.innerText  = parseInt(el.innerText) + 1
        }
      }
    );
  }
  
  // ------------------------------------------------------------------
  ShowMembers()
  {
    let self  = this;
    
    P.AddFullScreen(
      'dblueg',
      '',
      `<h2 class="Center">Forum Members</h2>
      <div class="FlexGrid12 MemberList"></div>
      <div class="ButtonBar Pad50">
        <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
      </div>
      `
    );
    
    let searchparams  = Clone(self.extraparams);    
    
    searchparams.command  = 'getmembers';
    
    P.LoadingAPI(
      '.MemberList',
      '/system/messageapi',
      searchparams,
      function(d, resultsdiv)
      {
        let ls  = [];
        
        if (IsEmpty(d.data))
        {
          ls.push(`<div class="Error">${T.nothingfound}</div>`);
        } else
        {
          // Sort names in alphabetical order
          let names = [];
          
          for (let k in d.data)
          {
            names.push([k, d.data[k]]);
          }
          
          names.sort((a, b) => StrCmp(a[1], b[1], 1));
          
          G.forummembers  = names;
          
          for (let name of names)
          {
            ls.push(`
              <div class="Pad25 Flex FlexCenter HoverHighlight" onclick="P.UserActionsPopup('${name[0]}', this)">
                ${P.HeadShotImg(name[0], 'Square400')}
                <div class="Pad25">${name[1]}</div>
              </div>
            `)
          }
        }
        
        P.HTML(resultsdiv, ls);
      }
    )
  }
}


// ********************************************************************
// Handler for rendering files attached to messages via P.AddDelayedEvent()
P.AddHandler(
  'pageload', 
  function()
  {
    P.AddHandler(
      'rendermessagefiles', 
      function(e)
      {
        let fileelements  = Q('.MessageFiles');
        let fileids       = [];
        
        for (let el of fileelements)
        {
          if (!el.dataset.rendered && el.dataset.fileids)
          {
            fileids.extend(el.dataset.fileids.split(','));
          }
        }
        
        if (fileids.length)
        {
          P.DialogAPI(
            '/system/fileapi',
            {
              command:        'search',
              fileids:        fileids,
              showallfiles:   1
            },
            function(d, resultsdiv, popupclass)
            {
              for (let el of fileelements)
              {
                if (!el.dataset.rendered && el.dataset.fileids)
                {
                  let thesefileids  = el.dataset.fileids.split(',');
                  let thesefileobjs = [];
                  
                  for (let fileobj of d.data)
                  {
                    if (thesefileids.indexOf(fileobj.idcode) != -1)
                    {
                      thesefileobjs.push(fileobj);
                    }
                  }
                  
                  if (thesefileobjs.length)
                  {
                    P.RenderFileObjects(el, thesefileobjs);
                  }
                }
              }
              
              P.ClosePopup(popupclass);
            }
          );
        }
      }
    );
  }
);

