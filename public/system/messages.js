"use strict";

// ====================================================================
G.OnPageLoad = function()
{  
  P.HTML(
    '.PageLoadContent', 
    `<h1>${T.messages}</h1>
    <div class="Messages"></div>
    `
  );
  
  G.messages = new MessageList({
    div:          '.Messages',
    reference:    'G.messages',
    enableadd:    1,
    enabledelete: 1,
    enablesearch: 1,
    enableedit:   1
  });
  
  G.messages.Display();
  G.messages.List();
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
