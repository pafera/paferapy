/* An asynchronous dynamic JavaScript and CSS loader which will load
 * scripts in order or in parallel and alert you when everything is 
 * loaded. 
 * 
 * This is essentially a simple version of the AMD/requirejs
 * loaders with manual loading order.
 *
 * Creates the global _loader object.
 *
 * Examples:
 *
 * * Load files sequentially
 *
 *		_loader.Load(['a.js', 'b.js', 'c.js']);
 *
 * * Load files in parallel:
 * 
 *		_loader.Load([['a.js', 'b.js', 'c.js']]);
 *
 * * Load some files in parallel but others sequentially:
 * 
 *		_loader.Load([['a.js', 'b.js'], 'c.js']);
 */
 
// ====================================================================
function IsArray(v)
{
	return (v instanceof Array)
		|| (Object.prototype.toString.apply(v) === '[object Array]');
};

// ====================================================================
String.prototype.endswith = function(suffix)
{
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

// ====================================================================
Array.prototype.contains = function(o)
{
	for (var i = 0, l = this.length; i < l; i++)
	{
		if (this[i] == o)
			return true;
	}

	return false;
};

// ====================================================================
// Thanks to http://stackoverflow.com/questions/3954438/remove-item-from-array-by-value
Array.prototype.remove = function()
{
	var what, a = arguments, L = a.length, ax;
	while (L && this.length)
	{
		what = a[--L];
		while ((ax = this.indexOf(what)) !== -1)
			this.splice(ax, 1);
	}
	return this;
};

// ********************************************************************
_loader	=	{

	// ------------------------------------------------------------------
	Clear: function()
	{
		this.filesloaded			=	[];
		this.filesprocessing	=	[];
		this.filestoload			=	[];
		this.onfinished				=	[];
		this.numadded					= 0;
		this.numloaded				= 0;
		this.debug						= 0;
	},

	// ------------------------------------------------------------------
	OnFinished: function(func, priority, timeout)
	{
		if (typeof func != 'function')
		{
			if (_loader.debug)
				console.log('OnFinished: Not function');
			return;
		}
	
		priority	=	priority || 0;
		timeout		=	timeout || 0;
	
		if (_loader.filesprocessing.length || _loader.filestoload.length)
		{
			_loader.onfinished.push([func, priority, timeout]);

			if (_loader.debug)
			{
				console.log('OnFinished: Deferring: ', func, _loader.onfinished);
			}

		} else
		{
			if (_loader.debug)
			{
				console.log('OnFinished: Executing: ', func);
			}

			func();
		}
	
		return _loader;
	},
	
	// ------------------------------------------------------------------
	InQueue: function(url)
	{
		for (var i = 0, l = _loader.filestoload.length; i < l; i++)
		{
			if (_loader.filestoload[i][0] == url)
			{
				return true;
			}
		}

		return (_loader.filesloaded.indexOf(url) !== -1
			|| _loader.filesprocessing.indexOf(url) !== -1
		)
	},

	// ------------------------------------------------------------------
	Load: function(url)
	{
		if (!url)
		{
			return _loader;
		}

		if (IsArray(url))
		{
			_loader.filestoload	=	_loader.filestoload.concat(url);

			if (!_loader.filesprocessing.length)
			{
				_loader.LoadNext();
			}
			
			return _loader;
		}

		if (_loader.InQueue(url))
		{
			return _loader;
		}

		if (_loader.filesprocessing.length)
		{
			_loader.filestoload.push(url);
			return _loader;
		}

		_loader.ReallyLoad(url);
	
		return _loader;
	},

	// ------------------------------------------------------------------
	LoadNext: function()
	{
		if (_loader.filestoload.length)
		{
			var url	=	_loader.filestoload.shift();

			if (IsArray(url))
			{
				for (var i = 0, l = url.length; i < l; i++)
				{
					_loader.ReallyLoad(url[i])
				}
			} else
			{
				_loader.ReallyLoad(url);
			}
			return _loader;
		}

		if (!_loader.filesprocessing.length)
		{
			// Use a short timeout to allow other scripts to add files to load
			// before we actually run the onfinished functions
			setTimeout(_loader.RunOnFinished, 200);
		}

		return _loader;
	},

	// ------------------------------------------------------------------
	RunOnFinished: function()
	{
		// Cancel if there are still scripts waiting to be loaded
		if (_loader.filesprocessing.length || _loader.filestoload.length)
		{
			return;
		}
	
		// Sort by priority
		_loader.onfinished.sort(
			function (a, b)
			{
				return a[1] - b[1];
			}
		);

		for (var i = 0, l = _loader.onfinished.length; i < l; i++)
		{
			var item	=	_loader.onfinished[i];
		
			if (item[2])
			{
				if (_loader.debug)
					console.log('_loader.RunOnFinished: setTimeout: ', item[0]);

				setTimeout(
					item[0],
					item[2]
				);
			} else
			{
				if (_loader.debug)
					console.log('_loader.RunOnFinished: Running: ', item[0]);

				item[0]();
			}
		}

		_loader.onfinished	=	[];
	},

	// ------------------------------------------------------------------
	ReallyLoad: function(url)
	{
		if (!_loader.numadded)
		{
			setTimeout(
				_loader.CheckLoaded,
				20000
			);
		}
	
		_loader.numadded++;

		if (_loader.debug)
		{
			console.log('_loader.ReallyLoad: Loading ', url);
		}

		if (url.endswith('.css'))
		{
			var link  	= document.createElement('link');
			link.rel  	= 'stylesheet';
			link.type 	= 'text/css';
			link.href 	= url;
		} else
		{
			var link  	= document.createElement('script');
		}
	
		link.src 		= url;
	
		// For most browsers
		link.addEventListener(
			'load',	
			function()
			{
				_loader.OnLoaded(url);
			},
			false
		);
	
		// For IE
		link.addEventListener(
			'readystatechange',
			function()
			{
				if (this.readyState == 'loaded'
					|| this.readyState == 'complete'
				)
					_loader.OnLoaded(url);
			}
		);
		
		_loader.filesprocessing.push(url);
		document.head.appendChild(link);

		return _loader;
	},

	// ------------------------------------------------------------------
	OnLoaded: function(url)
	{
		if (_loader.debug)
		{
			console.log('_loader.OnLoaded: ', url);
		}

		_loader.numloaded++;
		_loader.filesprocessing.remove(url);
		_loader.LoadNext();
	
		return _loader;
	},

	// ------------------------------------------------------------------
	// Certain older mobile browsers don't support the onload event.
	// This check will sniff them out and instruct them to download
	// Chrome or Firefox.
	CheckLoaded: function()
	{
		if (_loader.numadded && !_loader.numloaded)
		{
			document.body.innerHTML = [
				'<p style="word-wrap: break-word;">',
					'Could not load ',
					json.stringify(_loader.filesprocessing),
				'</p>'
				/*'<p>Please update your browser to view this site.</p>',
				'<p><a href="https://www.mozilla.org/" style="color: white; background-color: #E66000; border: 0.5em solid #E66000; border-radius: 0.5em; display: inline-block;">Download Firefox</a></p>',
				'<p><a href="www.google.com/chrome/browser/index.html" style="color: white; background-color: #009; border: 0.5em solid #009; border-radius: 0.5em; display: inline-block;">Download Chrome</a></p>',*/
			].join('\n');
		}
	
		return _loader;
	}
}

_loader.Clear();

