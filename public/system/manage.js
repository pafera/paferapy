// ********************************************************************
G.OnPageLoad 	= function ()
{
	let ls 	= ['<div class="Flex FlexWrap">'];

	for (let i = 0, l = P.languages.length; i < l; i++)
	{
		let item 	= P.languages[i];

		ls.push(
			`<a class="MinWidth800 Pad50 Rounded Outset Margin25 Center Color${i % 6 + 1}" onclick="P.SetLanguage('${Last(item[2])}')">
				${item[0]}
			</av>`
		);
	}

	ls.push('</div>');

	P.HTML('.SetLanguage', ls);

	P.LoadingAPI(
		'.PersonalInfo',
		'system/users$view',
		{
		},
		function(d, el)
		{
			let item 	= d.item;

			P.EditPopup(
				[
          ['icon', 'custom', item.icon, SYSTEM_SYSTEM[58], 'UserIcon'],
					['phonenumber', 'text', item.phonenumber, SYSTEM_SYSTEM[42]],
					['place', 'text', item.place, SYSTEM_SYSTEM[43]],
					['password', 'password', '', SYSTEM_SYSTEM[17]],
					['email', 'email', item.email, SYSTEM_SYSTEM[44]],
					['englishname', 'text', P.BestTranslation(item.usernames, 'en'), G.SYSTEM_ADMIN[10]],
					['chinesename', 'text', P.BestTranslation(item.usernames, 'zh'), G.SYSTEM_ADMIN[10]]
				],
				function(el, values)
				{
          P.StoreInArray(values, 'usernames.en', values.englishname);
          P.StoreInArray(values, 'usernames.zh', values.chinesename);
          
          delete values.englishname;
          delete values.chinesename;
          
          values.id   = P.usercode;
          
					P.LoadingAPI(
						'.PersonalInfoResults',
						'system/users$save',
						values,
						function(d, el)
						{
							P.HTML(el, `<div class="greenb Pad50">${SYSTEM_SYSTEM[41]}</div>`);
						}
					);
				},
				{
					formdiv:					'.PersonalInfo',
					cancelfunc:				function()
						{
							window.history.back();
						},
					extrabuttons:  		`<a class=Color4 href="/logout">${SYSTEM_SYSTEM[27]}</a>`
				}
			);
      
      P.HTML(
        '.UserIcon',
        `<div class="Flex FlexCenter">
          <a class="Center Block Raised Rounded Color2 Pad50 ChooseHeadshot" style="color: black !important;">
            <img class="Square800 HeadShotIcon"
              src="${item.icon 
                ? P.baseurl + 'a/system/getfilethumb$' + item.icon
                : P.baseurl + 'f/system/icons/user.svg'}"
            ><br>
            ${SYSTEM_SYSTEM[109]}
          </a>
        </div>`
      );
      
      P.On(
        '.ChooseHeadshot',
        'click',
        function()
        {
          G.ChooseFile(
            function(id)
            {
              E('.HeadShotIcon').setAttribute('src', P.baseurl + 'a/system/getfilethumb$' + id);
              E('.icon').value  = id;
            }
          );
        }
      );
		}
	);

	ls 	= [];

	if (IsEmpty(G.wallpapers))
	{
		ls.push('<div class=Error>' + SYSTEM_SYSTEM[49] + '</div>');
	} else
	{
		G.wallpapers.sort();

		for (let i = 0, l = G.wallpapers.length; i < l; i++)
		{
			let item 	= G.wallpapers[i];

			ls.push(
				`<a class="FlexItem MinWidth800 MinHeight600 Outset Margin25" onclick="P.SetWallpaper('${item}')">
					<img src="${P.baseurl}f/system/wallpapers/${item}" class="CoverFit HoverHighlight Border20">
				</a>`
			);
		}
	}

	P.HTML('.Wallpapers', ls);

	ls 			= [];

	if (IsEmpty(G.adminpages))
	{
		ls.push('<div class=Error>' + SYSTEM_SYSTEM[49] + '</div>');
	} else
	{
		let color	= 1;

		G.adminpages.sort(
			function(a, b)
			{
				return StrCmp(a.title, b.title, 1);
			}
		);

		for (let i = 0, l = G.adminpages.length; i < l; i++)
		{
			let item 	= G.adminpages[i];

			ls.push(
				`<a class="Flex FlexCenter FlexWrap FlexItem MinWidth1200 Pad50 Rounded Outset Margin50 Color${color}" href="${P.baseurl}${item.path.substr(1)}">
					<div class="FlexItem">
						${item.title 
							? EncodeEntities(item.title)
							: ''}
					</div>
					<div class="Size80 FlexItem BreakAll">${P.baseurl}${item.path.substr(1)}</div>
				</a>`
			);

			color++;

			if (color > 6)
				color 	= 1;
		}
	}

	P.HTML('.ManageLinks', ls);
}

P.AddHandler('pageload', G.OnPageLoad);
