"use strict";

// ====================================================================
G.OnPageLoad 	= function ()
{
  P.EditPopup(
    [
      ['headshot', 'file', '', SYSTEM_SYSTEM[58], '', 'required accept="image/*" capture="camera"'],
      ['englishname', 'text', '', G.SYSTEM_ADMIN[9], '', 'required'],
      ['chinesename', 'text', '', G.SYSTEM_ADMIN[10], '', 'required'],
    ],
    function(el, values)
    {
      if (!values.headshot
        || !values.englishname
        || !values.chinesename
      )
      {
        P.ErrorPopup(SYSTEM_SYSTEM[48]);
        return;
      }
      
      P.ResizeImage(
        'input[name=headshot]', 
        640, 
        function(durl)
        {
          values.headshot = durl;
          
          P.LoadingAPI(
            '.PersonalInfoResults',
            'system/requiredinfo$save',
            values,
            function(d, el)
            {
              P.HTML(el, `<div class="greenb Pad50">${G.SYSTEM_ADMIN[11]}</div>`);
            }
          );
        }
      );
    },
    {
      formdiv: '.PersonalInfo',
      cancelfunc: function() { P.LoadURL(P.baseurl + '/system/logout'); },
      headertext: '<p>' + G.SYSTEM_ADMIN[12] + '</p>'
    }
  );      
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
