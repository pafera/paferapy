"use strict";

G.USER_CAN_MANAGE_SELF       = 0x10;

// ********************************************************************
class PasskeysList extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    config.displayfields  = [
      ['idcode',      'text',       T.name],
      ['modified',    'timestamp',  T.modified],
      ['ip',          'text',       'IP'],
    ];
    
    config.showidcode   = 1;
    config.enablesearch = 1;
    config.enabledelete = 1;
    config.enableadd    = 1;
    config.enableselect = 1;
    config.limit        = 20;
        
    super(config);    
  }
  
  // ------------------------------------------------------------------
  OnList(resultsdiv, start, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/system/userapi',
      {
        command:    'searchpasskeys',
        start:      start,
        keyword:    searchterm
      },
      function(d, resultsdiv)
      {
        self.items  = d.data;
        self.count  = d.count;
        
        self.DisplayItems();
      }
    );
  }
  
  // ------------------------------------------------------------------
  Add()
  {
    let self  = this;
    
    P.InputPopup(
      'Please type a name for your passkey',
      function(value)
      {
        G.AddPasskey(value);
      },
      '',
      {
        parent: window.event.target
      }
    );
  }
  
  // ------------------------------------------------------------------
  OnDelete(dbids, resultsdiv)
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/system/userapi',
      {
        command:    'deletepasskeys',
        ids:        dbids
      },
      function(d, resultsdiv)
      {
        P.Toast(
          `<div class="Pad50 greenb">${T.allgood}</div>`,
          resultsdiv,
          2000,
          {
            onfinished: function()
              {
                self.List();
              }
          }
        )
      }
    );
  }  
}

// ********************************************************************
class FileList extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    config.apiurl = '/system/fileapi';
    
    config.displayfields  = [
      ['thumbnail', 'custom', ''],
      ['filename', 'text', ''],
      ['description', 'text', T.description],
      ['modified', 'text', T.modified],
      ['size', 'int', T.size]
    ];
    
    config.extrabuttons   = `
      <a class="Color3" onclick="${config.reference}.UploadFile()">${T.add}</a>
    `;
    
    config.showidcode   = 1;
    config.enablesearch = 1;
    config.enabledelete = 1;
    config.enableedit   = 1;
    config.limit        = 20;
        
    super(config);    
    
    this.PUBLIC       = 0x01;
    this.PROTECTED    = 0x02;
    this.PRIVATE      = 0x04;
    
    this.extraactions.push(
      [T.download, 'Download', 4],
      [T.copylink, 'CopyLink', 5]
    );
  }
  
  // ------------------------------------------------------------------
  /** Returns the edit fields for use in P.EditPopup(). If you want to 
   * have different fields for different objects, then override this 
   * function.
   * 
   * @param {object} obj - The original object to edit
   */
  GetEditFields(obj)
  {
    let self  = this;
    
    return [
      ['filename', 'text', 0, T.filename],
      ['description', 'text', 0, T.description],
      ['flags', 'hidden', 0, ''],
      ['security', 'radioflags', 0, T.security, 
        [
          [T.public, self.PUBLIC, obj.flags & self.PUBLIC, ''],
          [T.protected, self.PROTECTED, obj.flags & self.PROTECTED, ''],
          [T.private, self.PRIVATE, obj.flags & self.PRIVATE, '']
        ]
      ]
    ];
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    return `
      <table>
        <tr>
          <td>
            <img src="${r.thumbnail}" class="Square400">
          </td>
          <td>
            <div class="Title">${r.filename}</div>
            <div class="Description">${r.description}</div>
            <div class="Modified">${UTCToLocal(new Date(r.modified * 1000))}</div>
            <div class="Size">${(r.size / 1048576).toFixed(2)} MiB</div>
          </td>
        </tr>
      </table>
    `;
  }
  
  // ------------------------------------------------------------------
  GetItemDisplayName(obj)
  {
    return obj.filename;
  }
  
  // ------------------------------------------------------------------
  UploadFile()
  {
    let self  = this;
    
    P.UploadFilePopup(
      '/system/upload',
      {
        onfinished:       function(e, xhr, filename)
        {
          self.List();
        }
      }
    );
  }
  
  // ------------------------------------------------------------------
  OnSaveData(obj)
  {
    let self  = this;
    
    // Clear previous security flags and use the new security value
    obj.flags = (
      obj.flags & (~(self.PUBLIC | self.PROTECTED | self.PRIVATE))
    ) | obj.security;
  }
  
  // ------------------------------------------------------------------
  Download(card, dbid)
  {
    let self  = this;
    
    window.open('/system/downloader/' + dbid, '_blank');
  }
  
  // ------------------------------------------------------------------
  CopyLink(card, dbid)
  {
    let self  = this;
    
    let obj = self.GetItemByID(dbid);
    
    if (obj)
    {
      navigator.clipboard.writeText(obj.url);
    }
  }
}

// ********************************************************************
class FriendsList extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    config.displayfields  = [
      ['displayname', 'text', ''],
      ['place',       'text', T.place]
    ];    
    
    config.showidcode   = 1;
    config.enablesearch = 1;
    config.enableselect = 1;
    config.enabledelete = 1;
    config.limit        = 20;
    
    config.extrabuttons = `
      ${P.ObjectToSelect(
        {
          'acquaintances':  T.acquaintances,
          'friends':        T.friends,
          'favorites':      T.favorites,
          'blacklist':      T.blacklist
        },
        'GroupType',
        'GroupType'        
      )}
    `;
        
    super(config);    
  }
  
  // ------------------------------------------------------------------
  Display()
  {
    let self  = this;
    
    super.Display();
    
    P.On(
      '.GroupType',
      'change',
      function(e)
      {
        self.List(0);
      }
    );
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    return `
      <div class="Grid GridCenter Pad25" 
        style="grid-template-columns: 4em 1fr;" 
        onclick="P.UserActionsPopup('${r.idcode}', this)"
        >
        <div>${P.HeadShotImg(r.idcode, 'Width400 Pad25')}</div>
        <div>${r.displayname}</div>
      </div>
    `;
  }
  
  // ------------------------------------------------------------------
  GetItemDisplayName(obj)
  {
    return obj.displayname;
  }
  
  // ------------------------------------------------------------------
  OnList(resultsdiv, start, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/system/userapi',
      {
        command:    'searchusergroups',
        grouptype:  E('.GroupType').value,
        start:      start,
        keyword:    searchterm
      },
      function(d, resultsdiv)
      {
        self.items      = d.data;
        self.itemcount  = d.count;
        
        self.DisplayItems();
      }
    );
  }
  
  // ------------------------------------------------------------------
  OnDelete(dbids, resultsdiv)
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/system/userapi',
      {
        command:    'changeusergroups',
        group:      E('.GroupType').value,
        action:     'delete',
        userids:    dbids
      },
      function(d, resultsdiv)
      {
        self.List(0)
      }
    );
  }
}


// ------------------------------------------------------------------
G.UploadHeadshot = function(userid)
{
  P.UploadFilePopup(
    '.UploadFile',
    {
      uploadurl:        '/system/upload/headshot/' + userid,
      resizeimage:      [640, 640],
      disabletitle:     1,
      disablesecurity:  1,
      onfinished:       function(e, xhr, filename)
      {
        E('.HeadShotImg').setAttribute('src', '/system/headshots/' + P.CodeDir(P.userid) + '.webp?t=' + Date.now());
        
        P.CloseThisPopup('.UploadFile');
      }
    }
  );
}

// ====================================================================
G.DisplayUserStatus = function()
{
  P.LoadingAPI(
    '.AccountTab',
    '/system/userapi',
    {
      command:  'load'
    },
    function(d, resultsdiv)
    {
      let info  = d.data;
      let ls    = [];
      
      let canmanageself  = (
        P.IsAdmin()
        || d.flags & G.USER_CAN_MANAGE_SELF
      );
      
      if (!canmanageself)
      {
        ls.push(`
          <div class="Error">
            ${T.cantchangesettings}
          </div>
          <div class="Center whiteb Margin25 Pad50 Rounded Raised">
            ${P.HeadShotImg(P.userid, 'Square1200 HeadShotImg')}<br>
            ${info.phonenumber}<br>
            ${info.place}<br>
            ${P.BestTranslation(info.displayname)}
          </div>
        `);
      }
      
      ls.push(`
        <div class="EditUserForm whiteb Margin25 Pad50 Rounded Raised"></div>
      `);
      
      P.HTML(resultsdiv, ls);
      
      let fields  = [];
      
      if (canmanageself)
      {
        fields  = [
          ['headshot', 'custom', 0, T.headshot],
          ['phonenumber', 'text', 0, T.phonenumber],
          ['place', 'text', 0, T.place],
          ['displayname', 'translation', 0, T.username],
          ['password', 'password', 0, T.password],
          ['password2', 'password', 0, T.passwordagain]
        ];
      } else
      {
        fields  = [
          ['password', 'password', 0, T.password],
          ['password2', 'password', 0, T.passwordagain]
        ];
      }
      
      P.EditPopup(
        fields,
        function(formdiv, data, resultsdiv, e)
        {
          if (data.password && data.password != data.password2)
          {
            P.ErrorPopup(T.passwordmismatch);
            return;
          }
          
          P.LoadingAPI(
            resultsdiv,
            '/system/userapi',
            {
              command:  'save',
              data:     data
            },
            function(d, resultsdiv)
            {
              P.HTML(resultsdiv, `<div class="greeng Pad50">${T.successfullysaved}</div>`);
              
              setTimeout(
                function()
                {
                  P.HTML(resultsdiv, '');
                },
                2000
              );
            }
          );
        },
        {
          formdiv:            '.EditUserForm',
          enabletranslation:  canmanageself,
          obj:                info,
          headertext:         `
            <h2 class="dgreen Center">
              ${T.updateaccountinformation}<br>
              ${T.userid}: ${P.userid}
            </h2>`
        }
      );
      
      if (canmanageself)
      {
        P.HTML(
          '.headshotDiv',
          `<div class="Center">
            ${P.HeadShotImg(P.userid, 'Square1200 HeadShotImg')}
          </div>
          <div class="ButtonBar">
            <a class="Color4" onclick="G.UploadHeadshot('${P.userid}')">
              ${T.upload}
            </a>        
            </div>
          `
        );        
      }
    }
  );
}

// ====================================================================
G.AddPasskey = function(keyname)
{
  if (!SimpleWebAuthnBrowser.browserSupportsWebAuthn())
  {
    P.ErrorPopup(`Your browser doesn't support passkeys.`);
    return;
  }
  
  P.DialogAPI(
    '/system/userapi',
    {
      command:  'registerpasskey'
    },
    function(d, resultsdiv)
    {
      try 
      {
        let registrationdata  = JSON.parse(d.data);
        
        SimpleWebAuthnBrowser.startRegistration(registrationdata)
          .then(
            function(authinfo)
            {
              P.LoadingAPI(
                resultsdiv,
                '/system/userapi',
                {
                  command:  'verifypasskeyregistration',
                  data:     authinfo,
                  keyname:  keyname
                },
                function(d, resultsdiv)
                {
                  P.Toast(
                    `<div class="Pad50 greenb">${T.allgood}</div>`,
                    resultsdiv,
                    2000,
                    {
                      onfinished:   function()
                      {
                        P.CloseThisPopup(resultsdiv);
                        
                        G.passkeyslist.List();
                      }
                    }
                  );
                }
              );
            }
          ).catch(
            function(err)
            {
              P.Toast(
                `<div class="Error">${err}</div>`,
                resultsdiv,
                2000,
                {
                  onfinished: function()
                    {
                      P.CloseThisPopup(resultsdiv);
                    }
                }
              );
            }
          );        
      } catch (err) 
      {
        P.Toast(
          `<div class="Error">${err}</div>`,
          resultsdiv,
          2000,
          {
            onfinished: function()
              {
                P.CloseThisPopup(resultsdiv);
              }
          }
        );
      }
    }
  );
}

// ====================================================================
G.OnPageLoad = function()
{
  let ls  = [
    `<div class="UserStatus"></div>
    <div class="UserTabBar"></div>
    <div>
      <div class="AccountTab"></div>
      <div class="FilesTab Hidden"></div>
      <div class="FriendsTab Hidden"></div>
      <div class="PasskeysTab Hidden"></div>
    </div>
    <h2>${T.tools}</h2>
    <div class="FlexGrid16">
      <a class="Color3 Bold Pad50 Margin25 Rounded Raised" href="/system/needapproval.html">${T.needapproval}</a>
    </div>
    `
  ];
  
  if (P.IsAdmin())
  {
    ls.push(`
      <h2>${T.administratortools}</h2>
      <div class="FlexGrid16">
        <a class="Color3 Bold Pad50 Margin25 Rounded Raised" href="/system/controlpanel.html">${T.controlpanel}</a>
        <a class="Color4 Bold Pad50 Margin25 Rounded Raised" href="/system/db.html">${T.databasemanagement}</a>
        <a class="Color5 Bold Pad50 Margin25 Rounded Raised" href="/system/manageusers.html">${T.usermanagement}</a>
        <a class="Color6 Bold Pad50 Margin25 Rounded Raised" href="/system/translations.html">${T.translations}</a>
      </div>
    `);
  }
  
  P.HTML('.PageLoadContent', ls);
  
  G.filelist  = new FileList({
    div:          '.FilesTab',
    reference:    'G.filelist'
  });
  
  G.filelist.Display();
  G.filelist.List();
  
  G.friendslist  = new FriendsList({
    div:          '.FriendsTab',
    reference:    'G.filelist'
  });
  
  G.friendslist.Display();
  G.friendslist.List();
  
  G.passkeyslist  = new PasskeysList({
    div:          '.PasskeysTab',
    reference:    'G.passkeyslist'
  });
  
  G.passkeyslist.Display();
  G.passkeyslist.List();
  
  setTimeout(
    function()
    {
      let tabs  = [
        ['Account', T.myaccount],
        ['Files', T.myfiles],
        ['Friends', T.myfriends],
        ['Passkeys', T.passkey]
      ];
      
      P.MakeTabs(
        '.UserTabBar',
        tabs
      );
      
      G.DisplayUserStatus();
    },
    500
  );
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
