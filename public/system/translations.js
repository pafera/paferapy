G.items1  =  [];
G.items2  =  [];

// ====================================================================
G.ListApps = function(onfinished)
{
  P.DialogAPI(
    '/system/translationsapi',
    {
      command:  'list'
    },
    function(d, resultsdiv, popupclass)
    {
      P.ClosePopup(popupclass);
      
      let apps  = d.data;
      
      G.translationapps = apps;
      
      let ls    = ['<option>Pick App</option>'];
      
      for (let k of Keys(apps))
      {
        ls.push(`<option>${k}</option>`)
      }
      
      P.HTML('.AppSelect', ls);
      
      P.HTML('.TranslationSelect', '<option>Pick Translations</option>');
      
      if (onfinished)
      {
        onfinished();
      }
    }
  );
}

// ====================================================================
G.ListTranslations = function(onfinished)
{
  let app           = E('.AppSelect').value;
  
  if (app == 'Pick App')
  {
    return;
  }
  
  let translations  = G.translationapps[app];  
  
  translations.sort();
  
  let ls  = ['<option>Pick Translations</option>'];
  
  for (let r of translations)
  {
    ls.push(`<option>${r}</option>`);
  }
  
  P.HTML('.TranslationSelect', ls);
  
  if (onfinished)
    onfinished();
}

// ===========================================================
G.LoadTranslation = function(onfinished)
{
  let lang1       = E('.Lang1Select').value;
  let lang2       = E('.Lang2Select').value;
  let app         = E('.AppSelect').value;
  let translation = E('.TranslationSelect').value;

  if (lang1 == 'Language 1' || translation == 'Language 2')
  {
    P.MessageBox('Please choose your languages');
    return;
  }
  
  P.LoadingAPI(
    '.Translations',
    '/system/translationsapi',
    {
      command:      'load',
      app:          app,
      translation:  translation,
      languages:    [lang1, lang2]
    },
    function(d)
    {
      G.translations  = d.data;
      
      G.MakeTranslationsTable();
      
      P.Enable('.TranslationActions .Button', 1);
      
      if (onfinished)
      {
        onfinished();
      }
    }
  );
}

// ===========================================================
G.MakeTranslationsTable = function()
{
  let lang1       = E('.Lang1Select').value;
  let lang2       = E('.Lang2Select').value;
  
  let translations1 = G.translations[lang1];
  let translations2 = G.translations[lang2];
  
  let keys  = Array.from(
    new Set(
      Keys(translations1).concat(Keys(translations2))
    )
  );
  keys.sort();
  
  let ls    =  [
    `<table class="Styled Width100P TranslationTable">
      <thead>
        <tr>
          <th>ID</th>
          <th>${lang1}</th>
          <th>${lang2}</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
    <tbody class="TranslationBody">`
  ];

  for (let key of keys)
  {
    let text1  =  translations1[key];
    let text2  =  translations2[key];

    if (!text1)
    {
      text1  =  '';
    }

    if (!text2)
    {
      text2  =  '';
    }
    
    text1  =  text1.length > 80 ? text1.substr(0, 80) + '...' : text1;
    text2  =  text2.length > 80 ? text2.substr(0, 80) + '...' : text2;

    ls.push(
      `<tr class="HoverHighlight Entry">
        <td class="Key">${key}</td>
        <td class="Lang1">${EncodeEntities(text1)}</td>
        <td class="Lang2">${EncodeEntities(text2)}</td>
        <td class="Color3 HoverHighlight EditButton">${T.edit}</td>
        <td class="Color1 HoverHighlight DeleteButton">${T.delete}</td>
      </tr>
      `
    );
  }

  ls.push(
      `</tbody>
    </table>
    <div class="ButtonBar">
      <a class="Color4" onclick="G.EditTranslation()">New Entry</a>
      <a class="Color3" onclick="G.SaveTranslations()">Save Changes</a>
    </div>
    <div class="SaveResults"></div>
    `
  );

  P.HTML('.Translations', ls);
}

// ===========================================================
G.OnTranslationClicked  = function(e)
{
  let target  = P.Target(e);
  let entry   = P.TargetClass(e, '.Entry');
  
  if (!entry)
  {
    return;
  }
  
  let textid  = entry.querySelectorAll('.Key')[0].innerText;
  
  let lang1         = E('.Lang1Select').value;
  let lang2         = E('.Lang2Select').value;
  let app           = E('.AppSelect').value;
  let translation   = E('.TranslationSelect').value;
  
  let translation1  = G.translations[lang1][textid];
  let translation2  = G.translations[lang2][textid];
  
  if (target.classList.contains('DeleteButton'))
  {
    delete G.translations[lang1][textid];
    delete G.translations[lang2][textid];
    
    G.MakeTranslationsTable();
  } else
  {
    G.EditTranslation(textid, lang1, lang2, translation1, translation2);
  }
}

// ===========================================================
G.EditTranslation = function(textid, lang1, lang2, translation1, translation2)
{
  if (!textid)
  {
    textid        = '';
    lang1         = E('.Lang1Select').value;
    lang2         = E('.Lang2Select').value;
    translation1  = '';
    translation2  = '';
  } 
  
  P.EditPopup(
    [
      ['translationname', 'text', textid, 'Translation Name', '', 'required'],
      ['translation1', 'multitext', translation1, lang1, '', 'required'],
      ['translation2', 'multitext', translation2, lang2, '', 'required']
    ],
    function(formdiv, data, resultsdiv)
    {
      if (textid && data.translationname != textid)
      {
        delete G.translations[lang1][textid];
        delete G.translations[lang2][textid];
      }
      
      G.translations[lang1][data.translationname]  = data.translation1;
      G.translations[lang2][data.translationname]  = data.translation2;
      
      P.RemoveFullScreen();
      
      G.MakeTranslationsTable();
      
      P.HTML('.SaveResults', '<div class="white redg Pad50">' + T.needsave + '</div>')
    },
    {
      fullscreen:         1
    }
  );  
}

// ===========================================================
G.SaveTranslations   = function()
{
  P.LoadingAPI(
    '.SaveResults',
    '/system/translationsapi',
    {
      command:      'save',
      app:          E('.AppSelect').value,
      translation:  E('.TranslationSelect').value,
      data:         G.translations
    },
    function(d, resultsdiv)
    {
      P.Toast(
        '<div class="white greeng Pad50">' + T.allgood + '</div>',
        resultsdiv
      );
    }
  );
}

// ===========================================================
G.RenameTranslation   = function(method)
{
  let app           = E('.AppSelect').value;
  let translation   = E('.TranslationSelect').value;
  
  if (app == 'Pick App' || translation == 'Pick Translation')
  {
    P.MessageBox('Please choose an app and translation first!');
    return;
  }
  
  P.EditPopup(
    [
      ['newname', 'text', translation, 'New Translations Name', '', 'required']
    ],
    function(formdiv, data, resultsdiv)
    {
      let newname = data.newname;
      let command = 'save';
      
      switch (method)
      {
        case 'rename':
          command = 'rename';
          break;
        case 'copy':
          command = 'copy';
          break;
      }
      
      P.LoadingAPI(
        resultsdiv,
        '/system/translationsapi',
        {
          command:      command,
          app:          app,
          translation:  newname,
          newname:      newname,
          oldname:      translation,
          data:         {}
        },
        function(d)
        {
          P.CloseThisPopup(resultsdiv);
          
          G.ListApps(
            function()
            {
              E('.AppSelect').value         = app;
              
              G.ListTranslations(
                function()
                {
                  E('.TranslationSelect').value = newname;
                }
              );
            }
          );
          
        }
      );
    }
  );
}

// ===========================================================
G.DeleteTranslation = function()
{
  let app           = E('.AppSelect').value;
  let translation   = E('.TranslationSelect').value;
  
  P.ConfirmDeletePopup(
    app + '.' + translation,
    function()
    {
      P.LoadingAPI(
        '.ConfirmDeleteResults',
        '/system/translationsapi',
        {
          command:      'delete',
          app:          app,
          translation:  translation
        },
        function(d, resultsdiv)
        {
          P.HTML(resultsdiv, '<div class="white greeng Pad50">' + T.allgood + '</div>');
          
          G.ListApps(
            function()
            {
              E('.AppSelect').value = app;
              G.ListTranslations();
            }
          );
          
          P.Enable('.TranslationActions .Button', 0);
          P.HTML('.Translations', '');
        }
      );
    }
  );
}

// ===========================================================
G.CheckSameLang = function(selectnum)
{
  let lang1       = E('.Lang1Select').value;
  let lang2       = E('.Lang2Select').value;

  if (lang1 != lang2
    && lang1 != 'Language 1'
    && lang2 != 'Language 2'
  )
  {
    return;
  }
  
  let valuetoavoid  = lang1;
  let select        = E('.Lang2Select');

  if (selectnum == 2)
  {
    valuetoavoid  = lang2;
    select        = E('.Lang1Select');
  }

  let options = select.options;

  for (let i = 1, l = options.length; i < l; i++)
  {
    if (options[i].value != valuetoavoid)
    {
      select.value  =  options[i].value;
      break;
    }
  }
}

// ====================================================================
G.OnPageLoad   = function()
{
  P.HTML(
    '.TranslationActions',
    `
  <select class="Lang1Select"></select>
  <select class="Lang2Select"></select>
  <select class="AppSelect"></select>
  <select class="TranslationSelect"></select>
  <a class="Color2 Button SearchButton" onclick="G.LoadTranslation()">Search</a>
  <a class="Color4 Button NewButton" onclick="G.RenameTranslation()">New</a>
  <a class="Color5 Button CopyButton" onclick="G.RenameTranslation('copy')">Copy</a>
  <a class="Color6 Button RenameButton" onclick="G.RenameTranslation('rename')">Rename</a>
  <a class="Color1 Button DeleteButton" onclick="G.DeleteTranslation();">Delete</a>
    `
  );
  
  P.Enable('.TranslationActions .Button', 0);
  
  let ls  = [];
  
  for (let i = 0, l = P.languages.length; i < l; i++)
  {
    ls.push(`<option>${P.languages[i]}</option>`);
  }
  
  P.HTML('.Lang1Select', ['<option>Language 1</option>'] + ls);
  P.HTML('.Lang2Select', ['<option>Language 2</option>'] + ls);
  
  P.On('.AppSelect', 'change', function() { G.ListTranslations(); });
  P.On('.TranslationSelect',   'change', function() { G.LoadTranslation(); });
  P.On('.Lang1Select',    'change', function() { G.CheckSameLang(1); });
  P.On('.Lang2Select',    'change', function() { G.CheckSameLang(2); });

  P.OnClick(
    '.Translations',
    P.LBUTTON,
    G.OnTranslationClicked, 
  );

  G.ListApps();
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
