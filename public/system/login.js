"use strict";

// ====================================================================
G.OnLoginSuccessful = function(d)
{
  let url = window.location.toString();
  
  let nextpagepos = url.indexOf('nextpage');
  
  if (nextpagepos != -1)
  {
    // This ensures that any query parameters from the next page 
    // URL are saved.
    url = url.substr(nextpagepos + 9).replace('&', '?');
    
    P.LoadURL(url);
  } else if (d.homeurl)
  {
    P.LoadURL(d.homeurl);
  } else
  {
    P.LoadURL('/learn/home.html');
  }
}

// ====================================================================
G.Login 	= function(el, fields)
{
  if (!fields.phonenumber
    || !fields.place
    || !fields.password
  )
  {
    P.ErrorPopup(T.missingfields);
    return;
  }
  
  fields.command  = 'login';

  P.LoadingAPI(
    '.LoginFormResults',
    '/system/userapi',
    fields,
    function(d, resultsdiv)
    {
      G.OnLoginSuccessful(d);
    }
  );
}

// ====================================================================
G.SendTextMessage 	= function()
{
  let phonenumber 	= E('.phonenumber').value;

  if (!phonenumber)
  {
    P.ErrorPopup(T.missingfields);
    return;
  }

  P.LoadingAPI(
    '.LoginFormResults',
    '/system/userapi',
    {
      command:      'sendsms',
      data: 	       phonenumber
    },
    function(d, resultsdiv)
    {
      P.HTML(
        resultsdiv,
        `<div class="greenb Pad50">
          ${T.allgood}
        </div>`
      );
    }
  );
}

// ====================================================================
G.SendEmail 	= function ()
{
  let email 	= E('.email').value;

  if (!email)
  {
    P.ErrorPopup(T.missingfields);
    return;
  }

  P.LoadingAPI(
    '.LoginFormResults',
    '/system/userapi',
    {
      command:      'sendemail',
      data: 	       email
    },
    function(d, resultsdiv)
    {
      P.HTML(
        resultsdiv,
        `<div class="greenb Pad50">
          ${T.allgood}
        </div>`
      );
    }
  );
}

// ====================================================================
G.CheckPasskey  = function()
{
  let phonenumber = E('.phonenumber').value.trim();
  let place       = E('.place').value.trim();
    
  if (!phonenumber || !place)
  {
    P.ErrorPopup('Please fill out phone number and place');
    return;
  }
  
  P.LoadingAPI(
    '.LoginFormResults',
    '/system/userapi',
    {
      command:        'getpasskeyauthentication',
      phonenumber:    phonenumber,
      place:          place
    },
    function(d, resultsdiv)
    {
      let registrationdata  = JSON.parse(d.data);
      
      try 
      {
        SimpleWebAuthnBrowser.startAuthentication(registrationdata).then(
          function(authinfo)
          {
            P.LoadingAPI(
              '.LoginFormResults',
              '/system/userapi',
              {
                command:      'verifypasskey',
                phonenumber:  phonenumber,
                place:        place,
                data:         authinfo
              },
              function(d, resultsdiv)
              {
                G.OnLoginSuccessful(d);
              }
            );
          }
        );        
      } catch (err) 
      {
        P.HTML(resultsdiv, `<div class="Error">${err}</div>`);
      }      
    }
  );
}

// ====================================================================
G.GenerateLoginLink  = function()
{
  P.DialogAPI(
    '/system/userapi',
    {
      command:        'generateloginlink'
    },
    function(d, resultsdiv)
    {
      let loginlink = `https://${window.location.hostname}/system/qr/${d.linkid}`;
      
      P.Popup(
        `<div class="Center Pad50">
          ${T.typeloginlink}<br>
          <br>
          ${loginlink}<br>
          <br>
          <div class="Center LoginQRCode"></div>
          <div class="ButtonBar">
            <a class="Color4" onclick="navigator.clipboard.writeText('${loginlink}');">${T.copylink}</a>
            <a class="Color1" onclick="P.CloseThisPopup(this)">${T.back}</a>
          </div>
        </div>
        `
      );
      
      let qrcode = new QRCode(E('.LoginQRCode'), loginlink);
      
      setTimeout(
        function()
        {
          E('.LoginQRCode img').style.display = 'inline';
        },
        500
      );
      
      setTimeout(
        function()
        {
          G.CheckLoginLinkStatus(d.linkid, resultsdiv);
        },
        5000
      );
    }
  );
}

// ====================================================================
G.CheckLoginLinkStatus  = function(linkid, resultsdiv)
{
  P.LoadingAPI(
    resultsdiv,
    '/system/userapi',
    {
      command:    'checkloginlinkstatus',
      linkid:     linkid
    },
    function(d, resultsdiv)
    {
      if (d.status == 'verified')
      {
        G.OnLoginSuccessful(d);
        return;
      }
      
      if (T[d.status])
      {
        P.HTML(resultsdiv, `<div class="orangeb">${T[d.status]}</div>`);
      } else
      {
        P.HTML(resultsdiv, `<div class="redb">${T[d.status]}</div>`);
      }
      
      if (d.status == 'waiting')
      {
        setTimeout(
          function()
          {
            G.CheckLoginLinkStatus(linkid, resultsdiv);
          },
          5000
        );
      }
    }
  );
}

// ====================================================================
G.SetupForm 	= function ()
{
  P.EditPopup(
    [
      ['phonenumber', 'text', '', T.phonenumber, '', 'required autocomplete="webauthn"'],
      ['place', 'text', '', T.place, '', 'required'],
      ['password', 'password', '', T.password, '', 'required'],
      ['email', 'email', '', T.email]
    ],
    G.Login,
    {
      formdiv:					'LoginForm',
      gobuttontext:			T.finished, 
      cancelbuttontext:	T.back,
      cancelfunc:				function()
        {
          window.history.back();
        },
      extrabuttons:			`${
          SimpleWebAuthnBrowser.browserSupportsWebAuthn()
          ? `<a class="Color5" onclick="G.CheckPasskey()">${T.passkey}</a>`
          : ''
        }
          <a class="Color6" onclick="G.GenerateLoginLink()">${T.qrcode}</a>
        </div>
        <h2 class="red">${T.forgotpassword}</h2>
        <div class="ButtonBar">
          <a class="Color6" href="/register.html">
            ${T.register}
          </a>
          <a class=Color4 onclick="G.SendTextMessage()">
              ${T.sendsms}
          </a>
          <a class=Color5 onclick="G.SendEmail()">
            ${T.sendemail}
          </a>
        </div>`
    }
  );

  P.OnEnter(
    '.LoginForm input',
    function()
    {
      G.Login('.LoginForm', P.FormToArray('.LoginForm'));
    }
  );
}

// ********************************************************************
P.AddHandler('pageload', G.SetupForm);
