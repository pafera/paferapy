// ********************************************************************
/** A version of PaferaChooser which is used by the system to let
 * the user choose from uploaded files.
 * 
 * @param {object} config - The same as PaferaChooser with the addition
 *    of filetype, which can be one of the following:
 */
class PaferaFileChooser extends PaferaChooser
{
	// ------------------------------------------------------------------
	constructor(config)
	{
		super(config);
		
		this.filetype	= config.filetype	|| 'all';
		
		this.extrabuttons	= `
			<select class="FileChooserFilter">
				<option value="myfiles">My Files</option>
				<option value="allfiles">All Files</option>
			</select>
		`;
	}
	
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
		let filter	= E('.FileChooserFilter');
		
		if (!filter)
		{
			return;
		}
		
    P.LoadingAPI(
      '.' + self.div + ' .AvailableCards',
      '/system/fileapi',
      {
        command:      'search',
        filetype:			self.filetype,
				filename:			searchterm ? searchterm : '',
				start:				start,
				filter:				filter.value
      },
      function(d)
      {
        self.available        = d.data;
        self.availablecount   = d.count;
        
        self.DisplayAvailable();
      }
    );		
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    return P.RenderFileInfo(r);
  }
  
  // ------------------------------------------------------------------
  OnChosenAdded(dbid, obj)
  {
		let self	= this;
		
		if (!self.multipleselect)
		{
			P.RemoveFullScreen();
			self.OnFinished();
		}
  }
}
