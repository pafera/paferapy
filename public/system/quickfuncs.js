"use strict";

// ====================================================================
G.MakeQuickFuncsButton = function(selector, avatar)
{
  selector	=	selector	|| '.QuickFuncsButton';
  avatar		= avatar		|| P.possumbot;

  P.MakeFloatingButton(
    selector, 
    `<div class="redb HoverHighlight Pad50 Rounded Raised QuickFuncsImage" 
      style="background-image: url('${P.baseurl}/system/avatars/${avatar}.png'); background-size: 100% 100%; width: 3em; height: 2.5em;" />
    </div>`,
    P.ShowQuickFuncsPopup
  );
}

// ====================================================================
G.ShowQuickFuncsPopup = function(e, command)
{
  command	= command || '';

  let popupid = P.Popup(
    `<div class="whiteb Pad50 Raised">
      <div class="FlexInput QuickFuncInput" data-name="quickfuncinput" data-value="${EncodeEntities(command)}"></div>
      <div class=QuickFuncsButtons></div>
      <div class=QuickFuncResults></div>
    </div>`,
    {
      width:					(window.innerWidth * 0.8) + 'px'
    }
  );
  
  P.FlexInput();
  
  P.MakeButtonBar(
    '.QuickFuncsButtons',
    [
      [T.finished, P.ProcessQuickFunc],
      [T.help, P.QuickFuncsHelp],
      [T.close, function() { P.CloseThisPopup('.QuickFuncInput'); }]
    ]
  );
    
  let textarea	= E(popupid + ' textarea');
  
  P.OnAltEnter(textarea, P.ProcessQuickFunc);
    
  textarea.focus();
}

// ====================================================================
G.AddQuickFunc = function(command, options, description, func)
{
  P.quickfuncs.push([command, options, description, func]);
  P.quickfuncs.sort(
    function(a, b) 
    {
      return b[0].length - a[0].length;
    }
  );
}

// ====================================================================
G.ProcessQuickFunc = function()
{
  let text	= E('.QuickFuncInput textarea').value;

  if (text)
  {
    let lower	= text.toLowerCase();
    let found	= 0;
  
    for (let i = 0, l = P.quickfuncs.length; i < l; i++)
    {
      let command = P.quickfuncs[i][0];
    
      if (lower.startswith(command.toLowerCase()))
      {
        P.quickfuncs[i][3](text.substr(command.length).trim(), E('.QuickFuncResults'));
        found	= 1;
        break;
      }
    }
  
    if (!found)
    {
      P.HTML(
        '.QuickFuncResults',
        '<div class="yellowb Pad50">' + T.nothingfound + '</div>'
      );
    }
  }
}

// ====================================================================
G.QuickFuncsHelp = function()
{
  let helptext	= [
    `<div class="Pad50 whiteb">
      <div class=Left>
        <h2></h2>
        <p></p>
        <h3></h3>
        <dl>`
  ];

  for (let i = 0, l = P.quickfuncs.length; i < l; i++)
  {
    let item	= P.quickfuncs[i];
    let color	= (i % 6) + 1;
  
    helptext.push(
      `<dt>
        <a class="Button Color${color}" 
          onclick="P.CopyQuickFuncCommand('${EncodeEntities(item[0])}')">
          ${item[1]}
        </a>
      </dt>
      <dd>
        <p>${item[2]}</p>
      </dd>`
    );
  }

  helptext.push(
        `</dl>
        <p></p>
        <div class=ButtonBar>
          <a class=Color1 onclick="P.RemoveFullScreen(1)">
            ${T.back}
          </a>
        </div>
      </div>
    </div>`
  );

  P.CloseThisPopup('.QuickFuncInput');

  P.AddFullScreen(
    'whiteb',
    '',
    helptext
  );
}

// ====================================================================
G.CopyQuickFuncCommand = function(command)
{
  P.RemoveFullScreen(1);
  P.ShowQuickFuncsPopup(0, command);
}

// ====================================================================
G.QuickChangeAvatar = function(text, resultsdiv)
{
  if (text.length)
  {
    P.LoadingAPI(
      resultsdiv,
      'possumbot/set',
      {
        avatar:	text
      },
      function(d, resultsdiv)
      {
        E('.QuickFuncsImage').style.backgroundImage  = `url("${P.FileURL('avatars/' + text + '.png', 'h')}")`;
        P.HTML(resultsdiv, `<div class="greenb Pad50">${T.allgood}</div>`);
      }
    );
  } else
  {	
    P.LoadingAPI(
      resultsdiv,
      'possumbot/list',
      {},
      function(d, resultsdiv)
      {
        let avatars	= d.items;
      
        let ls	= ['<div class=Cards4>'];
      
        for (let i = 0, l = avatars.length; i < l; i++)
        {
          ls.push(
            `<div class="lgrayb Card HoverHighlight" onclick="P.QuickChangeAvatar('${avatars[i]}')">
              <img class=CardIcon src="${P.FileURL('avatars/' + avatars[i] + '.png', 'h')}">
              <div class=Center>${avatars[i]}</div>
            </div>`
          );
        }
      
        ls.push('</div><div class=Cleared></div>');
      
        P.HTML(resultsdiv, ls);
      }
    );
  }
}
