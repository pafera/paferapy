"use strict";

G.extrauseractions  = [];

// ====================================================================
G.RenderUser = function(r)
{
  return `
      <div class="HeadShot">
        ${P.HeadShotImg(r.idcode, 'Square400')}
      </div>
      <div class="DisplayName">
        ${P.BestTranslation(r.displayname)}
      </div>
      <div class="PhoneNumber">
        ${r.phonenumber}
      </div>
      <div class="Place">
        ${r.place}
      </div>
  `;
}

// ********************************************************************
class GroupChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    let self  = this;
    
    this.multipleselect = 1;
    
    this.onfinished = function(dbid, obj)
    {
      P.RemoveFullScreen();
    }
    
    this.useridcode     = config.useridcode;
    
    this.linkedidcodes  = [];
    
    this.onfinished = function()
    {
      let linkedidcodes = [];
      
      for (let chosengroup of self.chosen)
      {
        linkedidcodes.push(chosengroup.idcode);
      }
      
      P.LoadingAPI(
        self.div + 'Results',
        '/system/dbapi',
        {
          command:      'linkarray',
          model1:       'system_user',
          model2:       'system_group',
          id1:          self.useridcode,
          id2s:         linkedidcodes
        },
        function(d, resultsdiv)
        {
          P.RemoveFullScreen();
        }
      );
    }
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm = '')
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/system/dbapi',
      {
        command:    'search',
        model:      'system_group',
        fields:     'rid, groupname, displayname',
        start:      start,
        limit:      self.limit,
        orderby:    'groupname'
      },
      function(d)
      {
        self.available      = d.data.filter(
          function(r)
          {
            return self.linkedidcodes.has(r.idcode);
          }
        );
        
        self.availablecount = d.count;
        
        self.DisplayAvailable();
      }
    );
  }  
  
  // ------------------------------------------------------------------
  GetChosen()
  {
    let self  = this;
    
    P.LoadingAPI(
      E('.' + self.div + ' ChosenCards'),
      '/system/dbapi',
      {
        command:    'linked',
        model1:     'system_user',
        model2:     'system_group',
        id1:        self.useridcode
      },
      function(d)
      {
        self.chosen         = d.data;
        
        self.linkedidcodes  = [];
        
        for (let linkedgroup of self.chosen)
        {
          self.linkedidcodes.push(linkedgroup.idcode);
        }
        
        self.DisplayChosen();
        
        self.GetAvailable();
      }
    );
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    let self  = this;
    
    return `
      <div class="DisplayName">${P.BestTranslation(r.displayname)}</div>
      <div class="GroupName">${r.groupname}</div>
    `;
  }
}


// ********************************************************************
class UsersChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    let self  = this;
    
    this.fieldname      = config.fieldname;
    this.multipleselect = 1;
    
    this.onfinished = function(dbid, obj)
    {
      let ids = [];
      
      let renderedusers = [];
      
      for (let r of self.chosen)
      {
        ids.push(r.idcode);
        
        renderedusers.push(self.RenderItem(r))
      }
      
      E('.' + self.fieldname).value = ids.join('\n');
      
      let renderdiv = '.' + self.fieldname + 'Users';
      
      if (renderdiv)
      {
        P.HTML(renderdiv, renderedusers);
      }
      
      P.RemoveFullScreen();
    }    
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    return G.RenderUser(r);
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm = '')
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/system/userapi',
      {
        command:    'search',
        keyword:    searchterm,
        start:      start,
        limit:      self.limit,
        getmanaged: 1
      },
      function(d)
      {
        self.available      = d.allusers;
        self.availablecount = d.alluserscount;
        
        if (self.fieldname == 'addmanagedusers')
        {
          self.chosen         = d.data;
          self.chosencount    = d.count;
        }
        
        self.DisplayAvailable();
        self.DisplayChosen();
      }
    );
  }  
}

// ********************************************************************
class UsersList extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    config.apiurl = '/system/userapi';
    
    config.displayfields  = [
      ['headshot', 'custom', ''],
      ['phonenumber', 'text', T.phonenumber],
      ['place', 'text', T.place],
      ['displayname', 'translation', T.username]
    ];
    
    config.showidcode   = 1;
    config.enablesearch = 1;
    config.enabledelete = 1;
    config.enableedit   = 1;
    config.enableselect = 1;
    
    if (IsEmpty(config.extraactions))
    {
      config.extraactions = [];
    }
    
    if (P.IsAdmin())
    {
      config.extraactions.push(['Groups', 'Groups', 4]);
    }
    
    if (!IsEmpty(G.extrauseractions))
    {
      config.extraactions.extend(G.extrauseractions);
    }
    
    config.saveandcontinue  = 1;
    
    config.headertext   = `
      <p class="Error AddUserHeader">Remember that you must save a new user before you can add a headshot.</p>
    `;
    
    super(config); 
    
    if (P.IsAdmin())
    {
      this.extrabuttons = `
        <input type="hidden" class="addmanagedusers">
        <a class="Color4" onclick="${this.reference}.Add()">${T.add}</a>
        <a class="Color5" onclick="${this.reference}.AddManagedUsers()">${T.addmanageduser}</a>
      `;
    }
  }
  
  // ------------------------------------------------------------------
  GetItemDisplayName(obj)
  {
    if (IsEmpty(obj.displayname))
    {
      return obj.idcode.toString();
    }
    
    return P.BestTranslation(obj.displayname);
  }
  
  // ------------------------------------------------------------------
  GetEditFields(obj)
  {
    if (P.IsAdmin())
    {
      return [
        ['idcode', 'hidden', 0, T.idcode],
        ['headshot', 'custom', 0, T.headshot],
        ['phonenumber', 'text', 0, T.phonenumber],
        ['place', 'text', 0, T.place],
        ['displayname', 'translation', 0, T.username],
        ['password', 'password', 0, T.password],
        ['expiredate', 'timestamp', 0, T.expiredate],
        ['homeurl', 'text', 0, T.homeurl],
        ['storagequota', 'int', 128, T.storagequota + ' (' + T.mib + ')'],
        ['numcanmanage', 'int', 0, T.numcanmanage],
        ['canmanage', 'custom', 0, T.canmanage],
        ['managedby', 'custom', 0, T.managedby],
        ['flags', 'bitflags', obj.flags, T.options,
          [
            [T.mustchangepassword,  'mustchangepassword', P.USER_MUST_CHANGE_PASSWORD],
            [T.disabled,            'disabled',           P.USER_DISABLED],
            [T.needapproval,        'needapproval',       P.USER_NEED_APPROVAL],
            [T.rejected,            'rejected',           P.USER_REJECTED],
            [T.canmanageself,       'canmanageself',      P.USER_CAN_MANAGE_SELF],
            [T.isadmin,             'isadmin',            P.USER_IS_ADMIN],
            [T.canupload,           'canupload',          P.USER_CAN_UPLOAD],
            [T.canuploadoriginals,  'canuploadoriginals', P.USER_CAN_UPLOAD_ORIGINALS],
            [T.canpostmessages,     'canpostmessages',    P.USER_CAN_POST_MESSAGES]
          ]
        ]
      ];    
    } else
    {
      return [
        ['idcode', 'hidden', 0, T.idcode],
        ['headshot', 'custom', 0, T.headshot],
        ['phonenumber', 'text', 0, T.phonenumber],
        ['place', 'text', 0, T.place],
        ['displayname', 'translation', 0, T.username],
        ['password', 'password', 0, T.password]
      ];    
    }
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    let self  = this;
    
    let ls    = [];
    
    if (self.usetable)
    {
      ls.push(`
        <td class="Pad25">
          ${P.HeadShotImg(r.idcode, 'Square400')}
        </td>
        <td class="Pad25">
          ${r.phonenumber}
        </td>
        <td class="Pad25">
          ${r.place}
        </td>
        <td class="Pad25">
          ${P.BestTranslation(r.displayname)}
        </td>
      `);
    } else
    {
      ls.push(`
        <table class="Pad25">
          <tr>
            <td rowspan="3">
              ${P.HeadShotImg(r.idcode, 'Square400')}
            </td>
            <td class="Pad25">
              ${r.phonenumber}
            </td>
          </tr>
          <tr>
            <td class="Pad25">
              ${r.place}
            </td>
          </tr>
          <tr>
            <td class="Pad25">
              ${P.BestTranslation(r.displayname)}
            </td>
          </tr>
        </table>
      `);
    }
    
    return ls.join('\n');
  }  
  
  // ------------------------------------------------------------------
  AddManagedUsers()
  {
    let self  = this;
    
    G.userchooser = new UsersChooser({
      div:          '.ChooseUsers',
      reference:    'G.userchooser',
      fieldname:    'addmanagedusers'
    });
    
    let oldonfinished   = G.userchooser.onfinished;
    
    G.userchooser.onfinished  = function()
    {
      oldonfinished();
      
      P.LoadingAPI(
        '.ChooseUsers .AvailableCards',
        '/system/userapi',
        {
          command:   'save',
          data:     {
            idcode:     P.userid,
            canmanage:  E('.addmanagedusers').value.split('\n')
          }
        },
        function(d, resultsdiv)
        {
          self.List();
        }
      );      
    }
    
    G.userchooser.Display();
  }
  
  // ------------------------------------------------------------------
  ChooseUsers(fieldname, userid)
  {
    let self  = this;
    
    G.userchooser = new UsersChooser({
      div:          '.ChooseUsers',
      reference:    'G.userchooser',
      fieldname:    fieldname
    });
    
    if (fieldname == 'canmanage' && G.currentcanmanage)
    {
      G.userchooser.chosen  = G.currentcanmanage;
    } else if (fieldname == 'managedby' && G.currentmanagedby)
    {
      G.userchooser.chosen  = G.currentmanagedby;
    }
    
    let oldonfinished   = G.userchooser.onfinished;
    
    G.userchooser.Display();
  }
  
  // ------------------------------------------------------------------
  OnEditForm(obj, formdiv)
  {
    let self  = this;
    
    self.currentobj = obj;
    
    // Can only upload a headshot after we get an ID from the database,
    // so new users must be saved first before changing the headshot.
    if (!obj.idcode)
    {
      P.Hide('.headshotRow');
      
      // If the expiredate is now when adding a new user, then set it 
      // to next month
      let expiredate  = E('.expiredate');
      let datevalue   = Date.parse(expiredate.value);
      let nowvalue    = Date.now();
      
      if ((datevalue - nowvalue) < 86400000)
      {
        let d = new Date();
        
        expiredate.value  = (new Date(d.getFullYear(), d.getMonth() + 1, d.getDay())).toISOString().substr(0, 10) + 'T00:00:00';
      }      
    } else
    {
      P.Hide('.AddUserHeader');
    }
    
    // Convert from bytes to megabytes
    let storagequota  = E('.storagequota');    
    
    if (storagequota.value)
    {
      storagequota.value  = (parseInt(storagequota.value) / 1024 / 1024).toFixed(0);
    }
    
    // Set quota for new users to 256MiB by default
    if (!parseInt(storagequota.value))
    {
      storagequota.value  = 256;
    }
    
    P.HTML(
      '.' + self.div + 'EditForm .headshotDiv',
      `<div class="Center Pad50">
        ${P.HeadShotImg(obj.idcode, 'CurrentHeadShot Square1600')}
      </div>
      <div class="ButtonBar">        
        <a class="Color4" onclick="${self.reference}.UploadHeadshot()">
          ${T.upload}
        </a>        
        </div>
      `
    );
    
    P.HTML(
      '.' + self.div + 'EditForm .canmanageDiv',
      `<div class="FlexGrid12 canmanageUsers"></div>
      <div class="ButtonBar">        
        <a class="Color4" onclick="${self.reference}.ChooseUsers('canmanage', '${obj.idcode}')">
          ${T.edit}
        </a>        
      </div>
      `
    );
    
    G.currentcanmanage  = [];
    
    if (!IsEmpty(obj.canmanage))
    {
      P.LoadingAPI(
        '.canmanageUsers',
        '/system/userapi',
        {
          command:  'search',
          userids:  obj.canmanage
        },
        function(d, resultsdiv)
        {
          G.currentcanmanage  = d.data;
          
          if (d.data)
          {
            let ls  = [];
          
            for (let r of d.data)
            {
              ls.push(`
                <div class="whiteb Raised Rounded Pad50 Margin50">
                ${G.RenderUser(r)}
                </div>
              `);
            }
            
            P.HTML(resultsdiv, ls);
          } else
          {
            P.HTML(resultsdiv, T.nothingfound);
          }
        }
      );
    }
    
    P.HTML(
      '.' + self.div + 'EditForm .managedbyDiv',
      `<div class="FlexGrid12 managedbyUsers"></div>
      <div class="ButtonBar">        
        <a class="Color4" onclick="${self.reference}.ChooseUsers('managedby', '${obj.idcode}')">
          ${T.edit}
        </a>        
      </div>
      `
    );
    
    G.currentmanagedby  = [];
    
    if (!IsEmpty(obj.managedby))
    {
      P.LoadingAPI(
        '.managedbyUsers',
        '/system/userapi',
        {
          command:  'search',
          userids:  obj.managedby
        },
        function(d, resultsdiv)
        {
          G.currentmanagedby  = d.data;
          
          if (d.data)
          {
            let ls  = [];
          
            for (let r of d.data)
            {
              ls.push(`
                <div class="whiteb Raised Rounded Pad50 Margin50">
                ${G.RenderUser(r)}
                </div>
              `);
            }
            
            P.HTML(resultsdiv, ls);
          } else
          {
            P.HTML(resultsdiv, T.nothingfound);
          }
        }
      );
    }
  }
  
  // ------------------------------------------------------------------
  UploadHeadshot()
  {
    let self  = this;
    
    P.UploadFilePopup(
      '/system/upload/headshot/' + self.currentobj.idcode,
      {
        resizeimage:    [640, 640],
        onfinished:     function()
        {
          E('.CurrentHeadShot').setAttribute('src', `/system/headshots/${P.CodeDir(self.currentobj.idcode)}.webp` + '?t=' + Date.now());
          
          P.CloseThisPopup('.UploadedFiles');
        }
      }
    );
  }
  
  // ------------------------------------------------------------------
  OnSaveData(data)
  {
    let self  = this;
    
    // Convert from megabytes to bytes
    if (data.storagequota)
    {
      data.storagequota *= 1024 * 1024;
    }
    
    // Convert string lists to arrays
    if (data.canmanage)
    {
      if (data.canmanage.has(','))
      {
        data.canmanage  = data.canmanage.split(',');
      }
      
      if (data.canmanage.has('\n'))
      {
        data.canmanage  = data.canmanage.split('\n');
      }
      
      if (!IsArray(data.canmanage))
      {
        data.canmanage  = [data.canmanage];
      }
    }
    
    if (data.managedby)
    {
      if (data.managedby.has(','))
      {
        data.managedby  = data.managedby.split(',');
      }
      
      if (data.managedby.has('\n'))
      {
        data.managedby  = data.managedby.split('\n');
      }
      
      if (!IsArray(data.managedby))
      {
        data.managedby  = [data.managedby];
      }
    }    
  }
  
  // ------------------------------------------------------------------
  OnSaveComplete(data, resultsdiv, saveandcontinue)
  {
    let self    = this;
    
    if (data.idcode)
    {
      E('.idcode').value  = data.idcode;
      
      P.Show('.headshotRow');
      
      self.currentobj.idcode  = data.idcode;
    }
    
    super.OnSaveComplete(data, resultsdiv, saveandcontinue);
    
    if (saveandcontinue)
    {
      P.Toast(
        `<div class="greeng Pad50">${T.allgood}</div>`,
        resultsdiv
      )
    }
  }
  
  // ------------------------------------------------------------------
  OnDelete(dbids)
  {
    let self  = this;
    
    P.DialogAPI(
      '/system/userapi',
      {
        command:  'deleteuser',
        ids:      dbids
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        self.List();
      }
    );        
  }
  
  // ------------------------------------------------------------------
  Groups(cardelement, dbid)
  {
    let self  = this;
    
    G.groupchooser  = new GroupChooser({
      div:        '.GroupChooser',
      reference:  'G.groupchooser',
      fields:     [
        'displayname',
        'groupname',
      ],
      useridcode: dbid
    });
    
    G.groupchooser.Display();
    G.groupchooser.GetChosen();
  }
}

// ====================================================================
G.OnPageLoad = function()
{
  P.HTML(
    '.PageLoadContent',
    `<h1>${document.title}</h1>
    <div class="UsersList"></div>
    `
  );  
  
  G.userslist = new UsersList({
    div:        '.UsersList',
    reference:  'G.userslist'
  });
  
  G.userslist.Display();
  G.userslist.List();    
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
