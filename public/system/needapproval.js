"use strict";

// ********************************************************************
class ApprovalList extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }
  
  // ------------------------------------------------------------------
  OnList(resultsdiv, start, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/system/userapi',
      {
        command:  'listneedapprovals',
        start:    start,
        keyword:  searchterm
      },
      function(d)
      {
        self.items    = d.data;
        self.numitems = d.count;
        
        self.DisplayItems();
      }
    );
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    let self  = this;
    
    return `
      <div class="FlexAlignSpacer"></div>
      <table class="Pad50 Left">
        <tr>
          <td class="Center" colspan="2">
            <img class="Square800" src="/system/newheadshots/${r.idcode.substr(0, 3)}/${r.idcode.substr(3)}.webp">
          </td>
        </tr>
        <tr>
          <th>${T.phonenumber}</th>
          <td>${r.phonenumber}</td>
        </tr>
        <tr>
          <th>${T.place}</th>
          <td>${r.place}</td>
        </tr>
        <tr>
          <th>${T.email}</th>
          <td>${r.email}</td>
        </tr>
        <tr>
          <th>${T.groups}</th>
          <td>${r.extrainfo.role}</td>
        </tr>
        <tr>
          <th>${T.comments}</th>
          <td>${r.extrainfo.comments}</td>
        </tr>
        <tr>
          <th>${T.status}</th>
          ${r.flags & 0x01 
            ? `<td class="blueb">${T.waiting}</td>` 
            : `<td class="redb">${T.denied}: ${r.denyreason}</td>`
          }
        </tr>
        <tr>
          <th>${T.email}</th>
          ${r.flags & 0x04
            ? `<td class="greenb">${T.validated}</td>` 
            : `<td class="redb">${T.notvalidated}</td>`
          }
        </tr>
      </table>
      <div class="ButtonBar Margin50">
        <a class="Color3" onclick="${self.reference}.Approve('${r.idcode}', 1)">${T.approve}</a>
        <a class="Color1" onclick="${self.reference}.Approve('${r.idcode}', 0)">${T.deny}</a>
      </div>
      <div class="FlexAlignSpacer"></div>
    `;
  }
  
  // ------------------------------------------------------------------
  Approve(dbid, approved)
  {
    let self  = this;
    
    if (!approved)
    {
      P.InputPopup(
        T.pleasetypedenyreason,
        function(reason)
        {
          if (reason)
          {
            P.DialogAPI(
              '/system/userapi',
              {
                command:  'approve',
                userid:   dbid,
                approve:  0,
                reason:   reason
              },
              function(d, resultsdiv, popupclass)
              {
                P.ClosePopup(popupclass);
                
                self.List();
              }
            );
          } else
          {
            P.ErrorPopup(T.missingfields);
          }
        }
      );
    } else
    {
      P.DialogAPI(
        '/system/userapi',
        {
          command:  'approve',
          userid:   dbid,
          approve:  1
        },
        function(d, resultsdiv, popupclass)
        {
          P.ClosePopup(popupclass);
          
          self.List();
        }
      );
    }
  }
}

// ====================================================================
G.OnPageLoad = function ()
{
  G.approvals = new ApprovalList({
    div:            '.ApprovalList',
    displayfields:  [],
    reference:      'G.approvals',
    enablesearch:   1,
    enabledelete:   1,
    liststyle:      'FlexGrid16',
    cardstyles:     'whiteb Width1600 MinHeight2600 Margin25 Pad25 Rounded Flex FlexCenter FlexVertical'
  });
  
  G.approvals.Display();
  G.approvals.List();  
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
