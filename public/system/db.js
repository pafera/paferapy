G.currentmodel        = '';
G.listfuncs           = {};
G.displayfuncs        = {};
G.editfuncs           = {};
G.customeditfuncs     = {};
G.savedatafuncs       = {};
G.displaylinkfuncs    = {};
G.displaylinkedfuncs  = {};

// ********************************************************************
class DBList extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    config.displayfields  = [];
    
    config.showidcode   = 1;
    config.enablesearch = 1;
    config.enabledelete = 1;
    config.enableadd    = 1;
    config.enableedit   = 1;
    config.enableselect = 1;
    config.limit        = 20;
    
    config.saveandcontinue  = 1;
        
    super(config);    
  }
  
  // ------------------------------------------------------------------
  GetEditFields(obj)
  {
    let fields      = P.ToggledButtons('.DBFields')['on'];      
    let model       = G.currentmodel;
    let modelinfo   = G.modelfields[model];
    let modelid     = G.modelids[model];
    
    let editfields  = [];
    
    fields.splice(0, 0, modelid);
    
    for (let i = 0, l = fields.length; i < l; i++)
    {
      let fieldname   = fields[i];
      let fieldtype   = modelinfo[fieldname][0].toLowerCase();
      
      if (fieldtype.has('int'))
      {
        editfields.push([fieldname, 'int', '', fieldname]);
      } else if (fieldtype.has('float'))
      {
        editfields.push([fieldname, 'float', '', fieldname]);
      } else
      {
        switch (fieldtype)
        {
          case 'datetime':
            editfields.push([fieldname, 'datetime-local', '', fieldname]);
            break;
          case 'translation':
            editfields.push([fieldname, 'translation', '', fieldname]);
            break;
          case 'password':
            editfields.push([fieldname, 'password', '', fieldname]);
            break;
          case 'multitext':
            editfields.push([fieldname, 'multitext', '', fieldname]);
            break;
          case 'newlinelist':
            editfields.push([fieldname, 'newlinelist', '', fieldname]);
            break;
          case 'list':
            editfields.push([fieldname, 'list', '', fieldname]);
            break;
          case 'dict':
            editfields.push([fieldname, 'dict', '', fieldname]);
            break;
          default:
            editfields.push([fieldname, 'text', '', fieldname]);
        };
      }
    }
    
    if (G.editfuncs[model])
    {
      G.editfuncs[model](obj, editfields);
    }
  
    return editfields;
  }
  
  // ------------------------------------------------------------------
  Display()
  {
    let self  = this;
    
    let orderby = {};
    
    let fields  = P.ToggledButtons('.DBFields')['on'];
    
    if (IsEmpty(fields))
    {
      fields  = G.modeldisplayfields[G.currentmodel];
    }
    
    for (let fieldname of fields)
    {
      orderby[fieldname]            = fieldname + ' ⬆️';
      orderby[fieldname + ' DESC']  = fieldname + ' ⬇️';
    }
    
    self.orderby  = orderby;
    
    super.Display();
  }
  
  // ------------------------------------------------------------------
  OnList(resultsdiv, start, searchterm)
  {
    let self  = this;
    
    let model = G.currentmodel;
    
    let fields  = [G.modelids[model]];
    fields  = fields.concat(P.ToggledButtons('.DBFields')['on']);
    
    if (G.listfuncs[model])
    {
      G.listfuncs[model](fields, condition, start, limit, G.modeldisplayfields[model].join(', '));
    } else
    {
      P.LoadingAPI(
        self.div,
        '/system/dbapi',
        {
          command:    'search',
          model:      model,
          fields:     fields,
          condition:  searchterm,
          start:      start,
          limit:      self.limit,
          orderby:    E('.' + self.div + ' .OrderBy').value
        },
        function(d)
        {
          self.items    = d.data;
          self.numitems = d.count;
          
          self.DisplayItems();
        }
      );
    }
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    let self    = this;    
    let model   = G.currentmodel;   
    let idfield = G.modelids[model];
    let ls      = [];
    
    if (G.displayfuncs[model])
    {
      ls.push(G.displayfuncs[model](r, self));
    } else
    {
      ls.push(super.OnRenderItem(r));
    }
    
    let modellinks  = G.modellinks[model];
  
    ls.push('<div class="Flex FlexCenter FlexWrap ButtonBar">');
    
    for (let j = 0, m = modellinks.length; j < m; j++)
    {
      ls.push(`<a class="Button LinksButton Color4" onclick="G.ChooseLinks('${r[idfield]}', this.innerText); P.CancelBubble();">${modellinks[j]}</a>`);
    }      
    
    ls.push('</div>');
    
    return ls.join('\n');
  }

  // ------------------------------------------------------------------
  Edit(card, dbid)
  {
    let self  = this;
    
    let model       = G.currentmodel;
    
    super.Edit(card, dbid);
    
    if (G.customeditfuncs[model])
    {
      G.customeditfuncs[model](self.GetItemByID(dbid));
    }  
  }

  // ------------------------------------------------------------------
  OnDelete(dbids, resultsdiv)
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/system/dbapi',
      {
        command:  'delete',
        model:    G.currentmodel,
        ids:      dbids
      },
      function(d)
      {
        self.List();
      }
    );    
  }
  
  // ------------------------------------------------------------------
  OnSave(formdiv, data, resultsdiv, e, saveandcontinue)
  {
    let self  = this;
    
    let model   = G.currentmodel;   
    
    if (G.savedatafuncs[model])
    {
      G.savedatafuncs[model](formdiv, data);
    }
    
    P.LoadingAPI(
      resultsdiv,
      '/system/dbapi',
      {
        command:  'save',
        model:    model,
        data:     data
      },
      function(d, resultsdiv)
      {
        if (e.target.classList.contains('SaveAndContinueButton'))
        {
          P.HTML(resultsdiv, '<div class="Center Pad25 greenb">' + T.allgood + '</div>');
          return;
        }
        
        P.RemoveFullScreen();
        
        self.List();
      }
    );
  }
}


// ====================================================================
G.NewItem = function()
{
  G.EditItem({});
}

// ====================================================================
G.EditItem = function (obj)
{
  let fields      = P.ToggledButtons('.DBFields')['on'];      
  let model       = E('.ModelSelect').value;
  let modelinfo   = G.modelfields[model];
  let modelid     = G.modelids[model];
  
  let editfields  = [];
  
  fields.splice(0, 0, modelid);
  
  for (let i = 0, l = fields.length; i < l; i++)
  {
    let fieldname   = fields[i];
    let fieldtype   = modelinfo[fieldname][0].toLowerCase();
    
    switch (fieldtype)
    {
      case 'int':
      case 'int16':
      case 'int64':
        editfields.push([fieldname, 'int', '', fieldname]);
        break;
      case 'float':
        editfields.push([fieldname, 'float', '', fieldname]);
        break;
      case 'datetime':
        editfields.push([fieldname, 'datetime-local', '', fieldname]);
        break;
      case 'translation':
        editfields.push([fieldname, 'translation', '', fieldname]);
        break;
      case 'password':
        editfields.push([fieldname, 'password', '', fieldname]);
        break;
      case 'multitext':
        editfields.push([fieldname, 'multitext', '', fieldname]);
        break;
      case 'newlinelist':
        editfields.push([fieldname, 'newlinelist', '', fieldname]);
        break;
      case 'list':
        editfields.push([fieldname, 'list', '', fieldname]);
        break;
      case 'dict':
        editfields.push([fieldname, 'dict', '', fieldname]);
        break;
      default:
        editfields.push([fieldname, 'text', '', fieldname]);
    };
  }
    
  if (G.editfuncs[model])
  {
    G.editfuncs[model](obj, editfields);
  }
  
  P.EditPopup(
    editfields,
    function(formdiv, data, resultsdiv, e)
    {
      if (obj[modelid])
      {
        data[modelid] = obj[modelid]
      }
      
      if (G.savedatafuncs[model])
      {
        G.savedatafuncs[model](formdiv, data);
      }
      
      P.LoadingAPI(
        resultsdiv,
        '/system/dbapi',
        {
          command:  'save',
          model:    model,
          data:     data
        },
        function(d, resultsdiv)
        {
          if (e.target.classList.contains('SaveAndContinueButton'))
          {
            P.HTML('.DBObjFormResults', '<div class="Center Pad25 greenb">' + T.allgood + '</div>');
            return;
          }
          
          P.RemoveFullScreen();
          G.ListItems();
        }
      );
    },
    {
      enabletranslation:  1,
      fullscreen:         1,
      formdiv:            '.DBObjForm',
      obj:                obj,
      saveandcontinue:    1
    }
  );
  
  if (G.customeditfuncs[model])
  {
    G.customeditfuncs[model](obj, editfields);
  }  
}

// ====================================================================
G.DeleteItem = function(ids)
{
  if (!IsArray(ids))
  {
    ids = [ids]
  }
  
  let model  = E('.ModelSelect').value;
  
  P.DialogAPI(
    '/system/dbapi',
    {
      command:  'delete',
      model:    model,
      data:     ids
    },
    function(d, resultsdiv, popupclass)
    {
      P.ClosePopup(popupclass);
      
      G.ListItems();
    }
  );
}

// ====================================================================
G.GetNumItems = function ()
{
  let num   = parseInt(E('.NumItems').value);
  
  if (isNaN(num) || num < 10)
  {
    num = 10;
  }
  
  return num;
}

// ====================================================================
G.GetLinkType = function ()
{
  let num   = parseInt(E('.LinkType').value);
  
  if (isNaN(num))
    num = 0;
  
  return num;
}

// ====================================================================
G.ListItems = function (start)
{
  if (start == undefined)
  {
    start = G.currentstart;
  }
  
  G.currentstart  = start;
  
  let limit   = G.GetNumItems();
  let model   = E('.ModelSelect').value;
  let modelid = G.modelids[model];
      
  if (!model)
  {
    return;
  }
  
  let fields  = [G.modelids[model]];
  fields  = fields.concat(P.ToggledButtons('.DBFields')['on']);
  
  let condition = E('.Condition').value;
  
  if (G.listfuncs[model])
  {
    G.listfuncs[model](fields, condition, start, limit, G.modeldisplayfields[model].join(', '));
  } else
  {
    P.LoadingAPI(
      '.DBTiles',
      '/system/dbapi',
      {
        command:    'search',
        model:      model,
        fields:     fields,
        condition:  condition,
        start:      start,
        limit:      limit,
        orderby:    G.modeldisplayfields[model].join(', ')
      },
      function(d)
      {
        let data      = d.data;
        
        G.objs        = data;
        G.totalcount  = d.count;
        
        G.DisplayItems(data);
      }
    );
  }
}

// ====================================================================
G.DisplayItems = function()
{
  let data        = G.objs;
  let displaytype = E('.DisplayType').value;
  
  let model       = E('.ModelSelect').value;
  let modelinfo   = G.modelfields[model];
  let modelid     = G.modelids[model];
  let modellinks  = G.modellinks[model];
  
  let dbtiles     = E('.DBTiles');
  let fields      = [modelid];
  
  fields  = fields.concat(P.ToggledButtons('.DBFields')['on']);
          
  let ls    = [];
  
  if (displaytype == 'card')
  {
    dbtiles.classList.add('FlexGrid20');
    
    if (IsEmpty(data))
    {
      ls.push(`<p class="Center">Nothing found</p>`)
    } else
    {
      for (let i = 0, l = data.length; i < l; i++)
      {
        let item  = data[i];
        
        ls.push(`<div class="whiteb Pad50 Margin25 Rounded Raised DBItem HoverHighlight" data-dbid="${item[modelid]}">`);
        
        if (G.displayfuncs[model])
        {
          G.displayfuncs[model]('card', fields, item, ls, modellinks, data);
        } else
        {
          ls.push(`<div class="blueg Center">${EncodeEntities(item[modelid].toString())} (${ToShortCode(item[modelid])})</div>`);
          
          let colornum  = 0;
          let colors    = ['red', 'dorange', 'green', 'blue', 'purple', 'brown'];
        
          for (let j = 1, m = fields.length; j < m; j++)
          {
            let k = fields[j];
            
            let value     = item[k];
            let valuetype = modelinfo[k][0];
            
            if (valuetype.has('TRANSLATION'))
            {
              value = P.BestTranslation(value);
            } else if (valuetype == 'JSON')
            {
              if (IsEmpty(value))
              {
                value = '';
              } else
              {
                value = JSON.stringify(value, null, 2);
              }
            }
            
            ls.push(`<div class="${colors[colornum]}" data-fieldname="${k}">${EncodeEntities(value.toString())}</div>`);
            
            colornum++;
            
            if (colornum >= colors.length)
            {
              colornum  = 0;
            }
          }
        }        
        
      ls.push(`
  <br>
  <div class="ButtonBar">
`);
      
      for (let j = 0, m = modellinks.length; j < m; j++)
      {
        ls.push(`<a class="Button LinksButton Color4">${modellinks[j]}</a>`);
      }      
      
      ls.push(`
    <a class="Button EditButton Color3">Edit</a>
    <a class="Button DeleteButton Color1">Delete</a>
    <a class="Button SecurityButton Color2">Security</a>
  </div>
</div>`);
      }
    }
  } else
  {
    dbtiles.classList.remove('FlexGrid20');
    
    ls.push(`<table class="Styled">
      <tr>
        <th>ID</th>
`);
    
    for (let i = 0, l = fields.length; i < l; i++)
    {
      ls.push(`<th>${fields[i]}</th>`);
    }
    
    for (let j = 0, m = modellinks.length; j < m; j++)
    {
      ls.push(`<th>Linked</th>`);
    }
      
    ls.push(`<th></th>
  <th></th>
  <th></th>
</tr>`);
    
    if (IsEmpty(data))
    {
      ls.push(`<tr><td colspan="${fields.length + 3 + modellinks.length}">Nothing found</td></tr>`);
    } else
    {    
      for (let i = 0, l = data.length; i < l; i++)
      {
        let item  = data[i];
        
        
        ls.push(`<tr class="HoverHighlight DBItem" data-dbid="${item[modelid]}">`);
        
        ls.push('<td class="blueg Center">' + item[modelid] + '</td>');
          
        ls.push(`<td>${EncodeEntities(item[modelid].toString())} (${ToShortCode(item[modelid])})</td>`);
        
        if (G.displayfuncs[model])
        {
          G.displayfuncs[model]('table', fields, item, ls, modellinks, data);
        } else
        {
          for (let j = 1, m = fields.length; j < m; j++)
          {
            let k = fields[j];
            
            let value     = item[k];
            let valuetype = modelinfo[k][0];
            
            if (valuetype.has('TRANSLATION'))
            {
              value = P.BestTranslation(value);
            } else if (valuetype == 'JSON')
            {
              if (IsEmpty(value))
              {
                value = '';
              } else
              {
                value = JSON.stringify(value);
              }
            }
            
            ls.push(`<td>${EncodeEntities(value.toString())}</td>`);
          }
        }
      
        for (let j = 0, m = modellinks.length; j < m; j++)
        {
          ls.push(`<td class="Button LinksButton Color4">${modellinks[j]}</td>`);
        }
      
      ls.push(`<td class="Button EditButton Color3">Edit</td>
    <td class="Button DeleteButton Color1">Delete</td>
    <td class="Button SecurityButton Color2">Security</td>
  </tr>`);
      
        ls.push('</tr>');
      }
    }
    
    ls.push('</table>');
  }
  
  P.HTML('.DBTiles', ls);
  
  P.HTML('.DBPageBar', P.PageBar(G.totalcount, G.currentstart, G.GetNumItems(), G.objs.length, 'G.ListItems'));
}

// ====================================================================
G.SaveLinkedObjs = function(idtoadd)
{
  let linkedids   = [];
  let linkedobjs  = Q('.LinkedObj');
  
  for (let i = 0, l = linkedobjs.length; i < l; i++)
  {
    linkedids.push(linkedobjs[i].dataset.dbid);
  }
  
  if (idtoadd)
  {
    linkedids.push(idtoadd);
  }
  
  let model         = G.linkedmodel;
  
  P.LoadingAPI(
    '.LinkedTiles',
    '/system/dbapi',
    {
      command:  'linkarray',
      model1:   G.linkmodel,
      model2:   G.linkedmodel,
      id1:      G.linkid,
      id2s:     linkedids,
      type:     G.GetLinkType()
    },
    function()
    {
      G.ListLinks();
    }
  );
}

// ====================================================================
G.MoveLinkedObj = function(pos, diff, deleteobj)
{
  let linkedobjs  = Q('.LinkedObj');
  
  let ls  = [];
  
  for (let i = 0, l = linkedobjs.length; i < l; i++)
  {
    ls.push(linkedobjs[i]);
  }
  
  linkedobjs  = ls;
  
  let obj         = linkedobjs.splice(pos, 1)[0];
  
  if (!deleteobj)
  {
    let newpos      = pos + diff;
    
    if (newpos < 0)
      newpos  = 0;
    
    if (newpos > linkedobjs.length)
      newpos  = linkedobjs.length;
    
    linkedobjs.splice(newpos, 0, obj);   
  }
  
  ls  = [];
  
  for (let i = 0, l = linkedobjs.length; i < l; i++)
  {
    ls.push(linkedobjs[i].outerHTML);
  }    
  
  P.HTML('.LinkedTiles', ls);  
    
  G.SaveLinkedObjs();
} 
  
// ====================================================================
G.LinkedObjContextMenu = P.MakeContextMenu(
  function(el)
  {
    let dbid  = el.dataset.dbid;
    let pos   = 0;
    
    let linkedobjs  = Q('.LinkedObj');
    let numlinks    = linkedobjs.length;
    
    for (let i = 0, l = numlinks; i < l; i++)
    {
      if (linkedobjs[i].dataset.dbid == dbid)
      {
        pos = i;
        break;
      }
    }
    
    let ls  = [];
    
    if (pos > 0)
    {
      ls.push(['Move to front', function() { G.MoveLinkedObj(pos, -pos); }]);
      
      if (pos > 4)
      {
        ls.push(['Move up 5', function() { G.MoveLinkedObj(pos, -5); }]);
      }
      
      ls.push(['Move up', function() { G.MoveLinkedObj(pos, -1); }]);
    }
    
    if (pos < numlinks - 1)
    {
      ls.push(['Move down', function() { G.MoveLinkedObj(pos, 1); }]);
      
      if (numlinks > 5 && pos < numlinks - 5)
      {
        ls.push(['Move down 5', function() { G.MoveLinkedObj(pos, 5); }]);
      }
      
      ls.push(['Move to back', function() { G.MoveLinkedObj(pos, numlinks); }]);
    }
    
    ls.push(['Unlink', function() { G.MoveLinkedObj(pos, 0, 1);}]);
    
    return ls;
  },
  {
    topclass: '.LinkedObj'
  }
);

// ====================================================================
G.ChooseLinks = function(dbid, model2)
{
  G.linkid              = dbid;
  G.linkmodel           = E('.ModelSelect').value;
  G.linkedmodel         = model2;
  G.availablelinkcount  = 0;
  G.linkstart           = 0;
  
  let ls  = [`<div class="Grid LinksGrid" style="grid-template-columns: 1fr 13em;">
      <div class="AvailableLinks" style="border-right: 0.1em solid white;">
        <h2 class="Center">Not Linked</h2>
        <div class="ButtonBar">
          <select class="LinkType">
            <option>Link Type</option>`
  ];
  
  for (let i = 0; i <= 100; i++)
  {
    ls.push(`<option>${i}</option>`);
  }
  
  ls.push(`
    </select>
        
          <input type="text" class="MaxWidth600 LinkCondition">
          <a class="Color1" onclick="G.ListLinks()">Search</a>
        </div>
        
        <div class="LinkTiles Flex FlexCenter FlexWrap"></div>
        
        <div class="LinkedTilePageBar"></div>
      </div>
      
      <div class="AlreadyLinked">
        <h2 class="Center">Linked</h2>
        <div class="LinkedTiles"></div>
      </div>
    </div>
    
    <div class="ButtonBar">
      <a class="Color3" onclick="P.RemoveFullScreen()">Finished</a>
    </div>  
  `);
  
  P.AddFullScreen(
    'dpurpleg',
    '',
    ls
  );

  P.LoadingAPI(
    '.LinkTiles',
    '/system/dbapi',
    {
      command:  'getmodelinfo',
      model:    model2
    },
    function(d)
    {
      let data  = d.data;
      
      G.modelfields[model2]        = data;
      G.modelids[model2]           = d.modelid;
      G.modellinks[model2]         = d.links;
      G.modeldisplayfields[model2] = d.displayfields;
      
      if (d.managejs)
      {
        eval(d.managejs);
      }
      
      let fields  = [];
      
      G.ListLinks();
      
      P.DoneTyping(
        '.LinkCondition',
        function(el)
        {
          G.ListLinks();
        }
      );
  
    }
  );

  P.OnClick(
    '.LinkTiles', 
    P.LBUTTON, 
    function (e)
    {
      let obj = P.TargetClass(e, '.LinkObj');
      
      if (!obj)
        return;
    
      let dbid	= obj.dataset.dbid;

      if (!dbid)
        return;
      
      G.SaveLinkedObjs(dbid);
    }
  );
  
  P.OnClick(
    '.LinkedTiles', 
    P.LBUTTON, 
    G.LinkedObjContextMenu
  );
}

// ====================================================================
G.ListLinks = function(start)
{
  if (start == undefined)
  {
    start = G.linkstart;
  } 
  
  G.linkstart = start;
  
  P.LoadingAPI(
    '.LinkedTiles',
    '/system/dbapi',
    {
      command:  'linked',
      model1:   G.linkmodel,
      model2:   G.linkedmodel,
      id1:      G.linkid,
      type:     G.GetLinkType()
    },
    function(d)
    {
      let data  = d.data;
      
      G.linkedobjs  = data;
      
      let ls  = [];
      
      let modelid       = G.modelids[G.linkedmodel];
      let displayfields = G.modeldisplayfields[G.linkedmodel];
      
      if (G.displaylinkedfuncs[G.linkedmodel])
      {
        G.displaylinkedfuncs[G.linkedmodel](data);
      } else
      {
        G.DisplayLinked(data);
      }
          
      let filter    = E('.LinkCondition').value;
      let condition = '';
      
      if (filter)
      {
        let ls            = [`${modelid} = '${filter}'`];
        
        for (let i = 0, l = displayfields.length; i < l; i++)
        {
          let fieldname = displayfields[i];
          let modeltype = G.modelfields[G.linkedmodel][fieldname];
          
          // Use partial search for text fields
          if (modeltype[0].has('TEXT')
            || modeltype[0].has('TRANSLATION')
            || modeltype[0].has('JSON')
          )
          {
            ls.push(`${fieldname} LIKE '%${EscapeSQL(filter)}%' `)
          } else
          {
            ls.push(`${fieldname} = '${EscapeSQL(filter)}'`)
          }
        }
        
        if (!IsEmpty(ls))
        {
          condition = 'WHERE ' + ls.join(' OR ');
        }
      }
      
      P.LoadingAPI(
        '.LinkTiles',
        '/system/dbapi',
        {
          command:    'search',
          model:      G.linkedmodel,
          condition:  condition,
          start:      start,
          limit:      100,
          orderby:    G.modeldisplayfields[G.linkedmodel].join(', ')
        },
        function(d)
        {
          let data  = d.data;
          
          G.availablelinks      = data;
          G.availablelinkcount  = d.count;
          
          if (G.displaylinkfuncs[G.linkedmodel])
          {
            G.displaylinkfuncs[G.linkedmodel](data);
          } else
          {
            G.DisplayLinks(data);
          }
        }
      );
    }
  );
}

// ====================================================================
G.DisplayLinks = function(data)
{
  let ls  = [];
  
  let modelid       = G.modelids[G.linkedmodel];
  let alreadylinked = [];
  let displayfields = G.modeldisplayfields[G.linkedmodel];
  
  for (let i = 0, l = G.linkedobjs.length; i < l; i++)
  {
    alreadylinked.push(G.linkedobjs[i][modelid]);
  }
  
  for (let i = 0, l = data.length; i < l; i++)
  {
    if (alreadylinked.has(data[i][modelid]))
      continue;
    
    ls.push(`<div class="Color1 Pad25 Rounded Margin25 Width1200 LinkObj" data-dbid="${EncodeEntities(data[i][modelid].toString())}">`);
    
    for (let j = 0, m = displayfields.length; j < m; j++)
    {
      let value = data[i][displayfields[j]];
      
      if (G.modelfields[G.linkedmodel][displayfields[j]][0] == 'TRANSLATION')
      {
        value = P.BestTranslation(value);
      }
      
      ls.push(value + '<br>');
    }
    
    ls.push('</div>');
  }
  
  P.HTML('.LinkTiles', ls); 
  
  P.HTML('.LinkedTilePageBar', P.PageBar(G.availablelinkcount, G.linkstart, 100, G.availablelinks.length, 'G.ListLinks'));  
}

// ====================================================================
G.DisplayLinked = function(data)
{
  let ls  = [];

  let modelid       = G.modelids[G.linkedmodel];
  let displayfields = G.modeldisplayfields[G.linkedmodel];      
      
  for (let i = 0, l = data.length; i < l; i++)
  {
    ls.push(`<div class="Color3 Pad25 Rounded Margin25 Width1200 LinkedObj" data-dbid="${EncodeEntities(data[i][modelid].toString())}">`);
    
    for (let j = 0, m = displayfields.length; j < m; j++)
    {
      let value = data[i][displayfields[j]];
      
      if (G.modelfields[G.linkedmodel][displayfields[j]][0] == 'TRANSLATION')
      {
        value = P.BestTranslation(value);
      }
      
      ls.push(value + '<br>');
    }
    
    ls.push('</div>');
  }
  
  P.HTML('.LinkedTiles', ls); 
}

// ====================================================================
G.Import = function()
{
  P.AddFullScreen(
    'dgreeng',
    '',
    `<h3>Import</h3>
    <form>
      <div>
        <input type="file" class="importfile" data-filetype="application/x-7z-compressed">
        <input type="hidden" class="UploadFileTitle">
      </div>
      <div class="ImportOptions"></div>
    </form>
    <div class="UploadedFiles"></div>
    <div class="UploadedFileResults"></div>
    <br>
    <div class="ButtonBar">
      <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
    </div>
    `
  );
  
  P.MakeRadioButtons(
    '.ImportOptions',
    [
      ['Keep existing table', '_', 1, ''],
      ['Truncate table', 'truncatetable', 0, ''],
      ['Recreate table', 'recreatetable', 0, '']
    ],
    function(el, newvalue)
    {
      E('.UploadFileTitle').value = newvalue;
    }
  );
  
  P.On(
    '.importfile',
    'change',
    function() 
    {
      P.HTML(
        '.UploadedFileResults',
        `<div class="dorangeb Pad50">${T.working}</div>`
      );
      
      P.UploadFile(
        '.importfile', 
        {
          uploadurl:  '/system/dbimport',
          onfinished: function(e, xhr, filename)
            {
              let d = JSON.parse(xhr.response);
              
              if (d.error)
              {
                P.HTML(
                  '.UploadedFileResults',
                  `<div class="Error Pad50">${d.error}</div>`
                );
              } else
              {
                P.HTML(
                  '.UploadedFileResults',
                  `<div class="lgreenb Pad50">${d.log}</div>`
                );
              }
            }
        }
      );					
    }
  );
}

// ====================================================================
G.Export = function()
{
  let selected  = Q('.Selected');
  
  if (!IsEmpty(selected))
  {
    let ids = [];
    
    for (let r of selected)
    {
      ids.push(r.dataset.dbid);
    }
    
    window.open(
      '/system/dbexport?models=' + G.currentmodel + '&ids=' + ids.join(','),
      '_blank'
    );
    return;
  }
  
  P.AddFullScreen(
    'dblueg',
    '',
    `<h3>Export</h3>
    <div class="ExportModelSelect"></div>
    <div class="ButtonBar">
      <a class="ExportSelectAll Color4">${T.selectall}</a>
      <a class="ExportModels Color3">Export</a>
      <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
    </div>
    <div class="ExportResults"></div>
    `
  );
  
  let modelbuttons  = [];
  
  for (let r of G.models)
  {
    modelbuttons.push([r, r, 1, 'unset', 'Width2000']);
  }
  
  P.MakeToggleButtons('.ExportModelSelect', modelbuttons);
  
  P.On(
    '.ExportSelectAll',
    'click',
    function(e)
    {
      for (let r of Q('.ExportModelSelect .ToggleButton'))
      {
        r.classList.remove('dredg', 'lgrayg');
        r.classList.add('lgreeng');
        
        r.dataset.toggled = 'on';
      }
    }
  ); 
  
  P.On(
    '.ExportModels',
    'click',
    function(e)
    {
      let selectedmodels  = P.ToggledButtons('.ExportModelSelect').on;
      
      if (IsEmpty(selectedmodels))
      {
        P.ErrorPopup('Please pick which models you want to export!');
        return;
      }
      
      window.open(
        '/system/dbexport?models=' + selectedmodels.join(','),
        '_blank'
      );
    }
  );  
}

// ====================================================================
G.CleanLinks = function()
{
  P.AddFullScreen(
    'dgreeng',
    '',
    `<h1>${T.cleanlinks}</h1>
    <pre class="CleanLinksReport"></pre>
    <div class="ButtonBar">
      <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
    </div>
    `
  );
  
  P.LoadingAPI(
    '.CleanLinksReport',
    '/system/dbapi',
    {
      command:  'cleanlinks'
    },
    function(d, resultsdiv)
    {
      P.HTML(resultsdiv, d.report);
    },
    {
      timeout:  300
    }
  );  
}

// ====================================================================
G.RunQuery = function()
{
  P.AddFullScreen(
    'dpurpleg',
    '',
    `<h1>${T.runquery}</h1>
    <div class="FlexInput" data-name="codearea"></div>
    <div class="ButtonBar">
      <a class="Color3 RunQuery">${T.finished}</a>
      <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
    </div>
    <pre class="PreWrap QueryResults"></pre>
    `
  );
  
  P.FlexInput();
  
  E('.codeareaTextArea').focus();
  
  P.On(
    '.RunQuery',
    'click',
    function()
    {
      P.LoadingAPI(
        '.QueryResults',
        '/system/dbapi',
        {
          command:  'query',
          query:    E('.codeareaTextArea').value
        },
        function(d, resultsdiv)
        {
          let results = d.data;
          let ls      = [];
          
          if (IsArray(results))
          {
            let l = results.length;
            let i = 1;
            
            for (r of results)
            {
              ls.push(`----------------${i}/${l}----------------\n`)
              ls.push(JSON.stringify(r, null, 2));
              ls.push('\n');
              i++;
            }
          } else
          {
            ls.push(results);
          }
          
          P.HTML(resultsdiv, ls);
        },
        {
          timeout:  300
        }
      );  
    }
  );
}


// ====================================================================
G.RunCode = function()
{
  P.AddFullScreen(
    'blackg',
    '',
    `<h1>${T.runcode}</h1>
    <div class="FlexInput" data-name="codearea"></div>
    <div class="ButtonBar">
      <a class="Color3 RunCode">${T.finished}</a>
      <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
    </div>
    <pre class="PreWrap CodeResults"></pre>
    `
  );
  
  P.FlexInput();
  
  E('.codeareaTextArea').focus();
  
  P.On(
    '.RunCode',
    'click',
    function()
    {
      P.LoadingAPI(
        '.CodeResults',
        '/system/dbapi',
        {
          command:  'exec',
          code:     E('.codeareaTextArea').value
        },
        function(d, resultsdiv)
        {
          let results = d.data;
          let ls      = [];
          
          if (IsArray(results))
          {
            for (let i = 1, l = results.length; i < l; i++)
            {
              let r = results[i];
              
              ls.push(`----------------${i}/${l}----------------
${JSON.stringify(r, null, 2)}
`);
            }
          } else if (IsObject(results))
          {
            ls.push(JSON.stringify(results, null, 2));
          } else
          {
            ls.push(results.toString())
          }
          
          P.HTML(resultsdiv, ls);
        },
        {
          timeout:  300
        }
      );  
    }
  );
}

// ********************************************************************
G.OnPageLoad 	= function ()
{
  P.HTML(
    '.PageLoadContent',
    `
<div class="Pad50 white">
  <h1>${T.databasemanagement}</h1>
  
  <div class="ButtonBar">
    <select class="ModelSelect"></select>
    <a class="Color4" onclick="G.CleanLinks()">${T.cleanlinks}</a>
    <a class="Color5" onclick="G.Import()">${T.importtext}</a>
    <a class="Color6" onclick="G.Export()">${T.exporttext}</a>
    <a class="Color1" onclick="G.RunQuery()">${T.runquery}</a>
    <a class="Color2" onclick="G.RunCode()">${T.runcode}</a>
  </div>
  
  <div class="ModelActions">
    <div class="DBFields"></div>    
    <div class="DBList"></div>
  </div>
</div>
    `
  );
  
  P.SetLayout({});
  
  P.Hide('.ModelActions');
  
  G.modelfields         = {};
  G.modelids            = {};
  G.modellinks          = {};
  G.modeldisplayfields  = {};
  
  G.objs              = [];
  G.currentstart      = 0;
  
  G.linkid            = 0;
  G.linkedmodel       = 0;
  G.linkobjs          = [];
  G.linkstart         = [];
  G.availablelinks    = [];
  G.linkedobjs        = [];
  
  G.dblist  = new DBList({
    div:          '.DBList',
    reference:    'G.dblist'
  });
  
  P.On(
    '.ModelSelect',
    'change',
    function(e)
    {
      let condition = E('.Condition');
      
      if (condition)
      {
        condition.value = '';
      }
      
      let model  = E('.ModelSelect').value;
      
      if (!model)
      {
        return;
      }
      
      G.currentmodel  = model;
      
      P.Show('.ModelActions', 'block');
      
      P.DialogAPI(
        '/system/dbapi',
        {
          command:  'getmodelinfo',
          model:    model
        },
        function(d, resultsdiv, popupclass)
        {
          P.ClosePopup(popupclass);
          
          let data  = d.data;
          
          G.modelfields[model]        = data;
          G.modelids[model]           = d.modelid;
          G.modellinks[model]         = d.links;
          G.modeldisplayfields[model] = d.displayfields;
          G.currentstart              = 0;
          
          if (d.managejs)
          {
            eval(d.managejs);
          }
          
          let fields        = [];
          
          for (let k in data)
          {
            if (k == d.modelid)
            {
              continue;
            }
            
            fields.push([k, k, k, d.displayfields.has(k) ? 'on' : 'unset'])
          }
          
          P.MakeToggleButtons(
            '.DBFields', 
            fields,
            {
              ontoggle: function(element, buttonname, newstate)
                {
                  G.dblist.Display();
                }
            }
          );
          
          G.dblist.Display();
          G.dblist.displayfields  = G.dblist.GetEditFields();          
        }
      );
    }
  );
  
  P.On(
    '.DisplayType',
    'change',
    G.DisplayItems
  );
  
  P.DoneTyping(
    '.Condition',
    function(el)
    {
      G.ListItems();
    }
  );  
  
  P.DialogAPI(
    '/system/dbapi',
    {
      command:  'getmodels'
    },
    function(d, resultsdiv, popupclass)
    {
      P.ClosePopup(popupclass);
      
      G.models  = d.data;
      
      let ls  = ['<option></option>'];
      
      for (let r of G.models)
      {
        ls.push(`<option>${r}</option>`)
      }
      
      P.HTML('.ModelSelect', ls);
    }
  );
}

P.AddHandler('pageload', G.OnPageLoad);
