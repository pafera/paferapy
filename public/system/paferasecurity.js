"use strict";

G.DB_CAN_CREATE                   = 0x1
G.DB_CANNOT_CREATE                = 0x2
G.DB_CAN_CHANGE                   = 0x4
G.DB_CANNOT_CHANGE                = 0x8
G.DB_CAN_DELETE                   = 0x10
G.DB_CANNOT_DELETE                = 0x20
G.DB_CAN_VIEW                     = 0x40
G.DB_CANNOT_VIEW                  = 0x80
G.DB_CAN_LINK                     = 0x100
G.DB_CANNOT_LINK                  = 0x200
G.DB_CAN_CHANGE_PERMISSIONS       = 0x400
G.DB_CANNOT_CHANGE_PERMISSIONS    = 0x800
G.DB_CAN_VIEW_PROTECTED           = 0x1000
G.DB_CANNOT_VIEW_PROTECTED        = 0x2000

G.DB_CAN_ALL      = 0x555
G.DB_CANNOT_ALL   = 0xaaa

// ********************************************************************
class PaferaUserPicker extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.fields = [
      'idcode',
      'displayname'
    ];
    
    if (!P.IsAdmin())
    {
      this.extrabuttons = `
        <select class="UserPickerGroupType" name="UserPickerGroupType">
          <option value="friends">Friends</options>
          <option value="acquaintances">Acquaintances</options>
          <option value="favorites">Favorites</options>
          <option value="blacklist">Blacklist</options>
        </select>
      `;
    }
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, searchterm)
  {
    let self  = this;
    
    let grouptype = E('.UserPickerGroupType') || '';
    
    if (grouptype)
    {
      grouptype = grouptype.value;
    }
    
    P.LoadingAPI(
      '.AvailableCards',
      '/system/securityapi',
      {
        command:    'searchusers',
        keyword:    searchterm,
        start:      start,
        count:      100,
        grouptype:  grouptype
      },
      function(d)
      {
        self.available      = d.data;
        self.availablecount = d.count;
        
        self.DisplayAvailable();
      }
    );
  }
  
  // ------------------------------------------------------------------
  DisplayItem(ls, r)
  {
    let self  = this;
    
    ls.push(`
      <div class="Center">
        ${P.HeadShotImg(r.idcode, 'Square400')}<br>
        ${r.displayname}<br>
        ${r.phonenumber ? r.phonenumber : ''}<br>
      </div>
    `)
  }
}


// ********************************************************************
class PaferaGroupPicker extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.fields = [
      'idcode',
      'displayname'
    ];
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      '.AvailableCards',
      '/system/securityapi',
      {
        command:  'searchgroups',
        keyword:  searchterm,
        start:    start,
        count:    100
      },
      function(d)
      {
        self.available      = d.data;
        self.availablecount = d.count;
        
        self.DisplayAvailable();
      }
    );
  }
  
  // ------------------------------------------------------------------
  DisplayItem(ls, r)
  {
    let self  = this;
    
    ls.push(`
      <div class="Center">
        ${r.displayname}
        ${r.groupname}<br>
      </div>
    `)
  }
}

// ********************************************************************
class PaferaSecurity
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    this.objid      = config.objid;
    this.model      = config.model;
    this.reference  = config.reference;
  }
  
  // ------------------------------------------------------------------
  Display()
  {
    let self  = this;
    
    P.AddFullScreen(
      'blackg',
      '',
      `<div class="SecurityPopup Pad50">
        <h3>Permissions for ${self.model}(${self.objid})</h3>
        <div class="SecurityConfig">
          <table class="Width100P Margin25">
            <tr>
              <th class="Left">Owner</th>
              <td class="DBOwner Color4 Raised Rounded Pad25 Center" onclick="${self.reference}.ChooseOwner()"></td>
            </tr>
          </table>
          <h4>Everyone</h4>
          <div class="DBAccess"></div>
          <table class="Width100P Margin25">
            <tr>
              <th class="Left">Group</th>
              <td class="DBGroup Color3 Raised Rounded Pad25 Center" onclick="${self.reference}.ChooseGroup()"></td>
            </tr>
          </table>
          <div class="DBGroupAccess"></div>
          <h4 class="Pad25">ACLs</h4>
          <div class="DBAcls"></div>
        </div>
        <div class="ResultsBar"></div>
        <div class="ButtonBar">
          <a class="Color3" onclick="${self.reference}.OnFinished()">Finished</a>
          <a class="Color1" onclick="P.RemoveFullScreen()">Cancel</a>
        </div>
      </div>
      `      
    );
    
    P.LoadingAPI(
      '.SecurityPopup .ResultsBar',
      '/system/securityapi',
      {
        command:  'load',
        model:    self.model,
        objid:    self.objid
      },
      function(d, resultsdiv)
      {
        P.HTML(resultsdiv, '');
        
        self.obj  = d;
        
        let o = d;
        
        P.HTML(
          '.SecurityPopup .DBOwner', 
          `<div class="Center">
            ${P.HeadShotImg(d.dbowner, 'Square400')}<br>
            ${d.ownername}
          </div>
          `
        );
        
        self.MakePermissionToggles('.DBAccess', o, 'dbaccess');
        
        P.HTML(
          '.SecurityPopup .DBGroup', 
          `<div class="Center">
            ${d.groupdisplayname}<br>
            ${d.groupname}
          </div>
          `
        );
        
        self.MakePermissionToggles('.DBGroupAccess', o, 'dbgroupaccess');
        
        self.DisplayACLs();
      }
    );
  }
  
  // ------------------------------------------------------------------
  DisplayACLs()
  {
    let self  = this;
    let ls    = [];
    
    let dbacl   = self.obj.dbacl;
    let users   = dbacl.users ? dbacl.users : 0;
    let groups  = dbacl.groups ? dbacl.groups : 0;
    
    if (users)
    {
      for (let k in users)
      {
        let userobj = users[k];
        
        ls.push(`
          <table class="Width100P Margin25">
            <tr>
              <th class="Left">User</th>
              <td class="Color3 Pad25 Center Bold">
                ${P.HeadShotImg(k, 'Square400')}<br>
                ${userobj.displayname}
              </td>
              <td class="Color1 Pad25 Center Bold" onclick="${self.reference}.DeleteACL('users', '${k}')">${T.delete}</td>
            </tr>
          </table>
          <div class="UserAccess UserAccess${k}"></div>
        `);
      }
    }
    
    if (groups)
    {
      for (let k in groups)
      {
        let groupobj  = groups[k];
        
        ls.push(`
          <table class="Width100P Margin25">
            <tr>
              <th class="Left">Group</th>
              <td class="Color3 Pad25 Center Bold">${groupobj.displayname}</td>
              <td class="Color1 Pad25 Center Bold" onclick="${self.reference}.DeleteACL('groups', '${k}')">${T.delete}</td>
            </tr>
          </table>
          <div class="GroupAccess GroupAccess${k}"></div>
        `);
      }
    }
    
    ls.push(`
      <div class="ButtonBar">
        <a class="Color4" onclick="${self.reference}.AddACL()">Add</a>
      </div>
    `);
    
    P.HTML('.SecurityPopup .DBAcls', ls);
    
    if (users)
    {
      for (let k in users)
      {
        self.MakePermissionToggles('.UserAccess' + k, self.obj, 'users', k);
      }
    }
    
    if (groups)
    {
      for (let k in groups)
      {
        self.MakePermissionToggles('.GroupAccess' + k, self.obj, 'groups', k);
      }
    }
  }
  
  // ------------------------------------------------------------------
  AddACL()
  {
    let self  = this;
    
    P.Popup(
      `<div class="whiteb Pad50 Raised Rounded">
        <div class="ButtonBar">
          <a class="Color4" onclick="${self.reference}.AddUser(); P.CloseThisPopup(this);">Add User</a>
          <a class="Color3" onclick="${self.reference}.AddGroup(); P.CloseThisPopup(this);">Add Group</a>
        </div>
      </div>
      `
    );
  }
  
  // ------------------------------------------------------------------
  DeleteACL(acltype, key)
  {
    let self  = this;
    
    delete self.obj['dbacl'][acltype][key];
    
    self.DisplayACLs();
  }
  
  // ------------------------------------------------------------------
  AddUser()
  {
    let self  = this;
    
    G.userpicker  = new PaferaUserPicker({
      div:        '.PaferaUserPicker',
      reference:  'G.userpicker',
      onfinished: function(userid, userobj)
        {
          if (!self.obj.dbacl)
          {
            self.obj.dbacl  = {
              'users':  {},
              'groups': {}
            };
          }
          
          userobj.dbaccess  = 0;
          
          self.obj.dbacl.users[userid]  = userobj;
          
          self.DisplayACLs();
        }
    });
    
    G.userpicker.Display();
  }
  
  // ------------------------------------------------------------------
  AddGroup()
  {
    let self  = this;
    
    G.grouppicker  = new PaferaGroupPicker({
      div:        '.PaferaGroupPicker',
      reference:  'G.grouppicker',
      onfinished: function(groupid, groupobj)
        {
          if (!self.obj.dbacl)
          {
            self.obj.dbacl  = {
              'users':  {},
              'groups': {}
            };
          }
          
          groupobj.dbaccess = 0;
          
          self.obj.dbacl.groups[groupid]  = groupobj;
          
          self.DisplayACLs();
        }
    });
    
    G.grouppicker.Display();
  }
  
  // ------------------------------------------------------------------
  MakePermissionToggles(selector, obj, field1, field2)
  {
    let v = 0;
    
    if (field2)
    {
      v = obj['dbacl'][field1][field2];
    } else
    {
      v = obj[field1];
    }
    
    P.MakeToggleButtons(
      selector,
      [
        ['View', 'view', 0, (v & G.DB_CAN_VIEW) ? 'on' : (v & G.DB_CANNOT_VIEW ? 'off' : 'unset') ],
        ['Change', 'change', 0, (v & G.DB_CAN_CHANGE) ? 'on' : (v & G.DB_CANNOT_CHANGE ? 'off' : 'unset') ],
        ['Create', 'create', 0, (v & G.DB_CAN_CREATE) ? 'on' : (v & G.DB_CANNOT_CREATE ? 'off' : 'unset') ],
        ['Delete', 'delete', 0, (v & G.DB_CAN_DELETE) ? 'on' : (v & G.DB_CANNOT_DELETE ? 'off' : 'unset') ]
      ],
      {
        tritoggle:  1,
        ontoggle:   function(el, name, state)
          {
            let origvalue = field2 ? obj['dbacl'][field1][field2]['dbaccess'] : obj[field1];
            
            switch (name)
            {
              case 'view':
                switch (state)
                {
                  case 'on':
                    origvalue = (origvalue & (~G.DB_CANNOT_VIEW)) | G.DB_CAN_VIEW;
                    break;
                  case 'off':
                    origvalue = (origvalue & (~G.DB_CAN_VIEW)) | G.DB_CANNOT_VIEW;
                    break;
                  case 'unset':
                    origvalue = (origvalue & (~G.DB_CAN_VIEW)) & (~G.DB_CANNOT_VIEW);
                }
                break;
              case 'change':
                switch (state)
                {
                  case 'on':
                    origvalue = (origvalue & (~G.DB_CANNOT_CHANGE)) | G.DB_CAN_CHANGE;
                    break;
                  case 'off':
                    origvalue = (origvalue & (~G.DB_CAN_CHANGE)) | G.DB_CANNOT_CHANGE;
                    break;
                  case 'unset':
                    origvalue = (origvalue & (~G.DB_CAN_CHANGE)) & (~G.DB_CANNOT_CHANGE);
                }
                break;
              case 'create':
                switch (state)
                {
                  case 'on':
                    origvalue = (origvalue & (~G.DB_CANNOT_CREATE)) | G.DB_CAN_CREATE;
                    break;
                  case 'off':
                    origvalue = (origvalue & (~G.DB_CAN_CREATE)) | G.DB_CANNOT_CREATE;
                    break;
                  case 'unset':
                    origvalue = (origvalue & (~G.DB_CAN_CREATE)) & (~G.DB_CANNOT_CREATE);
                }
                break;
              case 'delete':
                switch (state)
                {
                  case 'on':
                    origvalue = (origvalue & (~G.DB_CANNOT_DELETE)) | G.DB_CAN_DELETE;
                    break;
                  case 'off':
                    origvalue = (origvalue & (~G.DB_CAN_DELETE)) | G.DB_CANNOT_DELETE;
                    break;
                  case 'unset':
                    origvalue = (origvalue & (~G.DB_CAN_DELETE)) & (~G.DB_CANNOT_DELETE);
                }
                break;
            }
            
            if (field2)
            {
              obj['dbacl'][field1][field2]['dbaccess'] = origvalue;
            } else
            {
              obj[field1] = origvalue;
            }
          }
      }
    );
  }
  
  // ------------------------------------------------------------------
  ChooseOwner()
  {
    let self  = this;
    
    G.userpicker  = new PaferaUserPicker({
      div:        '.PaferaUserPicker',
      reference:  'G.userpicker',
      onfinished: function(userid, userobj)
        {
          self.obj.dbowner    = userid;
          self.obj.ownername  = userobj.displayname;
          
          P.HTML(
            '.SecurityPopup .DBOwner', 
            `<div class="Center">
              ${P.HeadShotImg(userid, 'Square400')}<br>
              ${userobj.displayname}
            </div>
            `
          );
        }
    });
    
    G.userpicker.Display();
  }
  
  // ------------------------------------------------------------------
  ChooseGroup()
  {
    let self  = this;
    
    G.grouppicker  = new PaferaGroupPicker({
      div:        '.PaferaGroupPicker',
      reference:  'G.grouppicker',
      onfinished: function(groupid, groupobj)
        {
          self.obj.dbgroup          = groupid;
          self.obj.groupname        = groupobj.groupname;
          self.obj.groupdisplayname = groupobj.displayname;
          
          P.HTML(
            '.SecurityPopup .DBGroup', 
            `<div class="Center">
              ${groupobj.displayname}<br>
              ${groupobj.groupname}
            </div>
            `
          );
        }
    });
    
    G.grouppicker.Display();
  }
  
  // ------------------------------------------------------------------
  OnFinished()
  {
    let acl = {
      'users':  {},
      'groups': {}      
    }
    
    for (let k in self.obj.users)
    {
      acl['users'][k] = self.obj.users[k]['dbaccess'];
    }
    
    for (let k in self.obj.groups)
    {
      acl['groups'][k] = self.obj.groups[k]['dbaccess'];
    }
    
    P.LoadingAPI(
      '.SecurityPopup .ResultsBar',
      '/system/securityapi',
      {
        command:        'save',
        model:          self.model,
        objid:          self.objid,
        dbowner:        self.obj.dbowner,
        dbaccess:       self.obj.dbaccess,
        dbgroup:        self.obj.dbgroup,
        dbgroupaccess:  self.obj.dbgroupaccess,
        dbacl:          acl
      },
      function(d, resultsdiv)
      {
        P.RemoveFullScreen();
      }
    );
    
  }
}
