// ====================================================================
G.SaveInfo  = function ()
{
  let data  = {};
  
  let inputs  = Q('input, select, textarea');
  
  for (let i = 0, l = inputs.length; i < l; i++)
  {
    let item  = inputs[i];
    
    if (item.getAttribute('type') == 'checkbox')
    {
      data[item.getAttribute('name')] = item.checked ? 1 : 0;
    } else
    {
      data[item.getAttribute('name')] = item.value;
    }
  }
  
  if (data.dbtype)
    data.dbtype = data.dbtype.toLowerCase();
  
  if (data.dbtype == 'sqlite')
    data['dbname']  = data.dbpath    
    
  if (data.jsfiles)
    data.jsfiles  = data.jsfiles.split('\n').filter(Boolean)
  
  if (data.cssfiles)
    data.cssfiles  = data.cssfiles.split('\n').filter(Boolean)
    
  if (data.languages)
    data.languages  = data.languages.split('\n').filter(Boolean)
    
  P.DialogAPI(
    '/system/controlpanelapi',
    {
      command:  'setinfo',
      data:     data
    },
    function(d)
    {
    }
  );
}

// ********************************************************************
G.OnPageLoad 	= function ()
{
  P.SetLayout({});
  
  P.HTML(
    '.ConfigTiles',
    [      
      `<div class="whiteb Pad50 Rounded Raised SiteInformation">
        <h4>Site Information</h4>
        <form>
          <div>
            <label>
              Website Name<br>
              <input class="Width100P" type="text" name="sitename">
            </label>
          </div>
          <div>
            <label>
              <div class="Flex Bordered MarginBottom HoverHighlight">
                <div class="Pad25">
                  <input class="" type="checkbox" name="addsitenametotitle">
                </div>
                <div>
                  Add website name to each pages' title
                </div>
              </div>
            </label>
          </div>
          <div>
            <label>
              Website Description<br>
              <textarea class="Width100P"  name="sitedescription"></textarea>
            </label>
            <br>
            <br>
          </div>
          <div>
            <label>
              Administrator Name<br>
              <input class="Width100P"  type="text" name="siteadmin">
            </label>
          </div>
          <div>
            <label>
              Administrator Password<br>
              <input class="Width100P"  type="password" name="sitepassword">
            </label>
          </div>
          <div>
            <label>
              Administrator Email<br>
              <input class="Width100P"  type="email" name="siteemail">
            </label>
          </div>
        </form>
        <div class="ButtonBar">
          <a class="Color3" onclick="G.SaveInfo()">Save</a>
        </div>
      </div>`,
      
      `<div class="whiteb Pad50 Rounded Raised DatabaseInformation">
        <h4>Database</h4>
        <form>
          <div>
            <label>
              Type<br>
              <select class="DBTypeSelect" name="dbtype">
                <option>SQLite</option>
                <option>MySQL</option>
              </select>
            </label>
          </div>
          <div class="SQLiteField">
            <label>
              Database Path <br>
              <input class="Width100P" type="text" name="dbpath">
            </label>
          </div>
          <div class="MySQLField">
            <label>
              Host<br>
              <input class="Width100P" type="text" name="dbhost">
            </label>
          </div>
          <div class="MySQLField">
            <label>
              Name <br>
              <input class="Width100P" type="text" name="dbname">
            </label>
          </div>
          <div class="MySQLField">
            <label>
              Username<br>
              <input class="Width100P" type="text" name="dbuser">
            </label>
          </div>
          <div class="MySQLField">
            <label>
              Password<br>
              <input class="Width100P" type="password" name="dbpassword">
            </label>
          </div>
        </form>
        <div class="ButtonBar">
          <a class="Color3" onclick="G.SaveInfo()">Save</a>
        </div>
      </div>`,
      
      `<div class="whiteb Pad50 Rounded Raised SiteConfiguration">
        <h4>JavaScript and CSS Files</h4>
        
        <p>
          Type the URLs of JavaScript or CSS files to autoload below; one URL per line.
        </p>
        
        <p>
          Note that these files will be automatically loaded by <em>every single page on your site</em>, so be sure that you actually need them!
        </p>
        
        <form>
          <div>
            <label>
              JavaScript Files<br>
              <textarea class="Width100P" rows="6" name="jsfiles"></textarea>
            </label>
          </div>
          <div>
            <label>
              CSS Files<br>
              <textarea class="Width100P" rows="6" name="cssfiles"></textarea>
            </label>
          </div>
        </form>
        
        <div class="ButtonBar">
          <a class="Color3" onclick="G.SaveInfo()">Save</a>
        </div>
      </div>`,
      
      `<div class="whiteb Pad50 Rounded Raised SiteConfiguration">
        <h4>Site Configuration</h4>
        <form>
          <div>
            <label>
              Available Languages (one code per line)<br>
              <textarea class="Width100P" rows="6" name="languages"></textarea>
            </label>
          </div>
          <div>
            <label>
              Session Timeout (seconds)<br>
              <input class="Width100P" type="number" min="300" max="2678400" name="sessiontimeout">
            </label>
          </div>
          <div>
            <label>
              <div class="Flex Bordered MarginBottom HoverHighlight">
                <div class="Pad25">
                  <input class="" type="checkbox" name="urltracking">
                </div>
                <div>
                  Track visitor URLS (useful, but slows down requests considerably and prevents automatic caching)
                </div>
              </div>
            </label>
          </div>
        </form>
        <div class="ButtonBar">
          <a class="Color3" onclick="G.SaveInfo()">Save</a>
        </div>
      </div>`,
      
      `<div class="whiteb Pad50 Rounded Raised SiteConfiguration">
        <h4>Advanced Management</h4>
        
        <div class="Flex FlexVertical FlexCenter">
          <a href="/system/db" class="Color1 Width100P Margin25 Pad50 Rounded">Database Models</a>
          <a href="/system/translations" class="Color2 Width100P Margin25 Pad50 Rounded">Translations</a>
        </div>
      </div>`      
    ]
  );
  
  P.Hide('.MySQLField');
  
  P.On(
    '.DBTypeSelect',
    'change',
    function(e)
    {
      if (e.target.value == 'MySQL')
      {
        P.Hide('.SQLiteField');
        P.Show('.MySQLField');
      } else
      {
        P.Hide('.MySQLField');
        P.Show('.SQLiteField');
      }
    }
  );
  
  P.DialogAPI(
    '/system/controlpanelapi',
    {
      command:  'getinfo'
    },
    function(d)
    {
      let data  = d.data;
      
      for (var k in data)
      {
        let el  = E('input[name=' + k + ']');
        
        if (el)
        {
          if (el.getAttribute('type') == 'checkbox')
          {
            el.checked  = data[k] ? 1 : 0;
          } else
          {
            el.value = data[k];
          }
        }
      }
      
      if (data.dbtype == 'sqlite')
      {
        E('select[name=dbtype]').value = 'SQLite';
        E('input[name=dbpath]').value  = data.dbname;
        E('input[name=dbname]').value  = '';
      } else if (data.dbtype == 'mysql')
      {
        E('select[name=dbtype]').value = 'MySQL';
        P.Hide('.SQLiteField');
        P.Show('.MySQLField');
      }
      
      if (data.languages)
      {
        E('textarea[name=languages]').value  = data.languages.join('\n');
      }
      
      if (data.cssfiles)
      {
        E('textarea[name=cssfiles]').value  = data.cssfiles.join('\n');
      }
      
      if (data.jsfiles)
      {
        E('textarea[name=jsfiles]').value  = data.jsfiles.join('\n');
      }
    }
  );
}

P.AddHandler('pageload', G.OnPageLoad);
