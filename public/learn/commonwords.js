"use strict";

G.CVCWORDS  = `bad
bag
bam
ban
bat
bed
beg
Ben
bet
bid
big
bin
bit
bog
bop
box
bud
bug
bum
bun
bus
but
cab
can
cap
cat
cob
cod
cog
con
cop
cot
cub
cud
cup
cut
dab
dad
dam
Dan
den
did
dig
dim
din
dip
dog
Don
dot
dud
dug
fad
fan
fat
fed
fig
fin
fit
fix
fog
fox
fun
gab
gag
gap
gas
gem
get
gig
gin
gob
god
got
gum
gun
Gus
gut
had
hag
ham
has
hat
hem
hen
hid
him
hip
his
hit
hog
hop
hot
hub
hug
hum
hut
jab
jam
jet
jib
jig
Jim
jip
job
jog
jot
jug
jut
keg
Ken
kid
Kim
kin
kit
lab
lad
lag
lap
led
leg
let
lid
lip
lit
lob
log
lop
lot
lug
mad
man
map
mat
men
met
mix
mob
mom
mop
mud
mug
mum
nab
nag
nap
net
nip
nod
not
nub
nun
nut
pad
Pam
pan
pat
peg
pen
pep
pet
pig
pin
pit
pod
pop
pot
pox
pug
pun
pup
rag
ram
ran
rap
rat
red
rid
rig
rim
rip
rob
rod
rot
rub
rug
rum
run
rut
sad
sag
Sam
sap
sat
set
sin
sip
sit
six
sob
sod
son
sub
sum
sun
tab
tad
tag
tan
tap
tax
Ted
ten
tin
tip
Tom
ton
top
tot
tub
tug
van
vat
vet
wag
wax
web
wed
wet
win
wit
won
yak
yam
yap
yes
yet
zap
zig
zip
zit`.split('\n');

G.CVCEWORDS = `bake
cake
fake
Jake
lake
make
rake
sake
take
wake
cane
lane
mane
pane
sane
vane
cape
nape
tape
base
case
vase
date
fate
gate
hate
late
mate
rate
fame
game
name
same
tame
cave
Dave
gave
nave
pave
rave
save
wave
dime
lime
mime
time
pipe
ripe
wipe
bide
hide
ride
side
tide
wide
life
wife
dine
fine
line
mine
nine
pine
vine
wine
bike
hike
like
pike
rise
wise
bite
kite
mite
rite
site
lobe
robe
code
lode
mode
node
rode
coke
joke
poke
woke
yoke
hole
mole
pole
role
cope
hope
mope
nope
rope
dose
hose
nose
pose
rose
dote
note
rote
tote
vote
cube
huge
dome
home
bone
cone
lone
tone
zone
mule
yule
fume
dune
June
fuse
muse
cute
mute`.split('\n');

G.EWORDS  = `bee
see
tree
feed
need
flee
reef
leek
beet
deep
queen
feet
sea
team
meat
bean
leaf
tear
flea
treat
tea`.split('\n');

G.ALLWORDS			= `the d,D d
of of o
and and a
to too oo
in in i
a u,A u A
is is i
that dat d a
for for or
it it i
as as a
was wus u
with wif f
be bE E
by bI I
on on o
not not o
he hE E
I I I
this dis d i
are R R rerecord
or or or
his his i
from from o
at at a
which wich w
but but u
have hav noe
an an a
had had a
they dA A
you yOU O U
were wer er noe
their dAr d A
one wun u
all al al
we wE E
can kan a k
her her er
has has a
there dAr d A
been ben e
if if i
more mor or noe
when wen w e
will wil il
would wood oo
who hoo h oo
so sO O
no nO O
she shE sh E
other o-der o d er
its its i
may mA A
these dEs d E
what wut w u
them dem d e
than dan d a
some som s o noe
him him i
time tIm I
into in-too i oo
only O-nlE O E
do doo oo
such such u
my mI I
new nU U
about u-bout u ou
out out ou
also al-sO al O
two too oo
any an-E a E
up up u
first first er
could kood oo k
our our ou
then den d e
most mOst O
see sE E
me mE E
should shood oo
after af-ter a er
said sed e
your yor or
very ve-rE e E
between bE-twEn E
made mAd A
many man-E a E
over O-ver O er
like lIk I
those dOs d O
did did i
now nou ou
even E-ven E e
well wL L
where wAr w A
must must u
people pE-pO E O
through froo f oo
how hou ou
same sAm A
work work or
being bE-ing E i
because bE-kos E o noe k
man man a
life lIf I
before bE-for E or noe
each Ech E ch
much much u ch
under un-der u er
years yErs E
way wA A
great grAt A
state stAt A
both bOf O f
use Us U
upon up-on u o
own On O
used Us-ed U e
however hou-ever ou e er
us us u
part pRt R
good good oo
world world or
make mAk A
three drE d E
while wIL w I L
long long o
day dA A
without wif-out i f ou
just just u
men men e
general jen-er-O j er O
during der-ing er i
another u-no-der u o d er
little li-tO i O
found found ou
here hEr E noe
system sis-tem i e
does dos o
down doun ou
number num-ber u er
case kAs A k
against u-ganst u a
might mIt I
still stil i
back bak a k
right rIt I
know nO O
place plAs A s
every ev-rE e E
government go-vern-ment o er e
too too oo
high hI I
old Od O
different dif-fer-ent i er e
states stAts A
take tAk A
year yEr E
power pou-er ou er
since sins i s rerecord
given giv-en i e
god god o rerecord
never ne-ver e er
social sO-shO O sh O
order or-der or er
water wa-ter a er
thus dus u
form form or
within wif-in i f
small smal al
shall shal sh al
public pub-lik u i k
large lRj R j
law lo o
come kom o noe k
less les e
children chil-dren il e
war wR R
point poynt oy
called kal-ed al e k
American u-mAr-i-kan u a A i k
again u-gan u a
often of-ten o e
go gO O
few fU U
among among a o
important im-por-tant i or a
end end e
get get e
house hous ou noe
second se-kend e k
though fO f O
present pre-sent e
example ex-am-pO e a O
himself him-sLf i L
women wim-en i e
last last a
say sA A
left left e
fact fakt a k k
off of o
set set e
per per er
yet yet e
hand hand a
came kAm A k
development dE-vL-op-ment E L o e
thought fout f ou
always al-wAs al A
far fR R
country kon-trE o I k
York york or
school skool k oo
information in-for-mA-shin i or A sh i
group groop oo
following fal-O-ing al O i
think dink d i
others o-ders p d er
political pO-li-ti-kO O i k
human hU-man U a
history his-tor-E i or E
united U-nI-ted U I e
give i giv noe
family fam-il-E a il E
find fInd I
need nEd E
figure fig-er i er noe
possible po-si-bO o i O
although al-fO al f O
rather ra-der a d er
later lA-ter A er
university U-nE-ver-si-tE U E er i
once wuns u s
course kors or noe k
until un-til u il
several se-ver-O e er O
national na-shin-O A sh i O
whole hOl h O noe
chapter chap-ter a er
early er-lE er E
four for or
home hOm O
means mEns E
things dings d i
process pro-ses o e s
nature nA-cher A ch er
above u-bov u o noe
therefore dAr-for A or noe
having having a i
certain ser-ten er e s
control kon-trO o O
data dA-ta/da-ta A a
name nAm A
act akt a k
change chanj a j ch
value vL-U L U
body bod-E o E
study stud-E u E
table ta-bO A O
become bE-kom E o noe k
whether we-der w e d er
city sit-E i E s
book bork or
taken tA-ken A e
side sId I
times tIms I
away aw-A a A
known nOn n O
period pE-ri-ed E i e
best best e
line lIn I
am am a
court kort or k
John Jon o
days dAs A
put poot oo
young young u
seen sEn E
common kom-en o e k
person per-sen er e
either E-der E d
let let e
why wI w I
land land a
head hed e
business bis-nes i e
company kom-pan-E o a E k
church church er ch
words words or
effect e-fekt e k
society sO-sI-e-tE O I E s
around u-round u ou
better be-ter e er
nothing no-ding o d
took took oo
white wIt w I
itself it-sLf i L
something som-ding o d i
light lIt I
question kwes-shin e sh i
almost al-mOst al O
went went e
interest in-trest/in-ter-est i er
mind mind I
next next e
least lEst E
level le-vO e O
themselves dem-sLvs d L e
economic E-kO-nom-ik E O o i k
child chILd ch I L
death def e
service ser-vis er i noe s
view vU U
action ak-shin a sh i k
five fIv I
press pres e
father fa-der a d er
further fer-der d er
love lov o noe
research rE-serch E er ch
area A-rE-a A E a
true troo oo
education ed-U-kA-shin e U A sh i k
self sLf L
age aj A j
necessary ne-ses-Ar-E e R E s
subject sub-jekt u e k
want wunt u
cases kAs-es A k e
ever ever e er
going gO-ing O i rerecord
problem prob-lem o e
free frE E
done don o
making mAk-ing A i
party pR-tE R E
king king i
together too-ge-der oo e d
century sen-cher-E s e ch er E
section sek-shin e sh i k
using U-sing U i
position pO-si-shin O i sh i
type tIp I
result rE-sult E ul
help hLp L
individual in-dE-vid-U-O i E U O
already al-re-dE al e E
matter mat-ter a er
full ful ul
half haf a
various vA-rE-us A E u
sense sens e noe
look lork or
based bAs-ed A e
whose hoos h oo
English ing-lish i sh
south souf ou f
total tO-tO O
class klas a k
became bE-kAm E A k
perhaps per-haps er a
London Lon-den o e
policy po-li-sE o i E s
members mem-bers e er
local lO-kO O k
enough e-nof e o
particular pR-tE-kU-ler R E U er k
rate rAt A
air Ar A
along u-long u o
mother mo-der o d er
knowledge nou-ledj n ou e j
face fAs A s
word werd er
kind kInd I
open O-pen O e
terms terms er
able A-bO A O
money mon-E o E
experience ex-pE-rE-ens e E s
conditions kon-di-shins o i sh i k
support su-port u or
problems pro-blems o e
real rEL E
black blak a k
language lan-gwij a i j
results rE-sults E ul
room room oo
force fors or s noe 
held hLd L
low lO O
according ak-kor-ding a or i k
usually U-shO-E U sh O E
north norf or f
show shO sh O
night nIt I
told tOd O
short short sh or
field fELd E L
reason rE-sen E e
asked ask-ed a e
quite kwIt I
nor nor or
health hLf L f
special spe-shO e sh O s
thing ding d i
analysis u-nL-E-sis u L E i
eyes Is I rerecord
especially es-pe-shO-E e sh O
lord lord or rerecord
woman wor-men or e
major mA-jor A or
similar si-mi-ler i il er
care kAr A k
theory fEor-E f E or
brought brot ou
whom hoom h oo
office of-fis o i s noe 
art Rt R
production prO-duk-shin O u sh i k
sometimes som-tIms o I
third derd d er
shown shOn sh O
British Brit-ish i sh
due doo oo
international in-ter-na-shin-O i er a sh O
began bE-gan E a
single sing-O i O
natural na-cher-O a ch er O
got got o
account u-kount u ou k
cause kaus o noe k
community kom-mU-ni-tE o U i E k
saw so o
heart hRt R
soon soon oo
changes chanjs ch a j
studies stu-dEs u E
groups groops oo
method me-fed e f 
evidence ev-i-dens e i s
seems sEms E
greater gr-Ater A er
required rE-kwIr-ed E I e
trade trAd A
foreign for-en or e
west west e
clear klAr A k
model mo-dO o O
near nEr E
students stoodents oo e
probably prob-a-blE o a E
French french e ch
gave gAv A
including in-kloo-ding i oo k
read rEd/red E
England in-gland i a
material ma-tE-rE-O a E O
term term er
past past a
report rE-port E or
considered kon-si-der-ed o i er e k
future fU-cher U er
higher hI-er I er
structure struk-cher u ch er k
fig fig i
available u-vA-lA-bO u A O
working wor-king or i
felt fLt L
tell tL L
amount u-mount u ou
really rE-lE E
function funk-shin u i k
keep kEp E
indeed in-dEd i E
growth grOf O
market mRket R e
non non o
increase in-krEs i E k
personal per-sen-O er e O
cost kost o k
mean mEn E
surface ser-fes er e s
knew nU U
idea I-dE-a I E a
lower lO-wer O er
note nOt O
program prOgram O a
treatment trEtment E e
six six i
food food oo
close klOs O k
systems sistems i e
blood blod o
population pop-U-lA-shin o U A sh i
central sen-trO e O s
character kA-rak-ter A a er k
president pre-si-dent e i
energy en-er-gE e er E
property pro-per-tE o er E
living liv-ing i
provide prO-vId O I
specific spe-si-fic e i s
science sI-ens I e s
return rE-tern E er
practice prak-tis a i k
hands hands a
role rOl O noe
countries koun-trEs ou E k
management man-aj-ment a e
toward tord or
son son o
says ses e
generally jen-er-O-lE e er O E
influence in-floo-ens i oo e s
purpose per-pes er e noe
America u-mAr-i-ka u A i k
received rE-sE-ved E e s
strong strong o
call kal al k
services ser-vis-s er i s
rights rIts I
current ker-rent er e k
believe bE-lEv E
effects ef-fekts e k
letter let-ter e er
looked lork-ed or e
story stor-E or E
forms forms or
seemed sEm-ed E e
ground ground ou
main mAn A
paper pA-per A er
works works or
areas A-rE-as A E a
modern mod-ern o er
provided prO-vI-ded O I e
moment mOm-ent O e
written rit-ten i t r
situation sit-U-A-shin i U A sh i
turn tern er 
feet fEt E
plan plan a
parts pRts R
values vL-Us L U
movement moov-ment oo e noe
private prI-vet I e
late lAt A
size sIz I
union U-nen
described dE-skrIb-ed E I e k
east Est E
test test e
difficult dif-fi-kult i ul k
feel fEL E L
river ri-ver i er
poor por or
attention u-ten-shin u e sh i
books borks or
town toun ou
space spAs A s
price prIs I s
turned tern-ed er e
rule rool oo
percent per-sent er e s
activity ak-ti-vi-tE a i E k
across u-kros u o k
play plA A
building bil-ding il i
anything an-E-ding
physical fi-si-kO i O k
capital kap-i-tO a i O k
hard hRd R
makes mAks A
sea sE E
cent sent e s
approach u-prOch u O
pressure pres-cher e ch er
finally fI-nO-lE I O E
military mil-i-tAr-E il i A E
middle mid-dO i O
sir ser er
longer long-er o er
spirit spi-rit i
continued kon-tin-U-ed o i U e k
basis bA-sis A i
army RmE R E
red red e
alone u-lOn u O
simple sim-pO i O
below bE-lO E O
series sE-rEs E
source sors noe s
increased in-krEs-ed i E e k
sent sent e
particularly pR-ti-kU-lar-lE R i U ar E k
earth erf er f
months monfs o 
department dE-pRt-ment E R e
heard herd er
questions kwes-shins e sh i
likely lIk-lE I E
lost lost o
complete kom-plEt o E k
behind bE-hind E i
taking tA-king A i
wife wIf I
lines lIns I
object ob-jekt o e k
hours ours ou
twenty twen-tE e E
established es-tab-lish-ed e a i sh e
committee kom-mit-tE o i E k
related rE-lA-ted E A e
range rAnj A j
truth troof oo f
income in-kom i o noe k
instead in-sted i e
beyond bE-ond o
rest rest e rerecord
developed dE-vL-op-ed E L o e
outside out-sId ou I
organization or-gan-i-zA-shin or a i A sh i
religious rE-li-jis E i j
board bord or
live liv/lIv i I
design dE-sIgn E I
needs nEds E
persons per-sens er e
except ex-sept e s
authority u-for-i-tE u or i E
patient pA-shint A sh i
respect rE-spekt E e k
latter lat-ter a er
sure shor or noe
culture kul-cher ul ch er k
relationship rE-lA-shin-ship E A sh i
ten ten e
methods me-feds e f
followed fal-lO-ed al O e
William wil-i-am il i a
condition kon-di-shin o i sh k
points poynts oy
addition u-di-shin u i sh
direct dI-rekt I e k
seem sEm E
industry in-dus-trE i u E
college kal-lej k al e
friends frends e
beginning bE-gin-ning E i
hundred hun-dred u e
manner man-ner a er
front front o
original or-i-gin-O or i O
patients pA-shints A sh i
appear u-pEr u E
include in-klood i oo noe k
shows shOs sh O
ways wAs A
activities ak-tiv-i-tEs a k i E
relations rE-lA-shins E A sh i
writing rIt-ing r I i
council koun-sil k ou s il
disease dE-sEs E
standard stan-derd a er
fire fIr I
German jer-man j er a
France frans a s
degree dE-grE E
towards tords or
produced prO-doos-ed O oo e s
leave lEv E
understand un-der-stand u er a
cells sLs s L
average a-ver-aj a er j
march mRch R ch
carried kAr-rEd k A E
length lengf e
difference dif-fer-ens i er e s
simply simp-lE i E
normal nor-mO or O
quality kwal-i-tE al i E
street strEt E
run run u
answer an-ser a er
morning morn-ing or i
loss los o
doing doo-ing
stage stAj A j
pay pA A
today too-dA oo A
decision dE-si-shin E s i sh
labor lA-bor A or
page pAj A j
published pu-blish-ed u i sh e
workers wor-kers or er
bank bank a
top top o
wanted wun-ted u e
journal jer-nO er O
passed pas-ed a e
door dor or
importance im-por-tans i or a
western wes-tern e er
tax tax a
involved in-valv-ed i al e
solution sO-loo-shin O oo sh i
hope hOp O
voice voys oy s
reading rE-ding E i
obtained ob-tAn-ed o A e
bring bring i
peace pEs E s
needed nEd-ed E e
placed plAs-ed A s
chief chEf ch E
species spE-sEs E
added ad-ed a e
Europe U-rop U o noe
looking lork-ing
factors fak-tors a k or
laws los o
behavior bE-hAv-yer E A er
die dI I
response rE-spon-s E o
led led e
association u-sO-si-A-shin u O i A sh i
performance per-for-mans er or a s
road rOd O
issue i-shU i sh U
consider kon-sid-er k o i er
equal E-kwO E O
learning lern-ing er i
India in-dE-a i E a
training trAn-ing A i
forces fors-es or s e
cut kut k u
earlier er-lE-er er E er
basic bA-sik A i k
member mem-ber e er
friend frend e
lead lEd E
appears u-pErs u E
types tIps I
schools skools k oo
music mU-sik U i k
James jAms A
temperature tem-per-u-cher e er u ch er
Christian kris-chen k i ch e
volume val-Um al U
kept kept e
review rE-vU E U
significant sig-ni-fi-kant i k a
former form-er or er
meaning mEn-ing E i
associated u-sO-sE-At-ed u O E A e
wrote rOt O
center sent-er e er
direction dI-rek-shin I e k sh i
everything ev-rE-ding e E d i
job job o
understanding un-der-stand-ing u er a i
region rE-jen E j e
big big i
hold hOd O
opinion O-pi-nE-en O i E e
text text e
June jUn U
date dAt A
application ap-pli-kA-shin a i k A sh
author o-for o or
cell sL s L
distribution dis-tri-bU-shin i U sh i
fall fal al
becomes bE-koms E k o
Washington wosh-ing-ten o sh i e
limited lim-i-ted i e
Indian in-dE-an i E a
presence pre-sens e s
sound sound ou
meeting mEt-ing E i
clearly klEr-lE k E
expected ex-pekt-ed e k
federal fe-de-rO e O
nearly nEr-lE E 
ed ed e
extent ex-tent e
parents pAr-ents A e
yes yes e
ideas I-dE-as I E a
medical me-di-kO e i O
observed ob-serv-ed o er e
actually ak-shU-lE a k sh U E
final fI-nO I O
George jorg j or noe
miles mILs I L
elements L-e-ments L e
list list i
project pro-jekt o e k
product pro-dukt o u k
throughout froo-out f oo ou
Christ krIst k I
discussion dis-kus-shin i k u sh i
applied u-plI-ed u I e
rules rools oo
produce prO-doos O oo s
religion rE-li-jin E i j
deep dEp
operation op-er-A-shin o er A sh i
issues i-shUs i sh U
else Ls L
mass mas a
coming kom-ing k o i
determined dE-ter-min-ed E er i e
European U-rO-pE-an U O E a
events E-vents E e
security se-kUr-it-E e U i E
oil oyL oy L
fine fIn I
distance dis-tens i e
move moov oo
effective E-fek-tiv E e k i
paid pAd A
nation nA-shin A sh i
step step
gives givs i
success suk-ses u e
literature li-ter-u-cher i er u ch er
products pro-dukts o u k
neither nE-der E d er
resources rE-sor-ses E or e
follow fal-lO al O
cross kros k o rerecord
ask ask a
levels le-vOs e O
immediately im-mE-di-at-lE i E a
certainly ser-ten-lE s er e E
principle prin-si-pO i s O
moral mor-O or O
July jU-lI U I
formed form-ed or e
reached rEch-ed E ch e
faith fAf A f
deal dEL E L
recent rE-sent E s e
legal lE-gO E O
administration ad-min-i-strA-shin a i A sh
comes koms k o
materials ma-tE-ri-Os a E i O
potential pO-ten-chO O e ch
presented prE-sent-ed E e rerecord
congress kon-gres k o e
civil si-vO s i O
directly dI-rekt-lE I e k E
meet mEt E
met met e
principles prin-si-pOs i s O
expression ex-pres-shin e sh i
myself mI-sLf I L
born born or
wide wId I
post pOst O
justice jus-tis u i noe
strength strengf e
relation rE-lA-shin E A sh i
statement stAt-ment A e
financial fI-nan-chO I a ch O
dead ded e
larger lR-jer R j er
cultural kal-cher-O k al ch er O
historical his-tor-i-kO i or k O
existence ex-is-tens e i s
built bilt il
weight wAt A
reference re-fer-ens e er s
feeling fEL-ing E L
round round ou
reported rE-port-ed E or e
risk risk i
appeared u-pEr-ed u E e
included in-klood-ed i k oo e
scale skAL k A L
individuals in-di-vid-U-Os  i U O
supply sup-plI u I
million mil-li-en il i e
differences dif-fer-ens-es i er e s
write rIt r I
doubt doubt ou
hour our ou
industrial in-dust-rE-O i u E O
April A-prO A O
primary prI-mAr-E I A E
base bAs A
letters let-ters e er
interests in-ter-ests/in-trests i er e
numbers num-bers u er
seven se-ven e
places plAs-s A s
wall wal al
complex kom-plex k o e
entire en-tIr e I
try trI I
article R-ti-kO R i k O
county coun-tE ou E
costs kost-s k o
thinking dink-ing d i
record re-kord/rE-kord E k or
flow flO O
follows fal-lOs al O
sun sun u
attempt u-tempt u e
bad bad a
instance in-stans i a s
commission kom-mis-shin k o i sh
easily E-sil-E E il
unit U-nit U i
died dId I
green grEn E
Charles chRl-es ch R e
week wEk E
demand dE-mand E a
acid a-sid a i s
environment en-vI-ron-ment e I o
proper pro-per o er
takes tAks A
positive po-si-tiv noe
talk tok o
active ak-tiv a k i noe
image im-aj i a j
plant plant a
freedom frE-dom E o
library lI-brAr-E I A E
dark dRk R
key kE E
mentioned men-shin-ed e ch i
speak spEk E
allowed u-lou-ed u ou e
whatever wut-ev-er u e er
concerned kon-sern-ed k o er e
measure me-sher e sh er
lives lIvs/livs i I 
merely mEr-lE E
fear fEr E
Paris pAr-is A i
independent in-dE-pen-dent i E e
heat hEt E
circumstances ser-kum-stan-ses s er k u a e
Thomas tom-us o u
giving giv-ing i
master mas-ter a er
tried trId I
China chI-nu I u
eye I I
upper up-per u er
gone gon o noe
actual ak-chU-O a k ch U O
cold cOd O
frequently frE-kwent-lE E e 
eight At A
ability u-bil-i-tE u il i E
unless un-les u e
regard rE-gRd E R
Henry hen-rE e E
status stat-es a e
hear hEr E
thousand dou-send d ou e
minutes min-its i
parties pR-tEs R E
stock stok o k
student stoo-dent oo e
rise rIs I
arms Rms R
factor fak-tor a k or
charge chRj ch R j
easy E-sE E
created krE-A-ted k E A e
stand stand a
started stRt-ed R e
content kon-tent k o e
share shAr A 
picture pik-cher i k ch er
herself her-sLf er L
agreement u-grE-ment u E e
remember rE-mem-ber E e er
constant kon-stant k o a
none non o noe
popular pop-U-ler o U er
appropriate u-prO-prE-et u O E e
style stIL I L
start stRt R
goods goods oo
considerable kon-si-der-u-bO k o i er u O
ready re-dE e E
occur u-ker u k er
notes nOts O`.split('\n');
	
G.MODES = [
  'All Words',
  'CVC Words',
  'CVCE Words',
  'E Words'
];

G.currentmode = 'All Words';

// ====================================================================
G.ChangeMode 	= function()
{
  let modebutton  = E('.ChangeModeButton');
  
  let modepos  = G.MODES.indexOf(G.currentmode);
  
  modepos++;
  
  if (modepos >= G.MODES.length)
  {
    modepos  = 0;
  }
  
  G.currentmode = G.MODES[modepos];
  
  modebutton.innerText  = G.currentmode;
  
  switch (G.currentmode)
  {
    case 'All Words':
      G.WORDS = G.ALLWORDS;
      break;
    case 'CVC Words':
      G.WORDS = G.CVCWORDS;
      break;
    case 'CVCE Words':
      G.WORDS = G.CVCEWORDS;
      break;
    case 'E Words':
      G.WORDS = G.EWORDS;
      break;
  };
  
  G.slidepos  = -1;
  
  G.ChangeSlide(1);
}

// ====================================================================
G.ChangeSlide 	= function(diff)
{
	G.slidepos 	= Bound(G.slidepos + diff, 0, G.WORDS.length - 1);	
	
	G.ShowSlide();
}

// ====================================================================
G.CheckAnswer = function()
{
  let w       = G.WORDS[G.slidepos];
  
  if (G.currentmode == 'All Words')
  {
    let ls              = [w[1]];
    let rightsounds     = w[2];
    let selectedsounds  = P.ToggledButtons('.ToggleArea')['on'];
    
    for (let sound of rightsounds)
    {
      if (selectedsounds.indexOf(sound) == -1)
      {
        ls.push(`<span class="lred">🙁 ${sound}</span>`);
      } else
      {
        ls.push(`<span class="lgreen">😁 ${sound}</span>`);
      }
    }
    
    for (let sound of selectedsounds)
    {
      if (rightsounds.indexOf(sound) == -1)
      {
        ls.push(`<span class="lpurple">🤔 ${sound}</span>`);
      } 
    }
    
    P.HTML('.AnswerArea', ls.join('<br>'));
  } else
  {
    
  }
}

// ====================================================================
G.ShowSlide 	= function()
{
  E('.SlidePosDisplay').innerText = (G.slidepos + 1) + '/' + G.WORDS.length;
  
  if (G.currentmode == 'All Words')
  {
    E('.ProblemArea').innerText 	= G.WORDS[G.slidepos][0];
    
    P.MakeToggleButtons(
      '.ToggleArea',
      [
        ['a', 'a'],
        ['e', 'e'],
        ['i', 'i'],
        ['o', 'o'],
        ['u', 'u'],
        ['A', 'A'],
        ['E', 'E'],
        ['I', 'I'],
        ['O', 'O'],
        ['U', 'U'],
        ['al', 'al'],
        ['ch', 'ch'],
        ['c → k', 'k'],
        ['c → s', 's'],
        ['-e', 'noe'],
        ['er', 'er'],
        ['g -> j', 'j'],
        ['il', 'il'],
        ['L', 'L'],
        ['n', 'n'],
        ['oo', 'oo'],
        ['or', 'or'],
        ['ou', 'ou'],
        ['oy', 'oy'],
        ['R', 'R'],
        ['sh', 'sh'],
        ['th → d', 'd'],
        ['th → f', 'f'],
        ['ul', 'ul'],
        ['wh → w', 'w'],
        ['wh → h', 'h'],
        ['wr → r', 'r']
      ],
      {
        noicon:     1,
        classes:    'Width600'
      }
    );
    
    P.HTML('.ButtonArea', `<a class="Color3" onclick="G.CheckAnswer()">Check</a>`);
  } else
  {
    E('.ProblemArea').innerText 	= G.WORDS[G.slidepos];
    E('.ToggleArea').innerText    = '';
    E('.ButtonArea').innerHTML    = '';
  }
  
  E('.AnswerArea').innerText    = '';
}

// ====================================================================
G.PronounceWord = function()
{
  let currentword = 0;
  
  if (G.currentmode == 'All Words')
  {
    currentword	= G.WORDS[G.slidepos][0];
  } else
  {
    currentword	= G.WORDS[G.slidepos];
  }
  
  G.CheckPronounciation(
    currentword,
    function(e)
    {      
    },
    {
      parent:   '.ProblemArea'
    }
  );
}

// ====================================================================
G.Shuffle = function()
{
  G.WORDS     = Shuffle(G.WORDS);
  G.slidepos  = 0;
  G.ShowSlide();
}

// ====================================================================
G.GotoSlidePopup = function()
{
  P.InputPopup(
    'Which slide would you like to go to?',
    function(num)
    {
      G.slidepos  = parseInt(num) - 1;
      G.ShowSlide();
    },
    `<input class="InputPopupInput" type="number" value="1" min="1" max="${G.WORDS.length}">`,    
  );
}

// ********************************************************************
G.OnPageLoad 	= function()
{
  G.WORDS  = G.ALLWORDS;
  
  for (let i = 0, l = G.WORDS.length; i < l; i++)
  {
    let r = G.WORDS[i];
    
    r = r.split(' ');
    
    G.WORDS[i]  = [
      r[0],
      r[1],
      r.slice(2)
    ];
  }
  
	G.slidepos				= -1;
	
  P.HTML(
    '.PageLoadContent', 
    `  
    <div class="Grid GridCenter MinHeight80VH">
      <div class="Center Margin25">
        <div class="ProblemArea white Size600 Pad50"></div>
        <div class="ToggleArea white Pad50"></div>
        <div class="ButtonArea ButtonBar"></div>
        <div class="AnswerArea white Size400 Pad25"></div>
      </div>
    </div>`
  );
  
  setTimeout(
    function()
    {
      P.SetLayout({bottom: 'auto'});
    },
    200
  );
  
  P.HTML(
    '.BottomBarGrid', 
		`<div class="blackb" style="position: relative; top: 0.2em;">
      <div class="ButtonBar">
        <a class="SlidePosDisplay Color3" onclick="G.GotoSlidePopup()"></a>
        <a class="Color1 MinWidth400" onclick="G.ChangeSlide(-1)">&lt;</a>
        <a class="ChangeModeButton Color5 MinWidth400" onclick="G.ChangeMode()">All Words</a>
        <a class="ShuffleButton Color6 MinWidth400" onclick="G.Shuffle()">Shuffle</a>
        <a class="CheckPronounciationButton Color2 MinWidth400" onclick="G.PronounceWord()">${T.speak}</a>
        <a class="Color4 MinWidth400" onclick="G.ChangeSlide(1)">&gt;</a>
      </div>
    </div>`
	);

  G.ChangeSlide(1);
}

P.AddHandler('pageload', G.OnPageLoad);
