"use strict";

G.SLIDES= [
  {
    title:      'word + word',
    content:    [
      'p<span class="Vowel">i</span>ne<span class="Separator">|</span><span class="Vowel">a</span>p<span class="Separator">|</span>p<span class="Vowel">le</span>',
      'w<span class="Vowel">a</span><span class="Separator">|</span>t<span class="Vowel">er</span><span class="Separator">|</span>m<span class="Vowel">el</span><span class="Separator">|</span><span class="Vowel">o</span>n',
      'b<span class="Vowel">oo</span>k<span class="Separator">|</span>b<span class="Vowel">a</span>g',
      'h<span class="Vowel">o</span>t<span class="Separator">|</span>d<span class="Vowel">o</span>g',
      'b<span class="Vowel">a</span>s<span class="Separator">|</span>k<span class="Vowel">e</span>t<span class="Separator">|</span>b<span class="Vowel">all</span>',
      'bl<span class="Vowel">a</span>ck<span class="Separator">|</span>b<span class="Vowel">oar</span>d'
    ]
  },
  {
    title:      'prefix + word',
    content:    [
      '<span class="Vowel">i</span>n<span class="Separator">|</span>s<span class="Vowel">i</span>de',
      '<span class="Vowel">ou</span>t<span class="Separator">|</span>s<span class="Vowel">i</span>de',
      'b<span class="Vowel">e</span><span class="Separator">|</span>s<span class="Vowel">i</span>de',
      '<span class="Vowel">u</span>n<span class="Separator">|</span>d<span class="Vowel">er</span><span class="Separator">|</span>w<span class="Vowel">ear</span>',
      '<span class="Vowel">ou</span>t<span class="Separator">|</span><span class="Vowel">er</span><span class="Separator">|</span>w<span class="Vowel">ear</span>',
      'tr<span class="Vowel">i</span><span class="Separator">|</span><span class="Vowel">a</span>n<span class="Separator">|</span>g<span class="Vowel">le</span>'
    ]
  },
  {
    title:      'word + postfix',
    content:    [
      't<span class="Vowel">ea</span>ch<span class="Separator">|</span><span class="Vowel">er</span>',
      'w<span class="Vowel">al</span>k<span class="Separator">|</span><span class="Vowel">er</span>',
      'qu<span class="Vowel">i</span>ck<span class="Separator">|</span>l<span class="Vowel">y</span>',
      '<span class="Vowel">ea</span>t<span class="Separator">|</span><span class="Vowel">i</span>ng',
      'h<span class="Vowel">igh</span><span class="Separator">|</span><span class="Vowel">er',
      'b<span class="Vowel">o</span>x<span class="Separator">|</span><span class="Vowel">es</span>'
    ]
  },
  {
    title:      'Double Consonants',
    content:    [
      'r<span class="Vowel">u</span>n<span class="Separator">|</span>n<span class="Vowel">i</span>ng',
      'f<span class="Vowel">a</span>t<span class="Separator">|</span>t<span class="Vowel">er</span>',
      'str<span class="Vowel">aw</span><span class="Separator">|</span>b<span class="Vowel">e</span>r<span class="Separator">|</span>r<span class="Vowel">y</span>',
      '<span class="Vowel">a</span>p<span class="Separator">|</span>p<span class="Vowel">le</span>',
      'h<span class="Vowel">a</span>p<span class="Separator">|</span>p<span class="Vowel">y</span>',
      'c<span class="Vowel">o</span>m<span class="Separator">|</span>m<span class="Vowel">u</span><span class="Separator">|</span>n</span><span class="Vowel">i</span><span class="Separator">|</span>t<span class="Vowel">y</span>'
    ]
  },
  {
    title:      'Single Separation',
    content:    [
      'f<span class="Vowel">a</span><span class="Separator">|</span>v<span class="Vowel">or</span><span class="Separator">|</span><span class="Vowel">i</span>te',
      'b<span class="Vowel">o</span><span class="Separator">|</span>d<span class="Vowel">y</span>',
      '<span class="Vowel">I</span><span class="Separator">|</span>v<span class="Vowel">y</span>',
      'b<span class="Vowel">e</span><span class="Separator">|</span>c<span class="Vowel">au</span>se',
      'br<span class="Vowel">o</span><span class="Separator">|</span>th<span class="Vowel">er</span>',
      'st<span class="Vowel">u</span><span class="Separator">|</span>d<span class="Vowel">y</span>'
    ]
  },
  {
    title:      'Double Separation',
    content:    [
      'n<span class="Vowel">u</span>m<span class="Separator">|</span>b<span class="Vowel">er</span>',
      '<span class="Vowel">u</span>n<span class="Separator">|</span>d<span class="Vowel">er</span>',
      '<span class="Vowel">a</span>f<span class="Separator">|</span>t<span class="Vowel">er</span>',
      'm<span class="Vowel">ar</span><span class="Separator">|</span>k<span class="Vowel">e</span>t',
      '<span class="Vowel">e</span>f<span class="Separator">|</span>f<span class="Vowel">or</span>t',
      'c<span class="Vowel">ir</span><span class="Separator">|</span>c<span class="Vowel">le</span>',
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">yesterday</div>',
    content:    [
    ]
  },
  {
    title:      'yesterday (answer)',
    content:    [
      'y<span class="Vowel">e</span>s<span class="Separator">|</span>t<span class="Vowel">er</span><span class="Separator">|</span>d<span class="Vowel">ay</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">tomorrow</div>',
    content:    [
    ]
  },
  {
    title:      'tomorrow (answer)',
    content:    [
      't<span class="Vowel">o</span><span class="Separator">|</span>m<span class="Vowel">or</span><span class="Separator">|</span>r<span class="Vowel">ow</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">government</div>',
    content:    [
    ]
  },
  {
    title:      'government (answer)',
    content:    [
      'g<span class="Vowel">o</span><span class="Separator">|</span>v<span class="Vowel">er</span>n<span class="Separator">|</span>m<span class="Vowel">e</span>nt'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">different</div>',
    content:    [
    ]
  },
  {
    title:      'different (answer)',
    content:    [
      'd<span class="Vowel">i</span>f<span class="Separator">|</span>f<span class="Vowel">er</span><span class="Separator">|</span><span class="Vowel">e</span>nt'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">important</div>',
    content:    [
    ]
  },
  {
    title:      'important (answer)',
    content:    [
      '<span class="Vowel">i</span>m<span class="Separator">|</span>p<span class="Vowel">or</span><span class="Separator">|</span>t<span class="Vowel">a</span>nt'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">community</div>',
    content:    [
    ]
  },
  {
    title:      'community (answer)',
    content:    [
      'c<span class="Vowel">o</span>m<span class="Separator">|</span>m<span class="Vowel">u</span><span class="Separator">|</span>n<span class="Vowel">i</span><span class="Separator">|</span>t<span class="Vowel">y</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">political</div>',
    content:    [
    ]
  },
  {
    title:      'political (answer)',
    content:    [
      'p<span class="Vowel">o</span><span class="Separator">|</span>l<span class="Vowel">i</span><span class="Separator">|</span>t<span class="Vowel">i</span><span class="Separator">|</span>c<span class="Vowel">al</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">president</div>',
    content:    [
    ]
  },
  {
    title:      'president (answer)',
    content:    [
      'pr<span class="Vowel">e</span><span class="Separator">|</span>s<span class="Vowel">i</span><span class="Separator">|</span>d<span class="Vowel">e</span>nt'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">information</div>',
    content:    [
    ]
  },
  {
    title:      'information (answer)',
    content:    [
      '<span class="Vowel">i</span>n<span class="Separator">|</span>f<span class="Vowel">or</span><span class="Separator">|</span>m<span class="Vowel">a</span><span class="Separator">|</span><span class="Vowel">tion</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">understand</div>',
    content:    [
    ]
  },
  {
    title:      'understand (answer)',
    content:    [
      '<span class="Vowel">u</span>n<span class="Separator">|</span>d<span class="Vowel">er</span><span class="Separator">|</span>st<span class="Vowel">an</span>d'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">together</div>',
    content:    [
    ]
  },
  {
    title:      'together (answer)',
    content:    [
      't<span class="Vowel">o</span><span class="Separator">|</span>g<span class="Vowel">e</span><span class="Separator">|</span>th<span class="Vowel">er</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">education</div>',
    content:    [
    ]
  },
  {
    title:      'education (answer)',
    content:    [
      '<span class="Vowel">e</span>d<span class="Separator">|</span><span class="Vowel">u</span><span class="Separator">|</span>c<span class="Vowel">a</span><span class="Separator">|</span><span class="Vowel">tion</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">remember</div>',
    content:    [
    ]
  },
  {
    title:      'remember (answer)',
    content:    [
      'r<span class="Vowel">e</span><span class="Separator">|</span>m<span class="Vowel">e</span>m<span class="Separator">|</span>b<span class="Vowel">er</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">experience</div>',
    content:    [
    ]
  },
  {
    title:      'experience (answer)',
    content:    [
      '<span class="Vowel">e</span>x<span class="Separator">|</span>p<span class="Vowel">e</span><span class="Separator">|</span>r<span class="Vowel">i</span><span class="Separator">|</span><span class="Vowel">en</span>ce'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">development</div>',
    content:    [
    ]
  },
  {
    title:      'development (answer)',
    content:    [
      'd<span class="Vowel">e</span><span class="Separator">|</span>v<span class="Vowel">el</span><span class="Separator">|</span><span class="Vowel">o</span>p<span class="Separator">|</span>m<span class="Vowel">en</span>t'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">possible</div>',
    content:    [
    ]
  },
  {
    title:      'possible (answer)',
    content:    [
      'p<span class="Vowel">o</span>s<span class="Separator">|</span>s<span class="Vowel">i</span><span class="Separator">|</span>b<span class="Vowel">le</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">military</div>',
    content:    [
    ]
  },
  {
    title:      'military (answer)',
    content:    [
      'm<span class="Vowel">il</span><span class="Separator">|</span><span class="Vowel">i</span><span class="Separator">|</span>t<span class="Vowel">a</span>r<span class="Separator">|</span><span class="Vowel">y</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">relationship</div>',
    content:    [
    ]
  },
  {
    title:      'relationship (answer)',
    content:    [
      'r<span class="Vowel">e</span><span class="Separator">|</span>l<span class="Vowel">a</span><span class="Separator">|</span><span class="Vowel">tion</span><span class="Separator">|</span>sh<span class="Vowel">i</span>p'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">international</div>',
    content:    [
    ]
  },
  {
    title:      'international (answer)',
    content:    [
      '<span class="Vowel">i</span>n<span class="Separator">|</span>t<span class="Vowel">er</span><span class="Separator">|</span>n<span class="Vowel">a</span><span class="Separator">|</span><span class="Vowel">tion</span><span class="Separator">|</span><span class="Vowel">al</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">director</div>',
    content:    [
    ]
  },
  {
    title:      'director (answer)',
    content:    [
      'd<span class="Vowel">i</span><span class="Separator">|</span>r<span class="Vowel">e</span>c<span class="Separator">|</span>t<span class="Vowel">or</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">position</div>',
    content:    [
    ]
  },
  {
    title:      'position (answer)',
    content:    [
      'p<span class="Vowel">o</span><span class="Separator">|</span>s<span class="Vowel">i</span><span class="Separator">|</span><span class="Vowel">tion</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">especially</div>',
    content:    [
    ]
  },
  {
    title:      'especially (answer)',
    content:    [
      '<span class="Vowel">e</span>s<span class="Separator">|</span>p<span class="Vowel">e</span><span class="Separator">|</span><span class="Vowel">cial</span><span class="Separator">|</span>l<span class="Vowel">y</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">official</div>',
    content:    [
    ]
  },
  {
    title:      'official (answer)',
    content:    [
      '<span class="Vowel">o</span>f<span class="Separator">|</span>f<span class="Vowel">i</span><span class="Separator">|</span><span class="Vowel">cial</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">everyone</div>',
    content:    [
    ]
  },
  {
    title:      'everyone (answer)',
    content:    [
      '<span class="Vowel">e</span>ve<span class="Separator">|</span>r<span class="Vowel">y</span><span class="Separator">|<span class="Vowel">one</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">activity</div>',
    content:    [
    ]
  },
  {
    title:      'activity (answer)',
    content:    [
      '<span class="Vowel">a</span>c<span class="Separator">|</span>t<span class="Vowel">i</span><span class="Separator">|</span>v<span class="Vowel">i</span><span class="Separator">|</span>t<span class="Vowel">y</span>'
    ]
  },
  {
    title:      '<div class="Pad50 HoverHighlight" onclick="G.SayThisWord(this)">situation</div>',
    content:    [
    ]
  },
  {
    title:      'situation (answer)',
    content:    [
      's<span class="Vowel">i</span><span class="Separator">|</span>t<span class="Vowel">u</span><span class="Separator">|</span><span class="Vowel">a</span><span class="Separator">|</span><span class="Vowel">tion</span>'
    ]
  },
];


// ====================================================================
G.OnPageLoad = function()
{
  let slides  = ['<div class="Slides" style="height: 100vh;">'];
  
  for (let slide of G.SLIDES)
  {
    let slidetext = [`<div class="Slide Center Size300">
      <h2 class="dgreen SlideTitle ${slide.title.indexOf('(answer)') != -1 ? 'Hidden' : ''}">${slide.title}</h2>
      <div class="SlideContent FlexGrid20">
    `]
    
    for (let word of slide.content)
    {
      slidetext.push(`<div class="SlideWord Pad50">${word}</div>`);
    }
    
    slidetext.push(`</div>
    </div>`);

    slides.push(slidetext.join('\n'));
  }
  
  slides.push('</div>');
  
  P.HTML(
    '.PageLoadContent', 
    slides
  );  
  
  P.MakeSlideShow(
    '.Slides',
    {
      keycontrol: 1
    }
  );
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
