"use strict";

// ********************************************************************
class SchoolAdministratorChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.schoolid = config.schoolid;
    
    this.multipleselect = 1;
    this.bgclass        = 'dredg';
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      '.' + self.div + ' .ChosenCards',
      '/learn/schoolapi',
      {
        command:      'getadministrators',
        schoolid:     self.schoolid
      },
      function(d)
      {
        self.chosen         = d.data;
        self.chosencount    = d.count;
        
        self.DisplayChosen();
        
        P.LoadingAPI(
          '.' + self.div + ' .AvailableCards',
          '/learn/teacherapi',
          {
            command:      'search',
            schoolid:     self.schoolid
          },
          function(d)
          {
            self.available      = [];
            self.availablecount = d.count;
            
            for (let r of d.data)
            {
              for (let s of self.chosen)
              {
                if (r.idcode == s.idcode)
                {
                  self.availablecount--;
                  continue;
                }
              }
              
              self.available.push(r);
            }
            
            self.DisplayAvailable();
          }
        );
      }
    );
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    let self  = this;
    
    return `<div class="Headshot Center">${P.HeadShotImg(r.idcode, 'Square400')}</div>
      <div class="DisplayName Center">${P.BestTranslation(r.displayname)}</div>
      <div class="PhoneNumber Center Size80">${P.BestTranslation(r.phonenumber)}</div>
      <div class="Email Center Size80">${P.BestTranslation(r.email)}</div>
    `;
  }
}

// ********************************************************************
class SchoolStudentChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.schoolid = config.schoolid;
    
    this.multipleselect = 1;
    this.bgclass        = 'dblueg';
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      '.' + self.div + ' .AvailableCards',
      '/learn/schoolapi',
      {
        command:      'getstudents',
        schoolid:     self.schoolid
      },
      function(d)
      {
        self.available        = d.data;
        self.availablecount   = d.count;
        
        self.chosen           = d.chosen;
        self.chosencount      = d.chosencount;
        
        self.DisplayAvailable();
        self.DisplayChosen();
      }
    );
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    let self  = this;
    
    return `<div class="Headshot Center">${P.HeadShotImg(r.idcode, 'Square400')}</div>
      <div class="DisplayName Center">${P.BestTranslation(r.displayname)}</div>
      <div class="PhoneNumber Center Size80">${P.BestTranslation(r.phonenumber)}</div>
    `;
  }
}

// ********************************************************************
class SchoolStudentApprover extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.schoolid = config.schoolid;
    
    this.multipleselect   = 1;
    this.customclickfunc  = 1;
    this.bgclass          = 'lblueg';
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      '.' + self.div + ' .AvailableCards',
      '/learn/schoolapi',
      {
        command:      'getwaitingstudents',
        schoolid:     self.schoolid
      },
      function(d)
      {
        self.available        = d.data;
        self.availablecount   = d.count;
        
        self.DisplayAvailable();
      }
    );
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    let self  = this;
    
    return `${
        r.status == 'denied'
        ? `<div class="ApprovalStatus redb Pad50 Margin25">${T.denied}: ${r.reason}</div>`
        : `<div class="ApprovalStatus blueb Pad50 Margin25">${T.waiting}: ${r.comment}</div>`
      }
      <div class="Headshot Center">${P.HeadShotImg(r.idcode, 'Square400')}</div>
      <div class="DisplayName Center Pad50">${r.displayname}</div>
      <div class="PhoneNumber Center Pad25">${r.phonenumber}</div>
      <div class="Email Center Pad25">${r.email}</div>
      <div class="Grade Center Pad25">${r.grade}</div>
      <div class="Pad25">${r.classinfo.coursenum}</div>
      <div class="Pad25">${r.classinfo.displayname}</div>
      <div class="Pad25">${r.classinfo.schedule}</div>
      <div class="Pad25">${r.classinfo.teachers}</div>
      <div class="ButtonBar">
        <a class="Color4" onclick="${self.reference}.Accept('${r.idcode}'); P.CancelBubble(event);">Accept</a>
        <a class="Color1" onclick="${self.reference}.Deny('${r.idcode}');  P.CancelBubble(event);">Deny</a>
      </div>
    `;
  }
  
  // ------------------------------------------------------------------
  Accept(studentid)
  {
    this.Approve(studentid, 1);
  }
  
  // ------------------------------------------------------------------
  Deny(idcode)
  {
    let self  = this;
    
    P.InputPopup(
      T.pleasetypedenialreason,
      function(reason)
      {
        if (reason)
        {
          self.Approve(idcode, 0, reason);
        } else
        {
          P.ErrorPopup(T.missingfields);
        }
      }
    );
  }
  
  // ------------------------------------------------------------------
  Approve(studentid, accept, reason = '')
  {
    let self  = this;
    
    P.DialogAPI(
      '/learn/schoolapi',
      {
        command:      'approvestudent',
        schoolid:     self.schoolid,
        studentid:    studentid,
        accept:       accept,
        reason:       reason
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        P.RemoveFullScreen();
        
        for (let i = 0, l = self.available.length; i < l; i++)
        {
          if (self.available[i].idcode == studentid)
          {
            self.available.splice(i, 1);
            break;
          }
        }
        
        self.DisplayAvailable();
      }
    );    
  }  
}

// ********************************************************************
class SchoolTeacherApprover extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.schoolid   = config.schoolid;
    this.liststyles = 'FlexGrid16';
    this.bgclass    = 'lorangeg';
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      '.' + self.div + ' .AvailableCards',
      '/learn/schoolapi',
      {
        command:      'getwaitingteachers',
        schoolid:     self.schoolid
      },
      function(d)
      {
        self.available        = d.data;
        self.availablecount   = d.count;
        
        self.DisplayAvailable();
      }
    );
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    let self  = this;
    
    return `<div class="Headshot Center">${P.HeadShotImg(r.idcode, 'Square400')}</div>
      <div class="DisplayName Center">${P.BestTranslation(r.displayname)}</div>
      <div class="PhoneNumber Center Size80">${P.BestTranslation(r.phonenumber)}</div>
      <div class="ButtonBar">
        <a class="Color4" onclick="${self.reference}.Accept('${r.idcode}'); P.CancelBubble(event);">Accept</a>
        <a class="Color1" onclick="${self.reference}.Deny('${r.idcode}');  P.CancelBubble(event);">Deny</a>
      </div>
    `;
  }
  
  // ------------------------------------------------------------------
  Accept(teacherid)
  {
    this.Approve(teacherid, 1);
  }
  
  // ------------------------------------------------------------------
  Deny(idcode)
  {
    this.Approve(teacherid, 0);
  }
  
  // ------------------------------------------------------------------
  Approve(teacherid, accept)
  {
    let self  = this;
    
    P.DialogAPI(
      '/learn/schoolapi',
      {
        command:      'approveteacher',
        schoolid:     self.schoolid,
        teacherid:    teacherid,
        accept:       accept
      },
      function(d)
      {
        P.RemoveFullScreen();
        
        for (let i = 0, l = self.available.length; i < l; i++)
        {
          if (self.available[i].idcode == teacherid)
          {
            self.available.splice(i, 1);
            break;
          }
        }
        
        self.DisplayAvailable();
      }
    );    
  }  
}

// ********************************************************************
class ClassTeacherApprover extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.multipleselect = 1;
    
    this.schoolid = config.schoolid;
    this.classid  = config.classid;
    this.bgclass  = 'dpurpleg';
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      '.' + self.div + ' .AvailableCards',
      '/learn/teacherapi',
      {
        command:      'search',
        schoolid:     self.schoolid,
        classid:      self.classid,
        keyword:      searchterm
      },
      function(d)
      {
        self.available        = d.data;
        self.chosen           = d.chosen;
        self.availablecount   = d.count;
        
        self.DisplayAvailable();
        self.DisplayChosen();
      }
    );
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    let self  = this;
    
    return `<div class="Headshot Center">${P.HeadShotImg(r.idcode, 'Square400')}</div>
      <div class="DisplayName Center">${P.BestTranslation(r.displayname)}</div>
      <div class="PhoneNumber Center Size80">${P.BestTranslation(r.phonenumber)}</div>
    `;
  }
  
  // ------------------------------------------------------------------
  OnFinished()
  {
    let self  = this;
    
    let teacherids  = [];
    
    for (let i = 0, l = self.chosen.length; i < l; i++)
    {
      teacherids.push(self.chosen[i].idcode);
    }
    
    P.DialogAPI(
      '/learn/classapi',
      {
        command:      'setteachers',
        schoolid:     self.schoolid,
        classid:      self.classid,
        teachers:     teacherids
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        P.RemoveFullScreen();
      }
    );    
  }  
}

// ********************************************************************
class ClassStudentsApprover extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.multipleselect = 1;
    
    this.schoolid = config.schoolid;
    this.classid  = config.classid;
    this.bgclass  = 'dblueg';
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      '.' + self.div + ' .AvailableCards',
      '/learn/studentapi',
      {
        command:      'search',
        schoolid:     self.schoolid,
        classid:      self.classid,
        keyword:      searchterm
      },
      function(d)
      {
        self.available        = d.data;
        self.chosen           = d.chosen;
        self.availablecount   = d.count;
        
        self.DisplayAvailable();
        self.DisplayChosen();
      }
    );
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    let self  = this;
    
    return `<div class="Headshot Center">${P.HeadShotImg(r.idcode, 'Square400')}</div>
      <div class="DisplayName Center">${P.BestTranslation(r.displayname)}</div>
      <div class="PhoneNumber Center Size80">${P.BestTranslation(r.phonenumber)}</div>
    `;
  }
  
  // ------------------------------------------------------------------
  OnFinished()
  {
    let self  = this;
    
    let studentids  = [];
    
    for (let i = 0, l = self.chosen.length; i < l; i++)
    {
      studentids.push(self.chosen[i].idcode);
    }
    
    P.DialogAPI(
      '/learn/classapi',
      {
        command:      'setstudents',
        schoolid:     self.schoolid,
        classid:      self.classid,
        students:     studentids
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        P.RemoveFullScreen();
      }
    );    
  }  
}

// ********************************************************************
class Students extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.apiurl = '/learn/studentapi';
    
    this.displayfields  = [
      ['headshot', 'headshot', 'Icon'],
      ['displayname', 'translation', 'Name'],
      ['phonenumber', 'translation', 'Phone'],
      ['grade', 'translation', 'Grade'],
      ['coins', 'int', 'Coins']
    ];
    
    this.editfields  = [
      ['displayname', 'translation', 0, 'Name'],
      ['phonenumber', 'translation', 0, 'Phone'],
      ['grade', 'translation', 0, 'Grade'],
      ['coins', 'int', 0, 'Coins'],
      ['items', 'json', 0, 'Items']
    ];
    
    this.enablesearch   = 1;
    this.showidcode     = 1;
    this.tablestyles    = 'Styled';
    this.liststyles     = 'FlexGrid16';
    this.cardstyles     = 'whiteb Raised Rounded Margin25 Pad50';
    
    this.schoolid = config.schoolid;
    
    this.extraparams  = {
      schoolid:   config.schoolid
    };
    
    this.extraactions.push(['Expel', 'Expel', 1]);
    
    this.extrabuttons = `
      <a class="Color4" onclick="G.students.WaitingForApproval()">Waiting for Approval</a>
    `;
    
    this.bgclass        = 'dblueg';
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    let self  = this;
    
    return `<div class="Headshot Center">${P.HeadShotImg(r.idcode, 'Square400')}</div>
      <div class="DisplayName Center">${P.BestTranslation(r.displayname)}</div>
      <div class="PhoneNumber Center Size80">${P.BestTranslation(r.phonenumber)}</div>
      <div class="Email Center Size80">${P.BestTranslation(r.email)}</div>
    `;
  }
  
  // ------------------------------------------------------------------
  Expel(card, dbid)
  {
    let self  = this;
    let obj   = self.GetItemByID(dbid);
    
    P.ConfirmDeletePopup(
      P.BestTranslation(obj.displayname),
      function()
      {
        P.LoadingAPI(
          '.ConfirmDeleteResults',
          self.apiurl,
          {
            command:  'expel',
            id:       dbid,
            schoolid: self.schoolid
          },
          function(d)
          {
            self.List();
          }
        );        
      }
    );
  }
  
  // ------------------------------------------------------------------
  WaitingForApproval()
  {
    let self  = this;
    
    G.schoolstudentchooser = new SchoolStudentApprover({
      div:        '.SchoolStudentChooser',
      reference:  'G.schoolstudentchooser',
      fields:     ['displayname'],
      schoolid:   self.schoolid
    });
    G.schoolstudentchooser.Display();
  }
}

// ********************************************************************
class Teachers extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.apiurl = '/learn/teacherapi';
    
    this.displayfields  = [
      ['headshot', 'headshot', 'Icon'],
      ['displayname', 'translation', 'Name'],
      ['phonenumber', 'translation', 'Phone']
    ];
    
    this.editfields  = [
      ['displayname', 'translation', 0, 'Name'],
      ['phonenumber', 'translation', 0, 'Phone']
    ];
    
    this.showidcode     = 1;
    this.tablestyles    = 'Styled';
    this.liststyles     = 'FlexGrid16';
    
    this.schoolid = config.schoolid;
    
    this.extraparams  = {
      schoolid:   config.schoolid
    };
    
    this.extraactions.push(['Expell', 'Expell', 1]);
    
    this.extrabuttons = `
      <a class="Color4" onclick="${this.reference}.WaitingForApproval()">Waiting for Approval</a>
      <a class="Color5" onclick="${this.reference}.AddYourself()">Add Yourself</a>
    `;
    
    this.bgclass        = 'dorangeg';
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    let self  = this;
    
    return `<div class="Headshot Center">${P.HeadShotImg(r.idcode, 'Square400')}</div>
      <div class="DisplayName Center">${P.BestTranslation(r.displayname)}</div>
      <div class="PhoneNumber Center Size80">${P.BestTranslation(r.phonenumber)}</div>
      <div class="Email Center Size80">${P.BestTranslation(r.email)}</div>
    `;
  }
  
  // ------------------------------------------------------------------
  Expel(card, dbid)
  {
    let self  = this;
    let obj   = self.GetItemByID(dbid);
    
    P.ConfirmDeletePopup(
      P.BestTranslation(obj.displayname),
      function()
      {
        P.LoadingAPI(
          '.ConfirmDeleteResults',
          self.apiurl,
          {
            command:  'expel',
            id:       dbid,
            schoolid: self.schoolid
          },
          function(d)
          {
            self.List();
          }
        );        
      }
    );
  }
  
  // ------------------------------------------------------------------
  WaitingForApproval()
  {
    let self  = this;
    
    G.schoolteacherchooser = new SchoolTeacherApprover({
      div:        '.SchoolTeacherChooser',
      reference:  'G.schoolteacherchooser',
      fields:     ['displayname'],
      schoolid:   self.schoolid,
      title:      'Waiting for Approval'
    });
    G.schoolteacherchooser.Display();
  }
  
  // ------------------------------------------------------------------
  AddYourself()
  {
    let self  = this;
    
    P.DialogAPI(
      '/learn/schoolapi',
      {
        command:    'approveteacher',
        teacherid:  P.userid,
        schoolid:   self.schoolid,
        accept:     1
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        self.List();
      }
    );
  }
}

// ********************************************************************
class Classes extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.apiurl   = '/learn/classapi';
    this.schoolid = config.schoolid;
    
    this.extraparams  = {
      schoolid:   config.schoolid
    };
    
    this.extraactions.push(['Teachers', 'Teachers', 5]);
    this.extraactions.push(['Students', 'Students', 4]);
    
    this.displayfields  = [
      ['coursenum', 'text', 'Course ID'],
      ['displayname', 'translation', 'Description'],
      ['description', 'translation', 'Address'],
      ['schedule', 'translation', 'Description'],
      ['teachers', 'translation', 'Teachers'],
      ['numstudents', 'int', 'Registered Students'],
      ['capacity', 'int', 'Maximum Students']
    ];
    
    this.editfields  = [
      ['coursenum', 'text', 0, 'Course ID'],
      ['displayname', 'translation', 0, 'Class'],
      ['description', 'translation', 0, 'Description'],
      ['schedule', 'translation', 0, 'Schedule'],
      ['capacity', 'int', 0, 'Maximum Students'],
      ['resettimezone', 'timezone', 0, 'Reset Timezone']
    ];
    
    this.showidcode     = 1;
    this.bgclass        = 'dgreeng';
  }
  
  // ------------------------------------------------------------------
  Teachers(card, dbid)
  {
    let self  = this;
    
    G.classteacherchooser = new ClassTeacherApprover({
      div:        '.ClassTeacherChooser',
      reference:  'G.classteacherchooser',
      schoolid:   self.schoolid,
      classid:    dbid
    });
    G.classteacherchooser.Display();
  }  
  
  // ------------------------------------------------------------------
  Students(card, dbid)
  {
    let self  = this;
    
    G.classstudentchooser = new ClassStudentsApprover({
      div:        '.ClassStudentsApprover',
      reference:  'G.classstudentchooser',
      schoolid:   self.schoolid,
      classid:    dbid
    });
    G.classstudentchooser.Display();
  }  
  
  // ------------------------------------------------------------------
  ChooseCurriculum(dbid)
  {
    let self  = this;
    
    G.classcurriculumchooser = new ClassCurriculumChooser({
      div:        '.ClassCurriculumChooser',
      reference:  'G.classcurriculumchooser',
      schoolid:   self.schoolid,
      classid:    dbid
    });
    
    G.classcurriculumchooser.Display();
  }  
}

// ********************************************************************
class Schools extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    config.enableadd    = 1;
    config.enableedit   = 1;
    config.enabledelete = 1;
    
    config.extraactions   = [
      ['Administrators', 'Administrators', 1],
      ['Teachers', 'Teachers', 2],
      ['Classes', 'Classes', 3],
      ['Students', 'Students', 4]
    ];    
    
    config.apiurl = '/learn/schoolapi';
    
    config.displayfields  = [
      ['icon', 'imagefile', 'Icon'],
      ['displayname', 'translation', 'Name'],
      ['description', 'translation', 'Description'],
      ['address', 'translation', 'Address']
    ];
    
    config.editfields  = [];
    
    config.showidcode     = 1;
    config.enablesearch   = 1;
    
    config.searchfilter   = 1;
    config.liststyles     = 'FlexGrid20';
    config.tablestyles    = 'whiteb Bordered';
    
    super(config);    
  }
  
  // ------------------------------------------------------------------
  GetEditFields(obj)
  {
    return [
      ['icon', 'imagefile', 0, 'Icon'],
      ['displayname', 'translation', 0, 'School Name'],
      ['description', 'translation', 0, 'Description'],
      ['address', 'translation', 0, 'Address'],
      ['phonenumber', 'translation', 0, 'Phone Number'],
      ['email', 'translation', 0, 'Email Address'],
      ['contactinfo', 'translation', 0, 'Contact Information'],
      ['flags', 'bitflags', obj.flags, T.options,
        [
          [T.autoapprove, 'autoapprove']
        ]
      ]
    ];    
  }
  
  // ------------------------------------------------------------------
  GetItemDisplayName(obj)
  {
    return P.BestTranslation(obj.displayname);
  }
  
  // ------------------------------------------------------------------
  OnSaveData(obj)
  {
    if (obj.autoapprove == 'on')
    {
      obj.flags |= G.SCHOOL_AUTO_APPROVE;
    } else
    {
      obj.flags &= ~(G.SCHOOL_AUTO_APPROVE);
    }
    
    if (obj.problemids)
    {
      obj.problemids = obj.problemids.split('\n');
    }
  }
  
  // ------------------------------------------------------------------
  Administrators(card, dbid)
  {
    let self  = this;
    
    G.administrators = new SchoolAdministratorChooser({
      div:        '.Administrators',
      reference:  'G.administrators',
      schoolid:   dbid,
      title:      '<h2>' + self.GetItemDisplayName(self.GetItemByID(dbid)) + ': ' + T.administrators + '</h2>'
    });
    
    G.administrators.Display();
  }
 
  // ------------------------------------------------------------------
  Students(card, dbid)
  {
    P.AddFullScreen(
      'dblueg',
      '',
      `<div class="Students"></div>
      <div class="ButtonBar">
        <a class="Color3" onclick="P.RemoveFullScreen()">Finished</a>
      </div>    
    `);
    
    G.students = new Students({
      div:        '.Students',
      reference:  'G.students',
      schoolid:   dbid
    });
    
    G.students.Display();
    G.students.List();
  }
  
  // ------------------------------------------------------------------
  Teachers(card, dbid)
  {
    P.AddFullScreen(
      'dorangeg',
      '',
      `<div class="Teachers"></div>
      <div class="ButtonBar">
        <a class="Color3" onclick="P.RemoveFullScreen()">Finished</a>
      </div>    
    `);
    
    G.teachers = new Teachers({
      div:        '.Teachers',
      reference:  'G.teachers',
      schoolid:   dbid
    });
    
    G.teachers.Display();
    G.teachers.List();
  }
  
  // ------------------------------------------------------------------
  Classes(card, dbid)
  {
    P.AddFullScreen(
      'dgreeng',
      '',
      `<div class="ClassList"></div>
      <div class="ButtonBar">
        <a class="Color3" onclick="P.RemoveFullScreen()">Finished</a>
      </div>    
    `);
    
    let isadministrator = P.HasGroup('school.administrators');
    
    G.classes = new Classes({
      reference:    'G.classes',
      schoolid:     dbid,
      div:          'ClassList',
      enableadd:    isadministrator,
      enableedit:   isadministrator,
      enabledelete: isadministrator
    });
    G.classes.Display();
    G.classes.List();
  }  
  
  // ------------------------------------------------------------------
  Curriculum(card, dbid)
  {
    P.AddFullScreen(
      'dpurpleg',
      '',
      `<div class="ClassList"></div>
      <div class="ButtonBar">
        <a class="Color3" onclick="P.RemoveFullScreen()">Finished</a>
      </div>    
    `);
    
    let isadministrator = P.HasGroup('school.administrators');
    
    G.curriculum = new SchoolCurriculumChooser({
      reference:    'G.curriculum',
      schoolid:     dbid,
      div:          'CurriculumList',
      enableadd:    isadministrator,
      enableedit:   isadministrator,
      enabledelete: isadministrator
    });
    
    G.curriculum.Display();
    
  }    
}

// ====================================================================
G.OnPageLoad = function()
{
  if (!P.userid)
  {
    P.LoadURL('/system/login.html?nextpage=' + encodeURIComponent(window.location.pathname));
    return;
  }
  
  let ls  = [
    '<h1>Learning Management</h1>'
  ];
  
  if (P.HasGroup('school.administrators'))
  {
    ls.push(`
      <h2>My Schools</h2>
      <div class="Schools"></div>
    `);
  }
  
  P.HTML('.PageContent', ls);
  
  if (P.HasGroup('school.administrators'))
  {
    G.schools = new Schools({
      div:          '.Schools',
      reference:    'G.schools'
    });
    
    G.schools.Display();
    G.schools.List();
  }
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
