"use strict";

G.extrauseractions.push(['Add to School', 'AddToSchool', 5]);

// ********************************************************************
class SchoolChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.fields   = [];
    this.userid   = config.userid;
    
    this.onfinished = function(dbid, obj)
    {
      P.LoadingAPI(
        '.SchoolChooserResults',
        '/learn/schoolapi',
        {
          command:    'adduser',
          schoolid:   dbid,
          userid:     this.userid,
          usertype:   G.schoolusertype
        },
        function(d)
        {
          P.RemoveFullScreen();
        }
      );
    }
    
    this.title  = `
      <h2>Add User to School</h2>
      <div class="UserTypeButtons"></div>
    `;
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm = '')
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/learn/schoolapi',
      {
        command:    'getlinked',
        keyword:    searchterm,
        start:      start,
        limit:      self.limit
      },
      function(d)
      {
        self.available      = d.data;
        self.availablecount = d.count;
        
        self.DisplayAvailable();
      }
    );
  }  
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    return `
      <div>
        <div class="CardIcon Center">
          ${P.ImgFile(r.icon, 'Square800')}
        </div>
        <div class="Bold">
          ${P.BestTranslation(r.displayname)}
        </div>
        <div class="Bold Size80" style="color: #ddf;">
          ${P.BestTranslation(r.description)}
        </div>
        <div class="PhoneNumber">
          ${P.BestTranslation(r.phone)}
        </div>
      </div>
    `;
  }
}

// ********************************************************************
P.AddHandler(
  'pageload', 
  function()
  {
    G.userslist.AddToSchool = function(card, dbid)
    {
      G.schoolchooser = new SchoolChooser({
        div:        '.SchoolChooser',
        reference:  'G.schoolchooser',
        userid:     dbid
      });
      
      G.schoolchooser.Display();
      
      G.schoolusertype  = 'student';
      
      P.MakeRadioButtons(
        '.UserTypeButtons',
        [
          ['Student', 'student', 1, ''],
          ['Teacher', 'teacher', 0, ''],
          ['Administrator', 'administrator', 0, '']
        ],
        function(element, newvalue)
        {
          G.schoolusertype  = newvalue;
        }
      );
    }    
  }
);

