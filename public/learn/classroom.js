"use strict";

G.studentflags  = 0;
G.studentitems  = {};

G.classid  = Last(window.location.pathname.split('/'));

if (G.classid == 'classroom')
{
  G.classid = '';
}

// ====================================================================
G.AddWeapons = function(ls, promptdata, items, score)
{
  ls.push(`
    <div class="StudentWeapons Flex FlexCenter FlexWrap">
      <a class="whiteb Margin25 Pad25 Width800 Center" onclick="G.BuyStudentItem('chair')">
        <img class="Square200" src="/phaser/images/chair.webp"> 1分 &nbsp;&nbsp
        有${G.studentitems['chair'] ? G.studentitems['chair'] : 0}
      </a>
      <a class="whiteb Margin25 Pad25 Width800 Center" onclick="G.BuyStudentItem('sofa')">
        <img class="Square200" src="/phaser/images/sofa.webp"> 2分 &nbsp;&nbsp
        有${G.studentitems['sofa'] ? G.studentitems['sofa'] : 0}
      </a>
      <a class="whiteb Margin25 Pad25 Width800 Center" onclick="G.BuyStudentItem('desk')">
        <img class="Square200" src="/phaser/images/desk.webp"> 3分 &nbsp;&nbsp
        有${G.studentitems['desk'] ? G.studentitems['desk'] : 0}
      </a>
      <a class="whiteb Margin25 Pad25 Width800 Center" onclick="G.BuyStudentItem('TV')">
        <img class="Square200" src="/phaser/images/TV.webp"> 4分 &nbsp;&nbsp
        有${G.studentitems['TV'] ? G.studentitems['TV'] : 0}
      </a>
      <a class="whiteb Margin25 Pad25 Width800 Center" onclick="G.BuyStudentItem('door')">
        <img class="Square200" src="/phaser/images/door.webp"> 5分 &nbsp;&nbsp;
        有${G.studentitems['door'] ? G.studentitems['door'] : 0}
      </a>
      <a class="whiteb Margin25 Pad25 Width800 Center" onclick="G.BuyStudentItem('window')">
        <img class="Square200" src="/phaser/images/window.webp"> 6分 &nbsp;&nbsp;
        有${G.studentitems['window'] ? G.studentitems['window'] : 0}
      </a>
      <a class="whiteb Margin25 Pad25 Width800 Center" onclick="G.BuyStudentItem('table')">
        <img class="Square200" src="/phaser/images/table.webp"> 7分 &nbsp;&nbsp;
        有${G.studentitems['table'] ? G.studentitems['table'] : 0}
      </a>
      <a class="whiteb Margin25 Pad25 Width800 Center" onclick="G.BuyStudentItem('bed')">
        <img class="Square200" src="/phaser/images/bed.webp"> 8分 &nbsp;&nbsp;
        有${G.studentitems['bed'] ? G.studentitems['bed'] : 0}
      </a>
    </div>
  `);  
}

// ====================================================================
G.DisplayPrompt = function(prompt, promptdata, items, score)
{
  items = items || {}
  score = score || 0;
  
  G.studentitems  = items;
  
  if (!prompt)
  {
    P.HTML('.Prompt', '');
    return;
  }
  
  let ls  = [`
    <h2>你现在有${score}分</h2>
  `];
  
  switch (prompt)
  {
    case 'animalchaser':
      let familymembers = Keys(promptdata);
      
      ls.push('<div class="FamilyMember Flex FlexCenter FlexWrap">');
      
      for (let i = 0, l = familymembers.length; i < l; i++)
      {
        let r = familymembers[i];
        
        let animal  = promptdata[r];
        
        ls.push(`
          <div class="Width200 Height800 greeng Center Flex FlexVertical Margin25">
            <div>
              <img class="Width200" src="/phaser/images/${r}.webp">
            </div>
            <div>
              <img class="Width200" src="/phaser/images/${animal}.webp">
            </div>
          </div>
        `);
      }
      
      ls.push('</div>');
      
      G.AddWeapons(ls, promptdata, items, score);
      break;
    case 'furniture':
    case 'furniturefoods':
      if (prompt == 'furniturefoods')
      {
        let familymembers = Keys(promptdata);
        let foods         = Keys(promptdata[familymembers[0]]);
        
        for (let i = 0, l = familymembers.length; i < l; i++)
        {
          let r = familymembers[i];
          
          ls.push(`<div class="FamilyMember Flex FlexCenter FlexWrap">
            <div class="Width200 browng Center Margin25">
              <img class="Width200" src="/phaser/images/${r}.webp">
            </div>
          `)
          
          for (let j = 0, m = foods.length; j < m; j++)
          {
            let s       = foods[j];
            let points  = promptdata[r][s];
            
            ls.push(`
              <div class="Width200 ${points > 0 ? 'greeng' : 'redg'} Center Margin25 Size80" style="padding: 0.9em 0em;">
                <img class="Square200" src="/phaser/images/${s.replace(' ', '.')}.webp">
                ${points}
              </div>
            `)
          }
          
          ls.push('</div>');
        }
      }
      
      G.AddWeapons(ls, promptdata, items, score);
      break;
  }
  
  P.HTML('.Prompt', ls);
}

// ====================================================================
G.SetStudentPrompt = function(prompt)
{
  if (!prompt)
  {
    return;
  }
  
  P.API(
    '/learn/answersapi',
    {
      command:  'setprompt',
      prompt:   prompt,
      classid:  G.classid
    },
    function(d)
    {
    }
  );
}

// ====================================================================
G.BuyStudentItem  = function(itemname)
{
  if (!itemname)
  {
    return;
  }
  
  P.API(
    '/learn/answersapi',
    {
      command:  'buyitem',
      data:     itemname,
      quantity: 1,
      classid:  G.classid
    },
    function(d)
    {
      if (d.error)
      {
        P.ErrorPopup(d.error);
        return;
      }
      
      G.DisplayPrompt(d.prompt, d.promptdata, d.items, d.score);
    }
  );
}

// ====================================================================
G.ChoosePromptPopup = function ()
{
  let ls 	= [`
    <div class="ChoosePrompt Size150 Center Flex FlexWrap FlexVertical FlexCenter">
      <a class="Color2 Width1600 Pad50" onclick="G.SetStudentPrompt('furniture'); P.RemoveFullScreen();">Purchase Furniture</a>
      <a class="Color3 Width1600 Pad50" onclick="G.SetStudentPrompt('furniturefoods'); P.RemoveFullScreen();">Furniture and Foods</a>
      <a class="Color4 Width1600 Pad50" onclick="G.SetStudentPrompt('animalchaser'); P.RemoveFullScreen();">Animal Chaser</a>
      <a class="Color5 Width1600 Pad50" onclick="G.SetStudentPrompt('drawing'); P.RemoveFullScreen();">Drawing</a>
      <a class="Color1 Width1600 Pad50" onclick="G.SetStudentPrompt(''); P.RemoveFullScreen();">None</a>
    </div>
    <br>
    <div class=ButtonBar>
      <a class="Color1" onclick="P.RemoveFullScreen()">Back</a>
    </div>`
  ];

  P.AddFullScreen('dpurpleg', '', ls);	  
}

// ====================================================================
G.GetPrompt = function ()
{
  let ls  = [];
  
  P.API(
    '/learn/answersapi',
    {
      command:  'getprompt',
      classid:  G.classid
    },
    function(d)
    {
      G.studentflags  = d.flags;

      if (d.challenges.length)
      {
        ls  = [`<h2>${T.challenges}</h2>
          <div class="ChallengeCards FlexGrid16">
        `];
        
        let currenttime = Date.now() / 1000;
  
        for (let i = 0, l = d.challenges.length; i < l; i++)
        {
          let s = d.challenges[i];
          
          let remainingtime = (Date.parse(s.endtime) / 1000) - currenttime;
      
          let bgcolor = s.completed ? 'lgreenb' : 'lorangeb';
      
          ls.push(`
            <div class="ChallengeCard ${bgcolor} Rounded Raised Bordered Margin25 Pad25">
              <div class="ChallengeTitle Bold">${P.BestTranslation(s.title)}</div>
              <div class="displayname">${P.BestTranslation(r.displayname)}</div>
              <table class="Width100P Border0">
                <tr>
                  <td>🪙 ${s.numpoints}</td>
                  <td>🏹 ${s.triesleft}</td>
                  <td class="TimeTicker EndTime" data-value="${s.timeremaining}"></td>
                </tr>
              </table>
              
              ${s.completed
                ? `<div class="Center Bold">${T.youpassed}</div>`
                : `
                  <div class="ButtonBar">
                    <a class="Color3" href="/learn/challenges.html?classid=${G.classid}&challengeid=${s.idcode}">${T.start}</a>
                  </div>
                `
              }
            </div>
          `);          
        }
        
        ls.push('</div>');
        
        P.HTML('.Challenges', ls);
      
        P.MakeTimeTicker();
      }
      
      switch (d.prompt)
      {
        case 'drawing':
          let drawingarea = E('.DrawingArea');
              
          if (!drawingarea)
          {
            P.HTML(
              '.AnswerForm',
              ` <div class="Center">
                  <canvas class="DrawingArea"></canvas>
                  <div class="Center DrawingColorPicker"></div>
                </div>
                <div class="ButtonBar">
                  <a class="Color4" onclick="P.RestorePreviousCanvas()">${T.undo}</a>
                  <a class="Color5" onclick="P.RestoreNextCanvas()">${T.redo}</a>
                  <a class="Color3" onclick="G.UploadDrawing()">${T.finished}</a>
                  <a class="Color2" onclick="G.ClearDrawing()">${T.reset}</a>
                  <a class="Color1" onclick="window.history.back()">${T.back}</a>
                </div>
                <div class="AnswerFormResults"></div>
              `
            );
            
            drawingarea = E('.DrawingArea');
            
            if (window.innerWidth > window.innerHeight)
            {
              drawingarea.style.width   = '70vh';
              drawingarea.style.height  = '70vh';
            } else
            {
              drawingarea.style.width   = '70vw';
              drawingarea.style.height  = '70vw';
            }
            
            ls  = [];
      
            [
              '#000000',
              '#8b8275',
              '#8c040a',
              '#8b8218',
              '#108213',
              '#118275',
              '#020a75',
              '#8c0d75',
              '#8b8235',
              '#044236',
              '#1682fc',
              '#064276',
              '#8c25fc',
              '#8b420e',
              '#ffffff',
              '#c8c2ba',
              '#fd0d1b',
              '#fffe38',
              '#29fe2f',
              '#2dffff',
              '#0b24fc',
              '#fd29fd',
              '#fffe75',
              '#2afe76',
              '#8bffff',
              '#8c83fd',
              '#fd1476',
              '#fe8235',      
             ].forEach(
              function(r)
              {
                ls.push(`
                  <span class="PenColor InlineBlock Square200" data-color="${r}" style="background-color: ${r};"></span>
                `);
              }
            )
            
            P.HTML('.DrawingColorPicker', ls);
            
            P.OnClick(
              '.DrawingColorPicker',
              P.LBUTTON,
              function(e)
              {
                let el  = P.TargetClass(e, '.PenColor');
                
                if (el.dataset && el.dataset.color)
                {
                  P.ctx.strokeStyle = el.dataset.color;
                }
              }
            );
            
            P.MakeDrawableCanvas(drawingarea);
          }
          break;
        default:
          G.DisplayPrompt(d.prompt, d.promptdata, d.items, d.score);
          
          if (d.coderesult && d.coderesult['result'])
          {
            P.HTML(
              '.AnswerFormResults',
              `
              <div class="Error">
                <pre>${d.coderesult['line']}</pre>
                <div class="CodeResult">${d.coderesult['result']}</div>
              </div>
              `
            );
          }
      }      
      
      setTimeout(G.GetPrompt, 5000);
    }
  );
}

// ====================================================================
G.UploadDrawing = function()
{
  let drawingarea = E('.DrawingArea');  
  
  if (drawingarea)
  {
    P.LoadingAPI(
      '.AnswerFormResults',
      '/learn/answersapi',
      {
        command:  'saveimage',
        classid:  G.classid,
        dataurl:  drawingarea.toDataURL("image/jpeg", 0.7)
      },
      function(d, resultsdiv)
      {
        P.HTML(
          resultsdiv,
          '<p class="Center greeng Pad50">' + T.answersaved + '</p>'
        );
        
        setTimeout(
          function()
          {
            P.HTML(resultsdiv, '');
          },
          2000
        )
      }
    );    
  }
}

// ====================================================================
G.ClearDrawing = function()
{
  P.ctx.beginPath();
  P.ctx.fillStyle = '#fff';
  P.ctx.fillRect(0, 0, P.canvas.width, P.canvas.height);
  P.ctx.closePath();
}

// ====================================================================
G.SubmitAnswer = function (answer)
{
  let resultsdiv  = E('.AnswerFormResults');
  
  P.LoadingAPI(
    resultsdiv,
    '/learn/answersapi',
    {
      command:  'save',
      answer:   answer,
      classid:  G.classid
    },
    function(d)
    {
      P.HTML(
        resultsdiv,
        '<p class="Center greeng Pad50">' + T.answersaved + '</p>'
      );
      
      setTimeout(
        function()
        {
          P.HTML(resultsdiv, '');
        },
        2000
      )
    }
  );
}

// ====================================================================
G.OnPageLoad = function ()
{
  let isstudent = P.groups.indexOf('students') != -1;
  let isteacher = P.groups.indexOf('teachers') != -1;
  
  if (!isstudent && !isteacher)
  {
    P.LoadURL('/system/login.html?nextpage=' + encodeURIComponent(window.location.pathname));
    return;
  }
  
  P.HTML(
    '.PageLoadContent',
    `<div class="Challenges"></div>
    <div class="Prompt"></div>
    <div class="AnswerForm white"></div>
    <div class="ButtonBar TeacherButtonBar"></div>
    <div class="AnswerSelect"></div>
    <div class="StudentScores"></div>
    `
  );
  
  if (isteacher)
  {
    P.HTML(
      '.TeacherButtonBar',
      `<a class="Color1" onclick="G.studentbar.Update();">Refresh</a>
        <a class="Color5 PickProblemsButton" onclick="G.problemchooser.Display()">Pick Problems</a>
        <a class="Color6 ShowProblemsButton" onclick="G.problemchooser.ShowAllProblems()">Show Problems</a>
        <a class="Color2 RandomProblemButton" onclick="G.problemchooser.ShowProblems('.AnswerSelect')">Random Problem</a>
        <a class="Color4 ChoosePromptButton" onclick="G.ChoosePromptPopup()">Choose Prompt</a>
      `
    );
  }
  
  P.EditPopup(
    [
      ['answer', 'multitext', '', T.answer]
    ],
    function(formdiv, data, resultsdiv, e)
    {
      G.SubmitAnswer(data.answer);
    },
    {
      formdiv:            '.AnswerForm'
    }
  );
  
  P.LoadingAPI(
    '.AnswerFormResults',
    '/learn/answersapi',
    {
      command:  'get',
      classid:  G.classid
    },
    function(d, resultsdiv)
    {
      E('.answerTextArea').value  = d.data;
      P.HTML(resultsdiv, '');
    }
  );
  
  setTimeout(G.GetPrompt, 5000);
      
  P.On(
    '.answerTextArea',
    'keydown',
    function(e)
    {
      if (e.key == 'Control' && e.location == 2)
      {
        G.SubmitAnswer(E('.answerTextArea').value)
      }
    }
  );
  
  P.OnAltEnter(
    '.answerTextArea',
    function()
    {
      G.SubmitAnswer(E('.answerTextArea').value)
    }
  );
  
  P.On(
    '.answerTextArea',
    'copy paste drag drop',
    function(e)
    {
      if (!(G.studentflags & G.ANSWER_ENABLE_COPY_PASTE))
      {
        e.preventDefault();
      }
    }
  );
  
  G.studentbar  = new StudentBar({
    div:        '.StudentScores',
    reference:  'G.studentbar'
  });
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
