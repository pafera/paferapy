"use strict";

// ====================================================================
G.OnPageLoad = function()
{
  let classid = P.GetQueryStrings().get('classid');
  
  if (!classid)
  {
    P.HTML(
      '.PageLoadContent', 
      `<h1>${T.forum}</h1>
    <div class="Error">${T.missingparams}</div>
    `);
    return;
  }
  
  P.HTML(
    '.PageLoadContent', 
    `<h1>${T.forum}</h1>
  <div class="Forum"></div>
  `);
  
  G.forum = new MessageList({
    div:          '.Forum',
    reference:    'G.forum',
    objtype:      'learn_schoolclass',
    objid:        classid,
    enableadd:    1,
    enabledelete: 1,
    enablesearch: 1,
    enableedit:   1,
    showmembers:  1
  });
  
  G.forum.CheckAndDisplay();
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
