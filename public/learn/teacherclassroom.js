 "use strict";

G.LOCK_ANSWER         = 0x01;
G.ALREADY_ANSWERED    = 0x02;
G.ENABLE_COPY_PASTE   = 0x04;

G.ANSWER_TEACHER    = 0x08;

G.timelimit 				= 30;
G.timeleft 					= 30;
G.timerstarted 			= 0;
G.testdisplay				= 0;
G.imagedisplay			= 0;
G.disablehidetopbar	= 1;
G.lastanswers				= {};
G.updatescores 			= 1;
G.lockanswers       = 0;
G.showanswers       = 1;
G.changescoreamount = 1;
G.enablecopypaste   = 0;

G.autoupdate        = 0;
G.autorun           = 0;

G.autoupdatetimeout  = time() + 7200;

G.rightanswer         = 0;
G.currentstudent      = 0;
G.shownstudentbars    = [];
G.alreadyrightids     = []
G.selectedstudentids  = [];

G.displaymode       = 'programming';

G.classid  = Last(window.location.pathname.split('/'));

if (G.classid == 'classroom')
{
  G.classid = '';
}

// ====================================================================
G.AdjustTimer 	= function()
{
  if (E('.FullScreenPickerResults'))
  {
    P.RemoveFullScreen();
    return;
  }

  let ls  = [];
  
  for (let i = 0; i <= 14400;)
  {
    ls.push(
      [SecondsToTime(i), 'greenb']
    );
    
    if (i < 20)
    {
      i++;
    } else if (i < 60)
    {
      i += 5;
    } else if (i < 600)
    {
      i += 10;
    } else if (i < 1200)
    {
      i += 30;
    } else if (i < 3600)
    {
      i += 60;
    } else
    {
      i += 600;
    }
  }
  
  P.ShowFullScreenPicker(
    'Choose Timer',
    ls,
    function(itemtext, resultsdiv)
    {
      if (itemtext.indexOf(':') != -1)
      {
        let parts = itemtext.split(':');
        
        G.timelimit = parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60 + parseInt(parts[2]);
      } else
      {
        G.timelimit = parseInt(itemtext);
      }
      
      G.timeleft  = G.timelimit;
      
      E('.AnswerTimer').innerText = SecondsToTime(G.timelimit);
      
      P.RemoveFullScreen();
    }
  );
}

// ====================================================================
G.SetTimer  = function(newtimelimit)
{
  G.StopTimer();
  G.timelimit = newtimelimit;
  G.ResetTimer();
}

// ====================================================================
G.GetClassAnswers	= function()
{
  if (!G.studentbar)
  {
    return;
  }
  
  G.studentbar.Update();
  
  let students    = G.studentbar.students;
  let ls          = [];      
  
  switch (G.displaymode)
  {
    case 'typing':
      let rightanswer   = '';
      let firsttimer    = 0;
      
      for (let i = 0, l = students.length; i < l; i++)
      {
        let r  = students[i];
        
        if (r.flags & G.ANSWER_TEACHER)
        {
          rightanswer = r.answer;
          break;
        }
      }
      
      if (rightanswer 
        && rightanswer != G.currentanswer 
        && !G.timerstarted
      )
      {
        firsttimer  = 1;
        
        G.alreadyrightids = [];
        
        P.HTML('.StudentAnswer', '');
        
        if (!G.timelimit)
        {
          G.timeleft  = Math.round(rightanswer.length * 1.2) + 3;
        }
        
        G.StartTimer();
      }
      
      students.forEach(
        function(r)
        {
          let idcode        = r.idcode;
          let studentcolor  = 'grayg';
          
          if (r.flags & G.ANSWER_TEACHER)
          {
            studentcolor  = 'blueg';
          } else if (rightanswer)
          {
            if (firsttimer && r.answer == rightanswer)
            {
              G.alreadyrightids.push(idcode);
            }
            
            if (r.answer == rightanswer)
            {
              if (G.displaymode == 'typing' 
                && G.timerstarted 
                && G.alreadyrightids.indexOf(idcode) == -1
                && G.updatescores
              )
              {
                G.studentbar.ChangeScores([
                  {
                    idcode: idcode,
                    right:  1,
                    bonus:  G.timeleft - 1
                  }
                ]);
                G.alreadyrightids.push(idcode);
              }
              
              studentcolor  = 'greeng';
            } else
            {
              studentcolor  = 'redg';
            }
          }
          
          ls.push(`
            <div class="Margin25 ${studentcolor} Flex FlexCenter Student">
              <a class="InlineBlock Center white Border${r.groupnum}" onmousedown="G.studentbar.ShowStudent(this)" style="flex: 0;" data-dbid="${idcode}">
                ${P.HeadShotImg(idcode, 'Square400')}<br>
                ${r.score}
              </a>
              <div class="MonoSpace Pad25 BreakAll StudentAnswer">
                ${G.showanswers ? r.answer : ''} 
                ${r.scoreadded 
                  ? (r.scoreadded > 0
                      ? '<br><br><div class="Center yellow Bold">+' + r.scoreadded + '</div>'
                      : '<br><br><div class="Center yellow Bold">' + r.scoreadded + '</div>'
                    )
                  : ''} 
              </div>
            </div>
          `);
        }
      );
      
      ls.push('</div>');
      
      P.HTML('.TypingDisplay', ls);
      
      G.currentanswer = rightanswer;
      break;
    case 'drawing':
      let currenttime   = Date.now();
      
      students.forEach(
        function(r)
        {          
          let idcode        = r.idcode;
          let studentcolor  = (r.flags & G.ANSWER_TEACHER) ? 'blueg' : 'blackg';
          
          ls.push(`
            <div class="Margin25 ${studentcolor} Flex FlexCenter Student">
              <a class="InlineBlock Center white Border${r.groupnum}" onmousedown="G.studentbar.ShowStudent(this)" style="flex: 0;" data-dbid="${idcode}">
                ${P.HeadShotImg(idcode, 'Square400')}<br>
                ${r.score}
              </a>
              <div class="MonoSpace Pad25 StudentAnswer">
                ${G.timerstarted 
                  ? ''
                  :  `<img class="Square1200" src="/learn/drawings/${P.CodeDir(r.idcode)}.webp?t=${currenttime}">`
                } 
              </div>
            </div>
          `);
        }
      );
      
      P.HTML('.DrawingDisplay', ls);
      break;
    default:
      break;
  } 

  if (G.autoupdate)
  {
    if (time() > G.autoupdatetimeout)
    {
      G.autoupdate = 0;
      return;          
    }
    
    setTimeout(
      G.GetClassAnswers,
      1000
    );
  }
}

// ====================================================================
G.SelectStudent = function(studentidcode)
{
  let students  = G.studentbar.students;
  
  for (let i = 0, l = students.length; i < l; i++)
  {
    let r    = students[i];
    let idcode  = r.idcode;
    
    if (idcode == studentidcode)
    {
      G.currentstudent  = r;
      E('.StudentCode').value = G.currentstudent.answer;
      
      if (G.autorun)
      {
        G.RunCode();
      }
    }
  }  
}

// ====================================================================
G.ToggleAutoRun = function()
{
  G.autorun = !G.autorun;
}

// ====================================================================
G.ToggleUpdateScores = function()
{
  G.updatescores  = !G.updatescores;
}

// ====================================================================
G.ToggleAutoUpdate = function()
{
  G.autoupdate = !G.autoupdate;
  
  if (G.autoupdate)
  {
    G.autoupdatetimeout  = time() + 3600;
    
    setTimeout(
      G.GetClassAnswers,
      1000
    );
  } 
}

// ====================================================================
G.ToggleLockAnswers = function()
{
  G.lockanswers  = !G.lockanswers;
  
  if (!G.timerstarted)
  {
    P.API(
      '/learn/answersapi',
      {
        command:  'lock',
        classid:  G.classid,
        enable:   G.lockanswers
      }
    );
  }
}


// ====================================================================
G.AddTime	= function(num)
{
	G.StopTimer();
  
	G.timelimit 	+= num;

	if (G.timelimit < 3)
  {
		G.timelimit 	= 3;
  }

	G.ResetTimer();
}

// ====================================================================
G.StartTimer	= function(pause)
{
  if (pause && G.timerstarted)
  {
    G.StopTimer();
    return;
  }

  switch (G.displaymode)
  {
    case 'drawing':
      P.HTML('.StudentAnswer', '');
  };
  
  G.showanswers  = 0;  
  
  P.API(
    '/learn/answersapi',
    {
      command:  'clearscoreadded',
      classid:  G.classid
    },
    function(d, resultsdiv)
    {
      if (!G.timerstarted && !G.timeleft)
      {
        G.ResetTimer();
      }

      G.showanswers  = 0;  
      G.timerstarted 	= 1;
      
      P.AddTimer(G.DisplayTimer, 1000, 'answertimer');

      if (G.lockanswers)
      {
        P.API(
          '/learn/answersapi',
          {
            command:  'lock',
            classid:  G.classid,
            enable:   0
          }
        );
      }   
    }
  );  
}

// ====================================================================
G.StopTimer	= function(dontshowanswer)
{
  if (!dontshowanswer)
  {
    G.showanswers = 1;
    L('StopTimer', G.showanswers);
  }
  
  G.timerstarted 	= 0;
  
  P.ClearTimer('answertimer');
}

// ====================================================================
G.ResetTimer	= function()
{
  G.StopTimer(1);
  
  G.timeleft 	  = G.timelimit;
  G.showanswers = 1;
  L('ResetTimer', G.showanswers);
  
  P.Fill('.AnswerTimer', SecondsToTime(G.timeleft));
}

// ====================================================================
G.DisplayTimer	= function()
{
  if (G.timerstarted)
  {
    if (G.timeleft)
    {
      G.timeleft--;
    }

    if (!G.timeleft)
    {
      G.OnTimeUp();
    } else
    {
      P.AddTimer(G.DisplayTimer, 1000, 'answertimer');
    }
  }

  P.Fill('.AnswerTimer', SecondsToTime(G.timeleft));
}

// ====================================================================
G.OnTimeUp	= function()
{
	G.StopTimer();	
  
  L('TimeUp', G.showanswers);
	G.showanswers  = 1;
  
  switch (G.displaymode)
  {
    case 'typing':
      let rightanswer = '';
      let students    = G.studentbar.students;
      
      for (let i = 0, l = students.length; i < l; i++)
      {
        let r    = students[i];
        
        if (r.flags & G.ANSWER_TEACHER)
        {
          rightanswer = r.answer;
          break;
        }
      }
      
      let scores  = [];
      
      for (let i = 0, l = students.length; i < l; i++)
      {
        let r             = students[i];
        let idcode        = r.idcode;
        let studentcolor  = 'grayg';
        
        if ((r.flags & G.ANSWER_TEACHER) 
          || (r.scoreadded && !r.groupnum)
        )
        {
          continue;
        } else if (rightanswer)
        {
          if (r.answer == rightanswer)
          {
            if (G.alreadyrightids.indexOf(idcode) == -1)
            {
              scores.push(
                {
                  idcode: idcode,
                  right:  1
                }
              )
            }
          } else
          {
            let penalty = Math.round(-rightanswer.length / 2);
            
            scores.push(
              {
                idcode: idcode,
                wrong:  1,
                bonus:  penalty
              }
            );
            
            if (r.groupnum)
            {
              for (let j = 0, m = students.length; j < m; j++)
              {
                let s = students[j];
                
                if (s.groupnum == r.groupnum && idcode != s.idcode)
                {
                  let foundprevious  = 0;
                  
                  for (let k = 0, n = scores.length; k < n; k++)
                  {
                    if (scores[k]['idcode'] == s.idcode)
                    {
                      scores[k]['bonus']  += penalty;
                      foundprevious = 1;
                      break;
                    }
                  }
                  
                  if (!foundprevious)
                  {
                    scores.push(
                      {
                        idcode: s.idcode,
                        wrong:  1,
                        bonus:  penalty
                      }
                    );
                  }
                }
              }
            }
          }
        }
      }
      
      if (scores.length && G.updatescores)
      {
        G.studentbar.ChangeScores(scores);
      }
      
      if (G.lockanswers)
      {
        P.API(
          '/learn/answersapi',
          {
            command:  'lock',
            classid:  G.classid,
            enable:   1
          }
        );
      }   
      break;
    case 'drawing':
      break;
    default:
      break;
    
  }
          
  if (!G.autoupdate)
  {
    G.GetClassAnswers();
  }
}

// ====================================================================
G.ChangeScoreAmount 	= function(diff)
{
  let amount 	= G.changescoreamount;

  amount 	+= diff;

  G.changescoreamount 	= Bound(amount, 1, 1, 200);
}

// ====================================================================
G.DisplayProgramming = function()
{
  G.displaymode = 'programming';
  
  let gameselect  = [
    `<select class="GameSelect">
      <option></option>
    `
  ];
  
  let gamenames = Keys(PHASER_SCENES);
  
  gamenames.sort();
  
  for (let i = 0, l = gamenames.length; i < l; i++)
  {
    gameselect.push(`
      <option>${gamenames[i]}</option>
    `)
  }
  
  gameselect.push('</select>');
  
  gameselect  = gameselect.join("\n");
  
  P.HTML(
    '.PageContent',
    `<div class="Flex FlexWrap FlexCenter">
      <div id="GameCanvas" class="Pad25 ImageDisplay"></div>
      <div class="Pad25 MaxWidth2400 StudentsDisplay">
        <div class="Margin25 StudentBar"></div>
        <div class="Clear"></div>
        <div class="ButtonBar StudentActions">
          ${gameselect}
          <a class="Color1" onclick="G.RestartGame()">Restart Game</a>
          <a class="Color2" onclick="G.GetClassAnswers()">Refresh</a>
          <a class="Color3" onclick="G.RunCode()">Run</a>
          <a class="Color3" onclick="G.RunBattleCode()">Run Battle</a>
          <a class="Color3" onclick="G.RunFoodCode()">Run All</a>
          <a class="Color4 SpeechToTextButton" onclick="G.SpeechToText()">Command</a>
          <a class="Color5 PickProblemsButton" onclick="G.problemchooser.Display()">Pick Problems</a>
          <a class="Color6 ShowProblemsButton" onclick="G.problemchooser.ShowAllProblems()">Show Problems</a>
          <a class="Color1 RandomProblemButton" onclick="G.problemchooser.ShowProblems('.ImageDisplay')">Random Problem</a>
          <a class="Color2 DialogGeneratorButton" onclick="G.problemchooser.DialogGenerator()">Dialog</a>
        </div>
        <textarea rows="6" cols="24" class="StudentCode"></textarea>
      </div>
      <div>
        <div class="CodeResults"></div>
      </div>
    </div>
    `
  );
  
  P.On(
    '.GameSelect',
    'change',
    function(e)
    {
      let gamename  = E('.GameSelect').value;
      
      if (gamename)
      {
        G.InitPhaser(gamename);
      }
    }
  )
  
  if (!G.autoupdate)
  {
    G.GetClassAnswers();
  }
}

// ====================================================================
G.ChangeTypingGrid = function(select)
{
  let width = select.value;
  
  let typinggrid  = E('.TypingDisplay');
  
  typinggrid.className  = 'TypingDisplay FlexGrid' + width;
}

// ====================================================================
G.DisplayTyping = function()
{
  G.displaymode = 'typing';
  
  P.HTML(
    '.PageContent',
    `<div class="ButtonBar">
      <select class="TypingGridWidth" onchange="G.ChangeTypingGrid(this)">
        <option>8</option>
        <option>10</option>
        <option>12</option>
        <option>16</option>
        <option selected>20</option>
        <option>24</option>
        <option>30</option>
      </select>
    </div>
    <div class="FlexGrid20 TypingDisplay">
    </div>
    `
  );
  
  if (!G.autoupdate)
  {
    G.ToggleAutoUpdate();
  }
}

// ====================================================================
G.DisplayDrawing = function()
{
  G.displaymode = 'drawing';
  
  P.HTML(
    '.PageContent',
    `<div class="FlexGrid16 DrawingDisplay">
    </div>
    `
  );
  
  if (G.autoupdate)
  {
    G.ToggleAutoUpdate();
  }
}

// ====================================================================
G.OnPageLoad 	= function()
{
  if (P.groups.indexOf('teachers') == -1)
  {
    P.LoadURL('/system/login.html?nextpage=' + encodeURIComponent(window.location.pathname));
    return;
  }
  
  P.HTML(
    '.TopBarGrid',
    [
      `<div class="blackb Center">
        <table class="Width100P">
          <tr>
            <td class="AnswerTimer Size200">
              00:00:30
            </td>
            <td class="Color2 MinWidth200 AdjustTimerButton" onclick="G.AdjustTimer()">
              ±
            </td>
            <td class="Color4 MinWidth200 StartTimerButton" onclick="G.StartTimer(1)">
              ⏯
            </td>
            <td class="Color5 MinWidth200 ResetTimerButton" onclick="G.ResetTimer()">
              ◼
            </td>
            <td class="Color6 SettingButton">
              Settings
            </td>
          </tr>
        </table>
      </div>`
    ]
  );
  
  G.DisplayProgramming();
  
  setTimeout(
    function()
    {
      P.SetLayout({top: 'auto'});
    },
    200
  );

  P.API(
    '/learn/answersapi',
    {
      command:    'setprompt',
      prompt:     'furniture',
      promptdata: FAMILY_ANIMALS,
      classid:  G.classid
    },
    function(d)
    {
    }
  );
  
  G.studentbar  = new StudentBar({
    div:        '.StudentBar',
    reference:  'G.studentbar'
  });
  
  G.studentbar.OnStudentClicked = function(el, student)
  {
    let codearea  = E('.StudentCode');
    
    if (codearea)
    {
      codearea.value = student.answer;
    }
  }
  
  P.OnClick(
    '.SettingButton',
    P.LBUTTON,
    P.MakeContextMenu(
      function(el, e)
      {
        let ls  = [
          ['New Class',         function() { G.studentbar.NewClass(); }],
          ['Auto Refresh\t' + (G.autoupdate ? '✅' : '❌'),  
            function() { 
              G.ToggleAutoUpdate();
            }
          ],
          ['Auto Run\t' + (G.autorun ? '✅' : '❌'),         function() { G.ToggleAutoRun(); }],
          ['Update Scores\t' + (G.updatescores ? '✅' : '❌'),  
            function() { 
              G.ToggleUpdateScores();
            }
          ],
          ['Lock Answers After Time Up\t' + (G.lockanswers ? '✅' : '❌'),  
            function() { 
              G.ToggleLockAnswers();
            }
          ],
          ['Reset Scores',      function() { G.studentbar.ResetScores(); }],
          ['Random Groups',     function() { G.studentbar.RandomGroups(); }],
          ['Clear Groups',      function() { G.studentbar.ClearGroups(); }],
          ['Programming Mode',  function() { G.DisplayProgramming(); }],
          ['Typing Mode',       function() { G.DisplayTyping(); }],
          ['Drawing Mode',   function() { G.DisplayDrawing(); }]
        ];
        
        return ls;
      }
    )
  );

  G.GetClassAnswers();
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
