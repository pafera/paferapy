 "use strict";

// ====================================================================
G.SpeechToText = function()
{
  P.Popup(
    `<div class="lgrayb Raised Rounded Pad50">
      <h2 class="Center purple Margin0">${T.speechtotext}</h2>
      <div class="RecordingStatus">
        <div class="greyb">${T.waitingforrecordpermission}</div>
      </div>
      <div class="Clear"></div>
      <div class="Margin25 ButtonBar">
        <a class="Color4" onclick="G.RecordAnother()">${T.recordanother}</a>
        <a class="Color5" onclick="G.StopRecording()">${T.stoprecording}</a>
        <a class="Color1" onclick="P.CloseThisPopup(this)">${T.back}</a>
      </div>
    </div>
    `,
    {
      popupclass: 'SpeechToTextPopup'
    }
  );
  
  P.MakeDraggable('.SpeechToTextPopup');
  
  G.RecordAnother();
}

// ====================================================================
G.StopRecording = function()
{
  if (P.opusrecorder)
  {
    P.opusrecorder.stop();
    P.opusrecorder.close();
    P.opusrecorder  = 0;
    
    P.HTML(displayel, `<div class="redg Pad50">${T.recordingstopped}</div>`);
  }
}

// ====================================================================
G.RecordAnother = function()
{
  P.RecordOpus(
    '.RecordingStatus',
    function(displayel, typearray)
    {
      P.HTML(displayel, `<div class="blueg Pad50">${T.contactingserver}</div>`);
      
      let xhr = new XMLHttpRequest();
      
      xhr.onreadystatechange  = function(e)
      {
        if (xhr.readyState === XMLHttpRequest.DONE) 
        {
          let status = xhr.status;
          
          if (status === 0 || (status >= 200 && status < 400)) 
          {
            P.HTML(displayel, `<div class="whiteb Pad50" style="border: 0.2em solid green;">${xhr.responseText}</div>`);
            
            let data  = JSON.parse(xhr.responseText);
            
            if (data.error)
            {
              P.HTML(displayel, `<div class="Error Pad50">${T.errorcontactingserver}: ${data.error}</div>`);
            }
            
            let words = data.output.split(/\s/);
            
            for (let i = 0, l = words.length; i < l; i++)
            {
              if (words[i])
              {
                words[i]  = words[i][0].toUpperCase() + words[i].substr(1);
              }
            }
            
            for (let i = 0, l = words.length; i < l; i++)
            {
              let command = words.slice(0, i + 1).join('');
              
              let args = words.slice(i + 1);
              
              if (args)
              {
                args = args.join(" ");
              }
              
              if (shiba[command])
              {
                shiba[command](args);
                break;
              } 
            }
          } else 
          {
            P.HTML(displayel, `<div class="Error Pad50">${T.errorcontactingserver}: ${status}</div>`);
          }
        }
      }
      
      xhr.open("POST", '/learn/opustotext');
      
      xhr.send(typearray);
    },
    4
  );
}

/* ********************************************************************
 * Most entries are genuine homophones, but some are there purely to disguise
 * the error rate from current speech to text models, so don't be surprised
 * if some entries are not really homophones. They're the result of 
 * helping poor kids using sub quality microphones on cheap phones in noisy 
 * environments to improve their grades.
 * 
 * To add your own words to the list, say the word for the speech 
 * recognizer three times, write the *correct* word first, then write 
 * the variations on the right.
 * ****************************************************************** */ 
G.SPEECH_HOMOPHONES = [
  ['a', 'ugh', 'the', 'of', 'or', 'i', "i'll"],
  ['an', 'in', 'and'],
  ['apples', 'across', 'airports', 'app'],
  ['are', 'or', 'all', 'our', 'out', 'of', 'for', 'were', 'one', 'order', 'when', 'so', 'on'],
  ['arm', 'on'],
  ['arrow', 'ammo'],
  ['art', 'ot'],
  ['as', 'ass'],
  ['ate', 'eight'],
  ['backward', 'backwards'],
  ['bad', 'that'],
  ['bag', 'back'],
  ['bake', 'bate', 'bates', 'bait'],
  ['ball', 'boss', 'false'],
  ['balls', 'bucks', 'basta', 'miles', 'spouse', 'pals', 'bounce', 'mouse', 'house', 'boss', 'hours', 'bottles', 'bows', 'volts', 'bowels', 'bones'],
  ['bam', 'damn'],
  ['ban', 'band'],
  ['bank', 'thank'],
  ['bare', 'bear'],
  ['base', 'dates', 'please'],
  ['bat', 'that'],
  ['bath', 'bass'],
  ['be', 'bee'],
  ['bean', 'being', 'beans', 'beings'],
  ['bear', 'beer', 'bail'],
  ['beat', 'beet'],
  ['bed', 'bad'],
  ['bees', 'beast'],
  ['beef', 'beets', 'beats', 'peace'],
  ['been', 'than', 'then'],
  ['beg', 'bag'],
  ['bend', 'ben'],
  ['beside', 'decide'],
  ['bet', 'that'],
  ['bid', 'did'],
  ['bide', 'died', 'bite', 'byd', 'by'],
  ['bike', 'bite', 'pike'],
  ['bin', 'then', 'ben', 'been'],
  ['bird', 'beard', 'boat', 'booth'],
  ['bit', 'it'],
  ['bite', 'right', 'tight'],
  ['bob', 'bulb'],
  ['bog', 'dog'],
  ['bone', 'bomb', 'boom'],
  ['bookbag', 'book'],
  ['bop', 'bap', 'but', 'bumped', 'bump', 'pop'],
  ['boring', 'bobby', 'bor'],
  ['bottle', 'battle'],
  ['bowl', 'bow'],
  ['box', 'fox'],
  ['brake', 'break'],
  ['broccoli', 'practically'],
  ['brown', 'program'],
  ['brother', 'beretta'],
  ['bud', 'but'],
  ['bum', 'tom', 'bombs', 'bomb'],
  ['bun', 'ben', 'man', 'then', 'fun'],
  ['bus', 'thus'],
  ['buy', 'by', 'bye'],
  ['buyer', 'fire'],
  ['cab', 'cap', 'capt', 'hab', 'tab', 'camp'],
  ['cake', 'take'],
  ['cane', 'kane', 'kain', 'cain', 'team', 'tongue', 'came', 'ten', 'town', 'tang'],
  ['cap', 'sap', 'tap', 'camp'],
  ['cape', 'tape'],
  ['case', 'taste', 'tastes'],
  ['cave', 'case', 'caves'],
  ['cat', "can't"],
  ['cell', 'sell'],
  ['cent', 'scent', 'sent'],
  ['cereal', 'serial'],
  ['cherries', "chevy's"],
  ['chip', 'trip'],
  ['chopstick', 'chopsticks', 'chapstick', 'chapsticks'],
  ['circle', 'silica'],
  ['clap', 'clamp'],
  ['clock', 'clerk'],
  ['clog', 'clogged'],
  ['coarse', 'course'],
  ['complement', 'compliment'],
  ['cob', 'cop', 'cobb', 'hub', 'hard'],
  ['cod', 'card'],
  ['cog', 'hog'],
  ['cold', 'code'],
  ['con', 'can', 'time'],
  ['cone', 'come', 'com', 'call', 'comb'],
  ['cot', 'cart', 'cut', 'cat'],
  ['cow', 'cows'],
  ['crap', 'crop'],
  ['cross', 'course'],
  ['cub', 'cup'],
  ['cube', 'q'],
  ['cucumbers', 'cucumb'],
  ['cud', 'cut', 'could'],
  ['dam', 'damn'],
  ['day', 'they'],
  ['dear', 'deer'],
  ['den', 'then'],
  ['die', 'dye'],
  ['dig', 'digg'],
  ['dime', 'time', 'nine'],
  ['din', 'thin'],
  ['dine', 'dying', 'time', 'die'],
  ['dip', 'depth'],
  ['dim', 'tim', 'damn', 'then', 'deal'],
  ['disc', 'disk'],
  ['do', 'to', 'did', 'the'],
  ['does', 'thus'],
  ['dog', 'dark'],
  ['dome', "don't", 'dont'],
  ['dot', 'that'],
  ['dote', 'dope', 'doped', "don't"],
  ['draw', 'juha', 'jaw', 'sure'],
  ['dud', 'dad'],
  ['dug', 'doug'],
  ['dune', 'doom', 'do'],
  ['eighteen', 'eating', 'eighteenth', '18th'],
  ['eighth', '8th'],
  ['eighty', 'at&t', 'at'],
  ['elephant', 'emerson', 'illicit', 'elephants'],
  ['eleven', 'eleventh', '11th'],
  ['eye', 'eyes', 'i'],
  ['fad', 'sad'],
  ['fair', 'fare'],
  ['fake', 'steak', 'sake', 'said'],
  ['fame', 'same', 'thing'],
  ['fan', 'san'],
  ['farm', 'from', 'far'],
  ['fat', 'sat', 'that'],
  ['fate', 'said', 'seat', 'state'],
  ['farmer', 'former'],
  ['fed', 'said'],
  ['ferret', 'favorite', 'senate', 'theft', 'separate'],
  ['fifth', '5th'],
  ['fifteen', 'fifteenth', '15th'],
  ['fig', 'sig'],
  ['fin', 'sin'],
  ['fine', 'sign', 'sigh', 'sighing'],
  ['finger', 'thinker', 'think'],
  ['fir', 'fur'],
  ['first', 'face', '1st'],
  ['fit', 'sit'],
  ['fish', 'cish'],
  ['fix', 'six'],
  ['flee', 'flea'],
  ['flour', 'flower'],
  ['fog', 'sog'],
  ['for', 'four', 'fore', 'full'],
  ['fourth', '4th'],
  ['fourteen', 'fourteenth', '14th'],
  ['fox', 'sucks'],  
  ['frog', 'throg', 'fro'],
  ['fruits', 'suits', 'foods', 'through', 'foot', 'fruit', 'food'],
  ['full', 'foe', 'so', 'though'],
  ['fume', 'film', 'theme', 'feel'],
  ['fun', 'son', 'sun'],
  ['fuse', 'seals', 'use'],
  ['gab', 'gap'],
  ['game', 'dame'],
  ['gap', 'camp'],
  ['gas', 'das'],
  ['gate', 'kate', 'date'],
  ['gem', 'jim'],
  ['get', 'yet', 'cat'],
  ['gig', 'good'],
  ['gin', 'jen', 'chin', 'jin'],
  ['give', 'kids'],
  ['glove', 'gloves'],
  ['go', 'goal'],
  ['gob', 'god', 'gulp', 'gop', 'gab'],
  ['going', 'coming'],
  ['gray', 'grey', 'great'],
  ['gum', 'gm', 'gun'],
  ['gus', 'gas'],
  ['gut', 'good'],
  ['hag', 'pag', 'tag', 'hug'],
  ['hair', 'hare', 'here'],
  ['hand', 'help', 'have'],
  ['has', 'hess'],
  ['hat', 'that'],
  ['have', 'has', 'halves', 'half'],
  ["he's", 'his'],
  ['head', 'hand'],
  ['heal', 'heel'],
  ['hear', 'here'],
  ['hello', 'huddle'],
  ['help', 'helped'],
  ['hen', 'him'],
  ['hem', 'him'],
  ['hid', 'head'],
  ['hike', 'height'],
  ['him', 'hymn'],
  ['hit', 'set'],
  ['hole', 'whole', 'whoa', 'home'],
  ['hop', 'hot'],
  ['hose', 'hosts'],
  ['hot', 'pot', 'heart', 'hat', 'hut'],
  ['hour', 'our'],
  ["how's", 'house'],
  ['hyphen', '-'],
  ['i', 'eye', "i'd"],
  ["i'm", 'and'],
  ['idle', 'idol'],
  ['in', 'inn'],
  ['is', 'as'],
  ['it', 'the'],
  ['its', "it's", 'it'],
  ['jab', 'chap'],
  ['jake', 'cheap'],
  ['jam', 'jan', "jan's"],
  ['jerk', 'chuck'],
  ['jib', 'gibb', 'tube', 'jeb', 'chips', 'jacob', 'jed', 'chip', 'jet', 'tiered'],
  ['jig', 'cigs', 'jake', 'cig', 'jeg', 'tig', 'kid', 'chick'],
  ["jim's", 'gems'],
  ['jip', 'tip', 'chip'],
  ['jot', 'chart', 'judd', 'chat'],
  ['juice', 'jukes'],
  ['jump', 'champ', 'junk', 'jumped'],
  ['jut', 'jet', 'shut'],
  ['keg', 'tag'],
  ['ken', 'ten', 'kin'],
  ['kin', "can't", 'him', 'can'],
  ['knight', 'night'],
  ['knee', 'me'],
  ['knot', 'not'],
  ['know', 'no'],
  ['knows', 'nose'],
  ['lad', 'glad', 'flood'],
  ['ladle', 'little', 'lethal', 'lado', 'later'],
  ['lag', 'flag'],
  ['lake', 'late'],
  ['laugh', 'blacks', 'last'],
  ['led', 'lead'],
  ['leek', 'leak'],
  ['left', 'last', 'laugh'],
  ['lemons', 'limits'],
  ['lid', 'lead', 'lit'],
  ['light', 'night'],
  ['like', 'life', 'likes', 'liked'],
  ['lime', 'line'],
  ['line', 'lie', 'lying'],
  ['lion', 'mayan'],
  ['lip', 'lit'],
  ['live', 'lives', 'lit', 'lay', 'knelt', 'left'],
  ['lob', 'lab', 'loud', 'log'],
  ['lobe', 'globe', 'loeb', 'logs'],
  ['lode', 'load'],
  ['lone', 'long', 'loan'],
  ['look', 'looked'],
  ['lop', 'lap', 'luck', 'lot'],
  ['low', 'loan'],
  ['lug', 'lugs', 'plug'],
  ['made', 'maid'],
  ['mail', 'male'],
  ['mane', 'main', 'man', 'maine'],
  ['markers', 'macos', 'marcos', 'marcus'],
  ['mat', 'matt'],
  ['meat', 'meet'],
  ["meg's", 'mags', 'makes'],
  ['mend', 'meant', 'men'],
  ['met', 'matt'],
  ['mime', 'mine', 'my'],
  ['mine', 'mind'],
  ['missed', 'mist', 'nest'],
  ['mite', 'might'],
  ['mole', 'no', 'mo'],
  ['mom', 'man'],
  ['moms', "mom's"],
  ['monkey', 'monkeys'],
  ['mop', 'map'],
  ['morning', 'mourning'],
  ['move', 'moves'],
  ['mug', 'lug'],
  ['nape', 'night', 'nate'],
  ['ninth', 'knife'],
  ['nip', 'neck', 'nit'],
  ['nub', 'nope'],
  ['mope', 'nope', 'smoke'],
  ['mule', 'meal'],
  ['muse', 'nice'],
  ['mute', 'neil'],
  ['nab', 'matt'],
  ['nag', 'mag', 'mags'],
  ['nip', 'knit'],
  ['nineteen', 'nineteenth', '19th'],
  ['ninety', 'night'],
  ['ninth', '9th'],
  ['nod', 'not'],
  ['none', 'nun'],
  ['nor', 'no'],
  ['nose', 'notes'],
  ['oar', 'or'],
  ['of', 'os', 'at'],
  ['on', 'all', 'and'],
  ['one', 'won'],
  ['onions', 'indians'],
  ['open', 'opened'],
  ['orange', 'homage', 'aunt'],
  ['oranges', 'ownages'],
  ['ostrich', 'estridge'],
  ['oval', 'over', 'ovo', 'also', 'opo'],
  ['p', 'pea', 'pee'],
  ['pad', 'pat'],
  ['pain', 'pane'],
  ['pair', 'pear', 'pay', 'payoff'],
  ['pan', 'can'],
  ['panda', 'panther', 'candor'],
  ['pane', 'palm', 'pain', 'kane'],
  ['passed', 'past'],
  ['part', 'pot', 'hot'],
  ['pave', 'have', 'pay'],
  ['peace', 'piece'],  
  ['peaches', 'teachers', 'pictures'],
  ['pears', "here's", 'pay', 'payers'],
  ['peas', 'peace'],
  ['peg', 'pag'],
  ['pen', 'penn'],
  ['pep', 'that', 'pat'],
  ['pet', 'pat', 'put'],
  ['phase', 'face'],
  ['phone', 'song'],
  ['phonic', 'sonic'],
  ['pig', 'pit', 'pic'],
  ['pin', 'pen', 'ten', 'then', 'penn'],
  ['pit', 'pitt'],
  ['plain', 'plane'],
  ['play', 'clay'],
  ['pod', 'part'],
  ['poke', 'polk'],
  ['pole', 'powell', 'poll', 'po'],
  ['poor', 'pour'],
  ['pop', 'pump'],
  ['pork', 'cook'],
  ['pose', 'polls', 'post', 'posts'],
  ['pox', 'parks', 'pax'],
  ['pray', 'prey'],
  ['present', 'crescent'],
  ['principal', 'principle'],
  ['profit', 'prophet'],
  ['pug', 'hug'],
  ['rabbit', 'rapid'],
  ['rainy', 'raining'],
  ['rake', 'rate'],
  ['ram', 'wham'],
  ['ran', 'when', 'gran', 'ram'],
  ['rap', 'wrap'],
  ['rat', 'right'],
  ['rate', 'wait'],
  ['rave', 'wave', 'waves'],
  ['read', 'wed'],
  ['real', 'reel'],
  ['red', 'read'],
  ['reef', 'reese', "we've"],
  ['rib', 'web', 'read'],
  ['rid', 'read'],
  ['ride', 'wide', 'right', 'write'],
  ['right', 'write'],
  ['ripe', 'wipe', 'white', 'right'],
  ['rise', 'rice', 'wise'],
  ['rite', 'right'],
  ['rob', 'wrap', 'lab', 'rapid'],
  ['robe', 'rope', 'row'],
  ['rocket', 'racket'],
  ['rod', 'lad', 'rad'],
  ['rode', 'road'],
  ['role', 'row', 'roll'],
  ['root', 'route'],
  ['rot', 'rat', 'right'],
  ['rote', 'wrote', 'vote'],
  ['ruler', 'rul'],
  ['rum', 'ram', 'well'],
  ['run', 'rene', 'ran', 'right', 'when', 'rent'],
  ['rust', 'rest'],
  ['rut', 'rat', 'that', 'read'],
  ['sail', 'sale'],
  ['sane', 'same', 'saying'],
  ['sat', 'said', 'set'],
  ['sea', 'see', 'c'],
  ['seam', 'seem'],
  ['second', '2nd'],
  ['shark', 'shock'],
  ['sight', 'site'],
  ['seventh', '7th'],
  ['seventeen', 'seventeenth', '17th'],
  ['sew', 'so'],
  ['shore', 'sure'],
  ['sing', 'see'],
  ['sink', 'sync'],
  ['sip', 'sit'],
  ['site', 'sight'],
  ['six', 'sixth', '6th'],
  ['sixteen', 'sixteenth', '16th'],
  ['skirt', 'scoot'],
  ['sob', 'saab', 'stop'],
  ['socks', 'sucks'],
  ['sod', 'sad'],
  ['sole', 'soul'],
  ['some', 'sum'],
  ['something', 'sunny'],
  ['son', 'sun'],
  ['spatula', 'spatul'],
  ['stair', 'stare'],
  ['star', 'store', 'staff', 'start'],
  ['stationary', 'stationery'],
  ['steal', 'steel'],
  ['strawberry', 'stroppy'],
  ['suite', 'sweet'],
  ['t', 'tea', 'tee'],
  ['tad', 'ted'],
  ['tame', 'team', 'time', 'told', 'tenth', 'hm'],
  ['tail', 'tale'],  
  ['tape', 'take'],
  ['ted', 'head'],
  ['ten', 'tenth', '10th', 'turf', 'have'],
  ['than', 'dan'],
  ['that', 'daddy', 'death'],
  ['them', 'damn'],
  ['there', 'their', "they're", 'dale', 'day', 'dear', 'the', 'yeah', 'you', 'they', 'deer', 'daya'],
  ['these', 'deeds', 'dietz', 'geez', 'd', "d's", 'ease'],
  ['thin', 'sem', 'sin', 'finn', 'fenton', 'fin'],
  ['this', 'is'],
  ['third', '3rd'],
  ['thirteen', "don'ting", 'doing', 'dotting', 'thirteenth', '13th'],
  ['thirty', 'dirty'],
  ['thirsty', 'dusty'],
  ['those', 'does', 'dos', 'dogs', 'dose', 'do', 'adults', 'doves'],
  ['three', 'dewey'],
  ['Thursday', 'thirsty'],
  ['tin', 'ten', 'kin', 'kim', 'tim'],
  ['to', 'two', 'too'],
  ['toe', 'tow', 'to'],
  ['ton', 'tongue', 'tons', 'time', 'hon', 'ten'],
  ['tone', 'tongue', 'tom', 'tome'],
  ['tonight', 'to'],
  ['tot', 'part', 'tart', 'that'],
  ['tote', 'talked'],
  ['touch', 'touched', 'text'],
  ['tug', 'target'],
  ['turn', 'time', 'to'],
  ['turtle', 'total'],
  ['tv', 't'],
  ['twelve', 'twelfth', '12th'],
  ['van', 'then'],
  ['vane', 'vain', 'zhang'],
  ['vase', 'lace', 'face', 'dates', 'these'],
  ['vat', 'that'],
  ['vet', 'that'],
  ['vine', 'time', 'line', 'vi', 'vying'],
  ['violet', 'filed'],
  ['vote', 'vogt'],
  ['wag', 'rag'],
  ['waist', 'waste'],
  ['wait', 'weight'],
  ['walk', 'block'],
  ['walking', 'rocking'],
  ['wake', 'wait'],
  ['wash', 'ross', 'rush', 'watch'],
  ['wasps', 'was', 'wasp'],
  ['wassup', 'whatsapp'],
  ['wave', 'ways'],
  ['way', 'weigh'],
  ['weak', 'week'],
  ['wear', 'where'],
  ['wed', 'well', 'red'],
  ['weiss', 'wife'],
  ['were', "we're"],
  ['what', 'was', 'red', 'wet', 'rough', 'my'],
  ['where', 'we'],
  ["who's", 'whose'],
  ['wife', 'rice'],
  ['win', 'when'],
  ['wine', 'why'],
  ['wish', 'which', 'rich'],
  ['with', 'width'],
  ['word', 'wood'],
  ['yak', 'yet'],
  ['yam', 'yan'],
  ['yap', 'yup'],
  ['yoke', 'yolk'],
  ['you', 'your', "you're"],
  ['your', 'a'],
  ['yuan', 'jan', 'yen', 'yan', 'again', 'year', 'and', 'here', 'in', 'years', 'y'],
  ['yule', "you'll", 'you'],
  ['zit', 'is'],
  ['zone', 'zoom']
];

G.COMBINED_WORDS  = [
  ['delight', 'thelight'],
  ['across', 'across'],
  ['thoseare', 'dosa']
];

G.NUMBERS  = [
  ['one', '1'],
  ['two', '2'],
  ['three', '3'],
  ['four', '4'],
  ['five', '5'],
  ['six', '6'],
  ['seven', '7'],
  ['eight', '8'],
  ['nine', '9'],
  ['ten', '10'],
  ['eleven', '11'],
  ['twelve', '12'],
  ['thirteen', '13'],
  ['fourteen', '14'],
  ['fifteen', '15'],
  ['sixteen', '16'],
  ['seventeen', '17'],
  ['eighteen', '18'],
  ['nineteen', '19'],
  ['twenty', '20'],
  ['thirty', '30'],
  ['forty', '40'],
  ['fifty', '50'],
  ['sixty', '60'],
  ['seventy', '70'],
  ['eighty', '80'],
  ['ninety', '90']
];

// ====================================================================
G.IsSpeechWordRight = function(spoken, answer)
{
  if (spoken == answer)
  {
    return 1;
  }
  
  // Some microphones don't pick "s" sounds very well
  if (spoken.length 
    && spoken[spoken.length - 1] == 's' 
    && spoken.substr(0, spoken.length - 1) == answer
  )
  {
    return 1;
  }
  
  for (let i = 0, l = G.SPEECH_HOMOPHONES.length; i < l; i++)
  {
    let r = G.SPEECH_HOMOPHONES[i]
    
    if (r.indexOf(answer) != -1 && r.indexOf(spoken) != -1)
    {
      return -1;
    }
  }
  return 0;
}

// ====================================================================
G.CheckPronounciation = function(answer, onfinished, options)
{
  answer    = answer.trim();
  
  options   = options || {};
  
  if (!options.selector)
  {
    options.selector  = '.PronounciationStatus';
  }
  
  if (!options.timelimit)
  {
    options.timelimit = 5 + Math.round(answer.length / 8);
  }
  
  let displayel  = E(options.selector);
  
  if (!displayel)
  {
    P.Popup(
      `<div class="Center">
          <div class="PronounciationStatus"></div>
          <br>
          <div class="ButtonBar">
            <a class="Color1" onclick="P.CloseThisPopup(this)">${T.back}</a>
          </div>
        </div>
      `,
      {
        closeonmouseout:  1,
        noanimation:      1,
        parent:           options.parent,
        backgroundColor:  "#eee",
        extraclasses:     "Pad25 Rounded Raised",
        closefunc:        function()
        {
          P.CancelRecording();
        }
      }
    );
    
    displayel  = E('.PronounciationStatus');
  }
  
  if (P.platform == 'termux')
  {
    P.HTML(displayel, '<div class="greenb Pad50">' + T.speaknow + '</div>');
    
    P.API(
      '/learn/problemapi',
      {
        command:  'speechtotext'
      },
      function(d)
      {
        G.CheckPronounciationResults(displayel, answer, d, onfinished);
      }
    )
  } else
  {
    P.RecordOpus(
      displayel,
      function(displayel, typearray)
      {
        P.HTML(displayel, `<div class="blueb Pad50">${T.contactingserver}</div>`);
        
        let xhr = new XMLHttpRequest();
        
        xhr.onreadystatechange  = function(e)
        {
          if (xhr.readyState === XMLHttpRequest.DONE) 
          {
            let status = xhr.status;
            
            if (status === 0 || (status >= 200 && status < 400)) 
            {
              let response  = JSON.parse(xhr.responseText);
            
              G.CheckPronounciationResults(displayel, answer, response, onfinished);
            } else 
            {
              P.HTML(displayel, `<div class="Error Pad50">${T.errorcontactingserver}: ${status}</div>`);
              
              if (onfinished)
              {
                onfinished(status);
              }            
            }
          }
        }
        
        xhr.open("POST", '/learn/opustotext');
        xhr.send(typearray);
      },
      options.timelimit
    );
  }
}

// ====================================================================
G.SayThisWord = function(el)
{
  G.CheckPronounciation(
    el.innerText.trim(), 
    function()
    {
      
    },
    {
      parent:   el
    }
  );
}

// ====================================================================
G.RemovePunctuation = function(word)
{
  return word.toLowerCase().replaceAll('?', '').replaceAll('.', '').replaceAll(',', '').replaceAll('!', '').replaceAll('-', ' ');
}

// ====================================================================
G.ExpandNumbers = function(words)
{
  for (let i = 0; i < words.length; i++)
  {
    let currentword = words[i];
    
    if (IsNumber(currentword))
    {
      let wordstack = [];
      let parts     = currentword.split('');
      
      for (;;)
      {
        let number  = parts.shift();
        
        switch (parts.length)
        {
          case 0:
            switch (number)
            {
              case '1':
                wordstack.push('one');
                break;
              case '2':
                wordstack.push('two');
                break;
              case '3':
                wordstack.push('three');
                break;
              case '4':
                wordstack.push('four');
                break;
              case '5':
                wordstack.push('five');
                break;
              case '6':
                wordstack.push('six');
                break;
              case '7':
                wordstack.push('seven');
                break;
              case '8':
                wordstack.push('eight');
                break;
              case '9':
                wordstack.push('nine');
                break;
            };
            break;
          case 1:
            switch (number)
            {
              case '1':
                let nextnumber  = parts.shift();
                
                switch (nextnumber)
                {
                  case '0':
                    wordstack.push('ten');
                    break;
                  case '1':
                    wordstack.push('eleven');
                    break;
                  case '2':
                    wordstack.push('twelve');
                    break;
                  case '3':
                    wordstack.push('thirteen');
                    break;
                  case '4':
                    wordstack.push('fourteen');
                    break;
                  case '5':
                    wordstack.push('fifteen');
                    break;
                  case '6':
                    wordstack.push('sixteen');
                    break;
                  case '7':
                    wordstack.push('seventeen');
                    break;
                  case '8':
                    wordstack.push('eighteen');
                    break;
                  case '9':
                    wordstack.push('nineteen');
                    break;
                };                
                break;
              case '2':
                wordstack.push('twenty');
                break;
              case '3':
                wordstack.push('thirty');
                break;
              case '4':
                wordstack.push('forty');
                break;
              case '5':
                wordstack.push('fifty');
                break;
              case '6':
                wordstack.push('sixty');
                break;
              case '7':
                wordstack.push('seventy');
                break;
              case '8':
                wordstack.push('eighty');
                break;
              case '9':
                wordstack.push('ninety');
                break;
            };
            break;
          case 2:
            switch (number)
            {
              case '1':
                wordstack.push('one');
                wordstack.push('hundred');
                break;
              case '2':
                wordstack.push('two');
                wordstack.push('hundred');
                break;
              case '3':
                wordstack.push('three');
                wordstack.push('hundred');
                break;
              case '4':
                wordstack.push('four');
                wordstack.push('hundred');
                break;
              case '5':
                wordstack.push('five');
                wordstack.push('hundred');
                break;
              case '6':
                wordstack.push('six');
                wordstack.push('hundred');
                break;
              case '7':
                wordstack.push('seven');
                wordstack.push('hundred');
                break;
              case '8':
                wordstack.push('eight');
                wordstack.push('hundred');
                break;
              case '9':
                wordstack.push('nine');
                wordstack.push('hundred');
                break;
            };
            break;
          case 3:
            switch (number)
            {
              case '1':
                wordstack.push('one');
                wordstack.push('thousand');
                break;
              case '2':
                wordstack.push('two');
                wordstack.push('thousand');
                break;
              case '3':
                wordstack.push('three');
                wordstack.push('thousand');
                break;
              case '4':
                wordstack.push('four');
                wordstack.push('thousand');
                break;
              case '5':
                wordstack.push('five');
                wordstack.push('thousand');
                break;
              case '6':
                wordstack.push('six');
                wordstack.push('thousand');
                break;
              case '7':
                wordstack.push('seven');
                wordstack.push('thousand');
                break;
              case '8':
                wordstack.push('eight');
                wordstack.push('thousand');
                break;
              case '9':
                wordstack.push('nine');
                wordstack.push('thousand');
                break;
            };
            break;          
        }
        
        if (!parts.length)
        {
          break;
        }        
      }
      
      let newwords = words.slice(0, i);      
      newwords.extend(wordstack);
      newwords.extend(words.slice(i + 1));
      
      words = newwords;
      i += wordstack.length;
    }
  }
  
  return words;
}

// ====================================================================
G.CheckPronounciationResults = function(displayel, answer, response, onfinished)
{
  if (response.error)
  {
    P.HTML(displayel, `<div class="Error Pad50">${T.errorcontactingserver}: ${response.error}</div>`);
    return;
  }
  
  let answerwords = G.ExpandNumbers(answer.split(/\s/));
  let spokenwords = G.ExpandNumbers(response.output.split(/\s/));
  
  let displaywords  = [`
    <div class="Pad50 Center">
      ${T.computerthinksyousaid}
    </div>
    <div class="Center Bold purple">${response.output}</div>
    <div class="Pad50 Center">
      <audio controls=1>
        <source src="${response.uploadedfile}"></source>
      </audio>                
    </div>
    <div class="Flex FlexCenter FlexWrap">
  `];
  
  let numright  = 0;
  let numwrong  = 0;
  
  for (let i = 0; i < answerwords.length; i++)
  {
    let spokenword = spokenwords[i] ? G.RemovePunctuation(spokenwords[i]) : '';
    
    let answer  = '';
    
    if (answerwords[i])
    {
      answer  = G.RemovePunctuation(answerwords[i]);
    }
    
    let nextspoken  = spokenwords[i + 1] ? G.RemovePunctuation(spokenwords[i + 1]) : '';
    let nextanswer  = answerwords[i + 1] ? G.RemovePunctuation(answerwords[i + 1]) : '';
      
    // Special processing for some noisy microphones where the voice 
    // recognition model places a "the" at the very front. This removes 
    // the first "the" and moves the rest up one place.
    if (i == 0 
      && spokenwords.length > 1
      && spokenword == 'the' 
    )
    {
      if (G.IsSpeechWordRight(nextspoken, answer))
      {
        spokenword  = nextspoken;
        spokenwords.shift();
      }
    }
    
    for (let numbers of G.NUMBERS)
    {
      
      if (answer == numbers[0] && spokenword == numbers[1])
      {
        spokenword  = answer;
        break;
      }
    }
    
    // Special processing for converting time from "one o'clock" to 
    // "1:00" as Google speech recognition likes to do
    if (spokenword.indexOf(':') != -1)
    {
      let parts = spokenword.split(':');
      
      if (parts[1] == '00')
      {
        spokenwords.splice(i + 1, 0, "o'clock");
      }
      
      for (let numbers of G.NUMBERS)
      {
        if (parts[0] == numbers[1])
        {
          spokenword  = numbers[0];
        }
        
        if (parts[1] == numbers[1])
        {
          spokenwords.splice(i + 1, 0, numbers[0]);
        }
      }
    }
    
    // Special processing for split acronyms such as TV or PE
    // or combined homophones such as "delight" and "the light"
    if (nextspoken)
    {
      let combinedword  = spokenword + nextspoken;
      
      if (G.IsSpeechWordRight(combinedword, answer))
      {
        spokenword  = combinedword;
        
        spokenwords.splice(i + 1, 1);
      }
    }
    
    if (nextanswer)
    {
      let combinedword  = answer + nextanswer;
      
      if (G.IsSpeechWordRight(spokenword, combinedword))
      {
        answer  = combinedword;
        
        answerwords.splice(i + 1, 1);
      }
    }
    
    // Normal processing
    if (G.IsSpeechWordRight(spokenword, answer))
    {
      numright++;
      displaywords.push(`
        <div class="greeng Pad25">
          ${answerwords[i]}<br>
          ${answerwords[i]}
        </div>
      `);
    } else
    {
      numwrong++;
      displaywords.push(`
        <div class="redg Pad25">
          ${answerwords[i]}<br>
          ${spokenword}
        </div>
      `);
    }
  }
  
  displaywords.push(`
    </div>
    <div class="Center">
      <span class="green">+${numright}</span>
      <span class="red">-${numwrong}</span>
    </div>
  `);
  
  P.HTML(displayel, displaywords);
  
  if (onfinished)
  {
    onfinished('', displayel, numright, numwrong, displaywords);
  }  
}
