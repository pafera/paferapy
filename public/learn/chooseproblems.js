"use strict";

G.CARD_COURSE             =	0x1;
G.CARD_UNIT               = 0x2;

G.CARD_LESSON             =	0x4;
G.CARD_CHILD              = 0x1;

G.CARD_PROBLEM					   =	0x1;
G.CARD_QUIZ_PROBLEM		     =	0x2;
G.CARD_TEST_PROBLEM			   =	0x3;

G.CARD_COMPACT_VIEW			   =	0x200;
G.CARD_SHOW_ANSWERS			   =	0x400;
G.CARD_SHOW_EXPLANATIONS	 =	0x800;

G.ANSWER_TEACHER            = 0x08;

/* ******************************************************************* */
class ProblemChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.cardstack      = [];
    this.problems       = [];
    this.enablestacks   = 1;
    this.multipleselect = 1;
    
    this.showversus     = 0;
    
    this.GREETINGS = {
      answers:  [
        'Hi.',
        'Hello.',
        'Howdy.',
        'Hey.',
        'Wassup!'
      ]
    };
    
    this.GOODBYES  = {
      answers:  [
        'Bye.',
        'Goodbye.',
        'See you later.',
        'See you tomorrow.',
        'Good luck!',
        'Have a nice day!'
      ]
    };
    
  }

  // ------------------------------------------------------------------
  HasChildren(o)
  {
    return !(o.flags & G.CARD_LESSON);
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    if (self.cardstack.length)
    {
      P.LoadingAPI(
        resultsdiv,
        '/learn/cardapi',
        {
          command:      'linked',
          cardidcode:   self.cardstack[self.cardstack.length - 1],
          linktype:     G.CARD_CHILD
        },
        function(d)
        {
          self.available      = d.data;
          self.availablecount  = d.count;
          
          self.DisplayAvailable();
        }
      );
    } else
    {
      P.LoadingAPI(
        resultsdiv,
        '/learn/cardapi',
        {
          command:  'search',
          title:    searchterm,
          flags:    G.CARD_COURSE,
          start:    G.cardsearchstart,
          count:    100
        },
        function(d)
        {
          self.available      = d.data;
          self.availablecount = d.count;
          
          self.DisplayAvailable();
        }
      );
    }
  }

  // ------------------------------------------------------------------
  OnFinished()
  {
    let self  = this;
    
    if (!self.chosen.length)
    {
      return;
    }
    
    let cardidcodes = [];
    
    for (let r of self.chosen)
    {
      cardidcodes.push(r.idcode)
    }
    
    P.DialogAPI(
      '/learn/problemapi',
      {
        command:      'cardproblems',
        cardidcodes:  cardidcodes,
        linktype:     G.CARD_PROBLEM
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        P.RemoveFullScreen();
        self.problems  = d.data;
      }
    );
  }

  // ------------------------------------------------------------------
  ShowAllProblems()
  {
    let self  = this;
    
    if (!self.problems.length)
    {
      P.ErrorPopup('Please pick some problems first!');
      return;
    }
    
    P.AddFullScreen(
      'dpurpleg',
      '',
      `<div class="FlexGrid24 ShowAllProblems"></div>    
        <div class="ButtonBar">
          <a class="Color3" onclick="P.RemoveFullScreen()">Back</a>
        </div>    
      `
    );
    
    let ls  = [];
    
    for (let i = 0, l = self.problems.length; i < l; i++)
    {
      let r = self.problems[i];
      
      ls.push(`
        <div class="whiteb Raised Margin25 Pad25 Center ProblemCard">
          ${
            r.problem.image
              ? `<div>
                  ${P.ImgFile(r.problem.image, 'Square800 CardImage')}
                </div>`
              : ""
          }
          <div class="ProblemContent">${r.problem.content}</div>
          <div class="AnswerContent">${r.answer.content}</div>
          ${
            r.answer.sound
              ? `<div>
                  ${P.SoundFile(r.answer.sound, '', 'controls=1')}
                </div>`
              : ""
          }
        </div>
      `);
    }
    
    P.HTML('.ShowAllProblems', ls);
    
    P.AutoSetGridColumns('.ShowAllProblems', 24);
  }

  // ------------------------------------------------------------------
  ShowProblems(selector)
  {
    let self  = this;
    
    if (!self.problems.length)
    {
      P.ErrorPopup('Please pick some problems first!');
      return;
    }
    
    selector  = selector  || '.RandomProblemPopup';
    
    self.currentproblem    = 0;
    self.userandomproblem  = 0;
    self.useproblemimage   = 1;
    self.useproblemaudio   = 1;
    self.useproblemvideo   = 1;
    self.useproblemtext    = 1;
    self.useproblemanswer  = 0;
    
    let dialogcontent = `
    <div class="StudentVersusDisplay Flex FlexCenter FlexVertical">
      <div class="Headshots Flex FlexCenter FlexWrap">
        <img class="Square600 Margin25 Hidden VersusHeadshot Versus0Headshot" src="">
        <img class="Square600 Margin25 Hidden Border1 VersusHeadshot Versus1Headshot" src="">
        <img class="Square600 Margin25 Hidden Border2 VersusHeadshot Versus2Headshot" src="">
        <img class="Square600 Margin25 Hidden Border3 VersusHeadshot Versus3Headshot" src="">
        <img class="Square600 Margin25 Hidden Border4 VersusHeadshot Versus4Headshot" src="">
        <img class="Square600 Margin25 Hidden Border5 VersusHeadshot Versus5Headshot" src="">
        <img class="Square600 Margin25 Hidden Border6 VersusHeadshot Versus6Headshot" src="">
      </div>
      <div class="ButtonBar">
        <a class="PreviousVersus Color5 Pad50 Margin25 Rounded Raised" onclick="${self.reference}.ChangeVersus(-1)">&lt;</a>
        <select class="VersusMode" onchange="${self.reference}.SetVersusMode()">
          <option></option>
          <option value="sequential">Sequential</option>
          <option value="questionandanswer">Question and Answer</option>
          <option value="random">Random</option>
        </select>
        <a class="NextVersus Color5 Pad50 Margin25 Rounded Raised" onclick="${self.reference}.ChangeVersus(1)">&gt;</a>
      </div>
    </div>
    <div class="whiteb Raised Pad25">
      <div class="RandomProblem"></div>
      <div class="ButtonBar">
        <div class="ToggleButton ToggleProblemRandomButton lgrayg" onclick="${self.reference}.RandomizeProblems()">Random</div>
        <div class="ToggleButton ToggleProblemImageButton lgreeng" onclick="${self.reference}.ToggleProblemImage()">Image</div>
        <div class="ToggleButton ToggleProblemAudioButton lgreeng" onclick="${self.reference}.ToggleProblemAudio()">Audio</div>
        <div class="ToggleButton ToggleProblemVideoButton lgreeng" onclick="${self.reference}.ToggleProblemVideo()">Video</div>
        <div class="ToggleButton ToggleProblemTextButton lgreeng" onclick="${self.reference}.ToggleProblemText()">Text</div>
        <div class="ToggleButton ToggleProblemAnswerButton lgrayg" onclick="${self.reference}.ToggleProblemAnswer()">Answer</div>
      </div>
      <div class="ButtonBar ProblemNavButtonBar">
        <a class="FirstProblemButton" onclick="${self.reference}.ChangeProblem(-9999)">&lt;&lt;</a>
        <a class="PreviousFiveProblemsButton" onclick="${self.reference}.ChangeProblem(-5)">&lt;5</a>
        <a class="PreviousProblemButton" onclick="${self.reference}.ChangeProblem(-1)">&lt;</a>
        <a class="Color4" onclick="${self.reference}.PronounceProblem()">Check Pronounciation</a>
        <select class="SecondsToListen">
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>8</option>
          <option>10</option>
          <option>12</option>
          <option>15</option>
          <option>17</option>
          <option>20</option>
        </select>
        <a class="Color1" onclick="P.CloseThisPopup(this)">Back</a>
        <a class="Color2" onclick="${self.reference}.ToggleVersus()">Versus</a>
        <a class="NextProblemButton" onclick="${self.reference}.ChangeProblem(1)">&gt;</a>
        <a class="NextFiveProblemsButton" onclick="${self.reference}.ChangeProblem(5)">5&gt;</a>
        <a class="LastProblemButton" onclick="${self.reference}.ChangeProblem(9999)">&gt;&gt;</a>
      </div>    
      <div class="PronounciationStatus"></div>
    </div>
    `;
    
    if (!E(selector))
    {
      P.Popup(
        dialogcontent,
        {
          popupclass: selector.substr(1)            
        }
      );
      
      P.MakeDraggable(selector)
    } else
    {
      P.HTML(selector, dialogcontent);
    }
    
    P.Hide('.StudentVersusDisplay');
    
    self.ChangeProblem();
  }

  // ------------------------------------------------------------------
  ToggleVersus()
  {
    let self  = this;
    
    if (self.showversus)
    {
      P.Hide('.StudentVersusDisplay');
    } else
    {
      P.Show('.StudentVersusDisplay');
    }
    
    self.showversus = !self.showversus;
  }
  
  // ------------------------------------------------------------------
  SetVersusMode()
  {
    let self  = this;
    
    let mode        = E('.VersusMode').value;
    let students    = G.studentbar.students;
    
    self.versuspos      = -1;
    self.versustotal    = 0;
    self.versuspool     = [];
    
    for (let r of students)
    {
      if (r.flags & G.ANSWER_TEACHER)
      {
        continue;
      }
      
      if (!IsArray(self.versuspool[r.groupnum]))
      {
        self.versuspool[r.groupnum] = [];
      }
      
      self.versuspool[r.groupnum].push(r.idcode)
      
      self.versustotal++;
    }
    
    self.ChangeVersus(1);
  }  
  
  // ------------------------------------------------------------------
  ChangeVersus(diff)
  {
    let self  = this;
    
    self.versuspos  += diff;
    
    if (self.versuspos < 0)
    {
      self.versuspos  = 0;
    }
    
    let mode        = E('.VersusMode').value;
    let numgroups   = self.versuspool.length;
    
    switch (mode)
    {
      case 'sequential':
        for (let i = 0, l = 6; i <= l; i++)
        {
          if (self.versuspool[i])
          {
            let idcode  = self.versuspool[i][self.versuspos % self.versuspool[i].length];
            
            let el  = E('.Versus' + i + 'Headshot');
            
            el.src = `/system/headshots/${P.CodeDir(idcode)}.webp`;
          } 
        }
        break;
      case 'questionandanswer':
        let targetpos   = self.versuspos % self.versustotal;
        let currentpos  = 0;
        let foundall    = 0;
        
        P.Hide('.VersusHeadshot');
        
        P.RemoveClasses(
          '.VersusHeadshot', 
          [
            'Border1', 
            'Border2', 
            'Border3', 
            'Border4', 
            'Border5', 
            'Border6'
          ]
        );
        
        for (let grouppos = 0; grouppos < 99; grouppos++)
        {
          for (let i = 0, l = 6; i <= l; i++)
          {
            if (self.versuspool[i])
            {
              if (targetpos == currentpos)
              {
                let idcode  = self.versuspool[i][grouppos % self.versuspool[i].length];
                
                let el      = E('.Versus0Headshot');
                
                el.src = `/system/headshots/${P.CodeDir(idcode)}.webp`;
                el.classList.add('Border' + i);
                
                P.Show(el);
              } else if (currentpos == targetpos + 1)
              {
                let idcode  = self.versuspool[i][grouppos % self.versuspool[i].length];
                
                let el      = E('.Versus1Headshot');
                
                el.src = `/system/headshots/${P.CodeDir(idcode)}.webp`;
                el.classList.add('Border' + i);
                
                P.Show(el);
                
                foundall  = 1;
                break;
              }
              
              currentpos++;
            } 
          }
          
          if (foundall)
          {
            break;
          }
        }
        break;
      case 'random':
        let el  = E('.Versus0Headshot');
        
        for (;;)
        {
          let group   = RandElement(Keys(self.versuspool));
          let idcode  = RandElement(self.versuspool[group]);
          let codedir = P.CodeDir(idcode);
          
          if (el.src.indexOf(codedir) == -1)
          {
            el.src = `/system/headshots/${codedir}.webp`;
            break;
          }
        }
        
        P.Show(el);        
        break;
    }
  }
  
  // ------------------------------------------------------------------
  ChangeProblem(diff)
  {
    let self  = this;
    
    diff  = diff  || 0;
    
    self.currentproblem  += diff;
    
    if (self.showversus)
    {
      self.ChangeVersus(diff);
    }
    
    if (self.currentproblem < 0)
    {
      self.currentproblem  = 0;
    }
    
    if (self.currentproblem > self.problems.length - 1)
    {
      self.currentproblem  = self.problems.length - 1;
    }

    let ls  = [];
    let p   = self.userandomproblem 
      ? self.randomproblems[self.currentproblem] 
      : self.problems[self.currentproblem];
    
    self.currentanswer  = p.answer.content;
    
    ls.push(`
      <div class="whiteb Margin25 Pad25 Center ProblemCard">
        ${
          p.problem.image && self.useproblemimage
            ? `<div>
                ${P.ImgFile(p.problem.image, 'Square800 CardImage')}
              </div>`
            : ""
        }
        ${
          self.useproblemtext
            ? `<div class="dgreen Pad50 ProblemContent">${p.problem.content}</div>`
            : ''
        }
        ${
          self.useproblemanswer
            ? `<div class="dpurple Pad50 AnswerContent">${p.answer.content}</div>`
            : ''
        }
        ${
          p.answer.sound && self.useproblemaudio
            ? `<div>
                ${P.SoundFile(p.answer.sound, '', 'controls=1')}
              </div>`
            : ""
        }
      </div>
      <div class="Pad50 Center">${self.currentproblem + 1}/${self.problems.length}</div>
    `);
    
    P.HTML('.RandomProblem', ls);
    
    let firstproblembutton  = E('.FirstProblemButton');
    let previousfivebutton  = E('.PreviousFiveProblemsButton');
    let previousbutton      = E('.PreviousProblemButton');
    let nextbutton          = E('.NextProblemButton');
    let nextfivebutton      = E('.NextFiveProblemsButton');
    let lastbutton          = E('.LastProblemButton');
    
    if (self.currentproblem > 0)
    {
      firstproblembutton.classList.remove('dredg', 'lgrayg');
      firstproblembutton.classList.add('purpleg');
      previousbutton.classList.remove('dredg', 'lgrayg');
      previousbutton.classList.add('purpleg');
    } else
    {
      firstproblembutton.classList.remove('dredg', 'purpleg');
      firstproblembutton.classList.add('lgrayg');
      previousbutton.classList.remove('dredg', 'purpleg');
      previousbutton.classList.add('lgrayg');
    }
    
    if (self.currentproblem > 4)
    {
      previousfivebutton.classList.remove('dredg', 'lgrayg');
      previousfivebutton.classList.add('purpleg');
    } else
    {
      previousfivebutton.classList.remove('dredg', 'purpleg');
      previousfivebutton.classList.add('lgrayg');
    }
    
    if (self.currentproblem < self.problems.length - 1)
    {
      lastbutton.classList.remove('dredg', 'lgrayg');
      lastbutton.classList.add('purpleg');
      nextbutton.classList.remove('dredg', 'lgrayg');
      nextbutton.classList.add('purpleg');
    } else
    {
      lastbutton.classList.remove('dredg', 'purpleg');
      lastbutton.classList.add('lgrayg');
      nextbutton.classList.remove('dredg', 'purpleg');
      nextbutton.classList.add('lgrayg');
    }
    
    if (self.currentproblem < self.problems.length - 6)
    {
      nextfivebutton.classList.remove('dredg', 'lgrayg');
      nextfivebutton.classList.add('purpleg');
    } else
    {
      nextfivebutton.classList.remove('dredg', 'purpleg');
      nextfivebutton.classList.add('lgrayg');
    }
    
    let answertext  = E('.answerTextArea');
    
    if (answertext)
    {
      answertext.value  = p.answer.content;
    }
  }

  // ------------------------------------------------------------------
  RandomizeProblems()
  {
    let self  = this;
    
    self.userandomproblem  = !self.userandomproblem;
    
    let button  = E('.ToggleProblemRandomButton');
    
    if (self.userandomproblem)
    {
      self.randomproblems  = Clone(self.problems);
      Shuffle(self.randomproblems);
      
      button.classList.remove('dredg', 'lgrayg');
      button.classList.add('lgreeng');
    } else
    {
      button.classList.remove('dredg', 'lgreeng');
      button.classList.add('lgrayg');
    }
    
    self.ChangeProblem();
  }

  // ------------------------------------------------------------------
  ToggleProblemImage()
  {
    let self  = this;
    
    self.useproblemimage  = !self.useproblemimage;
    
    let button  = E('.ToggleProblemImageButton');
    
    if (self.useproblemimage)
    {
      button.classList.remove('dredg', 'lgrayg');
      button.classList.add('lgreeng');
    } else
    {
      button.classList.remove('dredg', 'lgreeng');
      button.classList.add('lgrayg');
    }
    
    self.ChangeProblem();
  }

  // ------------------------------------------------------------------
  ToggleProblemAudio()
  {
    let self  = this;
    
    self.useproblemaudio  = !self.useproblemaudio;
    
    let button  = E('.ToggleProblemAudioButton');
    
    if (self.useproblemaudio)
    {
      button.classList.remove('dredg', 'lgrayg');
      button.classList.add('lgreeng');
    } else
    {
      button.classList.remove('dredg', 'lgreeng');
      button.classList.add('lgrayg');
    }
    
    self.ChangeProblem();
  }

  // ------------------------------------------------------------------
  ToggleProblemVideo()
  {
    let self  = this;
    
    self.useproblemvideo  = !self.useproblemvideo;
    
    let button  = E('.ToggleProblemVideoButton');
    
    if (self.useproblemvideo)
    {
      button.classList.remove('dredg', 'lgrayg');
      button.classList.add('lgreeng');
    } else
    {
      button.classList.remove('dredg', 'lgreeng');
      button.classList.add('lgrayg');
    }
    
    self.ChangeProblem();
  }

  // ------------------------------------------------------------------
  ToggleProblemText()
  {
    let self  = this;
    
    self.useproblemtext  = !self.useproblemtext;
    
    let button  = E('.ToggleProblemTextButton');
    
    if (self.useproblemtext)
    {
      button.classList.remove('dredg', 'lgrayg');
      button.classList.add('lgreeng');
    } else
    {
      button.classList.remove('dredg', 'lgreeng');
      button.classList.add('lgrayg');
    }
    
    self.ChangeProblem();
  }

  // ------------------------------------------------------------------
  ToggleProblemAnswer()
  {
    let self  = this;
    
    self.useproblemanswer  = !self.useproblemanswer;
    
    let button  = E('.ToggleProblemAnswerButton');
    
    if (self.useproblemanswer)
    {
      button.classList.remove('dredg', 'lgrayg');
      button.classList.add('lgreeng');
    } else
    {
      button.classList.remove('dredg', 'lgreeng');
      button.classList.add('lgrayg');
    }
    
    self.ChangeProblem();
  }

  // ------------------------------------------------------------------
  PronounceProblem()
  {
    let self  = this;
    
    let p   = self.userandomproblem 
      ? self.randomproblems[self.currentproblem] 
      : self.problems[self.currentproblem];
      
    G.CheckPronounciation(
      p.answer.content, 
      function() {},
      {
        timelimit:  parseInt(E('.SecondsToListen').value)
      }
    );
  }
  
  // ------------------------------------------------------------------
  DialogGenerator()
  {
    let self  = this;
    
    if (IsEmpty(self.chosen))
    {
      P.ErrorPopup('Please pick some problems first!');
      return;
    }
    
    P.AddFullScreen(
      'dtealg',
      '',
      `<h2 class="Center">Dialog Generator</h2>
      <h3 class="Center">Available Students</h3>
      <div class="Pad25 Center SpeakerPool"></div>
      <h3 class="Center">Chosen Students</h3>
      <div class="Pad25 Center PickedSpeakers"></div>
      <div class="ButtonBar">
        <select class="DialogNumLessons">
          <option value="0">Use All Lessons</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
        </select>
        </select>
        <a class="Color1" onclick="${self.reference}.GenerateDialog()">${T.finished}</a>
      </div>
      <div class="DialogDisplay"></div>
      <div class="ButtonBar">
        <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
      </div>
      `
    );
    
    self.studentpool    = [];
    self.pickedstudents = [];
    
    for (let student of G.studentbar.students)
    {
      self.studentpool.push(student.idcode);
    }
    
    self.RenderStudentPicker();
    
    P.On(
      '.SpeakerPool',
      'click',
      function(e)
      {
        let el      = P.TargetClass(e, '.StudentPick');
        
        if (!el)
        {
          return;
        }
        
        let idcode  = el.dataset.idcode;
        
        el.remove();
        
        self.studentpool.remove(idcode);
        
        self.pickedstudents.push(idcode);
        
        self.RenderStudentPicker();
      }
    );
    
    P.On(
      '.PickedSpeakers',
      'click',
      function(e)
      {
        let el      = P.TargetClass(e, '.StudentPick');
        
        if (!el)
        {
          return;
        }
        
        let idcode  = el.dataset.idcode;
        
        el.remove();
        
        self.pickedstudents.remove(idcode);
        
        self.studentpool.push(idcode);
        
        self.RenderStudentPicker();
      }
    );
    
    let cardidcodes = [];
    
    for (let r of self.chosen)
    {
      cardidcodes.push(r.idcode);
    }
    
    P.LoadingAPI(
      '.DialogDisplay',
      '/learn/problemapi',
      {
        command:          'cardproblems',
        cardidcodes:      cardidcodes,
        linktype:         G.CARD_PROBLEM,
        separatebyclass:  1
      },
      function(d, resultsdiv)
      {
        self.dialogpool = [];
        
        for (let problemset of d.data)
        {
          let questions = [];
          let answers   = [];
          
          for (let problem of problemset)
          {
            let answertext  = problem.answer.content;
            
            if (answertext.indexOf('?') != -1)
            {
              questions.push(answertext);
            } else
            {
              answers.push(answertext);
            }
          }
          
          let textpool  = {answers: answers};
          
          if (questions.length)
          {
            textpool.questions  = questions;
          }
          
          self.dialogpool.push(textpool);
        }
        
        P.Toast(
          `<div class="greeng Center Pad50">
            ${T.ready}
          </div>
          `,
          resultsdiv
        );
      }
    );
  }
  
  // ------------------------------------------------------------------
  RenderStudentPicker()
  {
    let self  = this;
    
    let ls  = [];
    
    for (let studentid of self.studentpool)
    {
      ls.push(`
        <div class="InlineBlock Border20 HoverHighlight BackgroundCover Square400 Margin25 StudentPick" data-idcode="${studentid}"
          style="background-image: url('${P.HeadShotURL(studentid)}'); border-color: transparent;">
        </div>
      `);
    }
    
    P.HTML('.SpeakerPool', ls);
    
    ls  = [];
    
    for (let studentid of self.pickedstudents)
    {
      ls.push(`
        <div class="InlineBlock Border20 HoverHighlight Square400 Margin25 StudentPick" data-idcode="${studentid}">
          ${P.HeadShotImg(studentid, 'Square400')}
        </div>
      `);
    }
    
    P.HTML('.PickedSpeakers', ls);
  }
  
  // ------------------------------------------------------------------
  GenerateDialog()
  {
    let self  = this;
    
    if (!self.pickedstudents.length)
    {
      P.ErrorPopup('Please pick some students to speak.');
      return;
    }
    
    let colors  = [
      'dgreen',
      'dblue',
      'dorange',
      'dpurple',
      'dred'
    ];
    
    let numlessons  = parseInt(E('.DialogNumLessons').value);
    let dialogpool  = Clone(self.dialogpool);
    
    if (numlessons)
    {
      dialogpool  = Shuffle(dialogpool).slice(0, numlessons);
    }
    
    dialogpool.unshift(self.GREETINGS);
    dialogpool.push(self.GOODBYES);
    
    let ls  = ['<table class="Pad50 Width100P whiteb DialogTable">'];
    
    for (let i = 0, l = dialogpool.length; i < l; i++)
    {
      let problemset  = dialogpool[i];
      
      for (let j = 1, m = self.pickedstudents.length; j <= m; j++)
      {
        ls.push(`<tr>
            <td class="DialogIcon Width600">
              ${P.HeadShotImg(self.pickedstudents[j - 1], 'Square400')}
            </td>
            <td class="DialogText ${colors[j - 1]}">`);
        
        let nextquestions     = 0;
        
        if (i < l - 1)
        {
          nextquestions = dialogpool[i + 1].questions;
        }
        
        ls.push(RandElement(problemset.answers));
        
        if (problemset.questions)
        {
          
          if (j == 1)
          {
            ls.push(RandElement(problemset.questions));
          } else if (j == m)
          {
            if (nextquestions)
            {
              ls.push(' ' + RandElement(nextquestions));
            }
          } else
          {
            ls.push(' ' + RandElement(problemset.questions));
          }
        } else
        {
          if (j == m && nextquestions)
          {
            ls.push(' ' + RandElement(nextquestions));
          }
        }
        
        ls.push(`
            </td>
            <td class="HoverHighlight Width400 Center Bold grayb ScoreButton">0</td>
            <td class="Color2 Center" onclick="${self.reference}.RecognizeSpeech(this)">
              ${T.speak}
            </td>
          </tr>
        `);
      }
    }
    
    ls.push(`
        <tr>
          <td colspan="2"></td>
          <td class="blackb Center Bold FinalScore">0</td>
          <td></td>
        </tr>
      </table>
    `);
    
    P.HTML('.DialogDisplay', ls);
    
    P.On(
      '.DialogTable',
      'click',
      function(e)
      {
        let classlist = e.target.classList;
        
        if (classlist.contains('ScoreButton'))
        {
          let numpoints = e.target.parentNode.querySelector('.DialogText').innerText.split(' ').length;
          
          if (classlist.contains('redb'))
          {
            classlist.remove('redb');            
            classlist.add('blueb');
            
            e.target.innerText  = (numpoints * numpoints) + ' 🌟';            
          } else if (classlist.contains('greenb'))
          {
            classlist.remove('greenb');
            classlist.add('redb');
            
            e.target.innerText  = '-' + numpoints;
          } else
          {
            classlist.remove('grayb');
            classlist.add('greenb');
            
            e.target.innerText  = '+' + numpoints;
          }
        }
        
        let totalscore  = 0;
        
        for (let r of Q('.ScoreButton'))
        {
          totalscore  += parseInt(r.innerText);
        }
        
        E('.FinalScore').innerText  = totalscore;
      }      
    );
  }
  
  // ------------------------------------------------------------------
  RecognizeSpeech(td)
  {
    // Switch to text td
    td  = td.previousElementSibling.previousElementSibling;
    
    let text  = td.innerText.trim().replace('?', '').replace('!', '').replace(',', '').replace('.', '').replace('-', ' ');
    
    G.CheckPronounciation(
      text,
      function(status)
      {        
      },
      {
        timelimit:    2 + (text.length / 6),
        parent:       td
      }
    );
  }
}

// ********************************************************************
G.problemchooser  = new ProblemChooser({
  div:        '.ProblemChooser',
  reference:  'G.problemchooser',
  fields:     ['title']
});

