"use strict";

// ********************************************************************
class ClassCurriculumChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.multipleselect = 1;
    
    this.schoolid = config.schoolid;
    this.classid  = config.classid;
    
    this.extrabuttons = `
      <a class="Color4" onclick="${this.reference}.Add()">${T.addnew}</a>
      ${P.ObjectToSelect(G.CARD_TYPES, 'cardtype', 'CardType', '')}
      ${P.ObjectToSelect(G.CARD_PRICES, 'cardprice', 'CardPrice', '')}
    `;
    
    this.bgclass        = 'dpurpleg';
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      '.' + self.div + ' .AvailableCards',
      '/learn/cardapi',
      {
        command:      'search',
        schoolid:     self.schoolid,
        classid:      self.classid,
        keyword:      searchterm,
        flags:        G.CARD_COURSE,
        cardtype:     parseInt(E('.CardType').value),
        cardprice:    parseInt(E('.CardPrice').value)
      },
      function(d)
      {
        self.available        = d.data;
        self.chosen           = d.chosen;
        self.availablecount   = d.count;
        
        self.DisplayAvailable();
        self.DisplayChosen();
      }
    );
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    let self  = this;
    
    return `<div class="Icon Center">${P.ImgFile(r.image, 'Square400')}</div>
      <div class="Title Center" style="color: white;">${r.title}</div>
      <div class="Description Size80" style="color: #ddd;">${r.description}</div>
    `;
  }
  
  // ------------------------------------------------------------------
  OnFinished()
  {
    let self  = this;
    
    let cardids  = [];
    
    for (let i = 0, l = self.chosen.length; i < l; i++)
    {
      cardids.push(self.chosen[i].idcode);
    }
    
    P.DialogAPI(
      '/learn/classapi',
      {
        command:      'setcurriculum',
        schoolid:     self.schoolid,
        classid:      self.classid,
        cardids:      cardids
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        P.RemoveFullScreen();
        
        P.ForceReloadPage();
      }
    );    
  }  

  // ------------------------------------------------------------------
  Add()
  {
    let self  = this;
    
    let editfields  = [
      ['title', 'text', 0, 'Title'],
      ['description', 'text', 0, 'Description'],
      ['image', 'imagefile', 0, 'Image'],
      ['sound', 'soundfile', 0, 'Sound'],
      ['video', 'videofile', 0, 'Video'],
      ['content', 'multitext', 0, 'Content'],
      ['markdown', 'multitext', 0, 'Markdown'],
      ['html', 'multitext', 0, 'HTML']
    ];
    
    P.EditPopup(
      editfields,
      function(formdiv, data, resultsdiv, e)
      {
        data.flags  = G.CARD_COURSE;
        
        P.LoadingAPI(
          resultsdiv,
          '/learn/cardapi',
          {
            command:   'save',
            data:      data
          },
          function(d, resultsdiv)
          {
            let newcard = d.data;
            self.chosen.push(newcard);
            self.DisplayChosen();
            P.RemoveFullScreen();
          }
        );
      },
      {
        fullscreen:         1
      }
    );         
  }
}

/* ******************************************************************* */
class LessonChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.cardstack      = [];
    this.enablestacks   = 1;
    this.multipleselect = 1;
    this.parent         = config.parent;
    
    this.onfinished     =   function()
    {
      let self  = this;
      
      self.parent.obj.lessons = self.chosen;
      
      self.parent.RenderLessons();
      
      P.RemoveFullScreen();
    }
  }

  // ------------------------------------------------------------------
  HasChildren(o)
  {
    return !(o.flags & G.CARD_LESSON);
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    if (self.cardstack.length)
    {
      P.LoadingAPI(
        '.' + self.div + ' .AvailableCards',
        '/learn/cardapi',
        {
          command:      'linked',
          cardidcode:   self.cardstack[self.cardstack.length - 1],
          linktype:     G.CARD_CHILD
        },
        function(d)
        {
          self.available      = d.data;
          self.availablecount  = d.count;
          
          self.DisplayAvailable();
        }
      );
    } else
    {
      P.LoadingAPI(
        '.' + self.div + ' .AvailableCards',
        '/learn/cardapi',
        {
          command:  'search',
          title:    searchterm,
          flags:    G.CARD_COURSE,
          start:    start,
          count:    100
        },
        function(d)
        {
          self.available      = d.data;
          self.availablecount = d.count;
          
          self.DisplayAvailable();
        }
      );
    }
  }
}

/* ******************************************************************* */
class ProblemChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.problems       = [];
    this.multipleselect = 1;
    this.parent         = config.parent;
  }

  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
        '.' + self.div + ' .AvailableCards',
      '/learn/problemapi',
      {
        command:  'search',
        keyword:  searchterm,
        start:    start,
        count:    100
      },
      function(d)
      {
        self.available      = d.data;
        self.availablecount = d.count;
        
        self.DisplayAvailable();
      }
    );
  }

  // ------------------------------------------------------------------
  RenderItem(r)
  {
    return G.RenderProblem(r);
  }
  
  // ------------------------------------------------------------------
  OnFinished()
  {
    let self  = this;
    
    self.parent.obj.problems = self.chosen;
    
    self.parent.RenderProblems();
    
    P.RemoveFullScreen();
  }
}

// ********************************************************************
class Challenges extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.apiurl = '/learn/challengeapi';
    
    this.displayfields  = [
      ['title', 'text', T.title],
      ['challenge', 'text', T.challenge],
      ['lessons', 'text', T.lessons],
      ['problems', 'text', T.problems],
      ['starttime', 'timestamp', T.start],
      ['endtime', 'timestamp', T.stop],
      ['averagescore', 'int', T.averagescore],
      ['averagepercent', 'int', T.averagepercent],
      ['percenttried', 'int', T.percenttried],
      ['percentcomplete', 'int', T.percentcomplete]
    ];
    
    this.editfields  = [
      ['idcode', 'hidden', 0, T.idcode],
      [
        'challengetype', 
        'select', 
        0, 
        T.challengetype, 
        G.CHALLENGE_TYPES
      ],
      [
        'challenge', 
        'select', 
        0, 
        T.challenge, 
        G.CHALLENGE_NAMES
      ],      
      ['title', 'translation', 0, T.title, '', 'required'],
      ['lessonids', 'custom', 0, T.lessons],
      ['problemids', 'custom', 0, T.problems],
      ['flags', 'bitflags', 0, T.options,
        [
          [T.needanalysis, 'needanalysis', 0x01, 'unset'],
          [T.usestudylist, 'usestudylist', 0x02, 'unset']
        ]
      ],
      ['starttime', 'timestamp', 0, T.start],
      ['endtime', 'timestamp', 0, T.stop],
      ['minscore', 'int', 0, T.passingscore],
      ['minpercentage', 'int', 50, T.passingpercentage],
      ['numpoints', 'int', 50, T.pointsforpassing, '', 'required'],
      ['didnttrypenalty', 'int', 50, T.didnttrypenalty],
      ['numtries', 'int', 3, T.numtries, '', 'required'],
      ['numproblems', 'int', 6, T.numproblems],
      ['timelimit', 'int', 0, T.timelimitinseconds],
      ['instructions', 'translation', 0, T.instructions],
      ['question', 'translation', 0, T.question],
      ['image', 'imagefile', 0, T.images],
      ['sound', 'soundfile', 0, T.sounds],
      ['video', 'videofile', 0, T.videos],
      ['files', 'filelist', 0, T.files]
    ];
    
    this.showidcode     = 0;
    this.liststyles     = 'FlexGrid16'
    this.tablestyles    = 'Styled';
    this.cardstyles     = 'whiteb Margin25 Pad50 Raised Rounded';
    this.enablesearch   = 1;
    this.enableadd      = 1;
    this.enabledelete   = 1;
    this.enableedit     = 1;
    this.enabletable    = 1;
    this.limit          = 20;
    this.classid        = config.classid;
    
    this.extraparams  = {
      classid:   config.classid
    };    
    
    this.extradataparams  = {
      classid:   config.classid
    };

    this.extraactions.push([T.tryout, 'TryOut', 5]);    
    this.extraactions.push([T.delete, 'Delete', 1]);
    this.extraactions.push([T.analyze, 'Analyze', 4]);
    
    this.extrabuttons = `
      <a class="Color4" onclick="${this.reference}.AutoGenerate()">${T.autogenerate}</a>
    `;
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    let self  = this;
    
    let lessons   = [];
    
    if (!IsEmpty(r.lessons))
    {
      r.lessons.forEach(
        function(s)
        {
          lessons.push(s.title)
        }
      )
    }
    
    let problems  = [];
    
    if (!IsEmpty(r.problems))
    {
      r.problems.forEach(
        function(s)
        {
          problems.push(s.problem.content)
        }
      );
    }
    
    if (r.challenge == 'Write Answer')
    {
      r.title = {'en': T.questionanswers}
    }
    
    if (r.flags & G.USE_STUDY_LIST)
    {
      r.title = {'en': T.studylist}
    }    
    
    if (self.usetable)
    {
      return `
          <td class="Title">${P.BestTranslation(r.title)}</td>
          <td class="Challenge blue">${r.challenge}</td>
          <td class="Lessons purple">${lessons.join('<br>')}</td>
          <td class="Problems brown">${problems.join('<br>')}</td>
          <td class="StartTime green">${UTCToLocal(r.starttime * 1000)}</td>
          <td class="EndTime red">${UTCToLocal(r.endtime * 1000)}</td>
          <td class="AverageScore blue">${r.averagescore}</td>
          <td class="AveragePercent purple">${r.averagepercent}</td>
          <td class="PercentTried brown">${r.percenttried}</td>
          <td class="PercentComplete green">${r.percentcomplete}</td>
      `;
    } else
    {
      return `
          <div class="Title Center Size120 Pad50">${P.BestTranslation(r.title)}</div>
          <div class="Challenge blue Pad25">${r.challenge}</div>
          ${IsEmpty(lessons)
            ? ''
            : `<div class="Lessons purple Pad25">${lessons.join('<br>')}</div>`
          }
          ${IsEmpty(problems)
            ? ''
            : `<div class="Problems brown Pad25">${problems.join('<br>')}</div>`
          }
          
          <div class="StartTime green Pad25">${UTCToLocal(r.starttime * 1000)}</div>
          <div class="EndTime red Pad25">${UTCToLocal(r.endtime * 1000)}</div>
          
          <div class="Flex FlexCenter Pad50">
            <div class="AverageScore blue">🎯 ${r.averagescore}</div>
            <div class="AveragePercent purple">✅ ${r.averagepercent}%</div>
            <div class="PercentTried brown">🙂 ${r.percenttried}%</div>
            <div class="PercentComplete green">😁 ${r.percentcomplete}%</div>
          </div>
      `;
    }
  }
  
  // ------------------------------------------------------------------
  OnSaveData(obj)
  {
    if (obj.lessonids)
    {
      obj.lessonids = obj.lessonids.split('\n');
    }
    
    if (obj.problemids)
    {
      obj.problemids = obj.problemids.split('\n');
    }
  }
  
  // ------------------------------------------------------------------
  OnEditForm(obj)
  {
    let self  = this;
    
    self.obj  = obj;
    
    self.SetupProblemPickers();
  }
  
  // ------------------------------------------------------------------
  SetupProblemPickers()
  {
    let self  = this;
    
    P.HTML(
      '.lessonidsDiv', 
      `
        <div class="FlexGrid16 LessonTitles"></div>
        <div class="ButtonBar">
          <a class="Color4" onclick="${self.reference}.ChooseLessons()">${T.addlessons}</a>
        </div>
      `           
    );
    
    P.HTML(
      '.problemidsDiv', 
      `
        <div class="FlexGrid16 ProblemTitles"></div>
        <div class="ButtonBar">
          <a class="Color4" onclick="${self.reference}.ChooseProblems()">${T.addproblems}</a>
        </div>
      `
    );
    
    self.RenderLessons();
    self.RenderProblems();
  }
  
  // ------------------------------------------------------------------
  RenderLessons()
  {
    let self  = this;
    
    if (self.obj.lessons)
    {
      let ls  = [];
      let ids = [];
      
      self.obj.lessons.forEach(
        function(r)
        {
          ls.push(`
            <div class="LessonTitle Rounded Pad25 Margin25 whiteb">
              ${r.title}
            </div>
          `);
          
          ids.push(r.idcode);
        }
      );
      
      P.HTML('.LessonTitles', ls);
      
      E('.lessonids').value = ids.join('\n');
    } else
    {
      P.HTML('.LessonTitles', T.nothingfound);
      
      E('.lessonids').value = '';
    }
  }
  
  // ------------------------------------------------------------------
  RenderProblems()
  {
    let self  = this;
    
    if (self.obj.problems)
    {
      let ls  = [];
      let ids = [];
      
      self.obj.problems.forEach(
        function(r)
        {
          ls.push(G.RenderProblem(r, 'Margin25 whiteb'));
          
          ids.push(r.idcode);
        }
      );
      
      P.HTML('.ProblemTitles', ls);
      
      E('.problemids').value = ids.join('\n');
    } else
    {
      P.HTML('.ProblemTitles', T.nothingfound);
      
      E('.problemids').value = '';
    }
  }
  
  // ------------------------------------------------------------------
  ChooseLessons()
  {
    let self  = this;
    
    G.lessonchooser  = new LessonChooser({
      div:        '.LessonChooser',
      reference:  'G.lessonchooser',
      fields:     ['title'],
      classid:    self.classid,
      parent:     self
    });
    
    if (self.obj.lessons)
    {
      G.lessonchooser.chosen  = self.obj.lessons;
    }
    
    G.lessonchooser.Display();
  }
  
  // ------------------------------------------------------------------
  ChooseProblems()
  {
    let self  = this;
    
    G.problemchooser  = new ProblemChooser({
      div:        '.ProblemChooser',
      reference:  'G.problemchooser',
      classid:    self.classid,
      parent:     self
    });
    
    if (self.obj.problems)
    {
      G.problemchooser.chosen  = self.obj.problems;
    }
    
    G.problemchooser.Display();
  }
  
  // ------------------------------------------------------------------
  TryOut(card, dbid)
  {
    let self  = this;
    let obj   = dbid ? self.GetItemByID(dbid) : 0;
    
    if (!obj)
    {
      return;
    }
        
    window.open(`/learn/challenges.html?classid=${self.classid}&challengeid=${obj.idcode}&practice=1`);
  }
  
  // ------------------------------------------------------------------
  AutoGenerate()
  {
    let self  = this;
    
    self.obj  = {};
    
    P.EditPopup(
      [
        ['starttime', 'timestamp', 0, T.start],
        ['endtime', 'timestamp', 0, T.stop],
        [
          'repeattime', 
          'select', 
          0, 
          T.challengelengthinhours, 
          [
            1,
            2,
            3,
            4,
            5,
            6,
            8,
            12,
            24,
            48,
            72,
            96,
            120,
            144,
            168
          ]
        ],
        [
          'challenge', 
          'select', 
          0, 
          T.challenge, 
          G.CHALLENGE_NAMES
        ],
        [
          'passingcriteria', 
          'select', 
          0, 
          T.passingcriteria, 
          G.PASSING_CRITERIA
        ],        
        ['minpassing', 'int', 0, T.startingcriteria, '', 'required'],
        ['maxpassing', 'int', 0, T.endingcriteria, '', 'required'],
        ['numpoints', 'int', 50, T.pointsforpassing, '', 'required'],
        ['didnttrypenalty', 'int', 50, T.didnttrypenalty],
        ['lessonids', 'custom', 0, T.lessons],
        ['problemids', 'custom', 0, T.problems],
        [
          'problemselect', 
          'select', 
          0, 
          T.selectproblems, 
          G.SELECT_PROBLEMS
        ],        
        ['numtries', 'int', 3, 'Number of Tries', '', 'required'],
        ['numproblems', 'int', 6, 'Number of Problems to Use'],
        ['timelimit', 'int', 0, 'Time Limit in Seconds']      
      ],
      function(formdiv, data, resultsdiv, e)
      {
        let starttime   = Date.parse(data.starttime) / 1000;
        let endtime     = Date.parse(data.endtime) / 1000;
        let repeattime  = parseInt(data.repeattime) * 3600;
                
        let numchallenges = Math.ceil((endtime - starttime) / repeattime);
        
        let minpassing          = parseInt(data.minpassing);
        let numpoints           = parseInt(data.numpoints);
        let didnttrypenalty     = parseInt(data.didnttrypenalty);
        let numtries            = parseInt(data.numtries);
        let numproblems         = parseInt(data.numproblems);
        let timelimit           = parseInt(data.timelimit);
        
        let passingstep   = (parseInt(data.maxpassing) - minpassing) / numchallenges;
        
        let lessonids     = data.lessonids.split('\n');
        let problemids    = data.problemids.split('\n');
        
        if (data.problemselect == 'Randomly')
        {
          lessonids   = Shuffle(lessonids);
          problemids  = Shuffle(problemids);
        }
        
        for (let i = 0, l = numchallenges; i < l; i++)
        {
          let o = {
            classid:              self.classid,
            title:                T.review,
            starttime:            new Date((starttime + (i * repeattime)) * 1000).toISOString(),
            endtime:              new Date((starttime + ((i + 1) * repeattime)) * 1000).toISOString(),
            challenge:            data.challenge,
            numpoints:            numpoints,
            didnttrypenalty:      didnttrypenalty,
            numtries:             numtries,
            numproblems:          numproblems,
            timelimit:            timelimit,
            challengetype:        G.CHALLENGE_HOMEWORK
          };
          
          if (data.passingcriteria == 'Score')
          {
            o.minscore       = minpassing + (i * passingstep);
          } else
          {
            o.minpercentage  = minpassing + (i * passingstep);
          }
          
          if (lessonids.length)
          {
            if (data.problemselect == 'Use All')
            {
              o.lessonids = lessonids;
              o.title     = '全部复习！';
            } else
            {
              o.lessonids = [ lessonids[i % lessonids.length] ];
              
              for (let j = 0, m = self.obj.lessons.length; j < m; j++)
              {
                let lesson  = self.obj.lessons[j];
                
                if (lesson.idcode == o.lessonids[0])
                {
                  o.title = lesson.title;
                  break;
                }
              }
            }
          }
          
          if (problemids.length)
          {
            o.problemids = Shuffle(problemids).slice(0, numproblems);
          }
          
          P.LoadingAPI(
            resultsdiv,
            '/learn/challengeapi',
            {
              command:  'save',
              data:     o
            }
          );
        }
        
        // Sometimes it takes a while for the database server to complete
        setTimeout(
          function()
          {
            self.List();
            P.RemoveFullScreen();
          },
          2000
        );
      },
      {
        fullscreen:     1,
        headertext:     `<h2>Multiple Challenges Helper</h2>`
      }
    );
    
    self.SetupProblemPickers();
  }
  
  // ------------------------------------------------------------------
  Analyze(card, dbid)
  {
    let self  = this;
    let obj   = dbid ? self.GetItemByID(dbid) : 0;    
    
    G.ViewChallengeAnalysis(self.classid, dbid, escape(obj ? P.BestTranslation(obj.title) : ''));
  }
}

// ********************************************************************
class StudyList extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.apiurl = '/learn/studylistapi';
    
    this.displayfields  = [
      ['title', 'text', T.title]
    ];
    
    this.editfields  = []

    this.tablestyles    = 'Styled';
    this.cardstyles     = 'Pad25 Margin25 whiteb Raised Rounded Width2000 MinHeight1600';
    this.enablesearch   = 1;
    this.enableadd      = 1;
    this.enabledelete   = 1;
    this.limit          = 20;
    this.classid        = config.classid;
    this.studentid      = config.studentid;
    
    this.extraparams  = {
      classid:    config.classid,
      studentid:  config.studentid
    };    
    
    this.extradataparams  = {
      classid:    config.classid,
      studentid:  config.studentid
    };

    this.extraactions.push([T.delete, 'Delete', 1]);
    
    this.listonchangeselectors  = '.StudentID';
  }

  // ------------------------------------------------------------------
  OnListParams(params)
  {
    let studentid = E('.StudentID');
    
    if (studentid)
    {
      params.studentid  = studentid.value;
    }    
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    let self  = this;
    
    let ls  = [];
    
    let color = 'whiteb';
    
    if (r.score > 0)
    {
      color = 'greenb';
    } else if (r.score < 0)
    {
      color = 'redb';
    }
    
    let extracontent  = [
      `<table class="Margin50 Width90P Rounded ">
        <tr class="${color}">
          <th>${T.score}</th>
          <td>${r.score}</td>
        </tr>
      `
    ];
    
    if (!IsEmpty(r.results))
    {
      let times = Keys(r.results);
      
      times.sort();
      
      times.forEach(
        function(t)
        {
          let score = r.results[t];
          
          if (score > 0)
          {
            color = 'greenb';
          } else if (score < 0)
          {
            color = 'redb';
          }
          
          extracontent.push(`
            <tr class="${color}">
              <th>${new Date(t * 1000).toISOString().substr(0, 19).replace('T', ' ')}</th>
              <td>${score}</td>
            </tr>            
          `);
        }
      );
    }
    
    extracontent.push('</table>');
    
    return G.RenderProblem(
      r.problem, 
      '',
      extracontent.join("\n")
    );
  }
  
  // ------------------------------------------------------------------
  Add()
  {
    let self  = this;
    
    let studentid = '';
    
    if (E('.StudentID'))
    {
      studentid = E('.StudentID').value;
    }  
    
    G.lessonchooser  = new LessonChooser({
      div:        '.LessonChooser',
      reference:  'G.lessonchooser',
      fields:     ['title'],
      classid:    self.classid
    });
    
    G.lessonchooser.onfinished  = function()
    {
      let chooser  = this;
      
      let newlessons  = [];
      
      chooser.chosen.forEach(
        function(r)
        {
          newlessons.push(r.idcode);
        }
      )
      
      P.LoadingAPI(
        '.LessonChooserResults',
        '/learn/studylistapi',
        {
          command:              'save',
          classid:              self.classid,
          studentid:            studentid,
          cardids:              newlessons
        },
        function(d, resultsdiv)
        {
          P.RemoveFullScreen();
          self.List();
        }
      );    
    }
    
    G.lessonchooser.Display();    
  }
}

// ********************************************************************
class QuestionAnswers extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.displayfields  = [
      ['question', 'translation', T.question],
      ['starttime', 'timestamp', T.start],
      ['endtime', 'timestamp', T.stop],
      ['averagescore', 'int', T.averagescore],
      ['averagepercent', 'int', T.averagepercent],
      ['percenttried', 'int', T.percenttried],
      ['percentcomplete', 'int', T.percentpassed]
    ];
    
    this.editfields  = [
      ['idcode', 'hidden', 0, T.idcode],
      ['starttime', 'timestamp', 0, T.start],
      ['endtime', 'timestamp', 0, T.stop],
      ['question', 'translation', 0, T.question],
      ['image', 'imagefile', 0, T.images],
      ['sound', 'soundfile', 0, T.sounds],
      ['video', 'videofile', 0, T.videos],
      ['files', 'filelist', 0, T.files]
    ];

    this.tablestyles    = 'Styled';
    this.cardstyles     = 'Pad25 Margin25 Pad50 Raised Rounded Width2000 MinHeight1600';
    this.enablesearch   = 1;
    this.enableadd      = 1;
    this.enableedit     = 1;
    this.enabledelete   = 1;
    this.limit          = 20;
    this.classid        = config.classid;
    
    this.extraactions.push([T.delete, 'Delete', 1]);    
    this.extraactions.push([T.viewanswers, 'ViewAnswers', 5]);
    this.extraactions.push([T.analyze, 'Analyze', 4]);
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    let self  = this;
    
    if (self.usetable)
    {
      return `
          <td class="Question">${P.BestTranslation(r.question)}</td>
          <td class="StartTime green">${UTCToLocal(r.starttime * 1000)}</td>
          <td class="EndTime red">${UTCToLocal(r.endtime * 1000)}</td>
          <td class="AverageScore blue">${r.averagescore}</td>
          <td class="AveragePercent purple">${r.averagepercent}</td>
          <td class="PercentTried brown">${r.percenttried}</td>
          <td class="PercentComplete green">${r.percentcomplete}</td>
      `;
    } else
    {
      return `
          <div class="Question Center Size120">${P.BestTranslation(r.question)}</div>
          <div class="StartTime green">${UTCToLocal(r.starttime * 1000)}</div>
          <div class="EndTime red">${UTCToLocal(r.endtime * 1000)}</div>
          
          <table class="Width100P Left Margin25 Bordered Pad25">
            <tr>
              <th>${T.averagescore}</th>
              <td>${r.averagescore}</td>
            </tr>
            <tr>
              <th>${T.averagepercent}</th>
              <td>${r.averagepercent}</td>
            </tr>
            <tr>
              <th>${T.percenttried}</th>
              <td>${r.percenttried}</td>
            </tr>
            <tr>
              <th>${T.percentcomplete}</th>
              <td>${r.percentcomplete}</td>
            </tr>
          </table>
      `;
    }
  }
  
  // ------------------------------------------------------------------
  OnList(resultsdiv, start, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/learn/challengeapi',
      {
        command:        'search',
        classid:        self.classid,
        challenge:      'Write Answer'
      },
      function(d, resultsdiv)
      {
        self.items    = d.data;
        self.numitems = d.count;
        
        self.DisplayItems();
      }
    );    
  }
  
  // ------------------------------------------------------------------
  OnSave(formdiv, data, resultsdiv, e)
  {
    let self  = this;
    
    if (IsEmpty(data.question))
    {
      P.ErrorPopup(T.missingquestion);
      return;
    }
    
    data.classid          = this.classid;
    data.challenge        = 'Write Answer';
    data.challengetype    = G.CHALLENGE_HOMEWORK;
    data.numtries         = 9999;
    data.didnttrypenalty  = 0;
    data.title            = T.questionanswers;
    
    if (data.files)
    {
      data.files  = data.files.split('\n');
    }
    
    P.LoadingAPI(
      resultsdiv,
      '/learn/challengeapi',
      {
        command:        'save',
        data:           data
      },
      function(d, resultsdiv)
      {
        P.RemoveFullScreen();
        self.List();
      }
    );    
  }
  
  // ------------------------------------------------------------------
  OnDelete(dbids, resultsdiv)
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/learn/challengeapi',
      {
        command:        'delete',
        classid:        self.classid,
        ids:            dbids
      },
      function(d, resultsdiv)
      {
        self.List();
      }
    );    
  }
  
  // ------------------------------------------------------------------
  ViewAnswers(card, dbid)
  {
    let self  = this;
    
    self.classinfo = G.GetClassInfo(self.classid);
    
    P.AddFullScreen(
      'dgrayg',
      '',
      `<h1>${T.questionanswers}: ${P.BestTranslation(self.classinfo.displayname)}</h1>
      <div class="AnswersList"></div>
      <div class="ButtonBar">
        <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
      </div>
      `
    );
    
    self.DisplayAnswers(dbid);
  }
    
  // ------------------------------------------------------------------
  DisplayAnswers(challengeid)
  {
    let self  = this;
    
    self.challengeid  = challengeid;
    
    P.LoadingAPI(
      '.AnswersList',
      '/learn/challengeapi',
      {
        command:        'getanswers',
        challengeid:    challengeid
      },
      function(d, resultsdiv)
      {
        let answers = d.data;
        
        let ls  = [];
        
        for (let k in self.classinfo.students)
        {
          let studentname = P.BestTranslation(self.classinfo.students[k]);
          let answer      = (k in answers) ? answers[k] : 0;
          
          ls.push(`
            <div class="Margin25 Pad50 ${(answer && answer.answer && answer.answer.content) ? 'greenb' : 'redb'} StudentCard">
              <table class="Width100P">
                <tr>
                  <td class="Width400 Center Color5 Raised Rounded" rowspan="2" onclick="${self.reference}.SetPoints(this, '${k}')">
                    ${P.HeadShotImg(k, 'Square400')}<br>
                    <div class="NumPoints">${answer ? answer.points : ''}</div>
                  </td>
                  <td class="StudentName Center">
                    ${studentname}
                  </td>
                </tr>
                <tr>
                  <td>
                    ${answer ? G.RenderCard(answer.answer) : ''}
                  </td>
                </tr>
              </table>
            </div>
          `);
        }
        
        P.HTML(resultsdiv, ls);
      }
    );    
  }
  
  // ------------------------------------------------------------------
  SetPoints(card, dbid)
  {
    let self  = this;
    
    card  = P.Parent(card, '.StudentCard');
    
    let studentname = card.querySelector('.StudentName').innerText;
    let pointsel    = card.querySelector('.NumPoints');
    
    let color       = '';
    let pointitems  = []
    
    for (let i = -100; i <= 100; i++)
    {
      if (i < 0)
      {
        color = 'redb';
      } else if (i > 0)
      {
        color = 'greenb';
      } else
      {
        color = 'whiteb';
      }
      
      pointitems.push([i, color]);
    }
    
    P.ShowFullScreenPicker(
      `${T.points}: ${studentname}`,
      pointitems,
      function(item, resultsdiv)
      {
        P.LoadingAPI(
          resultsdiv,
          '/learn/answersapi',
          {
            command:      'setpoints',
            challengeid:  self.challengeid,
            studentid:    dbid,
            points:       item
          },
          function(d)
          {
            pointsel.innerText  = item;
            P.RemoveFullScreen();
          }
        );
      }
    );
  }
  
  // ------------------------------------------------------------------
  Analyze(card, dbid)
  {
    let self  = this;
    let obj   = dbid ? self.GetItemByID(dbid) : 0;    
    
    G.ViewChallengeAnalysis(
      self.classid, 
      dbid, 
      escape(
        obj 
        ? P.BestTranslation(obj.title) 
        : ''
      )
    );
  }
}

// ********************************************************************
class ClassFinder extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.apiurl = '/learn/classapi';
    
    this.displayfields  = [
      ['title', 'text', T.title]
    ];
    
    this.editfields  = []

    this.tablestyles    = 'Styled';
    this.liststyles     = 'FlexGrid16';
    this.cardstyles     = 'Pad25 Margin25 whiteb Raised Rounded';
    this.enablesearch   = 1;
    
    this.extraactions.push(['Apply', 'Apply', 4]);
  }

  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    let self  = this;
    
    return `
      <div class="Center Icon">
        ${P.ImgFile(r.icon, 'Width1200')}
      </div>
      <h3 class="Center black">${P.BestTranslation(r.displayname)}</h3>
      <div class="green">${P.BestTranslation(r.description)}</div>
      <div class="purple Pad25">${P.BestTranslation(r.teachers)}</div>
      <div class="brown Pad25">${P.BestTranslation(r.schedule)} ${P.TIMEZONES[r.resettimezone]}</div>
      <div class="blue Pad25">${P.BestTranslation(r.schoolname)}</div>
      <div class="red Pad25">${r.coursenum}</div>
      <div class="dorange Center">${r.numstudents} / ${r.capacity}</div>
    `
  }
  
  // ------------------------------------------------------------------
  Apply(cardelement, dbid)
  {
    let self  = this;
    
    let schoolid  = 0;
    
    for (let r of self.items)
    {
      if (r.idcode == dbid)
      {
        schoolid  = r.schoolid;
      }
    }
    
    if (schoolid)
    {
      G.ApplyToClass(schoolid, classid);
    } else
    {
      P.ErrorPopup('Missing schoolid for ' + dbid);
    }
  }
}

// ====================================================================
G.ApplyToClass  = function(schoolid, classid)
{
  P.InputPopup(
    T.typereasonforapplying,
    function(reason)
    {
      if (reason)
      {
        P.DialogAPI(
          '/learn/schoolapi',
          {
            command:    'apply',
            schoolid:   schoolid,
            classid:    classid,
            comment:    reason
          },
          function(d, resultsdiv, popupclass)
          {
            if (d.approved)
            {
              P.HTML(resultsdiv, `<div class="greeng Pad50">${T.autoapproved}</div>`);
            } else
            {
              P.HTML(resultsdiv, `<div class="greeng Pad50">${T.applicationsuccessful}</div>`);
            }
            
            setTimeout(
              function()
              {
                P.ClosePopup(popupclass);
                
                if (d.approved)
                {
                  P.ForceReloadPage();
                }
              },
              2000
            );
          }
        );
      } else
      {
        P.ErrorPopup(T.missingfields);
      }
    }
  );
}

// ====================================================================
G.ViewChallengeAnalysis  = function(classid, challengeid, title, reanalyze = 0)
{
  title = title || '';
  
  title = unescape(title);
  
  P.AddFullScreen(
    'dpurpleg',
    '',
    `<h2 class="Center">Challenge Analysis: ${title}</h2>
    <div class="ChallengeAnalysis"></div>`
  );
  
  P.LoadingAPI(
    '.ChallengeAnalysis',
    '/learn/challengeapi',
    {
      command:      'analyze',
      classid:      classid,
      challengeid:  challengeid,
      reanalyze:    reanalyze
    },
    function(d, resultsdiv)
    {
      let obj       = d.data;
      
      let passed    = [];
      let failed    = [];
      let didnttry  = [];
      
      for (let k in obj.results)
      {
        let r = obj.results[k];
        
        switch (r[1])
        {
          case G.CHALLENGE_DIDNT_TRY:
            didnttry.push([
              k,
              r[0],
              r[2],
              r[3]
            ]);
            break;
          case G.CHALLENGE_LAZY_TRIED:
          case G.CHALLENGE_TRIED:
            failed.push([
              k,
              r[0],
              r[2],
              r[3]
            ]);
            break;
          default:
            passed.push([
              k,
              r[0],
              r[2],
              r[3]
            ]);
        }
      }
      
      passed.sort((a, b) => StrCmpI(P.BestTranslation(a[1]), P.BestTranslation(b[1])));
      failed.sort((a, b) => StrCmpI(P.BestTranslation(a[1]), P.BestTranslation(b[1])));
      didnttry.sort((a, b) => StrCmpI(P.BestTranslation(a[1]), P.BestTranslation(b[1])));
      
      let ls  = [
        `
        <h3>Passed</h3>
        `
      ];
      
      if (passed.length)
      {
        ls.push(`<div class="FlexGrid8 Center">`);
        
        passed.forEach(
          (r) => ls.push(`
            <div class="StudentHeadShot Margin25">
              ${P.HeadShotImg(r[0], 'Square400')}<br>
              ${P.BestTranslation(r[1])}<br>
              +${r[2]}<br>
              ${r[3]}%
            </div>
          `)
        )
        
        ls.push('</div>');
      } else
      {
        ls.push(`<div class="Center">${T.nothingfound}</div>`)
      }
      
      ls.push('<h3>Failed</h3>');
      
      if (failed.length)
      {
        ls.push(`<div class="FlexGrid8 Center">`);
        
        failed.forEach(
          (r) => ls.push(`
            <div class="StudentHeadShot Margin25">
              ${P.HeadShotImg(r[0], 'Square400')}<br>
              ${P.BestTranslation(r[1])}<br>
              +${r[2]}<br>
              ${r[3]}%
            </div>
          `)
        )
        
        ls.push('</div>');
      } else
      {
        ls.push(`<div class="Center">${T.nothingfound}</div>`)
      }
      
      ls.push(`<h3>Didn't Try</h3>`);
      
      if (didnttry.length)
      {
        ls.push(`<div class="FlexGrid8 Center">`);
        
        didnttry.forEach(
          (r) => ls.push(`
            <div class="StudentHeadShot Margin25">
              ${P.HeadShotImg(r[0], 'Square400')}<br>
              ${P.BestTranslation(r[1])}
            </div>
          `)
        )
        
        ls.push('</div>');
      } else
      {
        ls.push(`<div class="Center">${T.nothingfound}</div>`)
      }
      
      ls.push(`<h3>Right Problems</h3>`);
      
      let problemids  = Keys(obj.analysis.rightproblems);
      
      if (problemids.length)
      {
        ls.push('<table class="Width100P Pad25 Center">');
        
        problemids.forEach(
          function(r)
          {
            let p = obj.analysis.rightproblems[r];
            
            ls.push(`<tr>
              <th>
                ${p.problemtext}
              </th>
              <td class="FlexGrid8">
            `);
            
            p.studentids.forEach(
              (r) => ls.push(`
                <div class="StudentHeadShot Margin25">
                  ${P.HeadShotImg(r, 'Square400')}
                </div>`
              )
            );
            
            ls.push('</tr>');
          }
        );
        
        ls.push('</table>');
      } else
      {
        ls.push(`<div class="Center">${T.nothingfound}</div>`)
      }
      
      ls.push(`<h3>Wrong Problems</h3>`);
      
      problemids  = Keys(obj.analysis.wrongproblems);
      
      if (problemids.length)
      {
        ls.push('<table class="Width100P Pad25 Center">');
        
        problemids.forEach(
          function(r)
          {
            let p = obj.analysis.wrongproblems[r];
            
            ls.push(`<tr>
              <th>
                ${p.problemtext}
              </th>
              <td class="FlexGrid8">
            `);
            
            p.studentids.forEach(
              (r) => ls.push(`
                <div class="StudentHeadShot Margin25">
                  ${P.HeadShotImg(r, 'Square400')}
                </div>`
              )
            );
            
            ls.push('</tr>');
          }
        );
        
        ls.push('</table>');
      } else
      {
        ls.push(`<div class="Center">${T.nothingfound}</div>`)
      }
      
      ls.push(`
        <div class="ButtonBar">
          <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
        </div>
      `);
      
      P.HTML(resultsdiv, ls);
    }
  );
}

// ====================================================================
G.ViewChallenges  = function(classid)
{
  P.AddFullScreen(
    'dpurpleg',
    '',
    `<div class="Challenges"></div>
    <div class="ButtonBar">
      <a class="Color3" onclick="P.RemoveFullScreen()">Finished</a>
    </div>    
  `);
  
  let classinfo = G.GetClassInfo(classid);
    
  G.challenges = new Challenges({
    div:        '.Challenges',
    reference:  'G.challenges',
    classid:    classid,
    title:      `<h2>${T.challenges}: ${P.BestTranslation(classinfo.displayname)}</h2>`
  });
  
  G.challenges.Display();
  G.challenges.List();
}  

// ====================================================================
G.ViewGrades  = function(classid, isteacher)
{
  let self  = this;
  
  let classinfo = G.GetClassInfo(classid);
  
  let ls  = [`<h1>${T.grades}: ${P.BestTranslation(classinfo.displayname)}</h1>
    <div class="Grades"></div>
    <div class="ButtonBar">
      <a class="Color3" onclick="P.RemoveFullScreen()">${T.back}</a>
    </div>    
  `];
  
  P.AddFullScreen(
    'dgreeng',
    '',
    ls
  );
  
  setTimeout(
    function()
    {
      G.DisplayGrades(classid, isteacher);
    },
    500
  );
}


// ====================================================================
G.DisplayGrades  = function(classid, isteacher)
{
  P.LoadingAPI(
    '.Grades',
    '/learn/challengeapi',
    {
      command:              'getgrades',
      classid:              classid
    },
    function(d, resultsdiv)
    {
      let grades  = d.data;
      
      if (!grades.length)
      {
        P.HTML(resultsdiv, T.nothingfound);
      } else
      {
        let classinfo = G.GetClassInfo(classid);
        
        let ls  = [`
          <table class="Styled Center Pad50 Width100P">
            <tr>
              <th>
                ${T.student}
              </th>
              <th>
                ${T.classscore}
              </th>
              <th>
                ${T.reviewscore}
              </th>
              <th>
                ${T.persistencescore}
              </th>
              <th>
                ${T.classpercent}
              </th>
              <th>
                ${T.reviewpercent}
              </th>
              <th>
                ${T.persistencepercent}
              </th>
              <th>
                ${T.finalscore}
              </th>
            </tr>
        `];
        
        grades.forEach(
          function(r)
          {
            ls.push(`
              <tr>
                <td>
                  ${P.HeadShotImg(ToShortCode(r.userid), 'Square400')}
                </td>
                <td>
                  ${r.classscore}
                </td>
                <td>
                  ${r.reviewscore}
                </td>
                <td>
                  ${r.persistencescore}
                </td>
                <td class="${ColorCode(r.classpercent, [80, 60, 40, 20])}">
                  ${r.classpercent}%
                </td>
                <td class="${ColorCode(r.reviewpercent, [80, 60, 40, 20])}">
                  ${r.reviewpercent}%
                </td>
                <td class="${ColorCode(r.persistencepercent, [90, 80, 70, 50])}">
                  ${r.persistencepercent}%
                </td>
                <td>
                  ${r.finalscore}
                </td>
              </tr>
            `);
          }
        );
        
        ls.push(`</table>
          <div class="whiteb Rounded Raised Margin25 Pad50">
            <math xmlns = "http://www.w3.org/1998/Math/MathML">
          
              <mi>${T.finalscore}</mi>
              <mo>=</mo>
              <mo>(</mo>
              <mi>${T.classscore}</mi>
              <mo>+</mo>
              <mi>${T.reviewscore}</mi>
              <mo>+</mo>
              <mi>${T.persistencescore}</mi>
              <mo>)</mo>
              <mo>×</mo>
              <mi>${T.classpercent}</mi>
              <mo>×</mo>
              <mi>${T.reviewpercent}</mi>
              <mo>×</mo>
              <mi>${T.persistencepercent}</mi>
        
            </math>
            
            <br>
            <br>
            
            <table>
              <tr>
                <td class="dpurpleg">${T.excellent}</td>
                <td class="dblueg">${T.great}</td>
                <td class="dgreeng">${T.good}</td>
                <td class="dorangeg">${T.poor}</td>
                <td class="dredg">${T.bad}</td>
              </tr>
            </table>
            
            <br>
            
            ${T.gradingexplanation}
          </div>
        `);
        
        P.HTML(resultsdiv, ls);
      }
    }
  );
}  

// ====================================================================
G.GetClassInfo  = function(classid)
{
  for (let i = 0, l = G.teacherclasses.length; i < l; i++)
  {
    let r = G.teacherclasses[i];
    
    if (r.idcode == classid)
    {
      return r;
    }
  }
  
  for (let i = 0, l = G.studentclasses.length; i < l; i++)
  {
    let r = G.studentclasses[i];
    
    if (r.idcode == classid)
    {
      return r;
    }
  }
  
  return {};
}

// ====================================================================
G.ViewActivityLog  = function(classid, isteacher)
{
  let self  = this;
  
  let classinfo = G.GetClassInfo(classid);
  
  let currenttime = new Date();
  let lastweek    = new Date();
  
  currenttime.setMinutes(-currenttime.getTimezoneOffset());
  lastweek.setMinutes(-lastweek.getTimezoneOffset());
  
  lastweek.setDate(currenttime.getDate() - 7);
  
  let ls  = [`<h1>${T.activitylog}: ${P.BestTranslation(classinfo.displayname)}</h1>`];
  
  if (isteacher)
  {
    ls.push(`    
      <table>
        <tr>
          <th>${T.fromtext}</th>
          <td><input type="datetime-local" class="StartDate" value="${lastweek.toISOString().substr(0, 19)}"></td>
        </tr>
        <tr>
          <th>${T.to}</th>
          <td><input type="datetime-local" class="EndDate" value="${currenttime.toISOString().substr(0, 19)}"></td>
        </tr>
        <tr>
          <th>${T.student}</th>
          <td>
            <select class="StudentID">
              <option value="">[${T.all}]</option>
    `);
    
    let studentnames  = [];
    
    for (let k in classinfo.students)
    {
      studentnames.push([k, P.BestTranslation(classinfo.students[k])]);
    }
    
    studentnames.sort(
      function(a, b)
      {
        return StrCmpI(a[1], b[1])
      }
    );
    
    studentnames.forEach(
      function(r)
      {
        ls.push(`<option value="${r[0]}">${r[1]}</option>`);
      }
    )
    
    ls.push(`
            </select>
          </td>
        </tr>
        <tr>
          <th>${T.type}</th>
          <td>      
            <select class="ChallengeType">
              <option value="0">${T.all}</option>
              <option value="1">${T.homework}</option>
              <option value="2">${T.classwork}</option>
              <option value="3">${T.classparticipation}</option>
              <option value="4">${T.quiz}</option>
              <option value="5">${T.test}</option>
              <option value="6">${T.exam}</option>
            </select>
          </td>          
        </tr>
      </table>
    `);
  }
  
  ls.push(`
    <div class="ButtonBar">
      <a class="Color5" onclick="G.DisplayChallengeResults('${classid}')">${T.challengeresults}</a>
      <a class="Color6" onclick="G.DisplayActivityLog('${classid}')">${T.activitylog}</a>
    </div>     
    <div class="Grades"></div>
    <div class="ButtonBar">
      <a class="Color3" onclick="P.RemoveFullScreen()">Finished</a>
    </div>    
  `);
  
  P.AddFullScreen(
    'dblueg',
    '',
    ls
  );
  
  setTimeout(
    function()
    {
      G.DisplayChallengeResults(classid, isteacher);
    },
    500
  );
}

// ====================================================================
G.ViewStudyList  = function(classid, isteacher)
{
  let self  = this;
  
  let classinfo = G.GetClassInfo(classid);
  
  let ls  = [`<h1>${T.studylist}: ${P.BestTranslation(classinfo.displayname)}</h1>`];
  
  if (isteacher)
  {
    ls.push(`    
      <table>
        <tr>
          <th>${T.student}</th>
          <td>
            <select class="StudentID">
              <option value="">[${T.all}]</option>
    `);
    
    let studentnames  = [];
    
    for (let k in classinfo.students)
    {
      studentnames.push([k, P.BestTranslation(classinfo.students[k])]);
    }
    
    studentnames.sort(
      function(a, b)
      {
        return StrCmpI(a[1], b[1])
      }
    );
    
    studentnames.forEach(
      function(r)
      {
        ls.push(`<option value="${r[0]}">${r[1]}</option>`);
      }
    )
    
    ls.push(`
            </select>
          </td>
        </tr>
      </table>
    `);
  }
  
  ls.push(`
    <div class="StudyList"></div>
    <div class="ButtonBar">
      <a class="Color3" onclick="P.RemoveFullScreen()">Finished</a>
    </div>    
  `);
  
  P.AddFullScreen(
    'dblackg',
    '',
    ls
  );
  
  let title = '';
  
  G.studylist = new StudyList({
    div:        '.StudyList',
    reference:  'G.studylist',
    classid:    classid,
    studentid:  isteacher ? '' : P.userid
  });
  
  G.studylist.Display();
  G.studylist.List();  
}

// ====================================================================
G.ViewClassSettings  = function(classid)
{
  let self  = this; 
  
  let classinfo = G.GetClassInfo(classid);
  
  if (IsEmpty(classinfo.settings))
  {
    classinfo.settings  = {};
  }
  
  if (IsEmpty(classinfo.settings.autohomework))
  {
    classinfo.settings.autohomework  = {
      challengetypes:   [],
      lessonids:        [],
      problemids:       [],
      minscore:         0,
      minpercentage:    0,
      numpoints:        50,
      didnttrypenalty:  50,
      problemselect:    'randomly'
    };
  }
  
  let challengetypes  = [];
  
  for (let k in G.CHALLENGE_NAMES)
  {
    let challengename = G.CHALLENGE_NAMES[k];
    
    challengetypes.push(
      [k, challengename, 0, classinfo.settings.autohomework.challengetypes.indexOf(challengename) > -1 ? 'on' : 'unset']
    );
  }
  
  P.EditPopup(
    [
      ['icon', 'imagefile', 0, T.icon],
      ['timezone', 'timezone', classinfo.resettimezone, T.resettime],
      ['challengetypes', 'bitflags', 0, T.options, challengetypes],
      ['minscore', 'int', 0, T.passingscore, '', 'required'],
      ['minpercentage', 'int', 50, T.passingpercentage, '', 'required'],
      ['numpoints', 'int', 50, T.pointsforpassing, '', 'required'],
      ['didnttrypenalty', 'int', 50, T.didnttrypenalty],
      [
        'problemselect', 
        'select', 
        classinfo.settings.autohomework.problemselect ? classinfo.settings.autohomework.problemselect : 'randomly', 
        T.selectproblems, 
        G.SELECT_PROBLEMS
      ],        
      ['numtries', 'int', 3, 'Number of Tries', '', 'required'],
      ['numproblems', 'int', 6, 'Number of Problems to Use'],
      ['timelimit', 'int', 0, 'Time Limit in Seconds'],      
      ['lessonids', 'custom', 0, T.lessons],
      ['problemids', 'custom', 0, T.problems],
      ['flags', 'bitflags', classinfo.flags, T.options,
        [
          [T.autoapprove,         'autoapprove',        G.CLASS_AUTO_APPROVE],
          [T.usehomeworkmanager,  'usehomeworkmanager', G.CLASS_AUTO_HOMEWORK],
          [T.usestudylist,        'usestudylist',       G.CLASS_REVIEW_STUDYLIST],
          [T.addhomeworkscores,   'addhomeworkscores',  G.CLASS_ADD_HOMEWORK_SCORES],
          [T.enablepractice,      'enablepractice',     G.CLASS_ENABLE_PRACTICE],
          [T.public,              'public',             G.CLASS_PUBLIC],
          [T.enableforum,         'enableforum',        G.CLASS_ENABLE_FORUM]
        ]
      ]      
    ],
    function(formdiv, data, resultsdiv, e)
    {
      let challengetypes  = [];
      
      for (let k in G.CHALLENGE_NAMES)
      {
        let challengename = G.CHALLENGE_NAMES[k];
        
        if (data[challengename] == 'on')
        {
          challengetypes.push(challengename);
        }
      }
      
      data.challengetypes     = challengetypes;
      
      data.autoapprove        = data.autoapprove == 'on' ? 1 : 0;
      data.usehomeworkmanager = data.usehomeworkmanager == 'on' ? 1 : 0;
      data.usestudylist       = data.usestudylist == 'on' ? 1 : 0;
      data.addhomeworkscores  = data.addhomeworkscores == 'on' ? 1 : 0;
      data.enablepractice     = data.enablepractice == 'on' ? 1 : 0;
      data.public             = data.public == 'on' ? 1 : 0;
      data.enableforum        = data.enableforum == 'on' ? 1 : 0;
      
      data.command            = 'setsettings';
      data.classid            = classid;
      
      if (data.usehomeworkmanager 
        && (!data.lessonids && !data.problemids)
      )
      {
        P.ErrorPopup(T.missingproblems);
        return;
      }
      
      if ((data.usehomeworkmanager || data.usestudylist)
        && IsEmpty(data.challengetypes)
      )
      {
        P.ErrorPopup(T.missingchallengetypes);
        return;
      }
      
      data.lessonids    = data.lessonids ? data.lessonids.split('\n') : [];
      data.problemids   = data.problemids ? data.problemids.split('\n') : [];
      
      P.LoadingAPI(
        resultsdiv,
        '/learn/classapi',
        data,
        function(d)
        {
          classinfo.resettime             = d.data.resettime;
          classinfo.settings.autohomework = d.data.autohomework;
          classinfo.flags                 = d.data.flags;
          
          P.RemoveFullScreen();
        }
      );
    },
    {
      fullscreen:     1,
      obj:            classinfo.settings.autohomework,
      headertext:     `<div class="Width100P MarginBottom Center Bold blueb Pad50">${classid}</div>`
    }
  );
  
  G.challenges  = new Challenges({
    div:        '.Challenges',
    reference:  'G.challenges',
    classid:    classid,
    title:      T.challenges+':'+P.BestTranslation(classinfo.displayname)    
  });
  
  G.challenges.obj   = classinfo.settings.autohomework;
  
  G.challenges.SetupProblemPickers();
  G.challenges.RenderLessons();
  G.challenges.RenderProblems();  
}

// ====================================================================
G.ViewQuestionAnswers  = function(classid)
{
  let self  = this;
  
  let classinfo = G.GetClassInfo(classid);
  
  let ls  = [`<h1>${T.questionanswers}: ${P.BestTranslation(classinfo.displayname)}</h1>`];
  
  ls.push(`
    <div class="QuestionAnswers"></div>
    <div class="ButtonBar">
      <a class="Color3" onclick="P.RemoveFullScreen()">Finished</a>
    </div>    
  `);
  
  P.AddFullScreen(
    'dtealg',
    '',
    ls
  );
  
  let title = '';
  
  G.questionanswers = new QuestionAnswers({
    div:        '.QuestionAnswers',
    reference:  'G.questionanswers',
    classid:    classid
  });
  
  G.questionanswers.Display();
  G.questionanswers.List();  
}


// ====================================================================
G.DisplayChallengeResults  = function(classid, isteacher)
{
  let starttime     = 0;
  let endtime       = 0;
  let challengetype = G.CHALLENGE_HOMEWORK;
  
  if (E('.StartDate'))
  {
    starttime       = E('.StartDate').value;
    endtime         = E('.EndDate').value;
    challengetype   = E('.ChallengeType').value;
  } else
  {
    starttime = new Date();
    endtime   = new Date();
    
    starttime.setDate(starttime.getDate() - 7);
  }  
  
  P.LoadingAPI(
    '.Grades',
    '/learn/challengeapi',
    {
      command:              'getchallengeresults',
      classid:              classid,
      starttime:            starttime,
      endtime:              endtime,
      challengetype:        challengetype
    },
    function(d, resultsdiv)
    {
      let challenges  = d.data;
      
      if (!challenges.length)
      {
        P.HTML(resultsdiv, T.nothingfound);
      } else
      {
        let students    = [];
        let totalpoints = {};
        
        for (let i = 0, l = challenges.length; i < l; i++)
        {
          let r = challenges[i];
          
          if (!IsEmpty(r.results))
          {
            for (let k in r.results)
            {
              students.push([k, P.BestTranslation(r.results[k][0])]);
              
              totalpoints[k]  = 0;
            }
            break;
          }
        }
        
        students.sort(
          function(a, b)
          {
            return StrCmpI(a[1], b[1]);
          }
        );
        
        let ls  = [
          `<table class="Pad50 Center">
            <tr>
              <th></th>
          `
        ];
        
        students.forEach(
          (r) => ls.push(`
            <th>
              ${P.HeadShotImg(r[0], 'Square400')}<br>
              ${r[1]}
            </th>
          `)
        )
        
        if (isteacher)
        {
          ls.push('<th></th>')
        }
        
        ls.push('</tr>');
        
        challenges.forEach(
          function(r)
          {
            ls.push(`<tr>
              <th>
                ${P.BestTranslation(r.title)}<br>
                ${UTCToLocal(r.endtime * 1000).substr(5, 5)}
              </th>
            `);
            
            students.forEach(
              function(s)
              {
                if (HasKey(r.results, s[0]))
                {
                  let result  = r.results[s[0]];
                  let color   = 'redb';
                  let points  = result[4];
                  
                  switch (result[1])
                  {
                    case G.CHALLENGE_LAZY_TRIED:
                      color   = 'dorangeb';
                      break;
                    case G.CHALLENGE_TRIED:
                      color   = 'yellowb';
                      break;
                    case G.CHALLENGE_COMPLETED:
                      color   = 'greenb';
                      break;
                    case G.CHALLENGE_EXCELLENT:
                      color   = 'blueb';
                      break;
                  };
                  
                  totalpoints[s[0]] += points;
                  
                  ls.push(`<td class="${color}">${points}</td>`);
                } else
                {
                  ls.push('<td class="blackb"></td>');
                }
              }
            );
            
            if (isteacher)
            {
              ls.push(`
                  <td class="Color4" onclick="G.ViewChallengeAnalysis('${classid}', '${r.idcode}', '${escape(P.BestTranslation(r.title))}')">
                    Analyze
                  </td>
              `);
            }
            
            ls.push('</tr>');            
          }
        )
        
        ls.push('<tr><th>Total</th>');
        
        students.forEach(
          function(s)
          {
            ls.push(`<td>${totalpoints[s[0]]}</td>`);
          }
        );
        
        ls.push('</tr></table>');
        
        P.HTML(resultsdiv, ls);
      }
    }
  );
}  

// ====================================================================
G.DisplayActivityLog  = function(classid, isteacher)
{
  let starttime     = 0;
  let endtime       = 0;
  let challengetype = 0;
  let studentid     = '';
  
  if (E('.StartDate'))
  {
    starttime       = E('.StartDate').value;
    endtime         = E('.EndDate').value;
    challengetype   = E('.ChallengeType').value;
    studentid       = E('.StudentID').value;
  } else
  {
    starttime = new Date();
    endtime   = new Date();
    
    starttime.setDate(starttime.getDate() - 7);
  }  
  
  P.LoadingAPI(
    '.Grades',
    '/learn/challengeapi',
    {
      command:              'getactivitylog',
      classid:              classid,
      starttime:            starttime,
      endtime:              endtime,
      challengetype:        challengetype,
      studentid:            studentid
    },
    function(d, resultsdiv)
    {
      let results  = d.data;
      
      if (!results.length)
      {
        P.HTML(resultsdiv, T.nothingfound);
      } else
      {
        let ls  = [`
          <table class="Styled Width100P">
            <tr>
              <th>
                ${T.time}
              </th>
              <th>
                ${T.student}
              </th>
              <th>
                ${T.challenge}
              </th>
              <th>
                ${T.lessons}
              </th>
              <th>
                ${T.score}
              </th>
              <th>
                ${T.percentage}
              </th>
            </tr>
        `];
        
        results.forEach(
          function(r)
          {
            let eventtime = UTCToLocal(r.starttime * 1000);
            
            ls.push(`
              <tr>
                <td>
                  ${eventtime}
                </td>
                <td>
                  ${P.HeadShotImg(r.studentid, 'Square400')}
                </td>
                <td>
                  ${T[r.challenge.toLowerCase().replaceAll('.', '').replaceAll(' ', '')]}
                </td>
                <td>
                  ${r.lessons}
                </td>
                <td>
                  ${r.score}
                </td>
                <td>
                  ${r.percentright}
                </td>
              </tr>              
            `);
          }
        );
        
        ls.push('</table>');

        P.HTML(resultsdiv, ls);
      }
    }
  );
}  

// ====================================================================
G.ChooseCurriculum = function(classid)
{
  let classobj  = 0;
  
  for (let r of G.teacherclasses)
  {
    if (r.idcode == classid)
    {
      classobj  = r;
      break;
    }
  }
  
  G.classcurriculumchooser = new ClassCurriculumChooser({
    div:        '.ClassCurriculumChooser',
    reference:  'G.classcurriculumchooser',
    schoolid:   classobj.schoolid,
    classid:    classid
  });
  
  G.classcurriculumchooser.Display();  
  
  P.On(
    '.CardType',
    'change',
    function(e)
    {
      G.classcurriculumchooser.GetAvailable();
    }
  );
  
  P.On(
    '.CardPrice',
    'change',
    function(e)
    {
      G.classcurriculumchooser.GetAvailable();
    }
  );
}

// ====================================================================
G.RenderChallenges = function()
{
  let ls  = [];
  
  let currenttime     = Date.now() / 1000;
  
  G.studentclasses.forEach(
    function(r)
    {
      for (let j = 0, m = r.challenges.length; j < m; j++)
      {
        let s = r.challenges[j];
        
        let bgcolor = 'lblueb';
        
        if (s.timeremaining < 3600)
        {
          bgcolor = 'lorangeb';
        } else if (s.timeremaining < 10800)
        {
          bgcolor = 'lyellowb';
        } else if (s.timeremaining < 43200)
        {
          bgcolor = 'lgreenb';
        }
        
        if (s.completed)
        {
          bgcolor = 'greeng';
        }
        
        if (!s.numpoints)
        {
          bgcolor = 'redg';
        }
        
        ls.push(`
          <div class="ChallengeCard ${bgcolor} Rounded Raised Bordered Margin25 Pad25">
            <div class="ChallengeTitle Bold">${P.BestTranslation(s.title)}</div>
            <div class="displayname">${P.BestTranslation(r.displayname)}</div>
            <table class="Width100P Border0">
              <tr>
                <td>🪙 ${s.numpoints}</td>
                <td>🏹 ${s.triesleft}</td>
                <td class="TimeTicker EndTime" data-value="${s.timeremaining}"></td>
              </tr>
            </table>
            
            ${s.completed
              ? `<div class="Center Bold">${T.youpassed}</div>`
              : `
                <div class="ButtonBar">
                  <a class="Color3" href="/learn/challenges.html?classid=${r.idcode}&challengeid=${s.idcode}">${T.start}</a>
                </div>
              `
            }
          </div>
        `);
      }      
    }
  );
  
  P.HTML('.StudentChallenges', ls);
  
  if (!IsEmpty(ls))
  {
    P.HTML('.StudentChallengesHeader', '<h2>' + T.challenges + '</h2>');
    P.MakeTimeTicker();
  }  
}

// ====================================================================
G.RenderStudent = function()
{
  let s = G.student;
  
  P.HTML(
    '.StudentStatus', 
    `
      <div class="StudentInfo whiteb Rounded Raised Bordered Margin25 Pad25">
        <div class="Pad50 Flex FlexCenter">
          ${P.HeadShotImg(s.idcode, 'Square600')}
          <span class="Pad50 dblue Size200">${P.BestTranslation(s.displayname)}</span>
        </div>
      </div>
    `
  );
}

// ====================================================================
G.RenderClasses = function()
{
  let ls  = [];
  
  G.teacherclasses.forEach(
    function(r)
    {
      ls.push(`
        <div class="ClassCard whiteb Rounded Raised Bordered Margin25 Pad25">
          <div class="Center Icon">
            ${P.ImgFile(r.icon, 'Width1200')}
          </div>
          <div class="CourseNum Center dpurple Pad50">${r.coursenum}</div>
          <div class="CourseDescription dblue Pad25">${P.BestTranslation(r.description)}</div>
          <div class="SchoolName dgreen Size80 Pad25">${P.BestTranslation(r.schoolname)}</div>
          <div class="ClassSchedule dbrown Size80 Pad25">${P.BestTranslation(r.schedule)}</div>
          <br>
          <div class="ButtonBar Flex FlexCenter">
            <a class="Color2" href="/learn/teacherclassroom.html/${r.idcode}">${T.enterteachersclassroom}</a>
            <a class="Color3" href="/learn/classroom.html/${r.idcode}">${T.enterclassroom}</a>
            ${r.flags & G.CLASS_ENABLE_FORUM 
              ? `<a class="Color5" href="/learn/forum.html?classid=${r.idcode}">${T.forum}</a>`
              : ''
            }
      `);
      
      for (let j = 0, m = r.courses.length; j < m; j++)
      {
        let s = r.courses[j];
        
        ls.push(`
          <a class="Color4" href="/learn/courses.html/${r.idcode}/${s.idcode}">${s.title}</a>
        `);
      }
      
      ls.push(`
            <a class="Color5" onclick="G.ViewChallenges('${r.idcode}')"">${T.challenges}</a>
            <a class="Color6" onclick="G.ViewGrades('${r.idcode}', 1)"">${T.grades}</a>
            <a class="Color1" onclick="G.ViewActivityLog('${r.idcode}', 1)"">${T.activitylog}</a>
            <a class="Color2" onclick="G.ViewStudyList('${r.idcode}', 1)"">${T.studylist}</a>
            <a class="Color3" onclick="G.ViewClassSettings('${r.idcode}')"">${T.settings}</a>
            <a class="Color4" onclick="G.ViewQuestionAnswers('${r.idcode}')"">${T.questionanswers}</a>
            <a class="Color5" onclick="G.ChooseCurriculum('${r.idcode}')"">${T.curriculum}</a>
            <a class="Color6" onclick="P.SendMessage(['all'], 'learn_schoolclass', '${r.idcode}')"">${T.sendgroupmessage}</a>
          </div>
        </div>
      `);      
    }
  );
    
  P.HTML('.TeacherClasses', ls);

  ls  = [];
  
  for (let r of G.studentclasses)
  {
    let ranking = '';
    
    if (r.rankings)
    {
      let rankingpos  = r.rankings.indexOf(P.userid);
      
      if (rankingpos > 0)
      {
        let rankingcolor  = 'dbrowng';
        
        switch (rankingpos)
        {
          case 1:
            rankingcolor  = 'dpurpleg';
            break;
          case 2:
            rankingcolor  = 'dblueg';
            break;
          case 3:
            rankingcolor  = 'dgreeng';
            break;
          case 4:
            rankingcolor  = 'dorangeg';
            break;
          case 5:
            rankingcolor  = 'dredg';
            break;
        }
        
        ranking = `
          <div class="Square200 FloatRight Margin25 Center Size200 Serif ${rankingcolor} Rounded">
            ${T.cardinalsign + rankingpos}
          </div>
        `;
      }
    }
    
    ls.push(`
      <div class="ClassCard whiteb Rounded Raised Bordered Margin25 Pad25">
        <div class="Center Icon">
          ${P.ImgFile(r.icon, 'Width1200')}
        </div>
        ${ranking}
        <div class="CourseNum dpurple Center Pad50">${r.coursenum}</div>sx
        <div class="CourseDescription blue Pad25">${P.BestTranslation(r.description)}</div>
        <div class="SchoolName dgreen Size80 Pad25">${P.BestTranslation(r.schoolname)}</div>
        <div class="ClassSchedule dbrown Size80 Pad25">${P.BestTranslation(r.schedule)}</div>
        <br>
        <div class="ButtonBar Flex FlexCenter">
          <a class="Color3" href="/learn/classroom.html/${r.idcode}">${T.enterclassroom}</a>
          ${r.flags & G.CLASS_ENABLE_FORUM 
            ? `<a class="Color5" href="/learn/forum.html?classid=${r.idcode}">${T.forum}</a>`
            : ''
          }
    `);
    
    for (let s of r.courses)
    {
      ls.push(`
        <a class="Color4" href="/learn/courses.html/${r.idcode}/${s.idcode}">${s.title}</a>
      `);
    }
    
    ls.push(`
        <a class="Color6" onclick="G.ViewGrades('${r.idcode}')"">${T.grades}</a>
        <a class="Color1" onclick="G.ViewActivityLog('${r.idcode}')"">${T.activitylog}</a>
        <a class="Color2" onclick="G.ViewStudyList('${r.idcode}')"">${T.studylist}</a>
        ${r.flags & G.ENABLE_PRACTICE
          ? `<a class="Color3" href="/learn/challenges.html?classid=${r.idcode}&practice=1">${T.practice}</a>`
          : ''
        }
      </div></div>
    `);      
  }
  
  P.HTML('.StudentClasses', ls);
}

// ====================================================================
G.FindMoreClasses = function()
{
  P.AddFullScreen(
    'dgreeng', 
    '', 
    `<h3>${T.appliedclasses}</h3>
    <div class="ApprovalStatus"></div>
    <h3>${T.findmoreclasses}</h3>
    <div class="ClassFinder"></div>
    <div class="ButtonBar">
      <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
    </div>
    `
  );
  
  G.classfinder = new ClassFinder({
    div:        '.ClassFinder',
    reference:  'G.classfinder'
  });
  
  G.classfinder.Display();
  G.classfinder.List();
  
  G.RenderApprovalStatus();
}

// ====================================================================
G.RenderApprovalStatus  = function()
{
  P.LoadingAPI(
    '.ApprovalStatus',
    '/learn/schoolapi',
    {
      command:  'getapprovalstatus'
    },
    function(d, resultsdiv)
    {
      let statuses  = d.data;
      
      if (!statuses.length)
      {
        P.HTML(resultsdiv, T.nothingfound);
      } else
      {
        let ls  = [`<table class="Width100P Pad50 Bordered Margin25">`];
      
        for (let r of statuses)
        {
          ls.push(`
            <tr>
              <td class="whiteb">
                <div class="CourseNum">${r.coursenum}</div>
                <div class="ClassName">${r.displayname}</div>
                <div class="Schedule">${r.schedule}</div>
              </td>
              ${r.status == 'waiting' 
                ? `<td class="Center blueb">${T.waiting}: ${r.comment}</td>`
                : `<td class="Center redb">
                  ${T.denied}: ${r.reason}
                  <div class="ButtonBar">
                    <a class="Color4" onclick="G.ApplyToClass('${r.schoolid}', '${r.classid}')">${T.reapply}</a>
                  </div>
                </td>`
              }
            </tr>
          `);
        }
        
        P.HTML(resultsdiv, ls);
      }
    }
  );  
}

// ====================================================================
G.OnPageLoad = function()
{
  let ls  = [
    `<div class="StudentStatus"></div>
    `
  ];
  
  if (G.isadministrator)
  {
    ls.push(`
      <h2>${T.administrator}</h2>
      <div class="FlexGrid20 AdministratorLinks">
        <a class="Color4 Pad50 Margin50 Raised Rounded" href="/learn/manage.html">
          ${T.manageschools}
        </a>
      </div>      
    `);
  }
  
  ls.push(`
    <div class="StudentChallengesHeader"></div>
    <div class="FlexGrid20 StudentChallenges"></div>
  `);
  
  if (!IsEmpty(G.teacherclasses))
  {
    ls.push(`
    <h2>${T.teacherclasses}</h2>
    <div class="TeacherClasses FlexGrid20"></div>
    `);
  }
  
  ls.push(`
    <h2>${T.studentclasses}</h2>
    <div class="StudentClasses FlexGrid20">${T.nothingfound}</div>
    <h2>${T.findmoreclasses}</h2>
    <div class="ButtonBar"">
      <a class="Color4" onclick="G.FindMoreClasses()">${T.findmoreclasses}</a>
    </div>
  `);
  
  if (P.HasGroup('teachers'))
  {
    ls.push(`
      <h2>Tools</h2>
      <div class="ButtonBar">
        <a class="Color3" href="/learn/commonwords.html">Common Words</a>
        <a class="Color4" href="/learn/phonicsreview.html">Phonics</a>
        <a class="Color5" href="/learn/phonicssyllables.html">Syllables</a>
      </div>
    `);
  }
    
  P.HTML('.PageLoadContent', ls);
  
  G.RenderChallenges();
  G.RenderStudent();
  G.RenderClasses();
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
