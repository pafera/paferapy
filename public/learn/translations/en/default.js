T.activitylog="Activity Log"
T.addchildcard="Add Child Card"
T.addlessons="Add Lessons"
T.addproblem="Add Problem"
T.addproblems="Add Problems"
T.addquickproblem="Quick Add Problem"
T.administrator="Administrator"
T.allowedschools="Allowed School IDs"
T.analyze="Analyze"
T.answer="Answer"
T.answerimage="Image"
T.answersaved="Answer saved!"
T.answersound="Sound"
T.answertext="Answer Text"
T.answervideo="Video"
T.applicationsuccessful="Your application was successful. Please wait for the school to approve you."
T.appliedclasses="You Have Applied to These Classes"
T.autoapprove="Automatically Approve New Students"
T.autoapproved="You have been automatically approved!"
T.cardfunction="Card Function"
T.cardtype="Card Type"
T.cardtype1="English"
T.cardtype2="Chinese"
T.cardtype3="Russian"
T.cardtype4="Math"
T.challengeresults="Challenge Results"
T.choice="Choice"
T.classparticipation="Class Participation"
T.classwork="Classwork"
T.curriculum="Curriculum"
T.enterclassroom="Enter Classroom"
T.enterteachersclassroom="Enter Teacher's Classroom"
T.exam="Exam"
T.explanation="Explanation"
T.findmoreclasses="Find More Classes"
T.grades="Grades"
T.gradingexplanation="<p>\nLow class percent means that the student did not answer most of the questions correctly during class participation. This may indicate shyness, lack of self-belief, inadequate preparation, or poor behavior.\n</p>\n<p>\nLow review percent means that the student did not answer most of the questions correctly while doing homework quizzes. This may indicate a failure to review the material before taking the quiz, test anxiety, or a bad environment at home.\n</p>\n<p>\nLow completion percent means that the student did not complete most of his or her assignments. This may indicate lack of study habits, lack of desire to succeed, or a bad environment at home.\n</p>"
T.homework="Homework"
T.homeworkmanager="Homework Manager"
T.lesson="Lesson"
T.lessons="Lessons"
T.lessonscanthavechildcards="Lessons cannot have child cards!"
T.manageschools="Manage Schools"
T.onlylessonscancontainproblems="Only lessons can contain problems."
T.points="Points"
T.practice="Practice"
T.problem="Problem"
T.problemimage="Image"
T.problems="Problems"
T.problemsound="Sound"
T.problemtext="Problem Text"
T.problemvideo="Video"
T.question="Question"
T.quiz="Quiz"
T.reapply="Reapply"
T.review="Review"
T.speak="\ud83d\udde3\ufe0f Speak"
T.student="Student"
T.studentclasses="Student Classes"
T.studylist="Study List"
T.teacherclasses="Teacher Classes"
T.test="Test"
T.timeremaining="Time Remaining"
T.triesremaining="Tries Remaining"
T.tryout="Try Out"
T.typereasonforapplying="Please Type Why You Want to Apply"
T.typing="\u2328\ufe0f Typing"
T.unit="Unit"
T.usehomeworkmanager="Use Homework Manager"
T.viewanswers="View Answers"
T.waitforteacher="Please wait for the teacher."
T.enableforum="Enable Forum"

