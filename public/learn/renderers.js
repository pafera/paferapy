"use strict";

G.CARD_COURSE               = 0x1;
G.CARD_UNIT                 = 0x2;
G.CARD_LESSON               = 0x4;
G.CARD_ASIDE                = 0x8;
G.CARD_PROBLEM              = 0x10;
G.CARD_ASIDE                = 0x20;

G.CARD_PRIVATE              = 0x40;
G.CARD_PROTECTED            = 0x80;
G.CARD_PUBLIC               = 0x100;
G.CARD_ALWAYS_SHOW_CONTENT  = 0x200;

G.CARD_SECURITY             = G.CARD_PUBLIC | G.CARD_PROTECTED | G.CARD_PRIVATE;;

G.CARD_CHILD              =  0x1;

G.CARD_LINK_PROBLEM       =  0x1;
G.CARD_LINK_QUIZ_PROBLEM  =  0x2;
G.CARD_LINK_TEST_PROBLEM  =  0x3;
G.CARD_LINK_EXAM_PROBLEM  =  0x4;

G.CARD_COMPACT_VIEW       =  0x200;
G.CARD_SHOW_ANSWERS       =  0x400;
G.CARD_SHOW_EXPLANATIONS  =  0x800;

G.CARD_TYPES  = {
  0:  T.cardtype,
  1:  T.cardtype1,
  2:  T.cardtype2,
  3:  T.cardtype3,
  4:  T.cardtype4
};

G.CARD_PRICES  = {
  0:  T.all,
  1:  T.free,
  2:  T.paid
};

G.CHALLENGE_USE_STUDYLIST       = 0x02;

G.CHALLENGE_HOMEWORK            = 1;
G.CHALLENGE_CLASSWORK           = 2;
G.CHALLENGE_CLASSPARTICIPATION  = 3;
G.CHALLENGE_QUIZ                = 4;
G.CHALLENGE_TEST                = 5;
G.CHALLENGE_EXAM                = 6;

G.CHALLENGE_DIDNT_TRY         = 0;
G.CHALLENGE_TRIED             = 1;
G.CHALLENGE_COMPLETED         = 2;
G.CHALLENGE_LAZY_TRIED        = 3;
G.CHALLENGE_EXCELLENT         = 4;

G.CHALLENGE_TYPES = {
  1:  T.homework,
  2:  T.classwork,
  3:  T.classparticipation,
  4:  T.quiz,
  5:  T.test,
  6:  T.exam
};

G.CHALLENGE_NAMES = {
  'Listen and Click':   T.listenandclick,
  'Read and Click':     T.readandclick,
  'Look and Speak':     T.lookandspeak,
  'Listen and Speak':   T.listenandspeak,
  'Read and Speak':     T.readandspeak,
  'Look and Type':      T.lookandtype,
  'Listen and Type':    T.listenandtype,
  'Read and Type':      T.readandtype,
  'Typing':             T.typing,
  'Write Answer':       T.writeanswer
};

G.PASSING_CRITERIA  = {
  'Score':      T.score,
  'Percentage': T.percentage
};

G.SELECT_PROBLEMS   = {
  'sequentially':   T.insequence,
  'randomly':       T.randomly,
  'all':            T.useall
};

G.CLASS_AUTO_APPROVE        = 0x01;
G.CLASS_AUTO_HOMEWORK       = 0x02;
G.CLASS_REVIEW_STUDYLIST    = 0x04;
G.CLASS_ADD_HOMEWORK_SCORES = 0x08;
G.CLASS_ENABLE_PRACTICE     = 0x10;
G.CLASS_PUBLIC              = 0x20;
G.CLASS_ENABLE_FORUM        = 0x40;


G.ANSWER_LOCKED             = 0x01
G.ANSWER_ANSWERED           = 0x02
G.ANSWER_ENABLE_COPY_PASTE  = 0x04
G.ANSWER_TEACHER            = 0x08

// ********************************************************************
G.RenderProblem = function(p, classes, extracontent)
{
  classes      = classes || '';
  extracontent = extracontent || '';
  
  let pimage      = p['problem']['image'];
  let psound      = p['problem']['sound'];
  let pvideo      = p['problem']['video'];
  
  let aimage      = p['answer']['image'];
  let asound      = p['answer']['sound'];
  let avideo      = p['answer']['video'];
  
  return `
    <div class="${classes}" data-idcode="${p['idcode']}">
      <table class="Width100P">
        <tr class="ProblemDiv">
          <td class="${pimage ? 'Width400' : ''} ProblemImage HoverHighlight" ${(pimage && aimage == pimage) ? 'rowspan="2"' : ''}>
            ${pimage
              ? '<img class="Width400" src="/system/files/' + P.CodeDir(pimage) + '.webp">'
              : ''}
          </td>
          <td>
            <div class="Pad25 dgreen ProblemContent">${p['problem']['content']}</div>
            ${psound
              ? `<div class="Pad25 ProblemSound">
                  <audio id="sound_${psound}">
                    <source src="/system/files/${P.CodeDir(psound)}.mp3">
                  </audio>
                  <div class="HoverHighlight Raised Pad50 Width200" onclick="E('#sound_${psound}').play()">🔊</div>
                </div>`
              : ''}
            ${pvideo
              ? '<div class="Pad25 ProblemVideo"><video class="Width400" controls=1><source src="/system/files/' + P.CodeDir(pvideo) + '.mp4"></video></div>'
              : ''}
          </td>
        </tr>
        <tr class="AnswerDiv">
          ${(aimage && aimage != pimage)
            ? '<td class="AnswerImage HoverHighlight"><img class="Width400" src="/system/files/' + P.CodeDir(aimage) + '.webp"></td>' 
            : ''}
          ${pimage
            ? ''
            : '<td></td>'}
          <td>
            <div class="Pad25 dblue AnswerContent">${p['answer']['content']}</div>
            ${asound
              ? `<div class="Pad25 AnswerSound">
                  <audio id="sound_${asound}">
                    <source src="/system/files/${P.CodeDir(asound)}.mp3">
                  </audio>
                  <div class="HoverHighlight Raised Rounded Width200 Pad50" onclick="E('#sound_${asound}').play()">🔊</div>
                </div>`
              : ''}
            ${avideo
              ? '<div class="Pad25 AnswerVideo"><video class="Width400" controls=1><source src="/system/files/' + P.CodeDir(avideo) + '.mp4"></video></div>'
              : ''}
          </td>
        </tr>
      </table>
      ${extracontent}
    </div>
  `;    
}

// ********************************************************************
G.RenderCard = function(card, classes, extracontent)
{
  classes       = classes || '';
  extracontent  = extracontent  || '';
  
  if (!IsEmpty(card.files) && card.idcode)
  {
    setTimeout(
      function()
      {
        let filesdiv  = E(`.Card[data-idcode='${card.idcode}'] .FilesDiv`);
        
        P.LoadingAPI(
          filesdiv,
          '/system/fileapi',
          {
            command:  'search',
            fileids:  card.files
          },
          function(d, resultsdiv)
          {
            P.RenderFileObjects(resultsdiv, d.data);
          }
        );
      },
      2000
    );
  }
  
  return `
    <div class="Card ${classes}" data-idcode="${card.idcode ? card.idcode : ''}">
      ${card.image
        ? `<div class="ImageDiv Center Pad50">
          ${P.ImgFile(card.image, 'MaxWidth60vw')}
        </div>
        `
        : ''
      }
      ${card.sound
        ? `<div class="SoundDiv Center Pad50">
          ${P.SoundFile(card.sound)}
        </div>
        `
        : ''
      }
      ${card.video
        ? `<div class="VideoDiv Center Pad50">
          ${P.VideoFile(card.video)}
        </div>
        `
        : ''
      }
      <div class="FlexGrid20 FilesDiv"></div>
      <div class="ContentDiv Pad50">
        ${card.html
          ? card.html
          : card.content
        }
      </div>
      ${extracontent}
    </div>
  `;
}

