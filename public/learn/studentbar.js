"use strict";

G.ANSWER_LOCKED             = 0x01
G.ANSWER_ANSWERED           = 0x02
G.ANSWER_ENABLE_COPY_PASTE  = 0x04
G.ANSWER_TEACHER            = 0x08

// ********************************************************************
class StudentBar
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    config    = config  || {};
    
    this.config       = config;
    
    this.div            = config.div;
    this.reference      = config.reference;
    this.students       = [];
    this.studentsdict   = {};
    this.numpoints      = 1;

    this.autoupdate         = 0;    
    this.autoupdatetimeout  = time() + 3600;    
    
    this.shownstudentbars  = [];
  }
  
  // ------------------------------------------------------------------
  GetAutoUpdate()
  {
    return this.autoupdate;
  } 
  
  // ------------------------------------------------------------------
  SetAutoUpdate(status)
  {
    this.autoupdate = status;
    
    if (status)
    {
      this.autoupdatetimeout  = time() + 3600;    
      
      setTimeout(
        self.Update,
        2000
      );
    }
  }
  
  // ------------------------------------------------------------------
  ChangeScores(students, usenumpoints = 0)
  {
    let self  = this;
    
    // Multiply bonus points by the chosen bonus amount and automatically
    // add right or wrong scores depending on whether the bonus is 
    // positive or negative.
    
    if (usenumpoints)
    {
      for (let r of students)
      {
        if (r.bonus)
        {
          if (r.bonus > 0)
          {
            r.right = 1;
          } else
          {
            r.wrong = 1;
          }
          
          // One point is already counted in right or wrong
          r.bonus *= (self.numpoints - 1);        
        }
      }
    }
    
    // Change the entire group's score when a student is part of a group
    if (students.length == 1)
    {
      let student   = self.studentsdict[students[0].idcode];
      
      if (!student)
      {
        return;
      }
      
      let groupnum 	= student.groupnum;
      
      if (groupnum)      
      {
        for (let i = 0, l = self.students.length; i < l; i++)
        {
          let item 	  = self.students[i];
          let idcode  = item.idcode;
          
          if (item.groupnum == groupnum && idcode != students[0].idcode)
          {
            let grouppartner  = Clone(students[0]);
            grouppartner.idcode = idcode;
            students.push(grouppartner);
          }
        }
      }
    }

    P.DialogAPI(
      '/learn/answersapi',
      {
        command:    'changescores',
        students:   students,
        classid:    G.classid
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        if (!self.autoupdate)
        {
          self.Update();
        }
      }
    );
  }

  // ------------------------------------------------------------------
  ResetScores()
  {
    let self  = this;
    
    P.DialogAPI(
      '/learn/answersapi',
      {
        command:    'resetscores',
        classid:    G.classid
      },
      function(d)
      {
        if (!self.autoupdate)
        {
          self.Update();
        }
      }
    );
  }
  
  // ------------------------------------------------------------------
  NewClass()
  {
    let self  = this;
    
    P.DialogAPI(
      '/learn/answersapi',
      {
        command:  "newclass",
        classid:  G.classid
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        if (!self.autoupdate)
        {
          self.Update();
        }
      }
    );
  }

  // ------------------------------------------------------------------
  RandomGroups()
  {
    let self  = this;
    
    P.EditPopup(
      [
        ['groupsize', 'int', 2, 'Group Size']
      ],
      function(formdiv, data, resultsdiv)
      {
        let groups 				= [];
        let groupsize 		= parseInt(data.groupsize);
        let students 			= [];
        
        for (let i = 0, l = self.students.length; i < l; i++)
        {
          let item 	= self.students[i];
          
          if (item.flags & G.ANSWER_TEACHER)
          {
            continue;
          }
          
          students.push(item);
        }
        
        let numstudents 	= students.length;			
        let numgroups 		= Math.floor(numstudents / groupsize);
        let leftovers 		= numstudents % groupsize;
        
        for (let i = 1; i <= numgroups; i++)
        {
          for (let j = 0; j < groupsize; j++)
            groups.push(i);
          
          if (leftovers)
          {
            groups.push(i);
            leftovers--;
          }
        }
        
        Shuffle(groups);
        
        let newgroups 	= {};
        
        for (let i = 0, l = students.length; i < l; i++)
        {
          newgroups[students[i].idcode] 	= groups[i];
        }
        
        P.DialogAPI(
          '/learn/answersapi',
          {
            command:  'setgroups',
            students: newgroups,
            classid:  G.classid
          },
          function(d, resultsdiv, popupclass)
          {
            P.ClosePopup(popupclass);
            
            if (!self.autoupdate)
            {
              self.Update();
            }
          }
        );				
      },
      {
        popupclass: 'RandomGroupsPopup'
      }
    );
  }  

  // ------------------------------------------------------------------
  ClearGroups()
  {
    let self  = this;
    
    P.DialogAPI(
      '/learn/answersapi',
      {
        command:  'cleargroups',
        classid:  G.classid
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        if (!self.autoupdate)
        {
          self.Update();
        }
      }
    );
  }
  
  // ------------------------------------------------------------------
  Update()
  {
    let self  = this;
    
    if (time() > this.autoupdatetimeout)
    {
      self.SetAutoUpdate(0);
    }
    
    P.API(
      '/learn/answersapi',
      {
        command:  'list',
        classid:  G.classid
      },
      function(d)
      {
        self.students = d.data;
        
        self.students.sort(
          function(a, b)
          {
            return b.score - a.score;
          }
        );
        
        let ls  = [`
          <div class="ButtonBar">
            <a class="Color4 StudentBarNumPoints" onclick="${self.reference}.ChooseNumPoints()">${self.numpoints}</a>
            <a class="Color3" onclick="${self.reference}.ChangeAll(1)">+</a>
            <a class="Color1" onclick="${self.reference}.ChangeAll()">-</a>
          </div>
        `];
        let studentsdict = {};
        
        ls.push('<div class="">')
        
        for (let i = 0, l = self.students.length; i < l; i++)
        {
          let r    = self.students[i];
          
          r.idcode      = ToShortCode(r.userid);
          r.studentname = P.BestTranslation(r.displayname);
          
          studentsdict[r.idcode] = r;
          
          ls.push(`
            <a class="InlineBlock Center white Border${r.groupnum} Student" onclick="${self.reference}.ShowStudent(this)" data-dbid="${r.idcode}">
              ${P.HeadShotImg(r.idcode, 'Square400')}<br>
              ${r.score}
            </a>
          `);
        }
        
        self.studentsdict  = studentsdict;
        
        ls.push('</div>');
        
        P.HTML(self.div, ls);
        
        if (self.autoupdate)
        {
          setTimeout(
            self.Update,
            1000
          );
        }
      }
    );
  }

  // ------------------------------------------------------------------
  ChooseNumPoints()
  {
    let self  = this;
    
    let ls  = [];
    
    for (let i = 1; i <= 100; i++)
    {
      ls.push([i.toString(), 'greenb']);
    }
    
    P.ShowFullScreenPicker(
      'Choose Bonus Points',
      ls,
      function(itemtext, resultsdiv)
      {
        E('.StudentBarNumPoints').innerText = itemtext;
        
        self.numpoints  = parseInt(itemtext);
        
        P.RemoveFullScreen();
      }
    );
  }
  
  // ------------------------------------------------------------------
  ChangeAll(isright)
  {
    let self  = this;
    
    let ls  = [];
    
    let bonusamount = isright ? 1 : -1;
        
    for (let r of self.students)
    {
      if (r.flags & G.ANSWER_TEACHER)
      {
        continue;
      }
      
      ls.push({
        idcode:   r.idcode,
        bonus:    bonusamount
      });
    }
    
    self.ChangeScores(ls, 1);
  }
  
  // ------------------------------------------------------------------
  OnStudentClicked(el, student)
  {
  }
  
  // ------------------------------------------------------------------
  ShowStudent(el)
  {
    let self  = this;
    
    let studentid  = el.dataset.dbid;
    
    if (self.shownstudentbars.indexOf(studentid) > -1)
    {
      return;
    }
    
    self.shownstudentbars.push(studentid);
    
    let student = self.studentsdict[studentid];
    
    self.OnStudentClicked(el, student);
    
    P.Popup(
      `<div class="lgrayb Pad25 Raised">
        <div class="Pad50">
          ${student.studentname} 
        </div>
        <div>
          <a class="Color3 Size200 Pad25 InlineBlock" onclick="${self.reference}.ChangeScores([
            {
              'idcode': '${studentid}',
              'right':  1
            }
          ])">+</a>
          <a class="Color1 Size200 Pad25 InlineBlock" onclick="${self.reference}.ChangeScores([
            {
              'idcode': '${studentid}',
              'wrong':  1
            }
          ])">-</a>
          <a class="Color2 Size200 Pad25 InlineBlock" onclick="${self.reference}.ChangeScores([
            {
              'idcode': '${studentid}',
              'strikes':  1
            }
          ])">*</a>
          <a class="Color5 Size200 Pad25 InlineBlock" onclick="${self.reference}.ChangeScores([
            {
              'idcode': '${studentid}',
              'bonus':  1
            }
          ])">$</a>
        </div>
        <div>
          <a class="Color4 Size200 InlineBlock" style="padding: 0em 0.25em;" onclick="${self.reference}.ChangeScores([
            {
              'idcode': '${studentid}',
              'right':  -1
            }
          ])">+</a>
          <a class="Color4 Size200 InlineBlock" style="padding: 0em 0.25em;" onclick="${self.reference}.ChangeScores([
            {
              'idcode': '${studentid}',
              'wrong':  -1
            }
          ])">-</a>
          <a class="Color4 Size200 InlineBlock" style="padding: 0em 0.25em;" onclick="${self.reference}.ChangeScores([
            {
              'idcode': '${studentid}',
              'strikes':  -1
            }
          ])">*</a>
          <a class="Color4 Size200 InlineBlock" style="padding: 0em 0.25em;" onclick="${self.reference}.ChangeScores([
            {
              'idcode': '${studentid}',
              'bonus':  -1
            }
          ])">$</a>
        </div>
        <div>
          <a class="Color6 Size200 InlineBlock" style="padding: 0em 0.25em;" onclick="${self.reference}.ChangeScorePopup('${studentid}', 'right')">+</a>
          <a class="Color6 Size200 InlineBlock" style="padding: 0em 0.25em;" onclick="${self.reference}.ChangeScorePopup('${studentid}', 'wrong')">-</a>
          <a class="Color6 Size200 InlineBlock" style="padding: 0em 0.25em;" onclick="${self.reference}.ChangeScorePopup('${studentid}', 'strikes')">*</a>
          <a class="Color6 Size200 InlineBlock" style="padding: 0em 0.25em;" onclick="${self.reference}.ChangeScorePopup('${studentid}', 'bonus')">$</a>
        </div>
        <div style="margin-top: 0.25em;">
          <a class="ToggleButton Size150 ${student.flags & G.ANSWER_LOCKED ? 'lgreeng': 'redg'}" 
            onclick="${self.reference}.ToggleFlags('${studentid}', ${G.ANSWER_LOCKED})">
            🔒
          </a>
          <a class="ToggleButton Size150 ${student.flags & G.ANSWER_ANSWERED ? 'lgreeng': 'redg'}" 
            onclick="${self.reference}.ToggleFlags('${studentid}', ${G.ANSWER_ANSWERED})">
            ✅
          </a>
          <a class="ToggleButton Size150 ${student.flags & G.ANSWER_ENABLE_COPY_PASTE ? 'lgreeng': 'redg'}" 
            onclick="${self.reference}.ToggleFlags('${studentid}', ${G.ANSWER_ENABLE_COPY_PASTE})">
            📋
          </a>
          <a class="ToggleButton Size150 ${student.flags & G.ANSWER_TEACHER ? 'lgreeng': 'redg'}" 
            onclick="${self.reference}.ToggleFlags('${studentid}', ${G.ANSWER_TEACHER})">
            🤵
          </a>
          <a class="Size150 Bordered" style="padding: 0em 0.3em;"
            onclick="${self.reference}.ChangeGroup('${studentid}')">
            ${student.groupnum}
          </a>
          <a class="Size150 Bordered" style="padding: 0em 0.3em;"
            onclick="${self.reference}.Delete('${studentid}')">
            ❌
          </a>
        </div>
        <div class="Center">
          <span class="green">+${student.numright}</span>
          <span class="red">-${student.numwrong}</span>
          <span class="dorange">*${student.numstrikes}</span>
          <span class="purple">$${student.bonusscore}</span>
        </div>
      </div>
      `,
      {
        width:            '11.5em',
        height:           'auto',
        popupclass:       'StudentBarPopup',
        parent:           el,
        closeonmouseout:  1,
        noanimation:        1,
        closefunc:        function()
          {
            self.shownstudentbars.remove(studentid);
          }
      }
    );
  }

  // ------------------------------------------------------------------
  Delete(studentid)
  {
    let self  = this;
    
    P.DialogAPI(
      '/learn/answersapi',
      {
        command:        'delete',
        studentid:      studentid,
        classid:        G.classid
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        if (!self.autoupdate)
        {
          self.Update();
        }        
      }
    );        
  }
  
  // ------------------------------------------------------------------
  ChangeScorePopup(studentid, changetype)
  {
    let self  = this;
    
    if (E('.FullScreenPickerResults'))
    {
      P.RemoveFullScreen();
      return;
    } 

    let ls  = [];
    
    for (let i = 100; i >= -100; i--)
    {
      if (i == 0)
      {
        continue;
      }
      
      if (i > 0)
      {
        ls.push([i.toString(), 'greenb']);
      } else
      {
        ls.push([i.toString(), 'redb']);
      }
    }
    
    P.ShowFullScreenPicker(
      'Change ' + changetype,
      ls,
      function(itemtext, resultsdiv)
      {
        let student = {
          idcode: studentid
        };
        
        student[changetype] = parseInt(itemtext);
        
        self.ChangeScores([student]);
        
        P.RemoveFullScreen();
      }
    );
  }

  // ------------------------------------------------------------------
  ToggleFlags(studentid, flag)
  {
    let self    = this;
    let student = self.studentsdict[studentid];
    
    if (student.flags & flag)
    {
      student.flags &= ~flag;
    } else
    {
      student.flags |= flag;
    }
    
    P.API(
      '/learn/answersapi',
      {
        command:        'setflags',
        studentid:  studentid,
        flags:          student.flags,
        classid:  G.classid
      },
      function(d)
      {
        if (!self.autoupdate)
        {
          self.Update();
        }        
      }
    );
  }    
  
  // ------------------------------------------------------------------
  ChangeGroup(studentid)
  {
    let self    = this;
    let student = self.studentsdict[studentid];
    
    student.groupnum++;
    
    if (student.groupnum > 6)
    {
      student.groupnum  = 0;
    } 
    
    P.API(
      '/learn/answersapi',
      {
        command:        'setgroup',
        id:             studentid,
        groupnum:       student.groupnum,
        classid:        G.classid
      },
      function(d)
      {
        if (!self.autoupdate)
        {
          self.Update();
        }        
      }
    );
  }    
}
