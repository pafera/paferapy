"use strict";

G.SLIDES  = [
  {
    title:    'CVC',
    content:    [
      'cat', 
      'red',
      'big',
      'dog',
      'but'
    ]
  },
  {
    title:    'CVCe',
    content:  [
      'make',
      'like',
      'note',
      'cute'
    ]
  },
  {
    title:    'ee, ea ➜ E',
    content:  [
      'beef',
      'feet',
      'teeth',
      'peach',
      'teach'
    ]
  },
  {
    title:    'c + e, i, y ➜ s',
    content:  [
      'rice',
      'nice',
      'dance',
      'ice',
      'face',
      'city',
      'icy',
      'cycle'
    ]
  },
  {
    title:    'g + e, y ➜ j',
    content:  [
      'gym',
      'magic',
      'large',
      'germ'
    ]
  },
  {
    title:    'ch',
    content:  [
      'chair',
      'cheese',
      'chicken',
      'touch',
      'peach',
      'lunch'
    ]
  },
  {
    title:    'sh',
    content:  [
      'fish',
      'wash',
      'brush',
      'ship',
      'sheep',
      'shop'
    ]
  },
  {
    title:    'th ➜ d',
    content:  [
      'the',
      'this',
      'that',
      'these',
      'those',
      'three',
      'thing'
    ]
  },
  {
    title:    'th ➜ f',
    content:  [
      'bath',
      'math',
      'with',
      'teeth',
      'ninth',
      'thin'
    ]
  },
  {
    title:    'igh ➜ I',
    content:  [
      'high',
      'light',
      'night',
      'right',
      'fight'
    ]
  },
  {
    title:    'eigh ➜ A',
    content:  [
      'eight',
      'neigh',
      'weigh',
      'weight',
      'sleigh'
    ]
  },
  {
    title:    'y ➜ I',
    content:  [
      'by',
      'my',
      'cry',
      'fly',
      'sky'
    ]
  },
  {
    title:    'y ➜ E',
    content:  [
      'happy',
      'thirsty',
      'study',
      'thirty',
      'sleepy'
    ]
  },
  {
    title:    'ay ➜ A',
    content:  [
      'day',
      'pay',
      'say',
      'play',
      'okay',
      'gray'
    ]
  },
  {
    title:    'ey ➜ A',
    content:  [
      'hey',
      'they',
      'prey',
      'grey'
    ]
  },
  {
    title:    'oy ➜ oy',
    content:  [
      'boy',
      'toy',
      'joy',
      'Roy'
    ]
  },
  {
    title:    'uy ➜ I',
    content:  [
      'buy',
      'guy'
    ]
  },
  {
    title:    'oo ➜ oo',
    content:  [
      'moo',
      'too',
      'zoo',
      'foot',
      'moon',
      'noon'
    ]
  },
  {
    title:    'oo ➜ or',
    content:  [
      'book',
      'look'
    ]
  },
  {
    title:    'le ➜ L',
    content:  [
      'sale',
      'tale',
      'tile',
      'pile',
      'whale',
      'while'
    ]
  },
  {
    title:    'le ➜ O',
    content:  [
      'table',
      'apple',
      'purple',
      'turtle',
      'circle',
      'noodle',
      'uncle'
    ]
  },
  {
    title:    'wh ➜ w',
    content:  [
      'why',
      'when',
      'what',
      'where',
      'which',
      'while'
    ]
  },
  {
    title:    'wh + o ➜ h',
    content:  [
      'who',
      'whom',
      'whose'
    ]
  },
  {
    title:    'ar ➜ R',
    content:  [
      'bar',
      'far',
      'jar',
      'tar'
    ]
  },
  {
    title:    'er, ir, ur ➜ er',
    content:  [
      'per',
      'her',
      'over',
      'sir',
      'hair',
      'burp',
      'fur',
      'pur',
    ]
  },
  {
    title:    'or, oar ➜ or',
    content:  [
      'for',
      'fork',
      'word',
      'board'
    ]
  },
  {
    title:    'al, ul ➜ al',
    content:  [
      'ball',
      'fall',
      'hall',
      'tall',
      'bulb',
      'gump'
    ]
  },
  {
    title:    'el ➜ L',
    content:  [
      'bell',
      'fell',
      'hell',
      'belt',
      'else',
      'feel',
      'elephant'
    ]
  },
  {
    title:    'il',
    content:  [
      'bill',
      'hill',
      'mill',
      'pill'
    ]
  },
  {
    title:    'ol ➜ O',
    content:  [
      'old',
      'cold',
      'fold',
      'sold',
      'hold'
    ]
  },
  {
    title:    'i ➜ y, u ➜ ue,<br> v ➜ ve, j ➜ ge',
    content:  [
      'by',
      'my',
      'blue',
      'clue',
      'have',
      'glove',
      'twelve',
      'age',
      'cage',
      'huge',
      'page'
    ]
  },
  {
    title:    'For More, Visit',
    content:  [
      '<a href="http://www.theschoolhouse.us">www.theschoolhouse.us</a>'
    ]
  },
];
    

// ====================================================================
G.OnPageLoad = function()
{
  let slides  = ['<div class="Slides" style="height: 100vh;">'];
  
  for (let slide of G.SLIDES)
  {
    let slidetext = [`<div class="Slide Center Size300">
      <h2 class="dgreen SlideTitle">${slide.title}</h2>
      <div class="SlideContent FlexGrid20">
    `]
    
    for (let word of slide.content)
    {
      slidetext.push(`<div class="SlideWord Pad50 HoverHighlight" onclick="G.SayThisWord(this)">${word}</div>`);
    }
    
    slidetext.push(`</div>
    </div>`);

    slides.push(slidetext.join('\n'));
  }
  
  slides.push('</div>');
  
  P.HTML(
    '.PageLoadContent', 
    slides
  );  
  
  P.MakeSlideShow(
    '.Slides',
    {
      keycontrol: 1
    }
  );
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
