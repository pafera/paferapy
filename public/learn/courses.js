"use strict";

G.testtypingtimelimit  = 0;
G.testtypingtimeleft   = 0;

G.maincard            = {};
G.childcards          = [];
G.siblingcards        = [];
G.currentproblemtype  = '';

// ********************************************************************
class ChildCardChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.multipleselect = 1;
    this.enablereorder  = 1;
    this.fields         = ['title', 'description'];    
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      '.' + self.div + ' .AvailableCards',
      '/learn/cardapi',
      {
        command:  'search',
        flags:    G.CARD_UNIT | G.CARD_LESSON,
        start:    start,
        keyword:  searchterm
      },
      function(d)
      {
        self.available        = d.data;
        self.availablecount   = d.count;
        
        self.DisplayAvailable();
      }
    );
  }
  
  // ------------------------------------------------------------------
  DisplayItem(ls, r)
  {
    let self  = this;
    
    ls.push(`<div class="CardImage">${P.ImgFile(r.image, 'Square400')}</div>
      <div class="CardTitle">${r.title}</div>
      <div class="CardDescription Size80">${r.description}</div>
    `);
  }
  
  // ------------------------------------------------------------------
  OnFinished()
  {
    let self      = this;
    let chosenids = [];
    
    for (let i = 0, l = self.chosen.length; i < l; i++)
    {
      let r = self.chosen[i];
      
      chosenids.push(r.idcode);
    }
    
    P.LoadingAPI(
      '.' + self.div + 'Results',
      '/learn/cardapi',
      {
        command:      'setchildcards',
        cardid:       self.cardid,
        childcardids: chosenids
      },
      function(d)
      {
        G.childcards  = d.data;
        
        P.RemoveFullScreen();
        
        G.RenderChildCards();
      }
    );
  }  
}

// ********************************************************************
class ProblemChooser extends PaferaChooser
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.fields = [];
    
    this.enablereorder  = 1;
    this.multipleselect = 1;
  }
  
  // ------------------------------------------------------------------
  OnGetAvailable(start, resultsdiv, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      '.' + self.div + ' .AvailableCards',
      '/learn/problemapi',
      {
        command:      'search',
        start:        start,
        keyword:      searchterm
      },
      function(d)
      {
        self.available        = d.data;
        self.availablecount   = d.count;
        
        self.DisplayAvailable();
      }
    );
  }
  
  // ------------------------------------------------------------------
  RenderItem(r)
  {
    return G.ProblemToHTML(r);
  }
  
  // ------------------------------------------------------------------
  OnFinished(removefullscreen = 1)
  {
    let self      = this;
    
    G.SaveProblems(self.chosen, '.' + self.div + 'Results');
    
    if (removefullscreen)
    {
      P.RemoveFullScreen();
    }
  }  
}

// ********************************************************************
class CardFinder extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    if (config.iscourse)
    {
      this.flags  = G.CARD_COURSE;
    } else if (config.ischildcard)
    {
      this.flags  = G.CARD_UNIT | G.CARD_LESSON;
    } else
    {
      this.flags  = G.CARD_PROBLEM;
    }
    
    if (config.iscourse || config.ischildcard)
    {
      this.displayfields  = [
        ['image',       'imagefile',  T.icon],
        ['title',       'text',       T.title],
        ['description', 'text',       T.description]
      ];
    } else
    {
      this.displayfields  = [
        ['image',     'imagefile',  T.icon],
        ['content',   'text',       T.content]
      ];
    }
    
    this.showidcode = 1;
  }
  
  // ------------------------------------------------------------------
  OnList(resultsdiv, start, searchterm)
  {
    let self  = this;
    
    P.LoadingAPI(
      resultsdiv,
      '/learn/cardapi',
      {
        command:  'search',
        keyword:  searchterm,
        flags:    self.flags
      },
      function(d, resultsdiv)
      {
        self.items    = d.data;
        self.numitems = d.count;
        
        self.DisplayItems();
      }
    );    
  }
}

// ====================================================================
G.SaveProblems = function(problemlist, resultsdiv)
{
  let chosenids = [];
  
  problemlist.forEach(
    function(r)
    {
      chosenids.push(r.idcode);
    }
  )
  
  P.LoadingAPI(
    resultsdiv,
    '/learn/cardapi',
    {
      command:      'setproblems',
      cardid:       G.maincard.idcode,
      problemids:   chosenids,
      problemtype:  G.currentproblemtype
    },
    function(d)
    {
      G[G.currentproblemtype + 'problems']  = d.data;
      
      G.RenderProblemCards(G.currentproblemtype);
    }
  );
}
  
// ====================================================================
G.PracticeProblem = function(el, activity)
{
  let card  = P.Parent(el, '.ProblemCard');
  let text  = card.querySelector('.AnswerContent').innerText;
  
  switch (activity)
  {
    case 'voice':
      G.CheckPronounciation(text, 0, {parent: el});
      break;
    case 'typing':
      G.TestTypingPopup(text, {parent: el});
      break;
  }
}

// ====================================================================
G.TestTypingPopup = function(answer, options)
{
  options = options || {}
  
  P.Popup(
    `<div class="whiteb Pad25 Rounded Raised Bordered Center">
        <div>
          <input type="text" class="testtypinganswer">
        </div>
        <div class="TypingTimerBackground blackb MinHeight150 Rounded">
          <div class="TypingTimerForeground lgreeng MinHeight150 Rounded"></div>
        </div>
        <br>
        <div class="ButtonBar">
          <a class="Color1" onclick="P.CloseThisPopup(this)">${T.back}</a>
        </div>
      </div>
    `,
    {
      closeonmouseout:  1,
      parent:           options.parent
    }
  );
  
  setTimeout(
    function()
    {
      let answerinput   = E('.testtypinganswer');
      
      answerinput.focus();
      
      P.OnEnter(
        answerinput,
        function()
        {
          if (answerinput.value == answer)
          {
            P.Play('ding');
            P.HTML('.TypingTimerBackground', `<div class="lgreeng">Great! +${G.testtypingtimeleft}</div>`);
          } else
          {
            P.Play('buzzer');
            P.HTML(
              '.TypingTimerBackground', 
              `<div class="dredg Pad25">Wrong!</div>
              <pre><code class="red">${answerinput.value}</code>
<code class="green">${answer}</code></pre>
              `
            );
          }
        }
      );
      
      G.testtypingtimelimit  = answer.length + 4;
      G.testtypingtimeleft   = G.testtypingtimelimit;
      
      setTimeout(
        G.OnTestTypingTimer,
        1000
      );
    },
    500
  )
}

// ====================================================================
G.OnTestTypingTimer = function()
{
  G.testtypingtimeleft--;
  
  if (!G.testtypingtimeleft)
  {
    P.HTML(
      '.TypingTimerBackground', 
      `<div class="yellowb">Time up!</div>`
    );
  } else
  {
    let el  = E('.TypingTimerForeground');
    
    if (el)
    {
      el.style.width = (G.testtypingtimeleft / G.testtypingtimelimit * 100) + '%';
      
      setTimeout(
        G.OnTestTypingTimer,
        1000
      );    
    }
  }
}

// ====================================================================
G.CardToHTML = function(card, classes, extracontent, attrs)
{
  classes       = classes || 'whiteb Raised Margin50 Pad25 Rounded';
  extracontent  = extracontent  || '';
  attrs         = attrs || '';
  
  return `<div class="${classes}" data-idcode="${card.idcode}" ${attrs}>
        ${card.title
          ? `<div class="CardTitle Center Pad25 Size125 dgreen">${card.title}</div>`
          : ''
        }
        ${card.image
          ? `<div class="Center CardImage">${P.ImgFile(card.image, 'HoverHighlight Pad50 Width600')}</div>`
          : ''
        }
        ${card.description
          ? `<h4 class="CardDescription dpurple">${card.description}</h4>`
          : ''
        }
        ${card.sound
          ? `<div class="Center CardSound">${P.SoundFile(card.sound)}</div>`
          : ''
        }
        ${card.video
          ? `<div class="Center CardVideo">${P.VideoFile(card.video)}</div>`
          : ''          
        }
        <div class="CardContent">
          ${card.html
            ? card.html
            : card.content
          }
        </div>
        ${extracontent}
      </div>
  `;  
}

// ====================================================================
G.ProblemToHTML = function(problem, classes, extracontent, attrs)
{
  classes       = classes || 'ProblemCard whiteb Raised Margin25 Pad25 Rounded MinWidth1200';
  extracontent  = extracontent  || '';
  attrs         = attrs || '';
  
  let p           = problem;
  
  let pimage      = p['problem']['image'];
  let psound      = p['problem']['sound'];
  let pvideo      = p['problem']['video'];
  
  let aimage      = p['answer']['image'];
  let asound      = p['answer']['sound'];
  let avideo      = p['answer']['video'];
    
  return `
    <div class="${classes}" data-idcode="${p['idcode']}" ${attrs}>
      <table class="Width100P">
        <tr class="ProblemDiv">
          <td class="${pimage ? 'Width400' : ''} ProblemImage HoverHighlight" ${(pimage && aimage == pimage) ? 'rowspan="2"' : ''}>
            ${pimage
              ? '<img class="Width400" src="/system/files/' + P.CodeDir(pimage) + '.webp">'
              : ''}
          </td>
          <td>
            <div class="Pad25 dgreen ProblemContent">${p['problem']['content']}</div>
            ${psound
              ? `<div class="Pad25 ProblemSound">
                  <audio id="sound_${psound}">
                    <source src="/system/files/${P.CodeDir(psound)}.mp3">
                  </audio>
                  <div class="HoverHighlight Raised Pad50 Width200" onclick="E('#sound_${psound}').play()">🔊</div>
                </div>`
              : ''}
            ${pvideo
              ? '<div class="Pad25 ProblemVideo"><video class="Width400" controls=1><source src="/system/files/' + P.CodeDir(pvideo) + '.mp4"></video></div>'
              : ''}
          </td>
        </tr>
        <tr class="AnswerDiv">
          ${(aimage && aimage != pimage)
            ? '<td class="AnswerImage HoverHighlight"><img class="Width400" src="/system/files/' + P.CodeDir(aimage) + '.webp"></td>' 
            : ''}
          ${pimage
            ? ''
            : '<td></td>'}
          <td>
            <div class="Pad25 dblue AnswerContent">${p['answer']['content']}</div>
            ${asound
              ? `<div class="Pad25 AnswerSound">
                  <audio id="sound_${asound}">
                    <source src="/system/files/${P.CodeDir(asound)}.mp3">
                  </audio>
                  <div class="HoverHighlight Raised Rounded Width200 Pad50" onclick="E('#sound_${asound}').play()">🔊</div>
                </div>`
              : ''}
            ${avideo
              ? '<div class="Pad25 AnswerVideo"><video class="Width400" controls=1><source src="/system/files/' + P.CodeDir(avideo) + '.mp4"></video></div>'
              : ''}
          </td>
        </tr>
      </table>
      ${extracontent}
      ${G.caneditcards
        ? `<div class="ButtonBar">
            <a class="Color3" onclick="G.EditExistingProblem('${problem.idcode}')">${T.edit}</a>
          </div>`
        : ''
      }
    </div>
  `;
}

// ====================================================================
G.RenderMainCard = function()
{
  P.HTML(
    '.MainCard', 
    G.CardToHTML(
      G.maincard,
      "MainCardContent whiteb Raised Margin50 Pad25 Rounded"
    )
  );
}

// ====================================================================
G.RenderChildCards = function()
{
  let ls  = [];
  
  let childcards  = G.childcards;
  let breadcrumbs = G.breadcrumbs;
  
  for (let i = 0, l = G.childcards.length; i < l; i++)
  {
    let card  = childcards[i];
    let url = '/learn/courses.html/' + G.classid + '/' + breadcrumbs.join('/') + '/' + card.idcode
    
    ls.push(`
      <div class="ChildCard whiteb Raised Rounded Pad25 Margin25 HoverHighlight" data-idcode="${card.idcode}">
        <a class="" href="${url}">
          <table class="Width100P">
            <tr>
              <td class="${card.image ? 'Width400' : ''}">
                ${card.image
                  ? '<img class="Width400" src="/system/files/' + P.CodeDir(card.image) + '.webp">' 
                  : ''}
              </td>
              <td>
                <div class="Pad25 dgreen">${card.title}</div>
                ${card.description
                  ? '<div class="Pad25 dpurple">' + card.description + '</div>' 
                  : ''                      
                }
              </td>
            </tr>
          </table>
        </a>
      </div>
    `);        
  }
  
  if (!G.maincardislesson && G.caneditcards)
  {
    ls.push(`
      <div class="ChildCard whiteb Raised Rounded Pad25 Margin25 Flex FlexCenter" data-idcode="">
        <a class="Color4 Center Pad50 Rounded Raised" onclick="G.AddChildCard()">
          ${T.addchildcard}
        </a>
      </div>
    `);        
  }
    
  P.HTML('.ChildCards', ls);
}

// ====================================================================
G.RenderProblemCards = function(problemtype = '')
{
  let ls  = [];
  let problems  = G[problemtype + 'problems'];
  
  let haspimage  = 0;
  let haspsound  = 0;
  let haspvideo  = 0;
  
  let hasaimage  = 0;
  let hasasound  = 0;
  let hasavideo  = 0;
  
  let allhavepimage  = 1;
  let allhavepsound  = 1;

  let allhaveaimage  = 1;
  let allhaveasound  = 1;
  
  if (!problems.length)
  {
    allhaveaimage = 0;
    allhavepsound = 0;
    allhaveaimage = 0;
    allhaveasound = 0;
  }
  
  for (let p of problems)
  {
    let voicerecognition  = '';
    let typingtest        = '';
    let extracontent      = '';
    
    let pimage      = p['problem']['image'];
    let psound      = p['problem']['sound'];
    let pvideo      = p['problem']['video'];
    
    let aimage      = p['answer']['image'];
    let asound      = p['answer']['sound'];
    let avideo      = p['answer']['video'];
    
    if (pimage)
    {
      haspimage = 1;
    } else
    {
      allhavepimage = 0;
    }
    
    if (psound)
    {
      haspsound = 1;
    } else
    {
      allhavepsound = 0;
    }
    
    if (pvideo)
    {
      haspvideo = 1;
    }
    
    if (aimage)
    {
      if (aimage != pimage)
      {
        hasaimage = 1;
      }
    } else
    {
      allhaveaimage = 0;
    }
    
    if (asound)
    {
      hasasound = 1;
    } else
    {
      allhaveasound = 0;
    }
    
    if (avideo)
    {
      hasavideo = 1;
    }
    
    if (asound)
    {
      voicerecognition  = `<a class="Color2" onclick="G.PracticeProblem(this, 'voice')">${T.speak}</a>`;
      typingtest        = `<a class="Color4" onclick="G.PracticeProblem(this, 'typing')">${T.typing}</a>`;
      
      extracontent      = '<div class="ButtonBar">' + voicerecognition + typingtest + '</div>';
    }
    
    ls.push(G.ProblemToHTML(p, '', extracontent));    
  }
  
  if (G.maincardislesson && G.caneditcards)
  {
    ls.push(`
      <div class="ProblemCard whiteb Raised Rounded Pad25 Margin25 Flex FlexCenter FlexVertical" data-idcode="">
        <a class="Color5 Center Pad50 Margin25 Rounded Raised Block" onclick="G.QuickAddProblem()">
          ${T.addquickproblem}
        </a>
        <a class="Color4 Center Pad50 Margin25 Rounded Raised Block" onclick="G.AddProblem()">
          ${T.addproblem}
        </a>
      </div>
    `);        
  }
  
  if (!problemtype)
  {
    P.HTML('.ProblemCards', ls);
  } else
  {
    P.HTML('.ProblemsList', ls);
    return;
  }
  
  let problemtogglefields  = [
    [T.problemtext, 'pcontent', 0, 'on']
  ];
  
  if (haspimage)
  {
    problemtogglefields.push([T.problemimage, 'pimage', 0, 'on']);
  }
    
  if (haspsound)
  {
    problemtogglefields.push([T.problemsound, 'psound', 0, 'on']);
  }
  
  if (haspvideo)
  {
    problemtogglefields.push([T.problemvideo, 'pvideo', 0, 'on']);
  }
  
  problemtogglefields.push([T.answertext, 'acontent', 0, 'unset']);
  
  if (hasaimage)
  {
    problemtogglefields.push([T.answerimage, 'aimage', 0, 'unset']);
  }
    
  if (hasasound)
  {
    problemtogglefields.push([T.answersound, 'asound', 0, 'unset']);
  }
  
  if (hasavideo)
  {
    problemtogglefields.push([T.answervideo, 'avideo', 0, 'unset']);
  }
  
  P.MakeToggleButtons(
    '.ProblemToggles',
    problemtogglefields,
    {
      ontoggle: function(el, name, newstate)
        {
          if (newstate == 'on')
          {
            switch (name)
            {
              case 'pcontent':
                P.Show('.ProblemContent');
                break;
              case 'pimage':
                P.Show('.ProblemImage');
                break;
              case 'psound':
                P.Show('.ProblemSound');
                break;
              case 'pvideo':
                P.Show('.ProblemVideo');
                break;
              case 'acontent':
                P.Show('.AnswerContent');
                break;
              case 'aimage':
                P.Show('.AnswerImage');
                break;
              case 'asound':
                P.Show('.AnswerSound');
                break;
              case 'avideo':
                P.Show('.AnswerVideo');
                break;
            };
          } else
          {
            switch (name)
            {
              case 'pcontent':
                P.Hide('.ProblemContent');
                break;
              case 'pimage':
                P.Hide('.ProblemImage');
                break;
              case 'psound':
                P.Hide('.ProblemSound');
                break;
              case 'pvideo':
                P.Hide('.ProblemVideo');
                break;
              case 'acontent':
                P.Hide('.AnswerContent');
                break;
              case 'aimage':
                P.Hide('.AnswerImage');
                break;
              case 'asound':
                P.Hide('.AnswerSound');
                break;
              case 'avideo':
                P.Hide('.AnswerVideo');
                break;
            }            
          }
        }
    }
  );
  
  P.Hide('.AnswerContent, .AnswerImage, .AnswerSound, .AnswerVideo');
  
  P.OnClick(
    '.CardImage, .ProblemImage, .AnswerImage',
    P.LBUTTON,
    function(e)
    {
      let src   = e.target.getAttribute('src');
      
      if (src)
      {
        P.Popup(
          `<div class="whiteb Pad25 Rounded Raised Bordered Center">
              <div>
                <img class="Width100P" src="${src}">
              </div>
              <div class="ButtonBar">
                <a class="Color1" onclick="P.CloseThisPopup(this)">Close</a>
              </div>
            </div>
          `,
          {
            parent:           e.target,
            closeonmouseout:  1
          }
        );
      }
    }
  );

  if (G.caneditcards)
  {
    ls  = [];
    
    if (allhavepimage)
    {
      if (allhaveasound)
      {
        ls.push(`
          <a class="ChallengeCard whiteb Rounded Raised Pad25 Margin25 Block" 
              href="/learn/challenges.html?challenge=listen.and.click&classid=${G.classid}&lessonids=${G.maincard.idcode}&practice=1">
            <table class="Width100P">
              <tr>
                <td class="Width400">
                  <img class="Width400" src="/learn/icons/listen.and.click.webp">
                </td>
                <td>
                  ${T.listenandclick}
                </td>
              </tr>
            </table>
          </a>
        `);
      }
      
      ls.push(`
        <a class="ChallengeCard whiteb Rounded Raised Pad25 Margin25 Block" 
            href="/learn/challenges.html?challenge=read.and.click&classid=${G.classid}&lessonids=${G.maincard.idcode}&practice=1">
          <table class="Width100P">
            <tr>
              <td class="Width400">
                <img class="Width400" src="/learn/icons/read.and.click.webp">
              </td>
              <td>
                ${T.readandclick}
              </td>
            </tr>
          </table>
        </a>
      `);
      
      ls.push(`
        <a class="ChallengeCard whiteb Rounded Raised Pad25 Margin25 Block" 
            href="/learn/challenges.html?challenge=look.and.type&classid=${G.classid}&lessonids=${G.maincard.idcode}&practice=1">
          <table class="Width100P">
            <tr>
              <td class="Width400">
                <img class="Width400" src="/learn/icons/look.and.type.webp">
              </td>
              <td>
                ${T.lookandtype}
              </td>
            </tr>
          </table>
        </a>
      `);
    }
    
    if (allhaveasound)
    {
      if (allhavepimage)
      {
        ls.push(`
          <a class="ChallengeCard whiteb Rounded Raised Pad25 Margin25 Block" 
              href="/learn/challenges.html?challenge=look.and.speak&classid=${G.classid}&lessonids=${G.maincard.idcode}&practice=1">
            <table class="Width100P">
              <tr>
                <td class="Width400">
                  <img class="Width400" src="/learn/icons/look.and.speak.webp">
                </td>
                <td>
                  ${T.lookandspeak}
                </td>
              </tr>
            </table>
          </a>
        `);
      }
      
      ls.push(`
        <a class="ChallengeCard whiteb Rounded Raised Pad25 Margin25 Block" 
            href="/learn/challenges.html?challenge=listen.and.speak&classid=${G.classid}&lessonids=${G.maincard.idcode}&practice=1">
          <table class="Width100P">
            <tr>
              <td class="Width400">
                <img class="Width400" src="/learn/icons/listen.and.speak.webp">
              </td>
              <td>
                ${T.listenandspeak}
              </td>
            </tr>
          </table>
        </a>
      `);
        
      ls.push(`
        <a class="ChallengeCard whiteb Rounded Raised Pad25 Margin25 Block" 
            href="/learn/challenges.html?challenge=read.and.speak&classid=${G.classid}&lessonids=${G.maincard.idcode}&practice=1">
          <table class="Width100P">
            <tr>
              <td class="Width400">
                <img class="Width400" src="/learn/icons/read.and.speak.webp">
              </td>
              <td>
                ${T.readandspeak}
              </td>
            </tr>
          </table>
        </a>
      `);
      
      ls.push(`
        <a class="ChallengeCard whiteb Rounded Raised Pad25 Margin25 Block" 
            href="/learn/challenges.html?challenge=listen.and.type&classid=${G.classid}&lessonids=${G.maincard.idcode}&practice=1">
          <table class="Width100P">
            <tr>
              <td class="Width400">
                <img class="Width400" src="/learn/icons/listen.and.type.webp">
              </td>
              <td>
                ${T.listenandtype}
              </td>
            </tr>
          </table>
        </a>
      `);
    }
    
    ls.push(`
      <a class="ChallengeCard whiteb Rounded Raised Pad25 Margin25 Block" 
          href="/learn/challenges.html?challenge=read.and.type&classid=${G.classid}&lessonids=${G.maincard.idcode}&practice=1">
        <table class="Width100P">
          <tr>
            <td class="Width400">
              <img class="Width400" src="/learn/icons/read.and.type.webp">
            </td>
            <td>
              ${T.readandtype}
            </td>
          </tr>
        </table>
      </a>
    `);
      
    P.HTML('.ChallengeCards', ls)
  }
}

// ====================================================================
G.RenderSiblingNav = function()
{
  let maincard        = G.maincard;
  let cardstack       = G.cardstack;
  let childcards      = G.childcards;
  let breadcrumbs     = G.breadcrumbs;
  
  G.siblingcards      = G.childstacks[cardstack.length - 2];
  
  let siblingcards    = G.siblingcards;
  
  let previoussibling = 0;
  let nextsibling     = 0;
  let parentcard      = 0;
  
  if (cardstack.length > 1)
  {
    parentcard  = cardstack[cardstack.length - 2];
  }
  
  if (!IsEmpty(siblingcards))
  {
    for (let i = 0, l = siblingcards.length; i < l; i++)
    {
      if (siblingcards[i].idcode == maincard.idcode)
      {
        if (i)
        {
          previoussibling = siblingcards[i - 1];
        }
        
        if (i < l - 1)
        {
          nextsibling = siblingcards[i + 1];
        }
        
        break;
      }
    }
  }
  
  P.HTML(
    '.SiblingNav',
    `<div class="Grid GridCenter" style="grid-template-columns: 1fr 1fr 1fr;">
          ${previoussibling
            ? `<a class="Table Color1 Raised Rounded Pad25 Margin25 Height100P Center" 
                href="/learn/courses.html/${G.classid}/${breadcrumbs.slice(0, breadcrumbs.length - 1).join('/')}/${previoussibling.idcode}">
                <div class="TableCell Middle">
                  ⮜ ${previoussibling.title}
                </div>
              </a>`
            : ''}
          ${parentcard
            ? `<a class="Table Color4 Raised Rounded Pad25 Margin25 Height100P Center"  
                href="/learn/courses.html/${G.classid}/${breadcrumbs.slice(0, breadcrumbs.length - 1).join('/')}">
                <div class="TableCell Middle">
                  ⮝ ${parentcard.title}
                </div>
              </a>`
            : ''}
          ${nextsibling
            ? `<a class="Table Color3 Raised Rounded Pad25 Margin25 Height100P Center" 
                href="/learn/courses.html/${G.classid}/${breadcrumbs.slice(0, breadcrumbs.length - 1).join('/')}/${nextsibling.idcode}">
                <div class="TableCell Middle">
                  ${nextsibling.title} ⮞
                </div>
              </a>`
            : ''}
      </div>
  `); 
}

// ====================================================================
G.EditCard = function(obj, onfinished, options)
{
  obj = obj || {};
  
  obj.flags = obj.flags || 0;
  
  G.currenteditcard = obj;
  
  options = options || {};
  
  let editfields  = [
    ['cardtype',       'select',     0,        T.cardtype,  G.CARD_TYPES]
  ];
  
  if (!options.isproblem 
    && (!(obj.flags && (obj.flags & G.CARD_PROBLEM)) 
      || options.ischildcard
    )
  )
  {
    let security  = obj.flags 
      ? obj.flags & G.CARD_SECURITY
      : G.CARD_PUBLIC;
      
    editfields  = editfields.concat([
      ['title',       'text',     0,        T.title],
      ['description', 'text',     0,        T.description],
      ['security',    'select',   security, T.security, 
        {
          0x100:    T.public,
          0x80:     T.protected,
          0x40:     T.public
        }
      ]
    ]);
    
    if (obj.flags & G.CARD_COURSE)
    {
      editfields.push(
        ['allowedschools', 'newlinelist', 0,    T.allowedschools]
      );
    }
  }
  
  editfields  = editfields.concat([
    ['image',     'imagefile', 0, T.images],
    ['sound',     'soundfile', 0, T.sounds],
    ['video',     'videofile', 0, T.videos],
    ['content',   'multitext', 0, T.content],
    ['markdown',  'multitext', 0, 'Markdown'],
    ['html',      'multitext', 0, 'HTML']
  ]);
  
  if (options.ischildcard)
  {
    editfields.push(
      ['flags', 'select', 2, 'Unit or Lesson', 
        {
            2:    T.unit,
            4:    T.lesson
        }
      ]
    );
  } else if (options.isproblem)
  {
    editfields.push(
      ['bitflags',  'bitflags',  obj.flags, T.options, 
        [
          [T.alwaysshowcontent, 'alwaysshowcontent', G.CARD_ALWAYS_SHOW_CONTENT]
        ]
      ]
    );
  }
  
  P.EditPopup(
    editfields,
    function(formdiv, data, resultsdiv, e)
    {
      data.idcode = G.currenteditcard.idcode ? G.currenteditcard.idcode : '';
      
      if (options.iscourse)
      {
        data.flags  = G.CARD_COURSE;
      } else if (options.isproblem)
      {
        data.flags  = G.CARD_PROBLEM;
      }
      
      data.flags  &= ~G.CARD_SECURITY;
      data.flags  |= parseInt(data.security);
      data.flags  |= parseInt(data.bitflags);
      
      P.LoadingAPI(
        resultsdiv,
        '/learn/cardapi',
        {
          command:   'save',
          data:      data
        },
        function(d, resultsdiv)
        {
          if (onfinished)
          {
            onfinished(d, resultsdiv);
          }
        }
      );
    },
    {
      formdiv:            '.EditCard',
      fullscreen:         1,
      background:         'dgreeng',
      obj:                obj,
      headertext:         `<div class="CardFinder"></div>`
    }
  );
  
  G.cardfinder  = new CardFinder({
      div:          '.CardFinder',
      reference:    'G.cardfinder',
      enablesearch: 1,
      iscourse:     options.iscourse,
      ischildcard:  options.ischildcard,
      isproblem:    options.isproblem,
      cardstyles:   'whiteb Rounded Raised HoverHighlight Width1200 Margin25',
      oncardclick:  function(cardel, dbid)
        {
          let cardobj = self.GetItemByID(dbid);
          
          G.currenteditcard = cardobj;
          
          let title       = cardobj.title ? cardobj.title : '';
          let description = cardobj.description ? cardobj.description : '';
          let image       = cardobj.image ? cardobj.image : '';
          let sound       = cardobj.sound ? cardobj.sound : ''
          let video       = cardobj.video ? cardobj.video : ''
          let content     = cardobj.content ? cardobj.content : ''
          let markdown    = cardobj.markdown ? cardobj.markdown : ''
          let html        = cardobj.html ? cardobj.html : ''
          
          let hastitle  = E('.EditCard input.title');
          
          if (hastitle)
          {
            E('.EditCard input.title').value  = title;
            E('.EditCard input.description').value  = description;
          }
          
          E('input.image').value = image;
          
          if (image)
          {
            E('.EditCard .imageRow .FileSource').setAttribute(
              'src', 
              P.FileURL(image) + '.webp'
            );
          } else
          {
            E('.EditCard .imageRow .FileSource').setAttribute('src', '');
          }
            
          E('.EditCard .imageRow .FileID').innerText  = image;
          
          E('input.sound').value = sound;
          
          if (sound)
          {
            E('.EditCard .soundRow .FileIcon').outerHTML  = P.SoundFile(sound, 'FileIcon', 'controls=1');
          } else
          {
            E('.EditCard .soundRow .FileSource').setAttribute('src', '');
          }
          
          E('.EditCard .soundRow .FileID').innerText  = sound;
          
          E('input.video').value = video;
          
          if (video)
          {
            E('.EditCard .videoRow .FileIcon').outerHTML  = P.VideoFile(sound, 'FileIcon', 'controls=1');
          } else
          {
            E('.EditCard .videoRow .FileSource').setAttribute('src', '');
          }
          
          E('.EditCard .videoRow .FileID').innerText  = video;
          
          E('.EditCard .contentRow .content').dataset.value = content;
          E('.EditCard .contentRow .contentTextArea').value = content;
          
          E('.EditCard .markdownRow .markdown').dataset.value = markdown;
          E('.EditCard .markdownRow .markdownTextArea').value = markdown;
          
          E('.EditCard .htmlRow .html').dataset.value = html;
          E('.EditCard .htmlRow .htmlTextArea').value = html;
        }
    }
  );
  
  G.cardfinder.Display();
}

// ====================================================================
G.EditMainCard = function()
{
  G.EditCard(
    G.maincard,
    function(d)
    {
      G.maincard  = d.data;
      P.RemoveFullScreen();
      G.RenderMainCard();
    }
  );
}

// ====================================================================
G.AddChildCard = function()
{
  let self  = this;
 
  G.EditCard(
    {},
    function(d, resultsdiv)
    {
      G.childcards.push(d.data);
      
      let chosenids = [];
      
      for (let i = 0, l = G.childcards.length; i < l; i++)
      {
        let r = G.childcards[i];
        
        chosenids.push(r.idcode);
      }
      
      P.LoadingAPI(
        resultsdiv,
        '/learn/cardapi',
        {
          command:      'setchildcards',
          cardid:       G.maincard.idcode,
          childcardids: chosenids
        },
        function(d)
        {
          G.childcards  = d.data;
          
          P.RemoveFullScreen();
          
          G.RenderChildCards();
        }
      );
    },
    {
      ischildcard:  1
    }
  );
}

// ====================================================================
G.EditChildCards = function()
{
  let self  = this;
  
  G.childcardchooser = new ChildCardChooser({
    div:        '.ChildCardChooser',
    reference:  'G.childcardchooser'
  });
  
  G.childcardchooser.cardid = G.maincard.idcode;
  G.childcardchooser.chosen = G.childcards;
  
  G.childcardchooser.Display();
}

// ====================================================================
G.EditProblem = function(problem, onfinished, options)
{
  problem   = problem || {};
  
  problem.timeout = problem.timeout || 0;
  
  let cardfields  = [
    'problemid', 
    'answerid',
    'explanationid',
    'choice1id',
    'choice2id',
    'choice3id',
    'choice4id',
    'choice5id',
    'choice6id',
    'choice7id'
  ];
  
  cardfields.forEach(
    function(r)
    {
      let field = r.substr(0, r.length - 2);
      
      if (!IsEmpty(problem[field]))
      {
        problem[field + 'id'] = problem[field]['idcode'];
      }
    }
  );
  
  G.currentproblem  = problem;
  
  P.EditPopup(
    [
      ['problemidcode', 'hidden', 0,                T.idcode],
      ['problemid',     'custom', 0,                T.problem,       'CardPlaceHolder'],
      ['answerid',      'custom', 0,                T.answer,        'CardPlaceHolder'],
      ['explanationid', 'custom', 0,                T.explanation,   'CardPlaceHolder'],
      ['choice1id',     'custom', 0,                T.choice + ' 1', 'CardPlaceHolder'],
      ['choice2id',     'custom', 0,                T.choice + ' 2', 'CardPlaceHolder'],
      ['choice3id',     'custom', 0,                T.choice + ' 3', 'CardPlaceHolder'],
      ['choice4id',     'custom', 0,                T.choice + ' 4', 'CardPlaceHolder'],
      ['choice5id',     'custom', 0,                T.choice + ' 5', 'CardPlaceHolder'],
      ['choice6id',     'custom', 0,                T.choice + ' 6', 'CardPlaceHolder'],
      ['choice7id',     'custom', 0,                T.choice + ' 7', 'CardPlaceHolder'],
      ['timelimit',     'int',    problem.timeout,  T.timelimit],
      ['points',        'int',    problem.points,   T.points]
    ],
    function(formdiv, data, resultsdiv, e)
    {
      P.LoadingAPI(
        resultsdiv,
        '/learn/problemapi',
        {
          command:      'save',
          data:         data
        },
        function(d)
        {
          G.currentproblem  = d.data;
          
          if (onfinished)
          {
            onfinished(d.data, resultsdiv);
          }
          
          P.RemoveFullScreen();
        }
      );
    },
    {
      fullscreen: 1,
      obj:        problem,
      headertext: T.editingprobleminstructions
    }
  );
  
  let placeholders  = Q('.CardPlaceHolder');
  
  for (let i = 0, l = placeholders.length; i < l; i++)
  {
    let r         = placeholders[i];
    let field     = r.dataset.field;
    
    field = field.substr(0, field.length - 2)
    
    if (field && problem[field])
    {
      P.HTML(
        r, 
        G.CardToHTML(
          problem[field], 
          'whiteb Pad50 HoverHighlight', 
          '', 
          `onclick="G.EditProblemCard(this, '${problem.idcode}', '${r.dataset.field}', '${problem[field]['idcode']}')"`
        )
      );
    }
  }
  
  for (let i = 0, l = cardfields.length; i < l; i++)
  {
    let r = cardfields[i];    
    
    let field = r.substr(0, r.length - 2)
    
    if (IsEmpty(problem[field]))
    {
      P.HTML(
        '.' + r + 'Div', 
        `<div class="whiteb Pad50 Raised Rounded Center Color4 HoverHighlight" 
          onclick="G.EditProblemCard(this, '${problem.idcode}', '${r}', '')">
          Add
        </div>
        `
      );
    }
  }
}

// ====================================================================
G.GetProblemByID = function(problemid)
{
  let problemsource = G[G.currentproblemtype + 'problems'];
  
  for (let r of problemsource)
  {
    if (r.idcode == problemid)
    {
      return r;
    }
  }
}

// ====================================================================
G.EditExistingProblem = function(problemid)
{
  let problemsource = G[G.currentproblemtype + 'problems'];
  
  G.EditProblem(
    G.GetProblemByID(problemid),
    function(problem, resultsdiv)
    {
      for (let i = 0, l = problemsource.length; i < l; i++)
      {
        if (problemsource[i].idcode == problemid)
        {
          problemsource[i] = problem;
          break;
        }
      }
      
      G.SaveProblems(problemsource, resultsdiv);
    }
  );
}

// ====================================================================
G.EditProblemCard = function(el, problemid, field, fieldidcode)
{
  let problem = G.currentproblem;
  
  let fieldobjname  = field.substr(0, field.length - 2)
  
  let problemcard = problem ? problem[fieldobjname] :  {};
  
  G.EditCard(
    problemcard,
    function(d, resultsdiv)
    {
      problem[fieldobjname] = d.data;
      
      E('input.' + fieldobjname + 'id').value = d.data.idcode;
      
      el.outerHTML  = G.CardToHTML(
        d.data, 
        'whiteb Pad50 HoverHighlight', 
        '', 
        `onclick="G.EditProblemCard(this, '${problem.idcode ? problem.idcode : ''}', '${field}', '${d.data.idcode}')"`
      );
      
      P.RemoveFullScreen();
    },
    {
      isproblem:  1
    }
  );
}

// ====================================================================
G.QuickAddProblem = function()
{
  P.EditPopup(
    [
      [
        'quickproblem', 
        'multitext', 
        '', 
        T.quickaddprobleminstructions
      ]
    ],
    function(formdiv, data, resultsdiv, e)
    {
      let parts   = data.quickproblem.split('\n');
      
      let problem = [];
      let answer  = [];
      
      let inproblem = 1;
      
      for (let i = 0, l = parts.length; i < l; i++)
      {
        let r = parts[i];
        
        if (r)
        {
          if (inproblem)
          {
            problem.push(r);
          } else
          {
            answer.push(r);
          }
        } else
        {
          inproblem = 0;
        }
      }
      
      problem = problem.join('\n').trim();
      answer  = answer.join('\n').trim();
      
      if (!problem || !answer)
      {
        P.HTML(
          resultsdiv, 
          `<div class="Error Pad25">
            ${T.missingproblemoranswer}
          </div>
          `
        );
      } else
      {
        let problemcardid = 0;
        let answercardid  = 0;
        
        P.LoadingAPI(
          resultsdiv,
          '/learn/cardapi',
          {
            command:   'save',
            data:      {
              content:  problem,
              flags:    G.CARD_PROBLEM
            }
          },
          function(d, resultsdiv)
          {          
            problemcardid = d.data.idcode;
            
            P.LoadingAPI(
              resultsdiv,
              '/learn/cardapi',
              {
                command:   'save',
                data:      {
                  content:  answer,
                  flags:    G.CARD_PROBLEM
                }
              },
              function(d, resultsdiv)
              {          
                answercardid = d.data.idcode;
                
                P.LoadingAPI(
                  resultsdiv,
                  '/learn/problemapi',
                  {
                    command:      'save',
                    data:         {
                      problemid:  problemcardid,
                      answerid:   answercardid
                    }
                  },
                  function(d)
                  {
                    let problemsource = G[G.currentproblemtype + 'problems'];
                    
                    problemsource.push(d.data);
                    
                    // Use the chooser to save the new set of problems
                    let chooser = new ProblemChooser({
                      div:        '.ProblemSaver',
                      reference:  'G.problemsaver'
                    });
                    
                    chooser.cardid  = G.maincard.idcode;
                    chooser.chosen  = problemsource;
                    chooser.OnFinished(0);
                    
                    P.CloseThisPopup('.QuickProblemPopup');
                  }
                );
              }
            );
            
          }
        );
      }
    },
    {
      classes:  'Rounded QuickProblemPopup'
    }
  )
}

// ====================================================================
G.AddProblem = function()
{
  let problemsource = G[G.currentproblemtype + 'problems'];
  
  G.EditProblem(
    {},
    function(d, resultsdiv)
    {
      problemsource.push(d);
      
      // Use the chooser to save the new set of problems
      let chooser = new ProblemChooser({
        div:        '.ProblemSaver',
        reference:  'G.problemsaver'
      });
      
      chooser.cardid  = G.maincard.idcode;
      chooser.chosen  = problemsource;
      chooser.OnFinished();
    }
  );
}

// ====================================================================
G.ShowProblems = function(problemtype = '')
{
  if (!problemtype)
  {
    P.ErrorPopup('Please choose a problem type');
    return;
  }
  
  let bgcolor = 'dgreeng';
  
  switch (problemtype)
  {
    case 'quiz':
      bgcolor = 'dblueg';
      break;
    case 'test':
      bgcolor = 'dpurpleg';
      break;
    case 'exam':
      bgcolor = 'dbrowng';
      break;
  }
  
  G.currentproblemtype    = problemtype;
  
  let header  = T.problems;
  
  if (problemtype)
  {
    header  = T[problemtype] + ' ' + T.problems;
  }
  
  P.AddFullScreen(
    bgcolor, 
    '', 
    `<h2>${header}</h2>
    <div class="FlexGrid16 ProblemsList"></div>
    <div class="ButtonBar">
      <a class="Color3" onclick="G.EditProblems('${problemtype}')">${T.editproblems}</a>
      <a class="Color1" onclick="P.RemoveFullScreen()">${T.back}</a>
    </div>
    `
  );
  
  G.RenderProblemCards(problemtype);
}

// ====================================================================
G.EditProblems = function(problemtype = '')
{
  let self  = this;
  
  G.problemchooser = new ProblemChooser({
    div:          '.ProblemChooser',
    reference:    'G.problemchooser'
  });
  
  G.currentproblemtype    = problemtype;
  
  G.problemchooser.cardid = G.maincard.idcode;
  G.problemchooser.chosen = G[problemtype + 'problems'];
  
  G.problemchooser.Display();
}

// ====================================================================
G.OnPageLoad = function()
{
  if (!G.cardstack)
  {
    return;
  }
  
  let ls          = [];
  
  G.breadcrumbs = [];
  
  for (let i = 0, l = G.cardstack.length; i < l; i++)
  {
    let item  = G.cardstack[i];
    
    G.breadcrumbs.push(item.idcode);
    
    if (i == 0)
    {
      ls.push(`
        <a class="Color1" style="padding: 0.5em;" href="/learn/courses.html/${G.classid}/${item.idcode}">
          ${item.title}
        </a>
      `);
    } else
    {
      ls.push('<select class="CardNavSelect" onchange="window.location = this.value">');
      
      let siblingcards  = G.childstacks[i - 1];
      
      for (let j = 0, m = siblingcards.length; j < m; j++)
      {
        let card  = siblingcards[j];
        
        let url   = '/learn/courses.html/' + G.classid + '/' + G.breadcrumbs.slice(0, G.breadcrumbs.length - 1).join('/') + '/' + card.idcode;
        
        ls.push(`<option value="${url}" ${item.idcode == card.idcode ? 'selected=1' : ''}>${card.title}</option>`);
      }
      
      ls.push('</select>');
    }
  }
  
  P.HTML('.CourseNavBar', ls);
  
  G.maincard  = G.cardstack[G.cardstack.length - 1];
  
  G.maincardislesson  = G.maincard.flags & G.CARD_LESSON;
  
  G.RenderMainCard();
    
  if (!G.maincardislesson)
  {
    G.childcards  = G.childstacks[G.cardstack.length - 1];
    G.RenderChildCards();
  } else
  {
    G.RenderProblemCards();
  }
  
  G.RenderSiblingNav();
  
  P.HTML(
    '.MainCardBar',
    G.caneditcards 
      ? `<a class="Color3" onclick="G.EditMainCard()">${T.editcard}</a>`
      : ''
  );  
  
  P.HTML(
    '.ChildCardBar',
    G.caneditcards && !(G.maincard.flags & G.CARD_LESSON)
      ? `<a class="Color3" onclick="G.EditChildCards()">${T.editchildcards}</a>`
      : ''
  );    
  
  P.HTML(
    '.ProblemBar',
    G.caneditcards && (G.maincard.flags & G.CARD_LESSON)
      ? `<a class="Color3" onclick="G.EditProblems()">${T.editproblems}</a>
      <a class="Color4" onclick="G.ShowProblems('quiz')">${T.quiz + ' ' + T.problems}</a>
      <a class="Color5" onclick="G.ShowProblems('test')">${T.test + ' ' + T.problems}</a>
      <a class="Color6" onclick="G.ShowProblems('exam')">${T.exam + ' ' + T.problems}</a>
      `
      : ''
  );    
  
  P.Play('buzzer', '/learn/buzzer.mp3', {autoplay: 0});
  P.Play('ding', '/learn/ding.mp3', {autoplay: 0});
}

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
