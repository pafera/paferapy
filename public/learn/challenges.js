"use strict";

// ********************************************************************
class Challenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    this.rightanswer         = 0;
    this.maxtime             = 10;
    this.timeremaining       = 10;
    this.timerid             = 0;
    this.gamestarted         = 0;
    this.gamefinished        = 0;
    this.numright            = 0;
    this.numwrong            = 0;
    this.score               = 0;
    this.maximumscore        = 0;
    this.questions           = [];
    this.rightproblems       = [];
    this.wrongproblems       = [];
    this.incheckinganswer    = 0;
    this.problemresults      = [];
    this.problemnum          = 0;
    this.activityid          = 0;
    this.lessonids           = [];
    this.unfinishedchallenge = 0;
  }
    
  // ------------------------------------------------------------------
  RemoveFinishedQuestion()
  {
    let self  = this;
    
    for (let i = 0, l = self.questions.length; i < l; i++)
    {
      let r = self.questions[i];
      
      if (self.rightanswer.id == r.id)
      {
        self.questions.splice(i, 1);
        break;
      }
    }
  }
        
  // ------------------------------------------------------------------
  SubmitAnswer(answer, ignorewrongproblems)
  {
    let self  = this;
    
    if (!self.timerid)
    {
      return;
    }
    
    if (self.incheckinganswer)
    {
      return;
    }
    
    self.incheckinganswer  =  1;
    
    let right       =  0;
    
    self.problemendtime  =  Date.now();
    self.problemnum++;
    
    let problemresult  = {
      id:             self.rightanswer.id,
      timeremaining:  self.timeremaining,
    };
        
    if (answer == self.rightanswer.answer)
    {
      P.Play('ding');
      
      P.AddFullScreen(
        'greeng',
        '',
        `<p class="Size300 Center">
          Right!
          <br>${self.rightanswer.display}
          <br>+${self.timeremaining}</p>
          <p>
            <a class="Button NextButton Color4" style="width: 95%;" tabindex="100">
              ${T.nextproblem}
            </a>
          </p>
        `,
        {
          noanimation:  1
        }
      );
      
      self.numright++;
      self.score  +=  (self.rightanswer.points ? self.rightanswer.points : self.timeremaining);
      self.rightproblems.push(self.rightanswer);
      right  =  1;
      
      problemresult['status'] = 'right';
    } else
    {
      if (ignorewrongproblems)
      {
        self.incheckinganswer  =  false;
        return;
      }
    
      P.Play('buzzer');
      P.AddFullScreen(
        'redg',
        '',
        `<p class="Size300 Center">
          Wrong!<br>
          ${self.rightanswer.display}
        </p>
        <p>
          <a class="Button NextButton Color3" style="width: 95%;" tabindex=100>
              ${T.nextproblem}
          </a>
        </p>
        `,
        {
          noanimation:  1
        }
      );
      
      self.numwrong++;
      self.wrongproblems.push(self.rightanswer);
      
      problemresult['status'] = 'wrong';
    }
    
    self.problemresults.push(problemresult)
    
    self.RemoveFinishedQuestion();
    
    self.SetupNextButton();
    
    window.scrollTo(0, 0);
    self.incheckinganswer  =  false;
    self.UpdateScore();
    clearInterval(self.timerid);
    self.timerid  =  0;
  }

  // ------------------------------------------------------------------
  SetupNextButton()
  {
    let self  = this;
    
    let button  =  E('.NextButton');
    
    P.OnClick(
      button,
      P.LBUTTON,
      function()
      {
        self.NextProblem();
      }
    );
    
    P.OnEnter(
      button,
      function(e)
      {
        self.NextProblem();
      }
    );
    
    button.focus();
  }

  // ------------------------------------------------------------------
  OnTimer(self)
  {
    if (self.timeremaining)
    {
      self.timeremaining--;
      
      E('.TimeRemaining').innerText = '🕑 ' + self.timeremaining;
    } else
    {
      self.TimeUp();
    }
  }

  // ------------------------------------------------------------------
  TimeUp()
  {
    let self  = this;
    
    clearInterval(self.timerid);
    self.timerid  =  0;
    self.problemendtime  =  Date.now();
    self.problemnum++;
    
    P.AddFullScreen(
      'yellowb',
      '',
      `<p class="Size300 Center">
        Time up!<br>
        ${self.rightanswer.display}
      </p>
      <p>
        <a class="Button NextButton Color3" style="width: 95%;" tabindex="100">
              ${T.nextproblem}
        </a>
      </p>`,
      {
        noanimation:  1
      }
    );
    
    self.SetupNextButton();
    
    self.RemoveFinishedQuestion();
    
    self.problemresults.push({
      id:       self.rightanswer.id,
      num:      self.problemnum,
      maxtime:  self.maxtime,
      seconds:  0,
      status:   'timeup'
    });
    
    window.scrollTo(0, 0);
    P.Play('buzzer');
    self.numwrong++;
    self.wrongproblems.push(self.rightanswer);
    self.UpdateScore();
  }

  // ------------------------------------------------------------------
  UpdateScore()
  {
    let self  = this;
    
    E('.Score').innerText     = '🪙 ' + self.score;
    E('.NumRight').innerText  = '✅ ' + self.numright;
    E('.NumWrong').innerText  = '❌ ' + self.numwrong;
  }

  // ------------------------------------------------------------------
  OnUnfinishedChallenge()
  {
    let self  = this;
    
    if (self.unfinishedchallenge)
    {
      return;
    }
    
    self.unfinishedchallenge  = 1;
    
    if (!self.gamefinished)
    {
      self.gamefinished = 1;
      
      if (!G.ispractice)
      {
        P.API(
          '/learn/challengeapi',
          {
            command:          'saveunfinishedresult',
            data:             {
              challengeresultid:  G.challengeresultid,
              problemresults:     self.problemresults,
              score:              self.score,
              maximumscore:       self.maximumscore,
              percentright:       (self.numright / (self.numright + self.numwrong) * 100)
            }
          },
          function(d)
          {
          }
        );
      }
      
      return T.challengenotfinished;
    } 
  }
  
  // ------------------------------------------------------------------
  SetupSecurity()
  {
    let self  = this;
    
    window.onbeforeunload = function()
      {
        self.OnUnfinishedChallenge();
        
        self.unfinishedchallenge  = 1;
      }
      
    P.OnUserSwitch(
      function()
      {
        if (self.timerid)
        {
          clearInterval(self.timerid);
        }
        
        if (self.gamefinished 
          || self.unfinishedchallenge 
          || !self.gamestarted
        )
        {
          return;
        }
        
        P.AddFullScreen(
          'dredg',
          '',
          `${T.youleftthispage}
          <div class="ButtonBar">
            <a class="Color3" onclick="window.location.reload()">${T.tryagain}</a>
          </div>
          `
        );
        
        self.OnUnfinishedChallenge();
        
        self.unfinishedchallenge  = 1;
      }
    );          
  }
  
  // ------------------------------------------------------------------
  NextProblem()
  {
    let self  = this;
    
    P.Hide('.StudyListProblems');
    
    P.RemoveFullScreen();
    
    let rect  =  P.AbsoluteRect('.ChallengeArea');
    
    window.scrollTo(0, rect.top);
        
    if (self.questions.length == 0 && !self.gamestarted)
    {
      self.SetupSecurity();
      
      self.gamestarted  = 1;
      
      P.HTML(
        '.BottomBarGrid',
        `
          <table class="Width100P blackb Center Size150">
            <tr>
              <td class="TimeRemaining lblue">🕑 0</td>
              <td class="NumRight lgreen">✅ 0</td>
              <td class="NumWrong lred">❌ 0</td>
              <td class="Score lpurple">🪙 0</td>
            </tr>
          </table>
        `
      );
      
      P.SetLayout({
        bottom: '2em'
      });
      
      self.OnGameStart();
    }
    
    let size  =  self.questions.length;
    
    if (size == 0 && self.gamestarted)
    {
      self.gamefinished = 1;
      P.SetLayout();
      
      self.UploadScore();
      return;
    } else
    {
      self.OnProblemStart();                
      
      self.problemstarttime =  Date.now();
      
      self.timeremaining    =  self.maxtime;
      
      // Skip the timer for challenges that handle their own time by 
      // setting maxtime to 0
      if (self.timeremaining)
      {
        self.timerid          =  setInterval(
          function()
          {
            self.OnTimer(self)
          }, 
          1000
        );
      }
    }
  }

  // ------------------------------------------------------------------
  UploadScore()
  {
    let self  = this;
    
    let ls     = [
      `<div>
        <h1 class="dpurple">${T.challengecomplete}</h1>
        
        <div class="Flex FlexCenter FlexVertical">
          <table class="Bordered Rounded Pad50 Margin25 Size150">
            <tr>
              <td class="blackg">${T.score}</td>
              <td>${self.score}</td>
              <td class="greeng"">${T.right}</td>
              <td class="green">${self.numright}</td>
              <td class="redg">${T.wrong}</td>
              <td class="red">${self.numwrong}</td>
            </tr>
          </table>
        
      <div class="FlexGrid20">
      <div class="Center Rounded Margin25 Pad50" style="border: 0.2em solid green;">
        
        <h2 class="dgreen">${T.right}</h2>
        
        <table class="FullWidth Width100P">`
    ];
    
    if (self.rightproblems.length)
    {
      self.rightproblems.forEach(
        function(r)
        {
          ls.push(`<tr><td>${r.display}</tr></td>`);
        }
      );
    } else
    {
      ls.push(`<tr><td>[None]</tr></td>`);
    }
    
    ls.push(`
      </table>
      
      </div>
      <div class="Center Rounded Margin25 Pad50" style="border: 0.2em solid red;">
      
      <h2 class="dred">${T.wrong}</h2>
      <table class="FullWidth Width100P">
      `
    );
    
    if (self.wrongproblems.length)
    {
      self.wrongproblems.forEach(
        function(r)
        {
          ls.push(`<tr><td>${r.display}</tr></td>`);
        }
      );
    } else
    {
      ls.push(`<tr><td>[None]</tr></td>`);
    }
    
    ls.push(`
            </table>
          </div>
        </div>
      </div>
      <div class="Pad50">
        <h2 class="dpurple">${T.savingscore}</h2>
        <p class="ChallengeSaveResults"></p>
      </div>
      </div>
    `);
    
    if (G.ispractice)
    {
      setTimeout(
        function()
        {
          P.AddFullScreen(
            'whiteb PracticeScreen',
            '',
            ls
          );
            
          P.HTML(
            '.ChallengeSaveResults',
            `<div class="ButtonBar">
                <a class="Color3 ChallengeConfirmButton" onclick="window.location.reload(1)" tabindex="110">${T.tryagain}</td>
                <a class="Color1 ChallengeCancelButton" onclick="window.history.back();" tabindex="111">${T.back}</a>
            </div>
            `
          );
        },
        500
      );
      
      return;
    }
    
    setTimeout(
      function()
      {
        P.AddFullScreen(
          'whiteb',
          '',
          ls
        );
    
        P.LoadingAPI(
          '.ChallengeSaveResults',
          '/learn/challengeapi',
          {
            command:          'saveresult',
            data:             {
              challengeresultid:  G.challengeresultid,
              problemresults:     self.problemresults,
              score:              self.score,
              maximumscore:       self.maximumscore,
              percentright:       (self.numright / (self.numright + self.numwrong) * 100)
            }
          },
          function(d, resultsdiv)
          {
            let passedmessage = '';
            
            if (G.challengeobj.minscore || G.challengeobj.minpercentage)
            {
              if (d.passed)
              {
                passedmessage = `<h4>You Passed!</h4>
                  <p class="green">${T.youpassed} (+${d.bonuspoints})</p>`;
              } else
              {
                passedmessage = `<h4>You Didn't Pass!</h4>
                  <p class="red">${T.youdidntpass}<br>                    
                    <br>
                    ${T.remainingattempts + d.remainingattempts}
                  </p>`;
              }
            }
            
            P.HTML(
              resultsdiv,
              `<p>${d.isnewhighscore ? T.isnewhighscore : T.isnotnewhighscore} ${d.previoushighscore}</p>
                ${passedmessage}
                <div class=ButtonBar>
                  ${ IsEmpty(G.challengeobj) || (!IsEmpty(G.challengeobj) && d.remainingattempts)
                    ? `<a class="Color3 ChallengeConfirmButton" onclick="window.location.reload(1)" tabindex="110">${T.tryagain}</td>`
                    : ''
                  }
                  <a class="Color1 ChallengeCancelButton" onclick="window.history.back();" tabindex="111">${T.back}</a>
              </div>
              `
            );
            
            P.OnEnter(
              '.ChallengeConfirmButton',
              function(e)
              {
                e.target.click();
              }
            );
          }
        );
      },
      500
    );
  }
}


// ********************************************************************
class ClickChallenge extends Challenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }

  // ------------------------------------------------------------------
  OnGameStart()
  {
    let self  = this;
    
    self.maximumscore = G.problems.length * 10;
    
    P.HTML(
      '.ChallengeArea',
      `
        <p class="ChallengeInstructions Center white Bold"></p>
        <div class="ChallengePrompt Pad50 purpleg Center Margin50 Rounded Size150"></div>
        <div class="ProblemImages FlexGrid10"></div>
      `
    );
    
    let ls  = [];
    
    G.problems.forEach(
      function(r)
      {
        ls.push(`
          <div class="ProblemImage Pad50 whiteb Raised Rounded Margin25 HoverHighlight Flex FlexCenter" onclick="G.challengegame.SubmitAnswer('${r.answer.idcode}')">
            ${P.ImgFile(r.problem.image, 'Width800')}
          </div>
        `);
        
        self.questions.push({
          id:					r.idcode,
          points:			0,
          timelimit:	10,
          image:      r.problem.image,
          question:   r.problem.content,
          answer:     r.answer.idcode,
          sound:			r.answer.sound,
          display:		P.ImgFile(r.problem.image, "Width400"),
          flags:      r.problem.flags
        });
      }
    )
    
    P.HTML('.ProblemImages', ls);
    
    self.OnGameStart2();
  }
  
  // ------------------------------------------------------------------
  OnProblemStart()
  {
    let self  = this;
    
    self.questions		=	Shuffle(self.questions);
    self.rightanswer	=	self.questions[0];
    self.maxtime			=	self.rightanswer.timelimit ? self.rightanswer.timelimit : 10;
    
    self.OnProblemStart2();
  }
}

// ********************************************************************
class ListenAndClick extends ClickChallenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }

  // ------------------------------------------------------------------
  OnGameStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengeInstructions', T.listenandclickinstructions);
    P.Hide('.ChallengePrompt');
  }
  
  // ------------------------------------------------------------------
  OnProblemStart2()
  {
    let self  = this;
    
    P.Play(self.rightanswer.sound);
  }
}


// ********************************************************************
class ReadAndClick extends ClickChallenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }
  
  // ------------------------------------------------------------------
  OnGameStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengeInstructions', T.readandclickinstructions);
  }
  
  // ------------------------------------------------------------------
  OnProblemStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengePrompt', self.rightanswer.question);
  }
}

// ********************************************************************
class TypeChallenge extends Challenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }
  
  // ------------------------------------------------------------------
  OnGameStart()
  {
    let self  = this;
    
    self.maximumscore = 0;
    
    P.HTML(
      '.ChallengeArea',
      `
        <p class="ChallengeInstructions Center white Bold"></p>
        <div class="ChallengePrompt Pad50 purpleg Center Margin50 Rounded Size150"></div>
        <div class="Pad50 Center">
          <input class="ChallengeInput Width100P" name="ChallengeInput">
        </div>
      `
    );
    
    P.OnEnter(
      '.ChallengeInput',
      function()
      {
        self.SubmitAnswer(E('.ChallengeInput').value)
      }
    );
    
    G.problems.forEach(
      function(r)
      {
        let answer  = r['answer']['content']
        
        self.questions.push({
          id:					r.idcode,
          points:			0,
          timelimit:	r.timelimit ? r.timelimit : answer.length + 4,
          answer:			answer,
          display:		answer,
          question:   r.problem.content,
          sound:      r.answer.sound,
          image:      r.problem.image,
          flags:      r.problem.flags
        });
        
        self.maximumscore += r.points ? r.points : answer.length + 4;
      }
    );
    
    self.questions		=	Shuffle(self.questions);
    
    self.OnGameStart2();
  }
  
  // ------------------------------------------------------------------
  OnProblemStart()
  {
    let self  = this;
    
    self.rightanswer	=	self.questions[0];
    self.maxtime			=	self.rightanswer.timelimit;
    
    L('Time limit', self.rightanswer.timelimit)
    
    let input   = E('.ChallengeInput');
    
    input.value = '';
    input.focus();
    
    self.OnProblemStart2();
  }  
}

// ********************************************************************
class ListenAndType extends TypeChallenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }
  
  // ------------------------------------------------------------------
  OnGameStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengeInstructions', T.listenandtypeinstructions);
    P.Hide('.ChallengePrompt');
  }
  
  // ------------------------------------------------------------------
  OnProblemStart2()
  {
    let self  = this;
    
    P.Play(self.rightanswer.sound);
  }  
}

// ********************************************************************
class ReadAndType extends TypeChallenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }
  
  // ------------------------------------------------------------------
  OnGameStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengeInstructions', T.readandtypeinstructions);
  }
  
  // ------------------------------------------------------------------
  OnProblemStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengePrompt', self.rightanswer.question);
  }    
}

// ********************************************************************
class LookAndType extends TypeChallenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }
  
  // ------------------------------------------------------------------
  OnGameStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengeInstructions', T.lookandtypeinstructions);
    P.Hide('.ChallengePrompt');
  }
  
  // ------------------------------------------------------------------
  OnProblemStart2()
  {
    let self  = this;
    
    let ls  = [P.ImgFile(self.rightanswer.image, "Square1200")];
    
    if (self.rightanswer.flags & G.CARD_ALWAYS_SHOW_CONTENT)
    {
      ls.push(`
        <br>
        ${self.rightanswer.question}
      `)
    }
    
    P.HTML('.ChallengePrompt', ls);
    P.Show('.ChallengePrompt');
  }    
}

// ********************************************************************
class SpeakChallenge extends Challenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }

  // ------------------------------------------------------------------
  OnGameStart()
  {
    let self  = this;
    
    self.maximumscore = 0;
    
    P.HTML(
      '.ChallengeArea',
      `
        <p class="ChallengeInstructions Center white Bold">
          ${T.listenandspeakinstructions}
        </p>
        <div class="ChallengePrompt Pad50 purpleg Center Margin50 Rounded Size150"></div>
        <div class="PronounciationStatus whiteb Rounded Size125 Pad50 Margin50"></div>
        <div class="Margin50 white Bold Size150 ScoreStatus Center"></div>
        <div class="NextButton Color3 Pad50 Rounded Raised Margin50 Center Bold" tabindex="100">${T.nextproblem}</div>
      `
    );
    
    self.SetupNextButton();
    
    let ls  = [];
    
    G.problems.forEach(
      function(r)
      {
        self.questions.push({
          id:					r.idcode,
          points:			0,
          timelimit:	0,
          image:      r.problem.image,
          problem:    r.problem.content,
          answer:     r.answer.content,
          sound:			r.answer.sound,
          display:    r.answer.content,
          flags:      r.problem.flags
        });        
      }
    );
    
    self.OnGameStart2();
  }
  
  // ------------------------------------------------------------------
  OnProblemStart()
  {
    let self  = this;
    
    clearInterval(self.timerid);
    
    P.Hide('.NextButton');
    P.Hide('.ScoreStatus');
    P.HTML('.PronounciationStatus', '');
    
    self.timerid  = 0;
    
    self.questions		=	Shuffle(self.questions);
    self.rightanswer	=	self.questions[0];
    self.maxtime			=	0;
    
    self.OnProblemStart2();
  }
  
  // ------------------------------------------------------------------
  CheckPronounciation()
  {
    let self  = this;

    let soundid = self.rightanswer.sound;
    
    G.CheckPronounciation(
      self.rightanswer.answer,
      function(error, el, numright, numwrong, html)
      {
        if (self.timerid)
        {
          clearInterval(self.timerid);
          self.timerid  =  0;
        }
          
        if (error)
        {
          P.HTML(
            '.PronounciationStatus'
            `<div class="Error Pad50">
              ${T.errorcontactingserver} ${error}
            </div>
            `
          );
        } else
        {
          self.numright += numright ? 1 : 0;
          self.numwrong += numright ? 0 : 1;
          
          let scoreadded  = numright;
          
          let problemresult  = {
            id:             self.rightanswer.id,
            timeremaining:  self.timeremaining,
          };

          if (!numwrong)
          {
            scoreadded  += 10;
          }
          
          // Under 50% correct results in a wrong answer
          if (
            numright < (
              (numright + numwrong) / 2
            )
          )
          {
            P.Play('buzzer');
            
            problemresult['status'] = 'wrong';
            
            self.wrongproblems.push(self.rightanswer);
          } else
          {
            P.Play('ding');
            
            problemresult['status'] = 'right';
            
            self.rightproblems.push(self.rightanswer);
          }
          
          self.problemresults.push(problemresult)
          
          self.RemoveFinishedQuestion();
          
          self.score        += scoreadded;
          self.maximumscore += 10 + numright + numwrong;
          
          self.UpdateScore();
          
          E('.ScoreStatus').innerText = '+ ' + scoreadded + ' 🪙';
          
          P.Show('.ScoreStatus');

          let el  = E('.NextButton');
          
          P.Show(el);
          el.focus();
        }
      },
      {
        timelimit:  P.media[soundid].duration() * 1.2 + 3
      }
    );
  }
}

// ********************************************************************
class ListenAndSpeak extends SpeakChallenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }

  // ------------------------------------------------------------------
  OnGameStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengeInstructions', T.listenandspeakinstructions);
    P.Hide('.ChallengePrompt');
  }
  
  // ------------------------------------------------------------------
  OnProblemStart2()
  {
    let self  = this;
    
    P.Play(
      self.rightanswer.sound,
      '',
      {
        onfinished: function()
          {
            self.CheckPronounciation();
          }
      }
    );
  }
}

// ********************************************************************
class ReadAndSpeak extends SpeakChallenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }

  // ------------------------------------------------------------------
  OnGameStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengeInstructions', T.readandspeakinstructions);
  }
  
  // ------------------------------------------------------------------
  OnProblemStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengePrompt', self.rightanswer.problem);
    
    setTimeout(
      function()
      {
        self.CheckPronounciation();
      },
      3000
    );
  }
}

// ********************************************************************
class LookAndSpeak extends SpeakChallenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }

  // ------------------------------------------------------------------
  OnGameStart2()
  {
    let self  = this;
    
    P.HTML('.ChallengeInstructions', T.lookandspeakinstructions);
  }
  
  // ------------------------------------------------------------------
  OnProblemStart2()
  {
    let self  = this;
    
    let ls  = [P.ImgFile(self.rightanswer.image, "Square1200")];
    
    if (self.rightanswer.flags & G.CARD_ALWAYS_SHOW_CONTENT)
    {
      ls.push(`
        <br>
        ${self.rightanswer.question}
      `)
    }
    
    P.HTML('.ChallengePrompt', ls);
    
    setTimeout(
      function()
      {
        self.CheckPronounciation();
      },
      3000
    );
  }
}


// ********************************************************************
class TypingChallenge extends Challenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
    
    this.wrongkeys  = {};
  }
  
  // ------------------------------------------------------------------
  NextProblem()
  {
    let self  = this;
    
    // This delay is to ensure that pressing Enter does not 
    // get eaten by the textarea and automatically makes the first
    // character wrong.
    setTimeout(
      function()
      {
        self.SetupTyping();
      },
      500
    );
  }
  
  // ------------------------------------------------------------------
  SetupTyping()
  {
    let self  = this;
    
    self.gamestarted  = 1;
    self.maximumscore = 0;
    
    self.question = G.challengeobj.question;
    
    let typingfields = [];
    
    for (let i = 0, l = self.question.length; i < l; i++)
    {
      let r = self.question[i];
      
      switch (r)
      {
        case ' ':
          r = '&nbsp;';
          break;
        case '\n':
          typingfields.push('<br>');
          break;
      }
      
      typingfields.push(`
        <span class="TypingField Char${i}">${r}</span>
      `);        
    }
    
    P.HTML(
      '.ChallengeArea',
      `
        <p class="ChallengeInstructions Center white Bold"></p>
        <div class="TypingText lgrayb Pad50 Margin25">
          ${typingfields.join('\n')}
        </div>
        <div class="Center Pad50">
          <textarea class="TypingInput Width400 Height150"></textarea>
        </div>
        <div class="TimeBar whiteb Margin50 Rounded MinHeight150"></div>
      `
    );

    let typinginput = E('.TypingInput');
    
    typinginput.focus();
    
    P.NoPaste(typinginput);
    
    self.SetupSecurity();
    
    P.On(
      typinginput, 
      'input',
      function(e)
      {
        let answer  = typinginput.value;
        
        let l = answer.length - 1;
        
        if (l < 0)
        {
          return;
        }
        
        let charfield = E('.Char' + l);
        
        if (answer[l] == self.question[l])
        {
          charfield.classList.remove('lredb');
          charfield.classList.add('lgreenb');
        } else if (charfield)
        {
          charfield.classList.remove('lgreenb');
          charfield.classList.add('lredb');
          
          self.numwrong++;
          
          let rightchar = self.question[l];
          let wrongchar = e.data;
          
          if (!wrongchar)
          {
            wrongchar = '?';
          }
          
          if (!(rightchar in self.wrongkeys))
          {
            self.wrongkeys[rightchar]  = {};
          }
          
          if (!(wrongchar in self.wrongkeys[rightchar]))
          {
            self.wrongkeys[rightchar][wrongchar]  = 0;
          }
          
          self.wrongkeys[rightchar][wrongchar]++;
        }
        
        if (l < self.question.length - 1)
        {
          charfield = E('.Char' + (l + 1));
          
          charfield.classList.remove('lgreenb');
          charfield.classList.remove('lredb');
        }
        
        if (l > self.question.length - 2)
        {
          let numstillwrong = Q('.TypingField.lredb').length;
          
          if (!numstillwrong)
          {
            self.UploadScore();
            return;
          }
        }
      }
    );
    
    self.starttime  = Date.now();
    
    if (G.challengeobj.timelimit)
    {
      self.timelimit  = G.challengeobj.timelimit;
      self.timeleft   = self.timelimit;
      
      P.MakeProgressBar(
        '.TimeBar', 
        {
          title:      T.timeleft,
          min:        0,
          max:        self.timelimit,
          value:      self.timelimit,
          showvalue:  1
        }
      );
      
      self.timerid    = setInterval(
        function(e)
        {
          self.timeleft--;
          
          P.SetProgressBarValue('.TimeBar', self.timeleft);
          
          if (!self.timeleft)
          {
            self.UploadScore(1);
          }
        },
        1000
      );
    }
  }
  
  // ------------------------------------------------------------------
  OnUnfinishedChallenge()
  {
    let self  = this;
  }
  
  // ------------------------------------------------------------------
  UploadScore(incomplete)
  {
    let self  = this;
    
    if (!self.gamestarted)
    {
      return;
    }
    
    self.gamestarted  = 0;
    
    if (self.timerid)
    {
      clearInterval(self.timerid);
      
      self.timerid  = 0;
    }
    
    self.endtime  = Date.now();
    
    self.timeused = (self.endtime - self.starttime) / 1000;
    self.numright = Q('.TypingField.lgreenb').length;;
    self.wpm      = (self.numright / self.timeused / 5 * 60);
    self.accuracy = (self.numright - self.numwrong) / self.numright * 100;
    self.score    = Math.round(self.wpm - (100 - self.accuracy))
    
    let numstillwrong = Q('.TypingField.lredb').length;
    
    if (numstillwrong)
    {
      self.score  = -200;
    }
    
    let ls     = [
      `<div>
        <h1 class="dpurple">${T.challengecomplete}</h1>
        
        <div class="Flex FlexCenter FlexVertical">
          <table class="Bordered Rounded Pad50 Margin25 Size150">
            <tr>
              <th class="blueg">${T.timeused}</th>
              <td>${self.timeused}</td>
            </tr>
            <tr>
              <th class="blackg">${T.letterstyped}</th>
              <td>${self.numright}</td>
            </tr>
            <tr>
              <th class="orangeg">${T.numwrong}</th>
              <td>${self.numwrong}</td>
            </tr>
            <tr>
              <th class="greeng">${T.wordsperminute}</th>
              <td class="green">${self.wpm.toFixed(2)}</td>
            </tr>
            <tr>
              <th class="redg">${T.accuracy}</th>
              <td>${self.accuracy.toFixed(2)}% (<span class="red">-${(100 - self.accuracy).toFixed(0)}</span>)</td>
            </tr>
            <tr>
              <th class="purpleg">${T.score}</th>
              <td class="purple Bold">${self.score}</td>
            </tr>
          </table>
      `
    ];
    
    if (!IsEmpty(self.wrongkeys))
    {
      ls.push(`
        <h2 class="red">${T.wrong}</h2>
        <table class="Styled Center Pad50">
          <tr>
            <th>${T.rightletter}</th>
            <th>${T.youtyped}</th>
            <th>${T.timeswrong}</th>
          </tr>
      `);
      
      let rightkeys = Keys(self.wrongkeys);
      
      rightkeys.sort();
      
      rightkeys.forEach(
        function(rightkey)
        {
          let wrongkeys = Keys(self.wrongkeys[rightkey]);
          
          wrongkeys.sort();
          
          ls.push(`
            <tr>
              <th rowspan="${wrongkeys.length}">${rightkey}</th>
          `);
          
          for (let i = 0, l = wrongkeys.length; i < l; i++)
          {
            let wrongkey  = wrongkeys[i];
            
            ls.push(`
              <td>${wrongkey}</td>
              <td>${self.wrongkeys[rightkey][wrongkey]}</td>
            `);
            
            if (i < l - 1)
            {
              ls.push(`
                </tr>
                <tr>
              `);
            }              
          }
          
          ls.push(`
            </tr>
          `);          
        }
      )
      
      ls.push(`
        </table>
      `)
    }

    ls.push(`
      </div>
      <div class="Pad50">
        <h2 class="dpurple">${T.savingscore}</h2>
        <p class="ChallengeSaveResults"></p>
      </div>
      </div>
    `);
    
    if (G.ispractice)
    {
      P.AddFullScreen(
        'whiteb',
        '',
        ls
      );
      
      P.HTML(
        '.ChallengeSaveResults',
        `<div class="ButtonBar">
            <a class="Color3 ChallengeConfirmButton" onclick="window.location.reload(1)" tabindex="110">${T.tryagain}</td>
            <a class="Color1 ChallengeCancelButton" onclick="window.history.back();" tabindex="111">${T.back}</a>
        </div>
        `
      );
      
      return;
    }    
    
    setTimeout(
      function()
      {
        P.AddFullScreen(
          'whiteb',
          '',
          ls
        );
        
        let problemresults  = [];
        let wrongkeys       = Keys(self.wrongkeys);
        
        wrongkeys.forEach(
          function(rightkey)
          {
            problemresults.push({
              'rightkey':   rightkey,
              'wrongkeys':  self.wrongkeys[rightkey]
            });
          }
        )
        
        if (IsEmpty(problemresults))
        {
          problemresults  = ['All right'];
        }
    
        P.LoadingAPI(
          '.ChallengeSaveResults',
          '/learn/challengeapi',
          {
            command:          incomplete ? 'saveunfinishedresult' : 'saveresult',
            data:             {
              challengeresultid:  G.challengeresultid,
              problemresults:     problemresults,
              score:              self.score,
              maximumscore:       250,
              percentright:       self.accuracy
            }
          },
          function(d, resultsdiv)
          {
            let passedmessage = '';
            
            if (G.challengeobj.minscore || G.challengeobj.minpercentage)
            {
              if (d.passed)
              {
                passedmessage = `<h4>You Passed!</h4>
                  <p class="green">${T.youpassed} (+${d.bonuspoints})</p>`;
              } else
              {
                passedmessage = `<h4>You Didn't Pass!</h4>
                  <p class="red">${T.youdidntpass}<br>                    
                    <br>
                    ${T.remainingattempts + d.remainingattempts}
                  </p>`;
              }
            }
            
            P.HTML(
              resultsdiv,
              `<p>${d.isnewhighscore ? T.isnewhighscore : T.isnotnewhighscore} ${d.previoushighscore}</p>
                ${passedmessage}
                <div class=ButtonBar>
                  ${ IsEmpty(G.challengeobj) || (!IsEmpty(G.challengeobj) && d.remainingattempts)
                    ? `<a class="Color3 ChallengeConfirmButton" onclick="window.location.reload(1)" tabindex="110">${T.tryagain}</td>`
                    : ''
                  }
                  <a class="Color1 ChallengeCancelButton" onclick="window.history.back();" tabindex="111">${T.back}</a>
              </div>
              `
            );
            
            P.OnEnter(
              '.ChallengeConfirmButton',
              function(e)
              {
                e.target.click();
              }
            );
            
            E('.ChallengeCancelButton').focus();
          }
        );
      },
      500
    );
  }  
}


// ********************************************************************
class WriteAnswerChallenge extends Challenge
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }
  
  // ------------------------------------------------------------------
  NextProblem()
  {
    let self  = this;
    
    // This delay is to ensure that pressing Enter does not 
    // get eaten by the textarea and automatically makes the first
    // character wrong.
    setTimeout(
      function()
      {
        self.SetupAnswerArea();
      },
      500
    );
  }
  
  // ------------------------------------------------------------------
  SetupAnswerArea()
  {
    let self  = this;
    
    self.gamestarted  = 1;
    self.maximumscore = 0;
    
    let ls  = [];
    
    let challengeobj  = G.challengeobj;
    
    challengeobj.html = challengeobj.question;
    
    P.HTML(
      '.ChallengeArea',
      `
        <p class="ChallengeInstructions Center white Bold">${T.writeanswerinstructions}</p>
        <div class="Question purpleg Center Bold Pad50 Size150">
          ${G.RenderCard(challengeobj)}
        </div>
        <div class="AnswerForm Margin50 Pad50 whiteb Raised Rounded"></div>
      `
    );
    
    P.EditPopup(
      [
        ['answer', 'multitext', G.previousanswer[0], T.answer],
        ['image', 'imagefile', G.previousanswer[1], T.images],
        ['sound', 'soundfile', G.previousanswer[2], T.sounds],
        ['video', 'videofile', G.previousanswer[3], T.videos],
        ['files', 'filelist', G.previousanswer[4], T.files]
      ],
      function(formdiv, data, resultsdiv, e)
      {
        self.UploadScore(data);
      },
      {
        formdiv:    '.AnswerForm'
      }
    );
    
    self.starttime  = Date.now();
  }
  
  // ------------------------------------------------------------------
  OnUnfinishedChallenge()
  {
    let self  = this;
  }
  
  // ------------------------------------------------------------------
  UploadScore(data)
  {
    let self  = this;
    
    if (!self.gamestarted)
    {
      return;
    }
    
    self.gamestarted  = 0;
    
    let ls     = [
      `<div>
        <h1 class="dpurple">${T.challengecomplete}</h1>
        <div class="Pad50">
          <p class="ChallengeSaveResults"></p>
        </div>
      </div>
    `];
    
    if (G.ispractice)
    {
      P.AddFullScreen(
        'whiteb',
        '',
        ls
      );
        
      P.HTML(
        '.ChallengeSaveResults',
        `<div class="ButtonBar">
            <a class="Color3 ChallengeConfirmButton" onclick="window.location.reload(1)" tabindex="110">${T.tryagain}</td>
            <a class="Color1 ChallengeCancelButton" onclick="window.history.back();" tabindex="111">${T.back}</a>
        </div>
        `
      );
      
      return;
    }
    
    setTimeout(
      function()
      {
        P.AddFullScreen(
          'whiteb',
          '',
          ls
        );
        
        P.LoadingAPI(
          '.ChallengeSaveResults',
          '/learn/challengeapi',
          {
            command:          'saveresult',
            data:             {
              challengeresultid:  G.challengeresultid,
              problemresults:     [
                data.answer,
                data.image,
                data.sound,
                data.video,
                data.files ? data.files.split('\n') : []
              ],
              score:              0,
              maximumscore:       100,
            }
          },
          function(d, resultsdiv)
          {
            let passedmessage = '';
            
            P.HTML(
              resultsdiv,
              `<h3 class="greeng Center Bold Pad50">
                ${T.successfullysaved}<br>
                ${T.waitforteacher}
              </h3>
              <div class=ButtonBar>
                  <a class="Color1 ChallengeCancelButton" onclick="window.history.back();" tabindex="111">${T.back}</a>
                </div>
              `
            );
            
            E('.ChallengeCancelButton').focus();
          }
        );
      },
      500
    );
  }  
}

// ====================================================================
G.OnPageLoad = function()
{ 
  if (!G.challengeresultid
    || !(G.challenge || G.challengeid)
    || (!G.challengeid && IsEmpty(G.lessonids) && IsEmpty(G.problemids))
  )
  {
    P.HTML(
      '.PageLoadContent',
      `<div class="Error Pad50">${T.nothingfound}</div>
      `
    );
    return;
  }
  
  let ls  = [];
  
  let availablechallenges = Keys(G.CHALLENGES);
  
  if (availablechallenges.indexOf(G.challenge) == -1)
  {
    P.HTML(
      '.PageLoadContent',
      `
        <div class="Error Pad50">
          ${T.unrecognizedchallenge} ${G.challenge}
        </div>
      `
    );
    return;
  }
  
  G.challengegame  = new G.CHALLENGES[G.challenge]();
  
  let title         = T[G.challenge.replaceAll('.', '')];
  let instructions  = '';
  
  if (!IsEmpty(G.challengeobj))
  {
    title         = P.BestTranslation(G.challengeobj.title);
    instructions  = P.BestTranslation(G.challengeobj.instructions);
  }
  
  ls.push(`<h1>${title}</h1>
    
    <div class="Instructions Center white">${instructions}</div>
    
    <div class="PassingScore Center white"></div>
    
    <div class="StudyListProblems FlexGrid16"></div>
    
    <div class="ChallengeArea">
      <div class="Center white Pad50">
        ${T.loadingchallenge}
      </div>
      <div>
        <a class="Color3 NextButton Pad50 Margin50 Raised Rounded Block Center" tabindex="100">
          <img src="/system/loading.gif">
        </a>
      </div>
      <div class="PreloadFiles Hidden"></div>
    </div>
  `);
  
  P.HTML('.PageLoadContent', ls);
  
  ls  = [];
  
  if (G.challengeobj.flags & G.CHALLENGE_USE_STUDYLIST)
  {
    G.problems.forEach(
      function(r)
      {
        ls.push(G.RenderProblem(r, 'Margin25 whiteb Pad50'));
      }
    );
    
    P.HTML('.StudyListProblems', ls);
  }
  
  P.Play('buzzer', '/learn/buzzer.mp3', {autoplay: 0});
  P.Play('ding', '/learn/ding.mp3', {autoplay: 0});
    
  ls  = [];
  
  G.problems.forEach(
    function(r)
    {
      G.PROBLEMFIELDS.forEach(
        function(s)
        {
          if (r[s] && r[s]['image'])
          {
            ls.push(P.ImgFile(r[s]['image']));
          }
          
          if (r[s] && r[s]['sound'])
          {
            P.Play(r[s]['sound'], P.FileURL(r[s]['sound']) + '.mp3', {autoplay: 0});
          }
          
          if (r[s] && r[s]['video'])
          {
            ls.push(P.VideoFile(r[s]['video'], '', 'preload="auto"'));
          }          
        }
      );
    }
  );
    
  P.HTML('.PreloadFiles', ls);
  
  if (G.challengeobj.minscore)
  {
    P.HTML('.PassingScore', T.passingscore + G.challengeobj.minscore + '.');
  } else if (G.challengeobj.minpercentage)
  {
    P.HTML('.PassingScore', T.passingpercentage + G.challengeobj.minpercentage + '%.');
  }
  
  setTimeout(
    function()
    {
      G.challengegame.SetupNextButton();
      E('.NextButton').innerText  = T.start;
    },
    3000
  );
}

// ********************************************************************
G.CHALLENGES  = {
  'listen.and.click': ListenAndClick,
  'listen.and.type':  ListenAndType,
  'listen.and.speak': ListenAndSpeak,
  'read.and.click':   ReadAndClick,
  'read.and.type':    ReadAndType,
  'read.and.speak':   ReadAndSpeak,
  'look.and.speak':   LookAndSpeak,
  'look.and.type':    LookAndType,
  'typing':           TypingChallenge,
  'write.answer':     WriteAnswerChallenge
};

G.PROBLEMFIELDS  = [
  'problem',
  'answer',
  'explanation',
  'choice1',
  'choice2',
  'choice3',
  'choice4',
  'choice5',
  'choice6',
  'choice7'
];

// ********************************************************************
P.AddHandler('pageload', G.OnPageLoad);
