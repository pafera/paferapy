"use strict";

// ********************************************************************
class CatList extends PaferaList
{
  // ------------------------------------------------------------------
  constructor(config)
  {
    super(config);
  }
  
  // ------------------------------------------------------------------
  OnRenderItem(r)
  {
    return `
      <div class="FlexAlignSpacer"></div>
      <div class="Center Pad50">
        <div>${P.ImgFile(r.image, 'Width1600')}</div>
        <div class="Size150 blue Pad25">${P.BestTranslation(r.name)}</div>
        <div class="purple Pad25">${P.BestTranslation(r.description)}</div>
        <div class="ButtonBar">
          <div class="Color3 Pad50 Raised Rounded" data-action="Like">👍 ${r.likes}</div>
          <div class="Width600"></div>
          <div class="Color1 Pad50 Raised Rounded" data-action="Dislike">👎 ${r.dislikes}</div>
        </div>
      </div>
      <div class="FlexAlignSpacer"></div>
    `;
  }
  
  // ------------------------------------------------------------------
  Like(card, dbid)
  {
    this.SaveLike(card, dbid, 1);
  }
  
  // ------------------------------------------------------------------
  Dislike(card, dbid)
  {
    this.SaveLike(card, dbid, -1);
  }
  
  // ------------------------------------------------------------------
  SaveLike(card, dbid, like)
  {
    let self  = this;
    
    P.DialogAPI(
      '/mpmcats/catapi',
      {
        command:  'like',
        idcode:   dbid,
        like:     like
      },
      function(d, resultsdiv, popupclass)
      {
        P.ClosePopup(popupclass);
        
        let item  = self.GetItemByID(dbid);
        
        if (like > 0)
        {
          card.querySelector("[data-action='Like']").innerText = `👍 ${item.likes + 1}`;
        } else
        {
          card.querySelector("[data-action='Dislike']").innerText = `👎 ${item.dislikes + 1}`;
        }
      }
    );
  }
}

// ********************************************************************
P.AddHandler(
  'pageload', 
  function()
  {
    P.HTML(
      '.PageLoadContent',
      `<h1>MPM Cats</h1>
      <div class="Center white Pad50">The best place on this website for all of your cat memes!</div>
      <div class="CatsList"></div>
      `
    );
    
    let iscateditor  = P.HasGroup('cateditors');
    
    let fields  = [
      ['image',       'imagefile',    0, 'Image'],
      ['name',        'translation',  0, 'Name'],
      ['description', 'translation',  0, 'Description'],
      ['likes',       'int',          0, 'Likes'],
      ['dislikes',    'int',          0, 'Dislikes']
    ];
  
    G.catslist = new CatList({
      div:            '.CatsList',
      apiurl:          '/mpmcats/catapi',
      displayfields:  fields,
      reference:      'G.catslist',
      enablesearch:   1,
      enableadd:      iscateditor,
      enableedit:     iscateditor,
      enabledlete:    iscateditor,
      liststyle:      'FlexGrid20',
      cardstyles:     'whiteb Width1600 MinHeight2600 Margin25 Pad25 Rounded Flex FlexCenter FlexVertical',
      orderby:        {
        'modified':   'Date',
        'likes':      'Likes',
        'dislikes':   'Dislikes',
      }
    });
    
    G.catslist.Display();
    G.catslist.List();
  }
); 
