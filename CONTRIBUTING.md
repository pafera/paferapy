All contributions are welcome, no matter large or small.

Please set your editor to using spaces instead of tabs and try to use Allman style for JavaScript coding.

Be aware that the code in this project was written over a period of years on an "Oh, I have this problem,
so I should probably add this functionality" basis and not on some massive overly planned, predestined
plan, so there are probably plenty of warts to work out. 
