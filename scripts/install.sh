#!/bin/sh
#
# Note: I normally update python packages globally and ignore warnings
# with the package manager. If you have your own virtual environment,
# then delete the sudo in front of pip commands.
#
# Certain packages such as festival, fd, ripgrep, and vosk are not 
# actually required to use the framework, but they come in handy for 
# advanced features and ease of developmenet.

GITURL="https://github.com/pafera/paferapy.git"

if hash pacman 2>/dev/null
then
  echo ">>>Installing paferapy for Arch Linux..."
  
  sudo pacman -Sy
  sudo pacman -S python
  sudo pacman -S git
  sudo pacman -S openssl-tool
  sudo pacman -S imagemagick
  sudo pacman -S ffmpeg
  sudo pacman -S p7zip
  sudo pacman -S openssh
  sudo pacman -S python-pip
  sudo pacman -S festival festival-us
  sudo pacman -S fd ripgrep

  sudo pip install --upgrade pip
  sudo pip install wheel
  sudo pip install flask
  sudo pip install user-agent-parser
  sudo pip install markdown
  sudo pip install vosk
  sudo pip install uwsgi
  sudo pip install webauthn
  sudo pip install qrcode
  
  # *********************************************************************
  # Uncomment if you want to use Mariadb
  #sudo pacman -S mariadb
  #sudo pip install mysql-connector

  # *********************************************************************
  # Uncomment if you want to use PostgreSQL
  #sudo pacman -S postgresql
  #sudo pip install psycopg2
elif hash termux-open 2>/dev/null
then
  echo ">>>Installing paferapy for Android Termux..."
  platform="termux"
  
  termux-change-repo
  pkg upgrade
  pkg install build-essential
  pkg install python
  pkg install git
  pkg install openssl-tool
  pkg install imagemagick
  pkg install ffmpeg
  pkg install p7zip
  pkg install openssh
  pkg install termux-api
  pkg install iproute2
  pkg install fd
  pkg install ripgrep
  pkg install python-cryptography
  termux-speech-to-text
  
  pip install --upgrade pip
  pip install wheel
  pip install flask
  pip install user-agent-parser
  pip install markdown
  pip install six
  pip install uwsgi
  pip install webauthn
  pip install qrcode
  
  # Vosk isn't actually available for termux yet, but we'll keep this 
  # here for whenever someone does the porting. I've tried myself with
  # *very* limited success.
  #
  # For now, we'll use termux-speech-to-text through the Termux: API 
  # APK. It only works server-side, but considering that we're running
  # the server from a phone, it's not hard to just put the phone beside
  # the person that's speaking.
  #pip install vosk
  
  # *********************************************************************
  # Uncomment if you want to use Mariadb
  #pkg install mariadb
  #pip install mysql-connector

  # *********************************************************************
  # Uncomment if you want to use PostgreSQL
  #pkg install postgresql
  #pip install psycopg2  
  
  # Setup ffmpegthumbnailer for termux
  wget pafera.com/files/termuxfiles.7z
  7z e termuxfiles.7z
  cp ffmpegthumbnailer /data/data/com.termux/files/usr/bin
  cp libffmpegthumbnailer.so  /data/data/com.termux/files/usr/lib
elif hash apt 2>/dev/null
then
  echo ">>>Installing paferapy for Ubuntu..."
  
  sudo apt update
  sudo apt install build-essential
  sudo apt install python3
  sudo apt install git
  sudo apt install openssl
  sudo apt install imagemagick
  sudo apt install ffmpeg
  sudo apt install p7zip-full
  sudo apt install openssh-client
  sudo apt install python3-pip
  sudo apt install festival
  sudo apt install fd-find
  sudo apt install ripgrep

  sudo pip install --upgrade pip
  sudo pip install wheel
  sudo pip install flask
  sudo pip install user-agent-parser
  sudo pip install markdown
  sudo pip install vosk
  sudo pip install uwsgi
  sudo pip install webauthn
  sudo pip install qrcode
  
  # *********************************************************************
  # Uncomment if you want to use Mariadb
  #sudo apt install mariadb
  #sudo pip install mysql-connector

  # *********************************************************************
  # Uncomment if you want to use PostgreSQL
  #sudo apt install postgresql
  #sudo pip install psycopg2
else
  echo "Unsupported system! Please edit this install file for your system!"
  exit 1
fi

# *********************************************************************
# Shared commands follow
git clone $GITURL

cd paferapy

# OpenSSL is needed for speech recognition since only https pages 
# can record audio
openssl req -x509 --newkey rsa:4096 --out sslcert.pem --keyout sslkey.pem --days 9999

echo 'Running stand alone server. Please type "localhost:8888" into your browser.'
scripts/runstandaloneserver
