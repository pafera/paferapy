#!/bin/python3
# 
# Simple script to removed expired HTML pages from the
# static directory.

import os
import os.path
import time
import json
import datetime

# Cache time in seconds for HTML files
APP_DIR     = '.'
CACHE_TIME  = 10

try:
  with open(APP_DIR + '/private/system/pafera.cfg', 'r') as f:
    siteconfig  = json.load(f)
    
    CACHE_TIME  = int(siteconfig['htmlcachetime'])
except Exception as e:
  pass

firstrun  = 1

while 1:
  currenttime = time.time()
  
  #print('Running cache cleanup at', datetime.datetime.fromtimestamp(currenttime))
  
  filelist  = []
  
  try:
    with open(APP_DIR + '/private/system/cachefiles.txt', 'r') as f:
      filelist  = f.read().split("\n")
      
    if filelist:
      for f in filelist:
        if not f:
          continue
        
        path  = APP_DIR + '/public' + f
        
        if firstrun:
          if os.path.exists(path):
            print(f'CleanCache:\tFirst run: cleaned {path} from cache.')
            os.remove(path)
        else:
          if os.path.exists(path) and os.stat(path).st_mtime + CACHE_TIME < currenttime:
            print(f'CleanCache:\tCleaned {path} from cache.')
            os.remove(path)
            
  except Exception as e:
    print(e)
    
  firstrun  = 0  
  time.sleep(CACHE_TIME)
    
