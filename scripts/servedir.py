#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Very simple directory browser designed so that students can 
# download APK files from my phone using their phones' built-in
# web browsers. Also doubles as an image and video viewer.
#
# This should be started with the runservedir script, changing
# the directories for your device as needed. 
#
# By default, the port used is 5000, so you just need to run 
# the script, type in your IP address plus :5000, and you're
# ready to go.
#
# When using the image viewer, you can swipe left and right to
# change the image, down to exit, or up to delete the image.
# Since mis-swipes are common, the deletion function actually
# just renames the file to a .deleted extension, meaning that 
# you'll have a chance to recover the image if you delete it 
# by accident.
#
# This script requires ffmpegthumbnailer and ImageMagick to be 
# installed for thumbnail functionality.

import os
import os.path
import json
import importlib
import datetime
import sys
import traceback
import subprocess
import re
import urllib.parse

from flask import Flask, session, redirect, url_for, request, current_app, g, send_from_directory, abort
from flask.sessions import SecureCookieSessionInterface
from markupsafe import escape

application = Flask('serverdir')
app = application
app.config['SESSION_COOKIE_SAMESITE'] = "Lax"

# -------------------------------------------------------------------
def MakeThumbnail(g, filepath, thumbfile):
  icondir = os.path.join('servedir', 'icons')
  
  os.makedirs(icondir, exist_ok = True)
  
  if thumbfile == 'folder.webp':
    return send_from_directory(icondir, "folder.webp")
  elif thumbfile == 'up.webp':
    return send_from_directory(icondir, "up.webp")
  
  os.makedirs(os.path.split(thumbfile)[0], exist_ok = True)
  
  extension = os.path.splitext(filepath)[1][1:]
  
  thumbresolution = '256'
    
  if extension in [
    'jpg',
    'jpeg',
    'gif',
    'png',
    'tif',
    'tiff',
    'svg',
    'psd',
    'bmp',
    'pcd',
    'pcx',
    'pct',
    'pgm',
    'ppm',
    'tga',
    'img',
    'raw',
    'webp',
    'wbmp',
    'eps',
    'cdr',
    'ai',
    'dwg',
    'indd',
    'dss',
    'fla',
    'ss',
  ]:
    try:
      stats     = os.stat(filepath)
      size      = stats.st_size
      mtime     = stats.st_mtime
    except Exception as e:
      size	 = 0
      mtime = 0
      
    if size < (20 * 1024):
      filedir, filename = os.path.split(filepath)
      return send_from_directory(filedir, filename)
      
    if os.path.exists(thumbfile):
      try:
        stats       = os.stat(thumbfile)
        thumbmtime  = stats.st_msize
      except Exception as e:
        thumbmtime = 0
      
      if mtime <= thumbmtime:
        filedir, filename = os.path.split(thumbfile)
        return send_from_directory(filedir, filename)
    
    subprocess.run(
      [
        'convert',
        filepath if extension != 'gif' else filepath + '[0]',
        '-thumbnail',
        thumbresolution + 'x' + thumbresolution + '^',
        '-gravity',
        'center',
        '-extent',
        thumbresolution + 'x' + thumbresolution,
        thumbfile
      ]
    )
    
    if os.path.exists(thumbfile):
      filedir, filename = os.path.split(thumbfile)
      return send_from_directory(filedir, filename)
      
    return send_from_directory(icondir, "image.webp")
  elif extension in [
    'doc',
    'docx',
    'odt',
    'rtf',
    'pub',
    'wpd',
    'qxd',
    'cdl',
    'eml',
  ]:
    return send_from_directory(icondir, "document.webp")
  elif extension in [
    'chm',
    'pdf',
    'epub',
    'mobi',
    'cbr',
    'cbz',
    'cb7',
    'cbt',
    'cba',
    'ibooks',
    'kf8',
    'pdg',
    'azw',
    'prc',
  ]:
    return send_from_directory(icondir, "book.webp")
  elif extension in [
    'mp3',
    'aac',
    'wma',
    'oga',
    'ogg',
    'm4a',
    'wav',
    'aif',
    'aiff',
    'dvf',
    'm4b',
    'm4p',
    'mid',
    'midi',
    'ram',
    'mp2',
  ]:
    return send_from_directory(icondir, "audio.webp")
  elif extension in [
    'mp4',
    'mpg',
    'avi',
    'wmv',
    'rm',
    'rmvb',
    'ogv',
    'ogm',
    'm4v',
    'mov',
    'flv',
    'f4v',
    '3gp',
    '3gpp',
    'vob',
    'asf',
    'divx',
    'mswmm',
    'asx',
    'amr',
    'mkv',
    'vp8',
    'webm',
  ]:
    if os.path.exists(thumbfile):
      try:
        stats       = os.stat(filepath)
        thumbstats  = os.stat(thumbfile)
        
        if stats.st_mtime <= thumbstats.st_mtime:
          filedir, filename = os.path.split(thumbfile)
          return send_from_directory(filedir, filename)
      except Exception as e:
        print(e)
    
    subprocess.run([
      'ffmpegthumbnailer',
      '-i',
      filepath,
      '-s',
      thumbresolution,
      '-o',
      thumbfile + '.jpg'
    ])
    
    if os.path.exists(thumbfile + '.jpg'):
      subprocess.run([
        'convert',
        thumbfile + '.jpg',
        '-thumbnail',
        thumbresolution + 'x' + thumbresolution + '^',
        '-gravity',
        'center',
        '-extent',
        thumbresolution + 'x' + thumbresolution,
        thumbfile
      ])
      os.remove(thumbfile + '.jpg')
      
    if os.path.exists(thumbfile):
      filedir, filename = os.path.split(thumbfile)
      return send_from_directory(filedir, filename)
      
    return send_from_directory(icondir, "video.webp")
  elif extension in [
    'zip',
    'gz',
    'bz2',
    'rar',
    'tar',
    'lz',
    'lzma',
    'lzo',
    'rz',
    'sfark',
    'xz',
    'z',
    '7z',
    's7z',
    'ace',
    'cab',
    'pak',
    'rk',
    'sfx',
    'sit',
    'sitx',
    'qbb',
    'jar',
    'pst',
    'hqx',
    'sea',
  ]:
    return send_from_directory(icondir, "archive.webp")
  elif extension in [
    'exe',
    'msi',
    'bat',
    'cmd',
  ]:
    return send_from_directory(icondir, "executable.webp")
  elif extension in [
    'xls',
    'xlsx',
    'ods',
    'qbw',
    'wps',
  ]:
    return send_from_directory(icondir, "spreadsheet.webp")
  elif extension in [
    'ppt',
    'pptx',
    'odp',
  ]:
    return send_from_directory(icondir, "presentation.webp")
  elif extension in [
    'txt',
    'ini',
    'cfg',
    'log',
  ]:
    return send_from_directory(icondir, "text.webp")
  elif extension in [
    'htm',
    'html',
    'mhtml',
  ]:
    return send_from_directory(icondir, "webpage.webp")
    
  return send_from_directory(icondir, "unknown.webp")

# =====================================================================
def MakePage(title, content):
  return f"""<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8">	
  <meta name="apple-mobile-web-app-capable" content="yes" /> 
  <meta name="mobile-web-app-capable" content="yes" /> 
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <link rel="manifest" href="/manifest.json">

  <title>{title} - ServeDir</title>

<style>

*, *::before, *::after 
{{
  box-sizing: border-box;
}}

.FileList
{{
  display:  flex;
  flex-wrap:	wrap;
  flex-flow:	center;
}}

.FileList a
{{
  display:          block;
  flex:             1;
  min-width:        16em;
  max-width:        24em;
  overflow:         auto;
  word-break:       break-all;
  padding:          0.5em;
  margin:           0.2em;
  text-decoration:  none;
}}

.FileList a:hover
{{
  background-color: yellow;
}}

.Directory
{{
  color:            black;
  background-color: yellow;
  font-weight:      bold;
}}

.File
{{
  color:            black;
  background-color: #afa;
}}

.FileEntry
{{
  display:  flex;
  align-items:			center;
  justify-content:	center;
}}

.FileEntry img
{{
  width:    6em;
  height:   5em;
  padding:  0.2em;
}}

.FileEntry div
{{
  flex:   1;
}}

.FullScreen
{{
  z-index:              10000; 
  position:             fixed; 
  top:                  0%; 
  left:                 0%; 
  width:                100vw; 
  height:               100vh; 
  color:                white; 
  background-color:     black; 
  background-size:      contain; 
  background-repeat:    no-repeat; 
  background-position:  center center; 
}}

.FullScreenButton
{{
  display:          block; 
  position:         fixed; 
  font-size:        2em; 
  text-align:       center;
  background-color: #999; 
  opacity:          30%; 
  cursor:           pointer;
  min-width:        1.4em;
  border-radius:    0.3em;
}}

</style>
  
</head>
<body>

<div class="FileList">
{content}
</div>

<script>

function IsString(v)
{{
	return (typeof v == 'string' || v instanceof String);
}}

function IsNum(v)
{{
	return typeof v == 'number';
}}

function IsArray(v)
{{
	return (v instanceof Array)
		|| (Object.prototype.toString.apply(v) === '[object Array]');
}};

window.I	= function(id)
{{
	return document.getElementById(id);
}}

window.Q	= function(selector)
{{
	if (IsString(selector))
	{{
		if (selector[0] == '#')
			return [ I(selector.substr(1)) ];
	
		return document.querySelectorAll(selector);
	}}
	
	if (IsArray(selector) || selector instanceof NodeList)
		return selector;
	
	if (selector)
		return [selector];
	
	return [];
}}


window.E	= function(selector)
{{
	return IsString(selector) 
		? document.querySelector(selector)
		: selector;
}}

window.L	= console.log;

function AddHandler(selector, events, func, options)
{{
  let elements 	= IsString(selector) ? Q(selector) : [selector];
  
  for (let i = 0, l = elements.length; i < l; i++)
  {{
    if (!elements[i])
      continue;
    
    let eventnames 	= events.split(' ');
    
    for (let j = 0, m = eventnames.length; j < m; j++)
    {{
      elements[i].addEventListener(eventnames[j], func, options);
    }}
  }}
  
  return this;
}}

function CancelBubble(e) 
{{
  e	=	e || window.event;

  if (!e)
    return;

  if (e.stopPropagation) 
  {{
    e.preventDefault();
    e.stopPropagation();
  }} else 
  {{
    e.cancelBubble = true;
  }}
  
  return this;
}}

function HTMLToDOM(html)
{{
  if (IsArray(html))
    html 	= html.join('\\n');
  
  return document.createRange().createContextualFragment(html);
}}

function ExitFullScreen()
{{
  E('.FullScreen').remove();
}}

function ShowElement(selector, show)
{{
  let elements 	= Q(selector);
  
  for (let i = 0, l = elements.length; i < l; i++)
  {{
    let item 	= elements[i];
    
    if (!item)
      continue;
    
    if (show)
    {{
      if (item.olddisplay && item.olddisplay != 'none')
      {{
        item.style.display 	= item.oldisplay;
      }} else
      {{
        item.style.display  = 'block';
      }}
    }} else if (item.style.display != 'none')
    {{
      item.olddisplay 		= item.style.display;
      item.style.display 	= 'none';
    }}
  }}
}}

function OnImageClick(e)
{{
    let images  = Q('.jpg, .jpeg, .gif, .png, .webp');
    
    let imagehrefs  = [];
    
    for (let i = 0, l = images.length; i < l; i++)
    {{
      imagehrefs.push(images[i].getAttribute('href'));
    }}
    
    window.slideshowhrefs = imagehrefs;
    
    let searchelement = e.target;
    
    while (searchelement.tagName != 'A')
      searchelement = searchelement.parentNode;
      
    let thishref  = searchelement.getAttribute('href');
    
    window.slideshowposition  = imagehrefs.indexOf(thishref);
  
    document.body.appendChild(
      HTMLToDOM(
        `<div class="FullScreen">
          <a class="FullScreenButton PreviousImageButton" style="left: 0%; top: 45%;" onclick="ChangeImage(-1)">&lt;</a>
          <a class="FullScreenButton NextImageButton" style="right: 0%; top: 45%;" onclick="ChangeImage(1)">&gt;</a>
          <a class="FullScreenButton DeleteButton" style="left: 20%; top: 0%; font-size: 1em; height: 1.5em;" onclick="DeleteFile();">🗑</a>
          <a class="FullScreenButton ExitButton" style="left: 40%; bottom: 0%; font-size: 1em;" onclick="ExitFullScreen();">x</a>
        </div>`
      )
    );
    
    ChangeImage(0);
    
    CancelBubble(e);
    
    E('.FullScreen').addEventListener('touchstart', OnTouchStart);
    E('.FullScreen').addEventListener('touchmove', OnTouchMove);
}}

function ChangeImage(diff)
{{
  window.slideshowposition += diff;
  
  if (window.slideshowposition < 0)
    window.slideshowposition = 0;
    
  if (window.slideshowposition > window.slideshowhrefs.length - 1)
    window.slideshowposition = window.slideshowhrefs.length - 1;
  
  E('.FullScreen').style.backgroundImage  = "url('" + window.slideshowhrefs[window.slideshowposition] + "')";
  
  ShowElement('.PreviousImageButton', window.slideshowposition != 0);
  ShowElement('.NextImageButton', window.slideshowposition != window.slideshowhrefs.length - 1);
  
  E('.ExitButton').innerHTML  = (window.slideshowposition + 1) + '/' + window.slideshowhrefs.length + ' <span color="red">X</span>';
  
  let href      = decodeURIComponent(window.slideshowhrefs[window.slideshowposition]);
  let filename  = href.substr(href.lastIndexOf('/') + 1)
  
  E('.DeleteButton').innerHTML  = filename + ' <span color="red">🗑</span>';
  
  window.swipelock  = 1;
  
  setTimeout(
    function()
    {{
      window.swipelock  = 0;
    }},
    200
  );
}}

function OnTouchStart(e)
{{
  window.lasttouchx = e.targetTouches[0].pageX;
  window.lasttouchy = e.targetTouches[0].pageY;
}}

function OnTouchMove(e)
{{
  if (window.swipelock || e.targetTouches.length > 1)
    return;
  
  let xdiff = e.targetTouches[0].pageX - window.lasttouchx;
  window.lasttouchx = e.targetTouches[0].pageX;
  
  let ydiff = e.targetTouches[0].pageY - window.lasttouchy;
  window.lasttouchy = e.targetTouches[0].pageY;
  
  if (xdiff > 12)
  {{
    ChangeImage(1);
  }} else if (xdiff < -12)
  {{
    ChangeImage(-1);
  }} else if (ydiff > 12)
  {{
    ExitFullScreen();
  }} else if (ydiff < -12)
  {{
    DeleteFile();
  }}
  
  e.preventDefault();
}}

function DeleteFile()
{{
  fetch('/__' + window.slideshowhrefs[window.slideshowposition]);
  
  window.slideshowhrefs.splice(window.slideshowposition, 1);
  
  if (window.slideshowhrefs.length == 0)
  {{
    ExitFullScreen();
  }} else
  {{
    ChangeImage(0);
  }}  
}}

AddHandler(
  '.jpg, .jpeg, .gif, .png, .webp',
  'click tap',
  OnImageClick
);

</script>

</body>
</html>"""


# =====================================================================
@app.route('/', defaults={'path': ''}, methods = ['GET', 'POST'])
@app.route('/<path:path>', methods = ['GET', 'POST'])
def index(path):
  g.request   = request
  g.app       = app
  g.servedir  = os.environ['FLASK_SERVEDIR']
  
  if not path:
    filepath  = g.servedir
    path      = ''
  else:
    path      = '/' + path.replace('..', '')
    filepath  = g.servedir + path
  
  thumbpath = ''
  
  # The special underscore directory serves thumbnails
  if len(path) > 3 and path.startswith('/_/'):
    path  = path[3:]
    filepath  = g.servedir + '/' + path
    
    if path == 'folder.webp' or path == 'up.webp':
      thumbpath = path
    else:
      thumbpath = os.path.join('servedir', 'thumbs', path + '.webp')
      
    return MakeThumbnail(g, filepath, thumbpath)
  
  # The special double underscore directory deletes the file
  if len(path) > 4 and path.startswith('/__/'):
    path  = path[4:]
    filepath  = g.servedir + '/' + path
    os.rename(filepath, filepath + '.deleted')
    return '{}'
  
  # Returns directory listings
  if os.path.isdir(filepath):
    filelist  = os.listdir(filepath)
    dirs      = []
    files     = []
    
    for f in filelist:
      if os.path.isdir(os.path.join(filepath, f)):
        dirs.append(f)
      else:
        files.append(f)
    
    dirs.sort(key = str.lower)
    files.sort(key = str.lower)
    
    ls  = []
    
    if path:
      ls.append(f"""<a class="Directory" href="{urllib.parse.quote(os.path.dirname(path))}">
  <div class="FileEntry">
    <img src="/_/up.webp">
    <div>{os.path.basename(path)}/..</div>
  </div>
</a>""")
    
    for d in dirs:
      ls.append(f"""<a class="Directory" href="{urllib.parse.quote(path)}/{urllib.parse.quote(d)}">
  <div class="FileEntry">
    <img src="/_/folder.webp">
    <div>{d}</div>
  </div>      
</a>""")
    
    for f in files:
      extension = os.path.splitext(f)[1][1:]
      filename  = os.path.join(g.servedir, path[1:] if path else '', f)
      
      try:
        size     = os.stat(filename).st_size
      except Exception as e:
        size	= 0
      ls.append(f"""<a class="File {extension}" href="{urllib.parse.quote(path)}/{urllib.parse.quote(f)}">
  <div class="FileEntry">
    <img src="/_{urllib.parse.quote(path)}/{urllib.parse.quote(f)}">
    <div>
      {f}<br>
      {int(size / 1024)}k
    </div>
  </div>      
      
</a>""")
    
    return MakePage(path, '\n'.join(ls))
  
  # Direct download the file
  elif os.path.exists(filepath):
    parts = os.path.split(filepath)
    return send_from_directory(parts[0], parts[1])
    
  return f"""Couldn't find {path}"""

# *********************************************************************
if __name__ == '__main__':
  
  if 'APP_DEBUG' in os.environ:
    app.debug = True
    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)
  
  #Cleanup thumbs directory
  subprocess.run(['find', 'servedir/thumbs', '-mtime', '+7', '-delete'])
  subprocess.run(['find', 'servedir/thumbs', '-type', 'd', '-empty', '-delete'])
  
  app.run()
 
