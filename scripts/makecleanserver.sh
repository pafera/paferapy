#!/bin/sh

# Change to server directory
SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
cd $(dirname $SCRIPT_DIR)

rm -rf public/system/*html
rm -rf private/system/*
rm -rf public/system/headshots
rm -rf public/system/newheadshots
rm -rf public/system/files
rm -rf private/system/files
rm -rf public/system/thumbs
rm -rf private/system/thumbs
