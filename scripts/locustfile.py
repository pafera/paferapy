import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
	wait_time = between(1, 2.5)

	@task
	def hello_world(self):
			self.client.get("/index.html")

	@task(3)
	def view_items(self):
			for item_id in range(10):
					self.client.get(f"/index.html")
					time.sleep(1)

	def on_start(self):
			self.client.get("/index.html")
