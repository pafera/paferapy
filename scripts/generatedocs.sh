#!/bin/bash
# 
# Be sure that you have doxygen and jsdoc installed on your system.

# Change to server directory
SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
cd $(dirname $SCRIPT_DIR)

# Generate python documentation
if [ "$1" = 'python' ] || [ "$1" = '' ]
then
	doxygen scripts/Doxyfile
fi

# Generate JavaScript documentation
if [ "$1" = 'js' ] || [ "$1" = '' ]
then 
	jsdoc -r -d public/docs/js public/system
fi


