#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import json
import importlib
import datetime
import sys
import traceback
import types
import re
import uuid
import urllib.parse
import subprocess
import signal
import time

# Set an absolute path here if you don't start the server in the app directory
APP_DIR = '.'

os.chdir(APP_DIR)

sys.path.append(APP_DIR + '/libs')

from flask import Flask, session, redirect, url_for, request, current_app, g, abort, send_from_directory
from flask.sessions import SecureCookieSessionInterface
from markupsafe import escape

from apps.system.utils import *
from apps.system.db import *
from apps.system.cache import *
from apps.system.session import *
from apps.system.page import *
from apps.system.user import *

application = Flask('pafera')
app = application

# Auto generate a flask configuration file if necessary
FLASK_CONFIG_FILE = APP_DIR + '/private/system/flaskconfig.py'

if not os.path.exists(FLASK_CONFIG_FILE):
  os.makedirs(os.path.dirname(FLASK_CONFIG_FILE), exist_ok = 1)
  
  with open(FLASK_CONFIG_FILE, 'w+') as f:
    f.write(f'''SECRET_KEY  = '{uuid.uuid4().hex}'
SESSION_COOKIE_SAMESITE	= 'Lax'
MAX_CONTENT_LENGTH			= 128 * 1024 * 1024            
''')

app.config.from_pyfile(APP_DIR + '/private/system/flaskconfig.py')

# =====================================================================
@app.route('/', defaults={'path': ''}, methods = ['GET', 'POST'])
@app.route('/<path:path>', methods = ['GET', 'POST'])
def index(path):
  """Handles all requests for the server. 
  
  We route all requests through here to handle the database and session
  logic in one place.
  """
  
  # For standalone development servers
  # Production servers should use nginx to serve static files
  if os.path.isfile('public/' + path):
    return send_from_directory(os.path.abspath('public'), path)
  
  result  = 404
        
  g.request   = request
  g.app       = app
  g.T         = types.SimpleNamespace()
  g.lang      = 'en'
  g.pagepath  = path
  
  newinstall  = 0
  
  # Load translations
  T = g.T
  
  for lang in list(g.request.accept_languages.values()):
    try:
      with open('public/system/translations/' + lang + '/system.js', 'r') as f:
        g.lang  = lang
        exec(f.read())
      break
    except Exception as e:
      pass
  
  # Load a default English translation if nothing else is available
  if not hasattr(T, 'back'):
    with open('public/system/translations/en/system.js', 'r') as f:
      exec(f.read())
          
  # Load the library configuration file, generating default 
  # settings if necessary
  try:
    try:
      with open('private/system/pafera.cfg', 'r') as f:
        g.siteconfig  = json.load(f)
    except Exception as e:
      import apps.system.initsite
      
      apps.system.initsite.InitConfig(g)  
      
    if g.siteconfig['adminpassword']:
      print(f'''
>>> Your auto-generated admin password is "{g.siteconfig['adminpassword']}"
>>> Please change this before doing anything else!
      ''')
        
    g.db  = DB(
      connectionname  = 'default',
      dbhost          = g.siteconfig['dbhost'],
      dbname          = g.siteconfig['dbname'], 
      dbtype          = g.siteconfig['dbtype'],
      dbuser          = g.siteconfig['dbuser'],
      dbpassword      = g.siteconfig['dbpassword'],
      dbflags         = g.siteconfig['dbflags'],
      lang            = g.lang,
    )
    
    if not g.db.HasTable('system_user'):
      import apps.system.initsite
      
      apps.system.initsite.InitDB(g)  
    
    # Handle session setup
    g.session = 0
    
    if 'sessionid' in session and session['sessionid']:
      try:
        g.session = g.db.Load(system_session, session['sessionid'])
      except Exception as e:
        if not g.siteconfig['production']:
          traceback.print_exc()
      
    if not g.session:
      g.session = system_session()
      g.session.New(g)
    
    g.session.CheckExpired(g)
    
    g.session.usercode  = ToShortCode(g.session.userid) if g.session.userid else ''
    
    g.db.userid   = g.session.userid
    g.db.groups   = g.session.get('groups', [])
    g.db.groupids = g.session.get('groupids', [])    
    
    if len(path) == 0:
      return redirect('/index.' + g.session.lang + '.html', 307)
      
    pathargs  = path.split('/')
    
    if len(pathargs) == 1:
      pathargs  = ['system'] + pathargs
      
    pathls  = ["apps"] + pathargs[:-1]
    pathlen = len(pathls)
    
    g.cache   = Cache()
  
    # All session information is passed to the browser via sessionvars.js 
    # so that the base page can be cached
    if path == 'system/sessionvars.js':
      import apps.system
      return apps.system.sessionvars(g)
    else:
      # Reload the session language if the user has chosen a different language
      if g.lang != g.session.lang:
        try:
          LoadTranslation(g, 'system', 'system')
          g.lang  = g.session.lang
        except Exception as e:
          print('Unable to load translation:', e)
      
      # Load paths which do not have strange characters
      if re.match('[\w/]*', path):
        result  = g.cache.Load('page_' + path + '_' + g.session.lang)
        
        if result:
          return result.decode('utf-8')
        else:
          result  = 404
      
      # Try to find a function or page to handle the path request
      # while setting up urlargs
      while pathlen > 0:
        g.pagename  = pathargs[pathlen - 1]
        g.urlargs   = pathargs[pathlen:]        
        
        try:
          mod = importlib.import_module('.'.join(pathls))
              
          if "ROUTES" in dir(mod):
            if g.pagename in mod.ROUTES:
              result = mod.ROUTES[g.pagename](g)
              break
        except ImportError as e:
          pass
        
        pagepath  = '/' + ('/'.join(pathargs[:pathlen]))
        
        if pagepath.endswith('.html'):
          pageparts = pagepath.split('.')
          
          nontranslatedpath = '/'.join(pageparts[:-2]) + '.html'
          
          p = g.db.Find(system_page, 'WHERE path = ?', nontranslatedpath)
          
          if p:
            p = p[0]
            
            pageparts   = pathargs[pathlen - 1].split('.')
            
            g.pagename  = '.'.join(pageparts[:-2]) + '.html'
          else:
            # See if the page is missing a langcode
            nontranslatedpath = '/'.join(pageparts[:-1]) + '.html'
            
            p = g.db.Find(system_page, 'WHERE path = ?', nontranslatedpath)
            
            # Yep; missing a langcode
            if p:
              correctpage = '/'.join(pageparts[:-1]) + '.' + g.session.lang + '.html'
              
              if g.urlargs:
                correctpage += '/' + '/'.join(g.urlargs)
                
              if request.query_string:
                correctpage += '?' + request.query_string.decode('utf-8')
                
              return redirect(correctpage)
          
          if p:
            if p.requiredgroups and 'admins' not in g.session['groups']:
              hasrequiredgroup  = 0
              
              for r in p.requiredgroups:
                if r in g.session['groups']:
                  hasrequiredgroup  = 1
                  break;
              
              if not hasrequiredgroup:
                nextpage  = '/system/login.html?nextpage=/' + path
                
                if request.query_string:
                  nextpage  += '&' + request.query_string.decode('utf-8')
                return redirect(nextpage)
            
            result = p.Render(g)
            
            if not (p.flags & PAGE_DONT_CACHE):
              with open('public/' + pagepath, 'w') as f:
                f.write(result)
              
              # Save the cached HTML file to the cleanup list for autodeletion
              # on cache timeout
              cachefilenames  = []
              savefilenames   = 0
              
              try:
                with open('private/system/cachefiles.txt', 'r+') as f:
                  cachefilenames  = f.read().split('\n')
              except Exception as e:
                if not g.siteconfig['production']:
                  print('Unable to read cachefilenames', e)
                  
              if cachefilenames:
                if path not in cachefilenames:
                  cachefilenames.append(pagepath)
                  savefilenames = 1
                else:
                  pass
              else:
                cachefilenames = [pagepath]
                savefilenames = 1
                
              if savefilenames:
                with open('private/system/cachefiles.txt', 'w') as f:
                  f.write("\n".join(list(set(cachefilenames))))
              
            break
          
        pathls  = pathls[:-1]
        pathlen = len(pathls)
    
    if g.session._changed:
      g.session.Set(
        endtime = time.time(), 
        data    = g.session.data
      )
      g.db.Update(g.session)
      g.db.Commit()
      
    if result == 404:
      result = g.db.Find(system_page, 'WHERE path = ?', '/404.html')[0].Render(g)
      g.db.Close()
      return result, 404
    
    g.db.Close()
    
    session['sessionid']  = g.session.rbid
  except Exception as e:
    traceback.print_exc()
    
    if g.db:
      g.db.Close()
  
  return result

# *********************************************************************
if __name__ == '__main__':
  if 'debug' in sys.argv:
    app.debug = True
    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)
  
  port  = sys.argv[1] if len(sys.argv) > 1 else '8888'
    
  if 'https' in sys.argv:
    print('Running SSL')
    app.run(host = '0.0.0.0', port = port, ssl_context = ('sslcert.pem', 'sslkey.pem'))
  else:
    app.run(host = '0.0.0.0', port = port)
 
